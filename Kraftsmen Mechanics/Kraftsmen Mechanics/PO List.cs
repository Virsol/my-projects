﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class PO_List : Form
    {
        private string supCode,supName,supAddress,supPerson,supContact, createdBy,dateCreated,datePosted,dateUpdated,docNum, status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        
        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string DatePosted
        {
            get { return datePosted; }
            set { datePosted = value; }
        }

        public string DateUpdated
        {
            get { return dateUpdated; }
            set { dateUpdated = value; }
        }
        public string SupPerson
        {
            get { return supPerson; }
            set { supPerson = value; }
        }

        public string SupContact
        {
            get { return supContact; }
            set { supContact = value; }
        }
        public string SupAddress
        {
            get { return supAddress; }
            set { supAddress = value; }
        }
        public string SupCode
        {
            get { return supCode; }
            set { supCode = value; }
        }


        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }

        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }

        public PO_List()
        {
            InitializeComponent();
        }

        Connection poListCon = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            poListCon.OpenConn();
            dgvPOList.DataSource = poListCon.ShowDataDGV("Select * From PO_List_view where ([Document Number]  like '%" + txtSearch.Text + "%' or [Supplier] like '%" + txtSearch.Text + "%') AND [Status] LIKE '" + cboStatus.Text + "'");
            poListCon.CloseConn();
        }

        
        private void cboStatus_TextChanged(object sender, EventArgs e)
        {
            if(cboStatus.Text == "OPEN")
            {
                poListCon.OpenConn();
                dgvPOList.DataSource = poListCon.ShowDataDGV("Select * from dbo.PO_List_view where Status = 'OPEN'");
                poListCon.CloseConn();
            }
            else if(cboStatus.Text == "CLOSED")
            {
                poListCon.OpenConn();
                dgvPOList.DataSource = poListCon.ShowDataDGV("Select * from dbo.PO_List_view where Status = 'CLOSED'");
                poListCon.CloseConn();
            }
        }

        private void PO_List_Load(object sender, EventArgs e)
        {
            poListCon.OpenConn();
            dgvPOList.DataSource = poListCon.ShowDataDGV("Select * from dbo.PO_List_view");
            poListCon.CloseConn();
            //----------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdStatus = new SqlCommand("Select * from dbo.status where status_id = '5' or status_id = '6'", conn);
            SqlDataReader drStatus = cmdStatus.ExecuteReader();
            DataTable dtStatus = new DataTable();
            dtStatus.Load(drStatus);
            cboStatus.DataSource = dtStatus;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStatus.ExecuteNonQuery();

            conn.Close();
            //----------------------------------------------------------------------------//
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgvPOList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string docPO = dgvPOList.CurrentRow.Cells[0].Value.ToString();
             status = dgvPOList.CurrentRow.Cells[7].Value.ToString();

            poListCon.OpenConn();
            SqlDataReader drPO = poListCon.DataReader("Select a.po_document_number 'Document Number', a.supplier_code 'Supplier Code', b.name 'Supplier Name', b.address 'Address', b.contact_number 'Contact Number', c.name 'Contact Person', a.date_created 'Date Created', a.date_posted 'Date Posted', a.date_upload 'Date Updated', d.employee_id 'Created by', e.status_desc 'Status' from po a left join supplier b on a.supplier_code = b.supplier_code left join contact_person c on a.contact_person_id = c.contact_person_id left join employee d on a.employee_id = d.employee_id left join [status] e on a.status_id = e.status_id where po_document_number = (Select RIGHT([Document Number],7) from PO_List_view where [Document Number] = '"+docPO+"')");
            drPO.Read();

            DocNum = drPO["Document Number"].ToString();
            SupCode = drPO["Supplier Code"].ToString();
            SupName = drPO["Supplier Name"].ToString();
            SupPerson = drPO["Contact Person"].ToString();
            SupContact = drPO["Contact Number"].ToString();
            SupAddress = drPO["Address"].ToString();
            
            DateCreated = drPO["Date Created"].ToString();
            DatePosted = drPO["Date Posted"].ToString();
            DateUpdated = drPO["Date Updated"].ToString();

            CreatedBy = drPO["Created By"].ToString();


            poListCon.CloseConn();
            this.Close();
        }
    }
}
