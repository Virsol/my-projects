﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
     public class SqlHelper
    {
        SqlConnection cn;
        public SqlHelper(string constring)
        {
            cn = new SqlConnection(constring);
        }

        public bool IsConnection
        {
            get
            {
                if (cn.State == System.Data.ConnectionState.Closed)
                    cn.Open();
                return true;
            }
        }
    }
}
