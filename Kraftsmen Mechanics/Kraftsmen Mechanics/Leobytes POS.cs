﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Kraftsmen_Mechanics
{
    public partial class Leobytes : Form
    {
        DBConnection conn = new DBConnection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private bool isCollapsed = true;
        private bool isTransactionsCollapsed = true;
        private bool isServicesCollapsed = true;
        private bool isReportsCollapsed = true;
        private bool isCategoriesCollapsed = true;
        public Leobytes()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshServiceNotification.Start();
            datetime.Start();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                btnSortPurchasing.IconChar = FontAwesome.Sharp.IconChar.SortUp;
                pnlPurchasing.Height += 10;
                if (pnlPurchasing.Size == pnlPurchasing.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {
                btnSortPurchasing.IconChar = FontAwesome.Sharp.IconChar.SortDown;
                pnlPurchasing.Height -= 10;
                if (pnlPurchasing.Size == pnlPurchasing.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void btnPurchasingOrder_Click(object sender, EventArgs e)
        {
            Purchasing_Order purchasing_Order = new Purchasing_Order();
            OpenForm(purchasing_Order);
            timer1.Start();
        }

        private void btnGRPO_Click(object sender, EventArgs e)
        {
            Goods_Receipt_Purchase_Order GRPO = new Goods_Receipt_Purchase_Order();
            OpenForm(GRPO);
            timer1.Start();
        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            AP_Invoice API = new AP_Invoice();
            OpenForm(API);
            timer1.Start();
        }

        private void btnJobEstimate_Click(object sender, EventArgs e)
        {
            TransactionTimer.Start();
            OpenForm(new Job_Estimate());
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
        }

        private void btnPurchasingReport_Click(object sender, EventArgs e)
        {
            Purchasing_Order_Report purchasing_Order_Report = new Purchasing_Order_Report();
            OpenForm(purchasing_Order_Report);
            ReportsTimer.Start();
        }

        private void btnSalesReport_Click(object sender, EventArgs e)
        {
            Sales_Report sales_Report = new Sales_Report();
            OpenForm(sales_Report);
            ReportsTimer.Start();
        }

        private void InventoryTimer_Tick(object sender, EventArgs e)
        {
        }

        private void ReportsTimer_Tick(object sender, EventArgs e)
        {
            if (isReportsCollapsed)
            {
                btnSortReports.IconChar = FontAwesome.Sharp.IconChar.SortUp;
                pnlReports.Height += 10;
                if (pnlReports.Size == pnlReports.MaximumSize)
                {
                    ReportsTimer.Stop();
                    isReportsCollapsed = false;
                }
            }
            else
            {
                btnSortReports.IconChar = FontAwesome.Sharp.IconChar.SortDown;
                pnlReports.Height -= 10;
                if (pnlReports.Size == pnlReports.MinimumSize)
                {
                    ReportsTimer.Stop();
                    isReportsCollapsed = true;
                }
            }
        }

        private void btnDropdownInventory_Click(object sender, EventArgs e)
        {
        }

        private void btnDropdownReports_Click(object sender, EventArgs e)
        {
        }

        private void manageInventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Inventory inventory = new Inventory();
            inventory.MdiParent = Kraftsmen_Mechanics.Leobytes.ActiveForm;
            inventory.Show();
        }

        private void btnJobOrder_Click(object sender, EventArgs e)
        {
            Job_Order job_order = new Job_Order();
            OpenForm(job_order);
            TransactionTimer.Start();
        }

        private void supplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Supplier_Master_Data sup_MasData = new Supplier_Master_Data();
            sup_MasData.ShowDialog();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (pnlMenu.Width == 0)
            {
                pnlMenu.Visible = false;
                pnlMenu.Width = 275;
                menuAnimation.ShowSync(pnlMenu);
            }
            else
            {
                pnlMenu.Visible = false;
                pnlMenu.Width = 0;
                menuAnimation.ShowSync(pnlMenu);
            }
        }



        private void pnlMenu_MouseHover(object sender, EventArgs e)
        {
            SlideOpenMenu();
        }


        public void SlideOpenMenu()
        {
            if (pnlMenu.Width == 50)
            {
                pnlMenu.Visible = false;
                pnlMenu.Width = 208;
                menuAnimation.ShowSync(pnlMenu);
            }
            else { }
        }

        private void btnDropdownPurchasing_MouseHover(object sender, EventArgs e)
        {
            SlideOpenMenu();
        }

        private void btnDropdownServices_MouseHover(object sender, EventArgs e)
        {
            SlideOpenMenu();
        }

        private void btnDropdownInventory_MouseHover(object sender, EventArgs e)
        {
            SlideOpenMenu();
        }

        private void btnDropdownReports_MouseHover(object sender, EventArgs e)
        {
            SlideOpenMenu();
        }
        private void OpenForm(Form form)
        {
                form.AutoScroll = true;
                form.TopLevel = false;
                form.WindowState = FormWindowState.Normal;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                /*I assume this code is in your ParentForm and so 'this' points to ParentForm that contains ContainerPanel*/
                this.PanelContainer.Controls.Add(form);
                this.PanelContainer.Dock = DockStyle.Fill;
                form.BringToFront();
                form.Show();
        }

        private void btnWorkingProgress_Click(object sender, EventArgs e)
        {
            Working_Progress WP = new Working_Progress();
            OpenForm(WP);
            TransactionTimer.Start();
        }

        private void btnServiceInvoice_Click(object sender, EventArgs e)
        {
            Service_Invoice SI = new Service_Invoice();
            OpenForm(SI);
            TransactionTimer.Start();
        }

        private void btnIncomingPayment_Click(object sender, EventArgs e)
        {
            Incoming_Payment IP = new Incoming_Payment();
            OpenForm(IP);
            TransactionTimer.Start();
        }

        private void btnCloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panelTitleBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                btnMaximize.IconChar = FontAwesome.Sharp.IconChar.WindowRestore;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                btnMaximize.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize;
            }

        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        #region trash
        private void button6_Click(object sender, EventArgs e)
        {

        }
        #endregion
        private void btnOutgoingPayment_Click(object sender, EventArgs e)
        {
            Outgoing_Payment outgoing_payment = new Outgoing_Payment();
            OpenForm(outgoing_payment);
            timer1.Start();
        }

        private void btnMinimize_MouseHover(object sender, EventArgs e)
        {
            btnMinimize.BackColor = Color.FromArgb(59, 186, 156);
        }

        private void btnMinimize_MouseLeave(object sender, EventArgs e)
        {
            btnMinimize.BackColor = Color.Transparent;
        }

        private void btnMaximize_MouseHover(object sender, EventArgs e)
        {
            btnMaximize.BackColor = Color.FromArgb(59, 186, 156);
        }

        private void btnMaximize_MouseLeave(object sender, EventArgs e)
        {
            btnMaximize.BackColor = Color.Transparent;
        }

        private void btnCloseForm_MouseHover(object sender, EventArgs e)
        {
            btnCloseForm.BackColor = Color.FromArgb(59, 186, 156);
        }

        private void btnCloseForm_MouseLeave(object sender, EventArgs e)
        {
            btnCloseForm.BackColor = Color.Transparent;
        }

        public void LoadServiceNotification()
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("select * from dbo.Work_progress_view where Status = 'ONGOING'", con);
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dtWP = new DataTable();
            da.SelectCommand = cmd;
            da.Fill(dtWP);
            cmd.ExecuteNonQuery();
            con.Close();

            con.Open();
            SqlCommand cmd_SI = new SqlCommand("select * from dbo.Sales_invoice_view where status = 'OPEN'", con);
            SqlDataAdapter da_si = new SqlDataAdapter();
            DataTable dtSI = new DataTable();
            da_si.SelectCommand = cmd_SI;
            da_si.Fill(dtSI);
            cmd_SI.ExecuteNonQuery();
            con.Close();

            int service_notification;

            if (dtWP.Rows.Count != 0)
            {
                lblWorkingProgressNumber.Visible = true;
                lblWorkingProgressNumber.Text = dtWP.Rows.Count.ToString();
            }
            else
            {
                lblWorkingProgressNumber.Visible = false;
            }
            if (dtSI.Rows.Count != 0)
            {
                lblIncomingPaymentNotification.Visible = true;
                lblIncomingPaymentNotification.Text = dtSI.Rows.Count.ToString();
            }
            else
            {
                lblIncomingPaymentNotification.Visible = false;
            }
            if (dtWP.Rows.Count != 0 || dtSI.Rows.Count != 0)
            {
                service_notification = (dtWP.Rows.Count + dtSI.Rows.Count);
                lblServicesNotificationCount.Visible = true;
                lblServicesNotificationCount.Text = service_notification.ToString();
            }
            else
            {
                lblServicesNotificationCount.Visible = false;
            }
        }

        private void PanelContainer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RefreshServiceNotification_Tick(object sender, EventArgs e)
        {
            LoadServiceNotification();
        }

        private void btnDropdownPurchasing_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btnDropdownTransaction_Click(object sender, EventArgs e)
        {
            TransactionTimer.Start();
        }

        private void btnDropdownReports_Click_1(object sender, EventArgs e)
        {
            ReportsTimer.Start();
        }

        private void btnDropdownCategories_Click(object sender, EventArgs e)
        {
            CategoriesDropdown.Start();
        }

        private void CategoriesDropdown_Tick(object sender, EventArgs e)
        {
            if (isCategoriesCollapsed)
            {
                btnSortCategories.IconChar = FontAwesome.Sharp.IconChar.SortUp;
                pnlCategories.Height += 10;
                if (pnlCategories.Size == pnlCategories.MaximumSize)
                {
                    CategoriesDropdown.Stop();
                    isCategoriesCollapsed = false;
                }
            }
            else
            {
                btnSortCategories.IconChar = FontAwesome.Sharp.IconChar.SortDown;
                pnlCategories.Height -= 10;
                if (pnlCategories.Size == pnlCategories.MinimumSize)
                {
                    CategoriesDropdown.Stop();
                    isCategoriesCollapsed = true;
                }
            }
        }

        private void datetime_Tick(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToLongDateString();
            lblTime.Text = DateTime.Now.ToLongTimeString();
        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            Customers customers = new Customers();
            OpenForm(customers);
        }

        private void btnVehicles_Click(object sender, EventArgs e)
        {
            Vehicle vehicle = new Vehicle();
            OpenForm(vehicle);
        }

        private void btnProducts_Click(object sender, EventArgs e)
        {
            Inventory inventory = new Inventory();
            OpenForm(inventory);
        }

        private void btnServices_Click(object sender, EventArgs e)
        {
            ServicesTimer.Start();
        }

        private void TransactionTimer_Tick(object sender, EventArgs e)
        {
            if (isTransactionsCollapsed)
            {

                btnSortTransaction.IconChar = FontAwesome.Sharp.IconChar.SortUp;
                pnlTransaction.Height += 10;
                if (pnlTransaction.Size == pnlTransaction.MaximumSize)
                {
                    TransactionTimer.Stop();
                    isTransactionsCollapsed = false;
                }
            }
            else
            {

                btnSortTransaction.IconChar = FontAwesome.Sharp.IconChar.SortDown;
                pnlTransaction.Height -= 10;
                if (pnlTransaction.Size == pnlTransaction.MinimumSize)
                {
                    TransactionTimer.Stop();
                    isTransactionsCollapsed = true;
                }
            }
        }

        private void ServicesTimer_Tick(object sender, EventArgs e)
        {
            if (isServicesCollapsed)
            {
                btnSortServices.IconChar = FontAwesome.Sharp.IconChar.SortUp;
                pnlServices.Height += 10;
                if (pnlServices.Size == pnlServices.MaximumSize)
                {
                    ServicesTimer.Stop();
                    isServicesCollapsed = false;
                }
            }
            else
            {
                btnSortServices.IconChar = FontAwesome.Sharp.IconChar.SortDown;
                pnlServices.Height -= 10;
                if (pnlServices.Size == pnlServices.MinimumSize)
                {
                    ServicesTimer.Stop();
                    isServicesCollapsed = true;
                }
            }
        }

        private void btnManageServices_Click(object sender, EventArgs e)
        {
            Services services = new Services();
            OpenForm(services);
            ServicesTimer.Start();
        }

        private void btnBillOfMaterials_Click(object sender, EventArgs e)
        {
            Bill_of_Material bill_Of_Material = new Bill_of_Material();
            OpenForm(bill_Of_Material);
            ServicesTimer.Start();
        }

        private void btnInventoryReport_Click(object sender, EventArgs e)
        {
            Inventory_Report inventory_Report = new Inventory_Report();
            OpenForm(inventory_Report);
            ReportsTimer.Start();
        }

        private void btnSortPurchasing_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btnSortTransaction_Click(object sender, EventArgs e)
        {
            TransactionTimer.Start();
        }

        private void btnSortServices_Click(object sender, EventArgs e)
        {
            ServicesTimer.Start();
        }

        private void btnSortReports_Click(object sender, EventArgs e)
        {
            ReportsTimer.Start();
        }

        private void btnSortCategories_Click(object sender, EventArgs e)
        {
            CategoriesDropdown.Start();
        }

        private void btnPurchasingList_Click(object sender, EventArgs e)
        {
            Purchasing_List purchasing_List = new Purchasing_List(this);
            OpenForm(purchasing_List);
            timer1.Start();
        }

        private void btnTransactionList_Click(object sender, EventArgs e)
        {
            Transaction_List transaction_List = new Transaction_List(this);
            OpenForm(transaction_List);
            TransactionTimer.Start();
        }
    }
}
