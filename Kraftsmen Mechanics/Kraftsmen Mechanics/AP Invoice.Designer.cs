﻿
namespace Kraftsmen_Mechanics
{
    partial class AP_Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AP_Invoice));
            this.cboContactPerson = new System.Windows.Forms.ComboBox();
            this.btnViewDocument = new System.Windows.Forms.Button();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpDateUploaded = new System.Windows.Forms.DateTimePicker();
            this.dtpDateCreated = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSupplierAddress = new System.Windows.Forms.TextBox();
            this.txtSupplierContact = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSupplierName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSupplierCode = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblGrpo_Status = new System.Windows.Forms.Label();
            this.lbl_Grpo_Num = new System.Windows.Forms.Label();
            this.dtpDatePosted = new System.Windows.Forms.DateTimePicker();
            this.txtUpdatedBy = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCreatedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chbInstallment = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblTotalPaymentDue = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTaxValue = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblDiscountValue = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTotalBeforeDiscount = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dgvItemList = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // cboContactPerson
            // 
            this.cboContactPerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboContactPerson.Enabled = false;
            this.cboContactPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboContactPerson.FormattingEnabled = true;
            this.cboContactPerson.Location = new System.Drawing.Point(216, 229);
            this.cboContactPerson.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboContactPerson.Name = "cboContactPerson";
            this.cboContactPerson.Size = new System.Drawing.Size(460, 26);
            this.cboContactPerson.TabIndex = 10;
            // 
            // btnViewDocument
            // 
            this.btnViewDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(207)))));
            this.btnViewDocument.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewDocument.FlatAppearance.BorderSize = 0;
            this.btnViewDocument.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnViewDocument.ForeColor = System.Drawing.Color.White;
            this.btnViewDocument.Image = ((System.Drawing.Image)(resources.GetObject("btnViewDocument.Image")));
            this.btnViewDocument.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewDocument.Location = new System.Drawing.Point(491, 0);
            this.btnViewDocument.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnViewDocument.Name = "btnViewDocument";
            this.btnViewDocument.Size = new System.Drawing.Size(165, 37);
            this.btnViewDocument.TabIndex = 27;
            this.btnViewDocument.Text = "View Document";
            this.btnViewDocument.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnViewDocument.UseVisualStyleBackColor = false;
            this.btnViewDocument.Click += new System.EventHandler(this.btnViewDocument_Click);
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.Enabled = false;
            this.txtDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDocumentNumber.Location = new System.Drawing.Point(525, 55);
            this.txtDocumentNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(129, 26);
            this.txtDocumentNumber.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label17.Location = new System.Drawing.Point(436, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 20);
            this.label17.TabIndex = 25;
            this.label17.Text = "Doc No.:";
            // 
            // cboStatus
            // 
            this.cboStatus.Enabled = false;
            this.cboStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Location = new System.Drawing.Point(223, 55);
            this.cboStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(196, 26);
            this.cboStatus.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label16.Location = new System.Drawing.Point(49, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 18);
            this.label16.TabIndex = 23;
            this.label16.Text = "Document Status:";
            // 
            // dtpDateUploaded
            // 
            this.dtpDateUploaded.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateUploaded.Location = new System.Drawing.Point(223, 185);
            this.dtpDateUploaded.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpDateUploaded.Name = "dtpDateUploaded";
            this.dtpDateUploaded.Size = new System.Drawing.Size(432, 24);
            this.dtpDateUploaded.TabIndex = 22;
            // 
            // dtpDateCreated
            // 
            this.dtpDateCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateCreated.Location = new System.Drawing.Point(223, 101);
            this.dtpDateCreated.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpDateCreated.Name = "dtpDateCreated";
            this.dtpDateCreated.Size = new System.Drawing.Size(432, 24);
            this.dtpDateCreated.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(87, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "Date Posted:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(55, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Contact Person:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(41, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Supplier Address:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(95, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 18);
            this.label6.TabIndex = 19;
            this.label6.Text = "Updated By:";
            // 
            // txtSupplierAddress
            // 
            this.txtSupplierAddress.Enabled = false;
            this.txtSupplierAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSupplierAddress.Location = new System.Drawing.Point(216, 183);
            this.txtSupplierAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSupplierAddress.Name = "txtSupplierAddress";
            this.txtSupplierAddress.Size = new System.Drawing.Size(456, 24);
            this.txtSupplierAddress.TabIndex = 6;
            // 
            // txtSupplierContact
            // 
            this.txtSupplierContact.Enabled = false;
            this.txtSupplierContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSupplierContact.Location = new System.Drawing.Point(216, 139);
            this.txtSupplierContact.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSupplierContact.Name = "txtSupplierContact";
            this.txtSupplierContact.Size = new System.Drawing.Size(456, 24);
            this.txtSupplierContact.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(60, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Supplier Name:";
            // 
            // txtSupplierName
            // 
            this.txtSupplierName.Enabled = false;
            this.txtSupplierName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSupplierName.Location = new System.Drawing.Point(216, 96);
            this.txtSupplierName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSupplierName.Name = "txtSupplierName";
            this.txtSupplierName.Size = new System.Drawing.Size(456, 24);
            this.txtSupplierName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(65, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Supplier Code:";
            // 
            // txtSupplierCode
            // 
            this.txtSupplierCode.Enabled = false;
            this.txtSupplierCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSupplierCode.Location = new System.Drawing.Point(216, 54);
            this.txtSupplierCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSupplierCode.Name = "txtSupplierCode";
            this.txtSupplierCode.Size = new System.Drawing.Size(456, 24);
            this.txtSupplierCode.TabIndex = 0;
            this.txtSupplierCode.TextChanged += new System.EventHandler(this.txtSupplierCode_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.lblGrpo_Status);
            this.groupBox2.Controls.Add(this.lbl_Grpo_Num);
            this.groupBox2.Controls.Add(this.btnViewDocument);
            this.groupBox2.Controls.Add(this.txtDocumentNumber);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.cboStatus);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.dtpDateUploaded);
            this.groupBox2.Controls.Add(this.dtpDatePosted);
            this.groupBox2.Controls.Add(this.dtpDateCreated);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtUpdatedBy);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtCreatedBy);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(925, 65);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(893, 319);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Document Details";
            // 
            // lblGrpo_Status
            // 
            this.lblGrpo_Status.AutoSize = true;
            this.lblGrpo_Status.Location = new System.Drawing.Point(351, 27);
            this.lblGrpo_Status.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGrpo_Status.Name = "lblGrpo_Status";
            this.lblGrpo_Status.Size = new System.Drawing.Size(0, 18);
            this.lblGrpo_Status.TabIndex = 29;
            this.lblGrpo_Status.Visible = false;
            // 
            // lbl_Grpo_Num
            // 
            this.lbl_Grpo_Num.AutoSize = true;
            this.lbl_Grpo_Num.Location = new System.Drawing.Point(289, 26);
            this.lbl_Grpo_Num.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Grpo_Num.Name = "lbl_Grpo_Num";
            this.lbl_Grpo_Num.Size = new System.Drawing.Size(12, 18);
            this.lbl_Grpo_Num.TabIndex = 28;
            this.lbl_Grpo_Num.Text = " ";
            // 
            // dtpDatePosted
            // 
            this.dtpDatePosted.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDatePosted.Location = new System.Drawing.Point(223, 142);
            this.dtpDatePosted.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpDatePosted.Name = "dtpDatePosted";
            this.dtpDatePosted.Size = new System.Drawing.Size(432, 24);
            this.dtpDatePosted.TabIndex = 21;
            // 
            // txtUpdatedBy
            // 
            this.txtUpdatedBy.Enabled = false;
            this.txtUpdatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUpdatedBy.Location = new System.Drawing.Point(223, 268);
            this.txtUpdatedBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUpdatedBy.Name = "txtUpdatedBy";
            this.txtUpdatedBy.Size = new System.Drawing.Size(432, 24);
            this.txtUpdatedBy.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(79, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 18);
            this.label10.TabIndex = 11;
            this.label10.Text = "Date Created:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(97, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "Created By:";
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Enabled = false;
            this.txtCreatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreatedBy.Location = new System.Drawing.Point(223, 228);
            this.txtCreatedBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Size = new System.Drawing.Size(432, 24);
            this.txtCreatedBy.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(76, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 18);
            this.label8.TabIndex = 15;
            this.label8.Text = "Date Updated:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(49, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Supplier Contact:";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Controls.Add(this.txtSearch);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.dgvItemList);
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox3.Location = new System.Drawing.Point(15, 388);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(1821, 569);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Details";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.chbInstallment);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.lblTotalPaymentDue);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.lblTaxValue);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.lblDiscountValue);
            this.groupBox4.Controls.Add(this.btnSave);
            this.groupBox4.Controls.Add(this.lblTotalBeforeDiscount);
            this.groupBox4.Controls.Add(this.btnCancel);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Location = new System.Drawing.Point(1104, 84);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(699, 454);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Payment Details";
            // 
            // chbInstallment
            // 
            this.chbInstallment.AutoSize = true;
            this.chbInstallment.Location = new System.Drawing.Point(413, 49);
            this.chbInstallment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbInstallment.Name = "chbInstallment";
            this.chbInstallment.Size = new System.Drawing.Size(100, 22);
            this.chbInstallment.TabIndex = 40;
            this.chbInstallment.Text = "Installment";
            this.chbInstallment.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label11.Location = new System.Drawing.Point(139, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Tax Value:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.Location = new System.Drawing.Point(139, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(178, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Total Before Discount:";
            // 
            // lblTotalPaymentDue
            // 
            this.lblTotalPaymentDue.AutoSize = true;
            this.lblTotalPaymentDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblTotalPaymentDue.Location = new System.Drawing.Point(479, 199);
            this.lblTotalPaymentDue.Name = "lblTotalPaymentDue";
            this.lblTotalPaymentDue.Size = new System.Drawing.Size(58, 29);
            this.lblTotalPaymentDue.TabIndex = 22;
            this.lblTotalPaymentDue.Text = "0.00";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.Location = new System.Drawing.Point(139, 129);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Discount Value:";
            // 
            // lblTaxValue
            // 
            this.lblTaxValue.AutoSize = true;
            this.lblTaxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblTaxValue.Location = new System.Drawing.Point(480, 162);
            this.lblTaxValue.Name = "lblTaxValue";
            this.lblTaxValue.Size = new System.Drawing.Size(40, 20);
            this.lblTaxValue.TabIndex = 21;
            this.lblTaxValue.Text = "0.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label15.Location = new System.Drawing.Point(137, 199);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(223, 29);
            this.label15.TabIndex = 16;
            this.label15.Text = "Total Payment Due:";
            // 
            // lblDiscountValue
            // 
            this.lblDiscountValue.AutoSize = true;
            this.lblDiscountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblDiscountValue.Location = new System.Drawing.Point(480, 129);
            this.lblDiscountValue.Name = "lblDiscountValue";
            this.lblDiscountValue.Size = new System.Drawing.Size(40, 20);
            this.lblDiscountValue.TabIndex = 20;
            this.lblDiscountValue.Text = "0.00";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(21, 270);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(316, 39);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblTotalBeforeDiscount
            // 
            this.lblTotalBeforeDiscount.AutoSize = true;
            this.lblTotalBeforeDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblTotalBeforeDiscount.Location = new System.Drawing.Point(480, 97);
            this.lblTotalBeforeDiscount.Name = "lblTotalBeforeDiscount";
            this.lblTotalBeforeDiscount.Size = new System.Drawing.Size(40, 20);
            this.lblTotalBeforeDiscount.TabIndex = 19;
            this.lblTotalBeforeDiscount.Text = "0.00";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(66)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(355, 270);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(315, 39);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(167)))), ((int)(((byte)(43)))));
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrint.ForeColor = System.Drawing.Color.White;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.Location = new System.Drawing.Point(896, 30);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(187, 37);
            this.btnPrint.TabIndex = 39;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSearch.Location = new System.Drawing.Point(99, 36);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(545, 26);
            this.txtSearch.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label13.Location = new System.Drawing.Point(17, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Search:";
            // 
            // dgvItemList
            // 
            this.dgvItemList.AllowUserToAddRows = false;
            this.dgvItemList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvItemList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvItemList.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemList.Location = new System.Drawing.Point(19, 84);
            this.dgvItemList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvItemList.MultiSelect = false;
            this.dgvItemList.Name = "dgvItemList";
            this.dgvItemList.ReadOnly = true;
            this.dgvItemList.RowHeadersWidth = 51;
            this.dgvItemList.RowTemplate.Height = 24;
            this.dgvItemList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItemList.Size = new System.Drawing.Size(1066, 454);
            this.dgvItemList.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.cboContactPerson);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtSupplierAddress);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSupplierContact);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtSupplierName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSupplierCode);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(12, 64);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(908, 319);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Company Details";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1848, 52);
            this.panel1.TabIndex = 58;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(15, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 25);
            this.label18.TabIndex = 24;
            this.label18.Text = "AP Invoice";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(1800, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 31);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // AP_Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1848, 970);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AP_Invoice";
            this.Text = "AP Invoice";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AP_Invoice_FormClosing);
            this.Load += new System.EventHandler(this.AP_Invoice_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox cboContactPerson;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtSupplierAddress;
        public System.Windows.Forms.TextBox txtSupplierContact;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtSupplierName;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtSupplierCode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_Grpo_Num;
        private System.Windows.Forms.Label lblGrpo_Status;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private Bunifu.Framework.UI.BunifuImageButton btnExit;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.TextBox txtDocumentNumber;
        public System.Windows.Forms.ComboBox cboStatus;
        public System.Windows.Forms.DateTimePicker dtpDateUploaded;
        public System.Windows.Forms.DateTimePicker dtpDateCreated;
        public System.Windows.Forms.DateTimePicker dtpDatePosted;
        public System.Windows.Forms.TextBox txtUpdatedBy;
        public System.Windows.Forms.TextBox txtCreatedBy;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.CheckBox chbInstallment;
        public System.Windows.Forms.Label lblTotalPaymentDue;
        public System.Windows.Forms.Label lblTaxValue;
        public System.Windows.Forms.Label lblDiscountValue;
        public System.Windows.Forms.Label lblTotalBeforeDiscount;
        public System.Windows.Forms.TextBox txtSearch;
        public System.Windows.Forms.DataGridView dgvItemList;
        public System.Windows.Forms.Button btnViewDocument;
    }
}