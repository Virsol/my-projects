﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace Kraftsmen_Mechanics
{
    public partial class Add_Bill_of_Materials : Form
    {
        public static string BCode;

        private string bCode;

        public string BomCode
        {
            get { return bCode; }
            set { bCode = value; }
        }
        public Add_Bill_of_Materials()
        {
            InitializeComponent();
        }

        Connection abom = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private void btnCancel_Click(object sender, EventArgs e)
        {
            abom.OpenConn();
            abom.ExecuteQuery("truncate table bom_draft");
            abom.CloseConn();
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            BCode = txtBOMCode.Text;

            if (txtBOMCode.Text == "" || txtBOMCode.Text == null || txtServiceCode.Text == "" || txtServiceCode.Text == null)
            {
                MessageBox.Show("Please fill up BOM Code / Supplier Code first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Form newFormDialog = new Form();
                try
                {
                    using (Items_List items_List = new Items_List())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        items_List.Owner = newFormDialog;

                        Add_BOM_Item addBomItm = new Add_BOM_Item();

                        //addBomItm.lblhidden.Text = bCode;

                        if (items_List.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();

                            showItem();

                            string xCode = txtServiceCode.Text;
                            //--------------------------------------------------------------------//
                            abom.OpenConn();
                            SqlDataReader drService = abom.DataReader("select format([item_price],'#,#.00') as 'Total Price' from inventory where inventory_code = '"+xCode+"'");
                            drService.Read();
                            double serPrice = double.Parse(drService["Total Price"].ToString());
                            abom.CloseConn();
                            //--------------------------------------------------------------------//
                            abom.OpenConn();
                            SqlDataReader dr = abom.DataReader("select format(sum([total_tax]), '#,#.00') as 'Tax', format(sum([total_price]), '#,#.00') as'Price'  from bom_draft");
                            dr.Read();
                            //double xTax = double.Parse(dr["Tax"].ToString());
                            double xPrice = double.Parse(dr["Price"].ToString());
                            abom.CloseConn();

                            double TotalPrice = serPrice + xPrice;

                            //txtTotalTax.Text = xTax.ToString();
                            txtTotalPrice.Text = TotalPrice.ToString();

                        }
                    }
                }
                catch (Exception)

                {

                }
                finally
                {
                    newFormDialog.Dispose();
                }
            }
        }

        private void btnServicesList_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Services_List services_List = new Services_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    services_List.Owner = newFormDialog;

                    if (services_List.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();

                        txtServiceCode.Text = services_List.SerCode;
                        txtServiceName.Text = services_List.SerName;
                        txtServiceDesc.Text = services_List.SerDesc;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void Add_Bill_of_Materials_Load(object sender, EventArgs e)
        {
           txtTotalPrice.ReadOnly = false;
            if(txtTotalPrice.Text == "" || txtTotalPrice.Text == null)
            {
                txtTotalPrice.Text = "0";
            }

            txtTotalTax.Visible = false;
            label5.Visible = false;
            abom.OpenConn();
            SqlDataReader drDocNo = abom.DataReader("select max(bom_code) as Bom_Doc from bom");
            drDocNo.Read();
            if(drDocNo["Bom_Doc"].ToString() != "" /*|| drDocNo["Bom_Doc"].ToString() != null*/)
            {
                string doc_Num = drDocNo["Bom_Doc"].ToString();
                int xdoc_Num = Int32.Parse(doc_Num) + 1;

                string Bom_docNo = xdoc_Num.ToString().PadLeft(7, '0');
                txtBOMCode.Text = Bom_docNo;
            }
            else
            {
                string docNum = "0000001";
                string bomDoc = docNum;
                int xdocNum = Int32.Parse(bomDoc);
                txtBOMCode.Text = bomDoc;
            }
            abom.CloseConn();
            showItem();
        }

        public void showItem()
        {
            abom.OpenConn();
            dgvBOMlist.DataSource = abom.ShowDataDGV("select * from bom_child");
            abom.CloseConn();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtServiceCode.Text == "" || txtServiceCode.Text == null)
            {
                MessageBox.Show("Invalid Service code.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string BOMCode = txtBOMCode.Text;
                string serCode = txtServiceCode.Text;
                string serName = txtServiceName.Text;
                string serDesc = txtServiceDesc.Text;
                //double serTax = double.Parse(txtTotalTax.Text.ToString());
                double serPrice = double.Parse(txtTotalPrice.Text.ToString());


                abom.OpenConn();
                abom.ExecuteQuery("insert into bom(bom_code, inventory_code, item_quantity, total_price, total_tax, [P/C], status)" +
                    "values('" + BOMCode + "', '" + serCode + "', '" + 1 + "', '" + serPrice + "', '" + 0 + "', '" + 1 + "', '" + 1 + "')");
                abom.CloseConn();

                abom.OpenConn();
                abom.ExecuteQuery("insert into bom(bom_code, inventory_code, item_quantity, total_price, total_tax, [P/C], status)" +
                    "select bom_code, inventory_code, item_quantity, '" + 0 + "', '" + 0 + "', [P/C], status from bom_draft");
                abom.CloseConn();

                abom.OpenConn();
                abom.ExecuteQuery("truncate table bom_draft");
                abom.CloseConn();

                MessageBox.Show("BOM has successfuly saved.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
        }
    }
}
