﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class AP_Invoice : Form
    {

        public string docNum,apDocNum, dateCreated, datePosted, dateUpdated;
        public int xdocNum;

        private string grpo_doc_num;
        private int contactPerson;
        private double totalAmount;

        public AP_Invoice()
        {
            InitializeComponent();
        }

        Connection conApInv = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void AP_Invoice_Load(object sender, EventArgs e)
        {
            btnPrint.Visible = false;

            conn.Open();
            SqlCommand cmdStat = new SqlCommand("Select * from dbo.status where status_id between 5 and 6", conn);
            SqlDataReader drStat = cmdStat.ExecuteReader();
            DataTable dtStat = new DataTable();
            dtStat.Load(drStat);
            cboStatus.DataSource = dtStat;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStat.ExecuteNonQuery();
            conn.Close();

            dateCreated = dtpDateCreated.Value.ToString("yyyy-MM-dd");
            datePosted = dtpDatePosted.Value.ToString("yyyy-MM-dd");
            dateUpdated = dtpDateUploaded.Value.ToString("yyyy-MM-dd");

            conApInv.OpenConn();
            SqlDataReader drHeader = conApInv.DataReader("select MAX(ap_inv_doc_num) as apInv_docNum from ap_inv_body");
            drHeader.Read();
            if (drHeader["apInv_docNum"].ToString() != "")
            {
                docNum = drHeader["apInv_docNum"].ToString();
                xdocNum = Int32.Parse(docNum) + 1;

                apDocNum = xdocNum.ToString().PadLeft(7, '0');
                txtDocumentNumber.Text = apDocNum;
            }
            else
            {
                docNum = "0000001";
                apDocNum = docNum;
                xdocNum = Int32.Parse(docNum);
                txtDocumentNumber.Text = apDocNum;
            }

            conApInv.CloseConn();
        }

        private void txtSupplierCode_TextChanged(object sender, EventArgs e)
        {
            conn.Open();
            try
            {
                string supcode = txtSupplierCode.Text;

                SqlCommand cmdCode = new SqlCommand("select * from dbo.contact_person where supplier_code = '" + supcode + "'", conn);
                SqlDataReader drCode = cmdCode.ExecuteReader();
                DataTable dtCode = new DataTable();
                dtCode.Load(drCode);
                cboContactPerson.DataSource = dtCode;
                cboContactPerson.DisplayMember = "name";
                cboContactPerson.ValueMember = "contact_person_id";
                cmdCode.ExecuteNonQuery();

            }
            catch (Exception)
            {

            }
            conn.Close();
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AP_Invoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null)
            {

            }
            else
            {
                DialogResult msg = MessageBox.Show("Closing this window will lose all your unsave data.\n Are you sure you want to close this window?", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (msg == DialogResult.OK)
                {
                    conApInv.OpenConn();
                    conApInv.ExecuteQuery("truncate table dbo.draft_ap_inv");
                    conApInv.CloseConn();
                }
                else if (msg == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewDocument_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (GRPO_List grpoList = new GRPO_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    grpoList.Owner = newFormDialog;

                    if (grpoList.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();

                        lbl_Grpo_Num.Text = grpoList.DocNum;
                        lbl_Grpo_Num.Visible = false;
                        lblGrpo_Status.Text = GRPO_List.xstatus;

                        conApInv.OpenConn();
                        conApInv.ExecuteQuery("truncate table draft_ap_inv");
                        conApInv.ExecuteQuery("insert into draft_ap_inv (ap_inv_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, discount, total_price)" +
                            "(select '" + txtDocumentNumber.Text + "', inventory_code, quantity, uom_id, price, tax_id, tax_price, discount, total_price from grpo_body where grpo_doc_num = '" + lbl_Grpo_Num.Text + "')");
                        conApInv.CloseConn();

                        txtSupplierCode.Text = grpoList.SupCode;
                        txtSupplierName.Text = grpoList.SupName;
                        cboContactPerson.Text = grpoList.SupPerson;
                        txtSupplierContact.Text = grpoList.SupContact;
                        txtSupplierAddress.Text = grpoList.SupAddress;
                        dtpDateCreated.Text = grpoList.DateCreated;
                        dtpDatePosted.Text = grpoList.DatePosted;
                        dtpDateUploaded.Text = grpoList.DateUpdated;

                        //cboStatus.Text = GRPO_List.xstatus;

                        txtCreatedBy.Text = grpoList.CreatedBy;

                        conApInv.OpenConn();
                        dgvItemList.DataSource = conApInv.ShowDataDGV("select * from draft_ap_inv where '" + grpoList.DocNum + "' = '" + lbl_Grpo_Num.Text + "'");
                        conApInv.CloseConn();

                        groupBox3.Enabled = true;
                        txtSearch.Enabled = true;
                        dgvItemList.ReadOnly = true;

                        if(lblGrpo_Status.Text == "CLOSED")
                        {
                            dtpDateCreated.Enabled = false;
                            dtpDatePosted.Enabled = false;
                            dtpDateUploaded.Enabled = false;
                            dgvItemList.ReadOnly = true;
                            txtSearch.Enabled = false;
                            btnSave.Enabled = false;
                        }

                    }
                    showTotal();
                    //cboStatus.Text = GRPO_List.xstatus;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            contactPerson = Int32.Parse(cboContactPerson.SelectedValue.ToString());
            totalAmount = double.Parse(lblTotalPaymentDue.Text);
            grpo_doc_num = lbl_Grpo_Num.Text;
            int inst;
            if(chbInstallment.Checked)
            {
                 inst = 1;
            }
            else
            {
                 inst = 0;
            }

            conApInv.OpenConn();
            SqlDataReader drDraft = conApInv.DataReader("select * from draft_ap_inv");

            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null || drDraft.Read())
            {

                conApInv.OpenConn();
                conApInv.ExecuteQuery("insert into ap_invoice (ap_invoice_doc_num, grpo_docnum, supplier_code, contact_person_id, date_posted, date_created, date_upload,employee_id,status_id, total_price, remaining_balance,installment) " +
                    "VALUES('" + txtDocumentNumber.Text + "', '" + grpo_doc_num + "', '" + txtSupplierCode.Text + "', '" + contactPerson + "', '" + datePosted + "','" + dateCreated + "','" + dateUpdated + "','" + 0 + "','" + 5 + "', '" + totalAmount + "', '"+totalAmount+"','"+ inst+ "')");
                conApInv.CloseConn();

                conApInv.OpenConn();
                conApInv.ExecuteQuery("insert into ap_inv_body (ap_inv_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price)" +
                    "select ap_inv_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price from draft_ap_inv");
                conApInv.CloseConn();

                MessageBox.Show("A/P Invoice Saved!", "Save Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information);

                conApInv.OpenConn();
                conApInv.ExecuteQuery("Update grpo set status_id = '" + 6 + "' where grpo_doc_num = '" + grpo_doc_num + "'");
                conApInv.CloseConn();

                conApInv.OpenConn();
                conApInv.ExecuteQuery("truncate table draft_ap_inv");
                conApInv.CloseConn();

                txtSupplierCode.Text = "";
                this.Close();
            }
            else
            {
                MessageBox.Show("Please fill up all the fields above.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            conApInv.CloseConn();

        }

        private void showTotal()
        {
            //-----------------------------------------//
            conApInv.OpenConn();

            SqlDataReader drTotal = conApInv.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from draft_ap_inv where ap_inv_doc_num = '" + txtDocumentNumber.Text + "'");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            lblTotalBeforeDiscount.Text = total_price;
            lblTaxValue.Text = total_tax;
            lblTotalPaymentDue.Text = total_amount;
            conApInv.CloseConn();
            //--------------------------------------------------------------------//
        }
    }
}
