﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public partial class Add_New_Customers : Form
    {
        DBConnection conn = new DBConnection();
        private string cusName;
        private string cusNumber;
        private string cusAddress;
        private string cusCity;
        private string cusPostalCode;
        public Add_New_Customers()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cusName = txtCustomerName.Text;
            cusNumber = txtContactNumber.Text;
            cusAddress = txtAddress.Text;
            cusCity = txtCity.Text;
            cusPostalCode = txtPostalCode.Text;
            if (cusName == null || cusNumber == null || cusAddress == null || cusCity == null || cusPostalCode == "")
            {
                MessageBox.Show("Please provide all the information","Information",MessageBoxButtons.OK ,MessageBoxIcon.Information);
            }
            else
            {
                conn.OpenConnection();
                conn.ExecuteQueries("Insert into customer (name, address, number, city, postal_code, status)" +
                    "values" +
                    "('" + cusName + "'" +
                    ",'" + cusAddress + "'" +
                    ",'" + cusNumber + "'" +
                    ",'" + cusCity + "'" +
                    ",'" + cusPostalCode + "'" +
                    ",'" + 1 + "')");
                conn.CloseConnection();
                MessageBox.Show("Customer Added");
                clear();
            }

        }
        private void clear()
        {
            txtCustomerName.Clear();
            txtContactNumber.Clear();
            txtCity.Clear();
            txtAddress.Clear();
            txtPostalCode.Clear();
        }
        private void Add_New_Customers_Load(object sender, EventArgs e)
        {

        }
        
    }
}
