﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Vehicle_List : Form
    {
        DBConnection conn = new DBConnection();
        private string search;
        private string vName, vColor, vPlateNumber;
        private int vMake, vID, vSize, vCat;
        public Vehicle_List()
        {
            InitializeComponent();
        }
        public string vehicleName
        {
            get { return vName; }
            set { vName = value; }
        }
        public string vehicleColor
        {
            get { return vColor; }
            set { vColor = value; }
        }
        public string vehiclePlateNumber
        {
            get { return vPlateNumber; }
            set { vPlateNumber = value; }
        }
        public int vehicleID
        {
            get { return vID; }
            set { vID = value; }
        }
        public int vehicleMake
        {
            get { return vMake; }
            set { vMake = value; }
        }
        public int vehicleSize
        {
            get { return vSize; }
            set { vSize = value; }
        }
        public int vehicleCategory
        {
            get { return vCat; }
            set { vCat = value; }
        }

        private void Vehicle_List_Load_1(object sender, EventArgs e)
        {
            refreshDgvVehicleList();
        }

        private void dgvVehicleList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

            vID = Int32.Parse(dgvVehicleList.CurrentRow.Cells[0].Value.ToString());
            conn.OpenConnection();
            SqlDataReader dr = conn.DataReader("select * from vehicle where vehicle_id = '" + vID + "'");
            if (dr.Read())
            {
                vSize = Int32.Parse(dr["size"].ToString());
                vMake = Int32.Parse(dr["auto_make_id"].ToString());
                vName = dr["model"].ToString();
                vCat = Int32.Parse(dr["vehicle_cat_id"].ToString());
            }
            else
            {
                MessageBox.Show("walang laman");
            }
            conn.CloseConnection();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvVehicleList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            vID = Int32.Parse(dgvVehicleList.CurrentRow.Cells[0].Value.ToString());
            conn.OpenConnection();
            SqlDataReader dr = conn.DataReader("select * from vehicle where vehicle_id = '" + vID + "'");
            if(dr.Read())
            {
                vSize = Int32.Parse(dr["size"].ToString());
                vMake = Int32.Parse(dr["auto_make_id"].ToString());
                vName = dr["model"].ToString();
                vCat = Int32.Parse(dr["vehicle_cat_id"].ToString());
            }
            else
            {
                MessageBox.Show("walang laman");
            }
            conn.CloseConnection();
            this.Close();
        }

        private void Vehicle_List_Load(object sender, EventArgs e)
        {
            refreshDgvVehicleList();
        }
        private void refreshDgvVehicleList()
        {

            conn.OpenConnection();
            dgvVehicleList.DataSource = conn.ShowDataInGridView("select * from list_of_vehicle where [Auto Make] like '%"+search+"%' or [Model] like '%"+search+"%'");
            conn.CloseConnection();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            search = txtSearch.Text;
            refreshDgvVehicleList();
        }
    }
}
