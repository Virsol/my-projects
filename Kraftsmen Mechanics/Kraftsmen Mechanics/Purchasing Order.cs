﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Purchasing_Order : Form
    {
        public static string uom = "";

        public string supCode, supName, supAdd, supCon;  
        
        public string DateCreated, DatePosted, DateUpdated,CreatedBy,UpdatedBy,po_docNo,doc_Num;
        public int docStatus,xdoc_Num;

        private string code, created, updated;
        private int status, doc_num, contact_person;
        private double total_amount;

        private string doc_No;

        public string document_No
        {
            get { return doc_No; }
            set { doc_No = value; }
        }
        
        
        public Purchasing_Order()
        {
            InitializeComponent();
        }

        Connection pocon = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewDocument_Click(object sender, EventArgs e)
        {
            pocon.OpenConn();
            pocon.ExecuteQuery("truncate table draft_po");
            pocon.CloseConn();
            Form newFormDialog = new Form();
            try
            {
                using (PO_List poList = new PO_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Normal;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    poList.Owner = newFormDialog;

                    if (poList.ShowDialog() != DialogResult.OK)
                    {
                        //Purchasing_Order po = new Purchasing_Order();
                        //po.ShowDialog();
                        newFormDialog.Dispose();

                        txtDocumentNumber.Text = poList.DocNum;
                        txtSupplierCode.Text = poList.SupCode;
                        txtSupplierName.Text = poList.SupName;
                        cboContactPerson.Text = poList.SupPerson;
                        txtSupplierContact.Text = poList.SupContact;
                        txtSupplierAddress.Text = poList.SupAddress;

                        dtpDateCreated.Text = poList.DateCreated;
                        dtpDatePosted.Text = poList.DatePosted;
                        dtpDateUploaded.Text = poList.DateUpdated;

                        cboStatus.Text = poList.Status;

                        txtCreatedBy.Text = poList.CreatedBy;

                        pocon.OpenConn();
                        pocon.ExecuteQuery("insert into draft_po " +
                            "select po_document_number, inventory_code, quantity, uom_id, price, tax_id, tax_price, discount, total_price from po_body where po_document_number = '" + txtDocumentNumber.Text + "'");
                        pocon.CloseConn();

                        pocon.OpenConn();
                        dgvOrderList.DataSource = pocon.ShowDataDGV("select [ID], [Item Code], [Item Name], [Item Description], [quantity], [Price], [UOM], [Tax], [Tax Rate], [Vat], [Discount], [Total Price] from draft_PO_view where [PO Document Number] = '" + txtDocumentNumber.Text + "'");
                        pocon.CloseConn();

                        //-----------------------------------------//
                        pocon.OpenConn();

                        SqlDataReader drTotal = pocon.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from po_body where po_document_number = '" + txtDocumentNumber.Text + "'");
                        drTotal.Read();

                        string total_price = drTotal["Total_Price"].ToString();
                        string total_tax = drTotal["Total_Tax"].ToString();
                        string total_amount = drTotal["Total_Amount"].ToString();
                        //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

                        lblTotalBeforeDiscount.Text = total_price;
                        lblTaxValue.Text = total_tax;
                        lblTotalPaymentDue.Text = total_amount;
                        pocon.CloseConn();

                        //--------------------------------------------------------------------//
                        groupBox1.Enabled = false;
                        groupBox3.Enabled = true;

                        cboStatus.Enabled = false;
                        txtDocumentNumber.Enabled = false;
                        dtpDateCreated.Enabled = false;
                        dtpDatePosted.Enabled = false;
                        dtpDateUploaded.Enabled = false;

                        btnSave.Enabled = false;
                        txtSearch.Enabled = false;
                        dgvItemList.Enabled = false;
                    }
                    groupBox1.Enabled = true;

                    txtSupplierCode.Enabled = false;
                    txtSupplierName.Enabled = false;
                    txtSupplierAddress.Enabled = false;
                    cboContactPerson.Enabled = false;
                    txtSupplierContact.Enabled = false;
                    cboContactPerson.Enabled = true;
                    docnumLoad();
                    cboStatus.Text = "OPEN";
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
            
        }

        private void Purchasing_Order_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null)
            {

            }
            else
            {
                DialogResult msg = MessageBox.Show("Closing this window will lose all your unsave data.\n Are you sure you want to close this window?", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (msg == DialogResult.OK)
                {
                    pocon.OpenConn();
                    pocon.ExecuteQuery("truncate table dbo.draft_po");
                    pocon.CloseConn();
                }
                else if (msg == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }
        public void showTotal()
        {
            pocon.OpenConn();

            SqlDataReader drTotal = pocon.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from draft_PO");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            lblTotalBeforeDiscount.Text = total_price;
            lblTaxValue.Text = total_tax;
            lblTotalPaymentDue.Text = total_amount;
            pocon.CloseConn();
        }
         public void showDraft()
        {
            pocon.OpenConn();
            dgvOrderList.DataSource = pocon.ShowDataDGV("select * from draft_PO_view");
            pocon.CloseConn();
        }

        private void dgvOrderList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string itm_ID = this.dgvOrderList.CurrentRow.Cells[1].Value.ToString();
            string itm_name = this.dgvOrderList.CurrentRow.Cells[2].Value.ToString();
            if (dgvOrderList.Columns[e.ColumnIndex].Index == 0)
            {
                if (MessageBox.Show("Do you want to void " + itm_name + "?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pocon.OpenConn();
                    pocon.ExecuteQuery("delete from draft_po where po_body_id = '" + itm_ID + "'");
                    pocon.CloseConn();
                    showTotal();
                    showDraft();
                }
            }
        }

        private void dgvItemList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        public void clear()
        {
            txtSupplierCode.Text = "";
            txtSupplierName.Text = "";
            txtSupplierAddress.Text = "";
            txtSupplierContact.Text = "";
            cboContactPerson.Text = "";
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void docnumLoad()
        {
            pocon.OpenConn();
            SqlDataReader drHead = pocon.DataReader("select MAX (po_document_number) as po_docnum from po_body");
            drHead.Read();
            if (drHead["po_docnum"].ToString() != "")
            {
                doc_Num = drHead["po_docnum"].ToString();
                xdoc_Num = Int32.Parse(doc_Num) + 1;

                po_docNo = xdoc_Num.ToString().PadLeft(7, '0');
                txtDocumentNumber.Text = po_docNo;
            }
            else
            {
                doc_Num = "0000001";
                po_docNo = doc_Num;
                xdoc_Num = Int32.Parse(doc_Num);
                txtDocumentNumber.Text = po_docNo;

                pocon.CloseConn();
            }
        }

        private void Purchasing_Order_Load(object sender, EventArgs e)
        {

            conn.Open();
            SqlCommand cmdStat = new SqlCommand("Select * from dbo.status where status_id between 5 AND 6" , conn);
            SqlDataReader drStat = cmdStat.ExecuteReader();
            DataTable dtStat = new DataTable();
            dtStat.Load(drStat);
            cboStatus.DataSource = dtStat;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStat.ExecuteNonQuery();
            conn.Close();

            pocon.OpenConn();
            dgvOrderList.DataSource = pocon.ShowDataDGV("select * from draft_po_view");
            pocon.CloseConn();

            DateCreated = dtpDateCreated.Value.ToString("yyyy-MM-dd");
            DatePosted = dtpDatePosted.Value.ToString("yyyy-MM-dd");
            DateUpdated = dtpDateUploaded.Value.ToString("yyyy-MM-dd");

            code = txtSupplierCode.Text;

            groupBox3.Enabled = true;

            txtSupplierCode.Enabled = false;
            txtSupplierName.Enabled = false;
            txtSupplierAddress.Enabled = false;
            cboContactPerson.Enabled = false;
            txtSupplierContact.Enabled = false;

            pocon.OpenConn();
            SqlDataReader drHead = pocon.DataReader("select MAX (po_document_number) as po_docnum from po_body");
            drHead.Read();
            if(drHead["po_docnum"].ToString() != "")
            {
                doc_Num = drHead["po_docnum"].ToString();
                xdoc_Num = Int32.Parse(doc_Num) + 1;

                po_docNo = xdoc_Num.ToString().PadLeft(7,'0');
                txtDocumentNumber.Text = po_docNo;
            }
            else
            {
                doc_Num = "0000001";
                po_docNo = doc_Num;
                xdoc_Num = Int32.Parse(doc_Num);
                txtDocumentNumber.Text = po_docNo;

                pocon.CloseConn();
            }

            //doc_No = po_docNo;

            pocon.OpenConn();
            dgvItemList.DataSource = pocon.ShowDataDGV("select * from Item_List");
            pocon.CloseConn(); 
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            pocon.OpenConn();
            dgvItemList.DataSource = pocon.ShowDataDGV("Select * From Item_List where [Product Code]  like '%"+txtSearch.Text+"%' or [Product Name] like '%"+txtSearch.Text+"%'");
            pocon.CloseConn();
        }
        
        private void dgvItemList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null || txtSupplierName.Text == "" || txtSupplierName.Text == null || txtSupplierContact.Text == "" || txtSupplierContact.Text == null)
            {
                MessageBox.Show("Please fill up the details first.", "Alert!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Form newFormDialog = new Form();
                try
                {
                    using (PO_Item_Details pO_Item_Details = new PO_Item_Details())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        pO_Item_Details.Owner = newFormDialog;

                        uom = this.dgvItemList.CurrentRow.Cells[5].Value.ToString();

                        pO_Item_Details.txtProductCode.Text = this.dgvItemList.CurrentRow.Cells[0].Value.ToString();
                        pO_Item_Details.txtProductName.Text = this.dgvItemList.CurrentRow.Cells[1].Value.ToString();
                        pO_Item_Details.txtProductDescription.Text = this.dgvItemList.CurrentRow.Cells[2].Value.ToString();
                        pO_Item_Details.txtProductPrice.Text = this.dgvItemList.CurrentRow.Cells[4].Value.ToString();
                        pO_Item_Details.lblhidden.Text = txtDocumentNumber.Text;

                        if (pO_Item_Details.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            pocon.OpenConn();
                            dgvOrderList.DataSource = pocon.ShowDataDGV("select * from draft_PO_view");
                            pocon.CloseConn();

                            lblTotalBeforeDiscount.Text = pO_Item_Details.t_Price;
                            lblTaxValue.Text = pO_Item_Details.t_Tax;
                            lblTotalPaymentDue.Text = pO_Item_Details.t_Value;
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }
            }
        }
        #region click event
        private void dgvItemList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void dtpDatePosted_ValueChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void dgvItemList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvOrderList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Purchasing_Order_Activated(object sender, EventArgs e)
        {

        }
        #endregion

        private void btnViewSupplier_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Suppliers_List suplist = new Suppliers_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        txtSupplierCode.Text = suplist.SupCode;
                        txtSupplierName.Text = suplist.SupName;
                        txtSupplierAddress.Text = suplist.SupAdd;
                        txtSupplierContact.Text = suplist.SupCon;

                        cboContactPerson.Text = "";

                        groupBox3.Enabled = true;

                    }

                    groupBox1.Enabled = true;

                    txtSupplierCode.Enabled = false;
                    txtSupplierName.Enabled = false;
                    txtSupplierAddress.Enabled = false;
                    cboContactPerson.Enabled = false;
                    txtSupplierContact.Enabled = false;
                    cboContactPerson.Enabled = true;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }   
        }
        private void txtSupplierCode_TextChanged(object sender, EventArgs e)
        {
            conn.Open();
            try
            {
                string supcode = txtSupplierCode.Text;

                SqlCommand cmd = new SqlCommand("select * from dbo.contact_person where supplier_code = '"+supcode+"'", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                cboContactPerson.DataSource = dt;
                cboContactPerson.DisplayMember = "name";
                cboContactPerson.ValueMember = "contact_person_id";
                cmd.ExecuteNonQuery();
            }
            catch(Exception)
            {

            }
            conn.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cboContactPerson.Text == "" || cboContactPerson.Text == null)
            {
                contact_person = Int32.Parse(0.ToString()) ;
            }
            else
            {
                contact_person = Int32.Parse(cboContactPerson.SelectedValue.ToString());
            }
            total_amount = double.Parse(lblTotalPaymentDue.Text);

            pocon.OpenConn();
            SqlDataReader drDraft = pocon.DataReader("select * from draft_po");

            
            if ((txtSupplierCode.Text != "" || txtSupplierCode.Text != null || txtSupplierName.Text != "" || txtSupplierName.Text != null || txtSupplierContact.Text != "" || txtSupplierContact.Text != null) && drDraft.Read())
            {
                pocon.OpenConn();
                pocon.ExecuteQuery("Insert into po (po_document_number, supplier_code, contact_person_id, date_created, date_posted, date_upload,status_id,total_price)" +
                    "Values" +
                    "('" + txtDocumentNumber.Text + "', '" + txtSupplierCode.Text + "' , '" + contact_person + "', '" + DateCreated + "', '" + DatePosted + "', '" + DateUpdated + "', '" + 5 + "', '" + total_amount + "') ");
                pocon.CloseConn();

                pocon.OpenConn();
                pocon.ExecuteQuery("insert into po_body (po_document_number, inventory_code, quantity, uom_id, price, tax_id, tax_price,total_price)" +
                    "select po_document_number, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price from draft_po");

                MessageBox.Show("Purchase Order Saved!", "Save Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information);
                pocon.ExecuteQuery("truncate table draft_po");
                pocon.CloseConn();
                clear();
                this.Close();
                
            }
            else
            {
                MessageBox.Show("Please fill up the details first.", "Alert!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            pocon.CloseConn();
        }

    }
}
