﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Add_New_Vehicle : Form
    {

        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        DBConnection conn = new DBConnection();

        private int nCboMake;
        private int nCboSize;
        private int nCboCat;
        public Add_New_Vehicle()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            nCboMake = Int32.Parse(this.cboMake.SelectedValue.ToString());
            nCboSize = Int32.Parse(this.cboSize.SelectedValue.ToString());
            nCboCat = Int32.Parse(this.cboCategory.SelectedValue.ToString());
            if (txtModel.Text != "")
            {
                conn.OpenConnection();
                conn.ExecuteQueries("insert into vehicle (auto_make_id, model, size, vehicle_cat_id) values ('"+nCboMake+"', '"+txtModel.Text+"','"+nCboSize+"', '"+nCboCat+"')");
                conn.CloseConnection();
                MessageBox.Show("Vehicle Added");
                this.Close();
            }
        }

        private void Add_New_Vehicle_Load(object sender, EventArgs e)
        {

            con.Open();
            SqlCommand cmdMake = new SqlCommand("select * from auto_make", con);
            SqlDataReader drMake = cmdMake.ExecuteReader();
            DataTable dtMake = new DataTable();
            dtMake.Load(drMake);
            cboMake.DataSource = dtMake;
            cboMake.DisplayMember = "auto_make_desc";
            cboMake.ValueMember = "auto_make_id";
            cmdMake.ExecuteNonQuery();
            con.Close();

            con.Open();
            SqlCommand cmdSize = new SqlCommand("select * from size", con);
            SqlDataReader drSize = cmdSize.ExecuteReader();
            DataTable dtSize = new DataTable();
            dtSize.Load(drSize);
            cboSize.DataSource = dtSize;
            cboSize.DisplayMember = "size_desc";
            cboSize.ValueMember = "size_id";
            cmdSize.ExecuteNonQuery();
            con.Close();

            con.Open();
            SqlCommand cmdCat = new SqlCommand("select * from vehicle_category", con);
            SqlDataReader drCat = cmdCat.ExecuteReader();
            DataTable dtCat = new DataTable();
            dtCat.Load(drCat);
            cboCategory.DataSource = dtCat;
            cboCategory.DisplayMember = "vehicle_cat_desc";
            cboCategory.ValueMember = "vehicle_cat_id";
            cmdCat.ExecuteNonQuery();
            con.Close();
        }

        private void cboMake_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
