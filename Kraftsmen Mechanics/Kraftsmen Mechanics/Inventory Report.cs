﻿using System;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public partial class Inventory_Report : Form
    {
        Connection conn = new Connection();
        public Inventory_Report()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            string stocks = cboInventoryMovement.SelectedItem.ToString();
            string inventory_level = cboReportType.SelectedItem.ToString();
            string date_from = dtpFrom.Value.ToString();
            string date_to = dtpTo.Value.ToString();
        
            try
            {
            if (stocks == "All")
                {
                    if (inventory_level == "Critical State")
                    {  
                   
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Used/Added Quantity',format(a.date_of_delivered,'MMM-dd-yyyy') 'Date',case when a.quantity <50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock In' 'Identifier' from stock_in a left join inventory b on a.inventory_code = b.inventory_code where a.quantity < 50 and a.date_of_delivered between '" + date_from + "' and '" + date_to + "' Union all Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'Quantity',a.used_quantity 'Used/Added Quantity',format(a.date,'MMM-dd-yyyy') 'Date',case when a.quantity < 50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock out' 'Identifier' from stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.quantity < 50 and a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                 
                    }
                     else
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Used/Added Quantity',format(a.date_of_delivered,'MMM-dd-yyyy') 'Date',case when a.quantity <50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock In' 'Identifier' from stock_in a left join inventory b on a.inventory_code = b.inventory_code where a.quantity > 50 and a.date_of_delivered between '" + date_from + "' and '" + date_to + "' Union all Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'Quantity',a.used_quantity 'Used/Added Quantity',format(a.date,'MMM-dd-yyyy') 'Date',case when a.quantity <50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock out' 'Identifier' from stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.quantity > 50 and a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                    }
                    if (inventory_level == "All")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Used/Added Quantity',format(a.date_of_delivered,'MMM-dd-yyyy') 'Date',case when a.quantity <50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock In' 'Identifier' from stock_in a left join inventory b on a.inventory_code = b.inventory_code where a.date_of_delivered between '" + date_from + "' and '" + date_to + "' Union all Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'Quantity',a.used_quantity 'Used/Added Quantity',format(a.date,'MMM-dd-yyyy') 'Date',case when a.quantity <50 then 'Critical'else 'Stable' end as 'Inventory Level','Stock out' 'Identifier' from stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.date between '" + date_from + "' and '" + date_to + "'");
                     
                        conn.CloseConn();
                    }
                }
            else if (stocks == "Stock-in")
            {
                    if (inventory_level == "Critical State")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Added Stock',a.date_of_delivered 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_in a left join inventory b on a.inventory_code = b.inventory_code where a.quantity < 50 and a.date_of_delivered between '"+date_from+"' and '"+date_to+"'");
                        conn.CloseConn();

                    }
                    else if (inventory_level == "Normal State")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Added Stock',a.date_of_delivered 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_in a left join inventory b on a.inventory_code = b.inventory_code where a.quantity > 50 and a.date_of_delivered between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn(); 
                    }
                    else if (inventory_level == "All")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.quantity_of_added_stock 'Added Stock',a.date_of_delivered 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_in a left join inventory b on a.inventory_code = b.inventory_code where  a.date_of_delivered between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                    }
            }
            else if (stocks == "Stock-Out")
                {
                    if (inventory_level == "Critical State")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.used_quantity 'Added Stock',a.date 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.quantity < 50 and a.date between '"+date_from+"' and '"+date_to+"'");
                        conn.CloseConn();
                    }
                    else if (inventory_level == "Normal State")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.used_quantity 'Added Stock',a.date 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.quantity > 50 and a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                    }
                    else if (inventory_level == "All")
                    {
                        conn.OpenConn();
                        dgvInventoryReportList.DataSource = conn.ShowDataDGV("Select a.inventory_code 'Item Code',b.inventory_name 'Item Name',a.quantity 'New Quantity',a.used_quantity 'Added Stock',a.date 'Date of Delivered',case when a.quantity <50 then 'Critical' else 'Stable' end as 'Inventory Level' from   stock_out a left join inventory b on a.inventory_code = b.inventory_code where a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                    }

                }
           }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void Inventory_Report_Load(object sender, EventArgs e)
        {
            cboInventoryMovement.SelectedIndex = 0;
            cboReportType.SelectedIndex = 0;
        }
    }
}
