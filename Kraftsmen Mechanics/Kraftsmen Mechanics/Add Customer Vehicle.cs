﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{

    public partial class Add_New_Customer : Form
    {
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        DBConnection dbcon = new DBConnection();

        public int CusID;
        public string nCustomerName;
        public string nCustomerNumber;
        public string nCustomerAddress;
        public string nCustomerCity;
        public string nCustomerPostal;

        public string nVehicleModel;
        public int nVehicleID;
        public int nVehicleMake;
        public int nVehicleSize;
        public int nVehicleCat;
        public string nVehicleColor;
        public string nVehiclePlateNum;
        private int cCusID = 0;
        

        public Add_New_Customer()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtContactNumber_TextChanged(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtContactNumber.Text, "^[a-zA-Z ]"))
            { 
                txtContactNumber.Text.Remove(txtContactNumber.Text.Length - 1);
            }
        }

        private void Add_New_Customer_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmdMake = new SqlCommand("select * from dbo.auto_make", con);
            SqlDataReader drMake = cmdMake.ExecuteReader();
            DataTable dtMake = new DataTable();
            dtMake.Load(drMake);

            cboMake.DataSource = dtMake;
            cboMake.DisplayMember = "auto_make_desc";
            cboMake.ValueMember = "auto_make_id";
            cmdMake.ExecuteNonQuery();


            SqlCommand cmdSize = new SqlCommand("select * from dbo.size", con);
            SqlDataReader drSize = cmdSize.ExecuteReader();
            DataTable dtSize = new DataTable();
            dtSize.Load(drSize);

            cboSize.DataSource = dtSize;
            cboSize.DisplayMember = "size_desc";
            cboSize.ValueMember = "size_id";
            cmdSize.ExecuteNonQuery();
            con.Close();

            con.Open();
            SqlCommand cmdCat = new SqlCommand("select * from vehicle_category", con);
            SqlDataReader drCat = cmdCat.ExecuteReader();
            DataTable dtCat = new DataTable();
            dtCat.Load(drCat);

            cboCategory.DataSource = dtCat;
            cboCategory.DisplayMember = "vehicle_cat_desc";
            cboCategory.ValueMember = "vehicle_cat_id";
            cmdCat.ExecuteNonQuery();
            con.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCustomerName.Text == "" || txtAddress.Text == "" || txtCustomerName.Text == "" || txtModel.Text == "" || txtVehicleColor.Text == "" || txtPlateNumber.Text == "")
            {
                MessageBox.Show("Please Fill up all Contents", "Notice!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

                nCustomerName = txtCustomerName.Text;
                nCustomerNumber = txtContactNumber.Text;
                nCustomerAddress = txtAddress.Text;
                nVehicleModel = txtModel.Text;

                dbcon.OpenConnection();

                //dbcon.ExecuteQueries("insert into dbo.customer (name, address, number, status) values ('" + nCustomerName + "','" + nCustomerAddress + "','" + nCustomerNumber + "', '" + 1 + "')");

                SqlDataReader drCus = dbcon.DataReader("select max(customer_id) as cus_id from dbo.customer");
                drCus.Read();
                CusID = Int32.Parse(drCus["cus_id"].ToString());
                dbcon.CloseConnection();

                dbcon.OpenConnection();
                nVehicleMake = Int32.Parse(cboMake.SelectedValue.ToString());
                nVehicleSize = Int32.Parse(cboSize.SelectedValue.ToString());
                nVehicleCat = Int32.Parse(cboCategory.SelectedValue.ToString());
                nVehicleColor = txtVehicleColor.Text;
                nVehiclePlateNum = txtPlateNumber.Text;
                
                //con.Open();
                //MessageBox.Show(nVehicleMake.ToString() + " " + nVehicleSize.ToString());
                //SqlCommand cmdInsertVehicle = new SqlCommand("insert into dbo.vehicle(customer_id, auto_make_id, model, size, color, plate_number) values ('" + CusID + "', '" + nVehicleMake + "', '" + nVehicleModel + "', '" + nVehicleSize + "', '" + nVehicleColor + "', '" + nVehiclePlateNum + "')",con);
                //cmdInsertVehicle.ExecuteNonQuery();
                //con.Close();
                dbcon.ExecuteQueries("insert into customer_vehicle (customer_id, vehicle_id, plate_number, color, status)" +
                    "values" +
                    "('"+cCusID+"'," +
                    " '"+nVehicleID+"'," +
                    " '"+nVehiclePlateNum+"'," +
                    " '"+nVehicleColor+"'," +
                    " '"+1+"')");
                dbcon.CloseConnection();
                MessageBox.Show("Vehicle Added!");

                refreshDgvVehicleOfCustomer();
                txtModel.Text = "";
                txtPlateNumber.Text = "";
                txtVehicleColor.Text = "";
            }
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerList_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Customer_List customer_List = new Customer_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    customer_List.Owner = newFormDialog;

                    if (customer_List.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        cCusID = customer_List.CusID;

                        if(cCusID != 0)
                        {
                            txtAddress.Text = customer_List.CusAddress;
                            txtCity.Text = customer_List.CusCity;
                            txtContactNumber.Text = customer_List.CusNo;
                            txtPostalCode.Text = customer_List.CusPortal;
                            txtCustomerName.Text = customer_List.CusName;
                            refreshDgvVehicleOfCustomer();
                            nCustomerAddress = txtAddress.Text;
                            nCustomerName = txtCustomerName.Text;
                            nCustomerNumber = txtContactNumber.Text;
                            nCustomerCity = txtCity.Text;
                            nCustomerPostal = txtPostalCode.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        private void refreshDgvVehicleOfCustomer()
        {
            dbcon.OpenConnection();
            dgvCustomerVehicle.DataSource = dbcon.ShowDataInGridView("select * from Vehicles_of_customer where [Customer ID] = '" + cCusID + "'");
            dbcon.CloseConnection();
        }
        private void btnVehicleList_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Vehicle_List vehicle_List = new Vehicle_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    vehicle_List.Owner = newFormDialog;

                    if (vehicle_List.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        nVehicleID = vehicle_List.vehicleID;
                        cboCategory.SelectedValue = vehicle_List.vehicleCategory.ToString();
                        cboSize.SelectedValue = vehicle_List.vehicleSize.ToString();
                        cboMake.SelectedValue = vehicle_List.vehicleMake.ToString();
                        txtModel.Text = vehicle_List.vehicleName;
                        cboMake.Enabled = false;
                        cboCategory.Enabled = false;
                        cboSize.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
    }
}
