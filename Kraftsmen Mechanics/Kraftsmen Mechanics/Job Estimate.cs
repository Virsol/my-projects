﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Estimate : Form
    {
        DBConnection conn = new DBConnection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        /*JobEstimate_Queries query = new JobEstimate_Queries();*/
        Customer_List cusList = new Customer_List();
        Transaction_List trans_list;
        

        public string ntxtSearch;
        public string ncboSearchBy;
        public string ntxtCustomerName;
        public string ncboVehicleModel;
        public string ntxtContactNumber;
        public string ntxtVehicleMake;
        public string ntxtVehicleColor;
        public string ntxtVehiclePlateNo;
        public string nDocumentNumber;
        public string docSeries;
        public string docNum;
        public string nDocumentSeries;
        public string nDateCreated;
        private int nEmployeeID;
        private int cusID;
        private int iDocNum;
        private string sDocNum;
        private int vehicleID;
        private int nTechnician;
        private string TotalItem;
        private string TotalPaymentDue;
        private string TotalTax;
        string ID;

        public string vehicleName;
        public Job_Estimate()
        {
            InitializeComponent();
        }

  
        public Job_Estimate(Transaction_List trans)
        {
            InitializeComponent();
            trans_list = trans;
        }
        public string DocNum
        {
            /*string num = iDocNum.ToString().PadLeft(4, '0');
            return num;*/
            get { return sDocNum; }
            set { sDocNum = value; }
        }
        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Job_Estimate_Load(object sender, EventArgs e)
        {
            nDateCreated = dtpDateCreated.Value.ToString("yyyy-MM-dd");
            conn.OpenConnection();
            con.Open();

            SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Estimate' and document_series.doc_tag_id = Document_tag.doc_tag_id");
            if (drDT.Read())
            {
                docSeries = drDT["series_desc"].ToString();
                conn.CloseConnection();
            }

            conn.OpenConnection();
            SqlDataReader drDN = conn.DataReader("select max(job_estimate_number) as current_doc_num from dbo.job_estimate_body");
            drDN.Read();
            if (drDN["current_doc_num"].ToString() != "")
            {
                docNum = drDN["current_doc_num"].ToString();
                iDocNum = Int32.Parse(docNum) + 1;

                nDocumentNumber = iDocNum.ToString().PadLeft(7, '0');
                nDocumentSeries = docSeries;
                lblDocumentNumber.Text = nDocumentNumber;
                lblDocumentSeries.Text = nDocumentSeries;

            }
            else
            {
                docNum = "0000001";
                nDocumentNumber = docNum;
                nDocumentSeries = docSeries;
                iDocNum = Int32.Parse(docNum);
                lblDocumentSeries.Text = nDocumentSeries;
                lblDocumentNumber.Text = nDocumentNumber;
                conn.CloseConnection();
            }
            conn.OpenConnection();
            SqlCommand cmd = new SqlCommand("select * from dbo.inventory_type", con);
            SqlDataReader drSearch = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(drSearch);
            cboSearchBy.DataSource = dt;
            cboSearchBy.DisplayMember = "inventory_desc";
            cboSearchBy.ValueMember = "inventory_type_id";
            cmd.ExecuteNonQuery();

            SqlCommand cmdEmp = new SqlCommand("select * from dbo.Employees_List", con);
            SqlDataReader drEmp = cmdEmp.ExecuteReader();
            DataTable dtEmp = new DataTable();
            dtEmp.Load(drEmp);
            cboEstimatedBy.DataSource = dtEmp;
            cboEstimatedBy.DisplayMember = "Name";
            cboEstimatedBy.ValueMember = "ID";
            cmdEmp.ExecuteNonQuery();
            nEmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());

            /*            SqlCommand cmdTech = new SqlCommand("select * from dbo.Technician", con);
                        SqlDataReader drTech = cmdTech.ExecuteReader();
                        DataTable dtTech = new DataTable();
                        dtTech.Load(drTech);
                        cboTechnicianName.DataSource = dtTech;
                        cboTechnicianName.DisplayMember = "Name";
                        cboTechnicianName.ValueMember = "ID";
                        cmdTech.ExecuteNonQuery();
                        nTechnician = Int32.Parse(cboTechnicianName.SelectedValue.ToString());*/

            sDocNum = iDocNum.ToString().PadLeft(7, '0');
            /*MessageBox.Show(DocNum);*/
            con.Close();
            conn.CloseConnection();

            if(trans_list != null)
            {
                //ID = trans_list.CusID.ToString();
                //MessageBox.Show(trans_list.DocNum);
                if (trans_list.CusID != 0)
                {
                    btnSave.Enabled = false;
                    conn.OpenConnection();
                    SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + trans_list.CusID + "'");
                    cusDR.Read();
                    txtCustomerName.Text = cusDR["name"].ToString();

                    txtContactNumber.Text = cusDR["Contact Number"].ToString();
                    txtAddress.Text = cusDR["address"].ToString();
                    conn.CloseConnection();

                    con.Open();
                    SqlCommand vehicleCMD = new SqlCommand("select * from vehicles_of_customer where [ID] = '" + trans_list.CarID + "'", con);
                    SqlDataReader vehicleDR = vehicleCMD.ExecuteReader();
                    DataTable vehicleDT = new DataTable();
                    vehicleDT.Load(vehicleDR);
                    cboVehicleModel.DataSource = vehicleDT;
                    cboVehicleModel.DisplayMember = "Vehicle Model";
                    cboVehicleModel.ValueMember = "ID";
                    con.Close();

                    con.Open();
                    SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                    SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                    DataTable dtCategory = new DataTable();
                    dtCategory.Load(drCategory);
                    cboCategory.DataSource = dtCategory;
                    cboCategory.DisplayMember = "vehicle_cat_desc";
                    cboCategory.ValueMember = "vehicle_cat_id";
                    cmdVeCat.ExecuteNonQuery();
                    con.Close();

                    cusID = trans_list.CusID;

                    cboCategory.Enabled = false;
                    rdoWithMaterial.Enabled = false;
                    rdoWithoutMaterial.Enabled = false;
                    dgvServiceList.Enabled = false;
                    cboSearchBy.Enabled = false;
                    txtCustomerName.Enabled = false;
                    txtAddress.Enabled = false;
                    txtContactNumber.Enabled = false;
                    txtSearch.Enabled = false;
                    cboEstimatedBy.Enabled = false;
                    dtpDateCreated.Enabled = false;
                    cboVehicleModel.Enabled = false;
                    dgvServiceOrderList.Enabled = false;
                    groupBox1.Enabled = false;
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    groupBox4.Enabled = false;
                    groupBox5.Enabled = false;

                    if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                    {
                        vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                        if (vehicleID != 0 && cusID != 0)
                        {

                            conn.OpenConnection();
                            /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                            SqlDataReader dr = cmdV.ExecuteReader();*/

                            SqlDataReader dr = conn.DataReader("select * from vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                            if (dr.Read())
                            {
                                cboVehicleModel.Enabled = true;
                                txtVehicleMake.Text = dr["Auto"].ToString();
                                txtVehicleSize.Text = dr["Size"].ToString();
                                txtVehicleColor.Text = dr["color"].ToString();
                                txtPlateNumber.Text = dr["Platenumber"].ToString();
                                cboCategory.Text = dr["Vehicle Category"].ToString();
                                conn.CloseConnection();
                            }
                            else
                            {
                                MessageBox.Show(vehicleID + " " + cusID);
                            }
                        }
                    }

                    else
                    {

                    }

                    con.Open();
                    SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + trans_list.empID + "'", con);
                    SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                    DataTable employeeDT = new DataTable();
                    employeeDT.Load(employeeDR);
                    cboEstimatedBy.DataSource = employeeDT;
                    cboEstimatedBy.DisplayMember = "Name";
                    cboEstimatedBy.ValueMember = "ID";
                    con.Close();



                    dtpDateCreated.Text = trans_list.Date;

                    conn.OpenConnection();
                    dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from job_estimate_view where [Job Estimate Number] = '" + trans_list.DocNum + "' ");
                    conn.CloseConnection();
                    

                    conn.OpenConnection();
                    SqlDataReader drTotal = conn.DataReader("select format(SUM(Price), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from job_estimate_view where [Job Estimate Number] = '" + trans_list.DocNum + "'");
                    if (drTotal.Read())
                    {
                        lblTaxValue.Text = drTotal["Tax_total"].ToString();
                        lblTotalItem.Text = drTotal["Price_total"].ToString();
                        lblTotalPaymentDue.Text = drTotal["Total_amount"].ToString();
                    }
                    conn.CloseConnection();
                }

            }
            else
            {
                dgvOrderListRefresh();
            }
            
            


            /*conn.OpenConnection();
            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_estimate_view");
            conn.CloseConnection();*/
            
            
        }
        private void dgvItemlistrefresh()
        {

            if(rdoWithMaterial.Checked)
            {
                conn.OpenConnection();
                dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "' and ([Service/Item Name] like '%" + ntxtSearch + "%' or [Service/Item Code] like '%" + ntxtSearch + "%')");
                conn.CloseConnection();
            }
            else if(rdoWithoutMaterial.Checked)
            {
                if (ncboSearchBy == "Item")
                {
                    conn.OpenConnection();
                    dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "' and ([Service/Item Name] like '%" + ntxtSearch + "%' or [Service/Item Code] like '%" + ntxtSearch + "%')");
                    conn.CloseConnection();
                }
                else if (ncboSearchBy == "Services")
                {
                    conn.OpenConnection();
                    dgvServiceList.DataSource = conn.ShowDataInGridView("select * from parent_view where [Service Code] like '%" + ntxtSearch + "%'");
                    conn.CloseConnection();
                }
            }
        }
        private void dgvOrderListRefresh()
        {
            conn.OpenConnection();
            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_estimate_view");
            conn.CloseConnection();
        }
        public string lblDocNum
        {
            get
            {
                return this.lblDocumentNumber.Text;
            }
            set
            {
                this.lblDocumentNumber.Text = value;
            }
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            ntxtSearch = txtSearch.Text;
            dgvItemlistrefresh();
        }

        private void cboSearchBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ncboSearchBy = this.cboSearchBy.GetItemText(this.cboSearchBy.SelectedItem);
            dgvItemlistrefresh();

        }

        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {
            ntxtCustomerName = txtCustomerName.Text;
        }

        private void dgvServiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string type = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
            Form newFormDialog = new Form();
            if ("Services" == type)
            {
                try
                {
                    using (Add_Service AddServices = new Add_Service())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddServices.Owner = newFormDialog;

                        AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();
                        con.Open();
                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddServices.cboServiceTax.DataSource = dtTax;
                        AddServices.cboServiceTax.DisplayMember = "tax_desc";
                        AddServices.cboServiceTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();

                        con.Close();
                        AddServices.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddServices.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddServices.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            conn.OpenConnection();
                            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_estimate_view");
                            refreshTotal();
                            conn.CloseConnection();
                        }
                    }        
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }

            }
            else if ("Item" == type)
            {
                try
                {
                    using(Add_Items AddItems = new Add_Items())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddItems.Owner = newFormDialog;

                        AddItems.txtItemName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddItems.txtItemCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddItems.txtPrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();

                        con.Open();
                        SqlCommand cmd = new SqlCommand("select * from dbo.uom", con);
                        SqlDataReader dr = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(dr);
                        AddItems.cboUOM.DataSource = dt;
                        AddItems.cboUOM.DisplayMember = "uom_desc";
                        AddItems.cboUOM.ValueMember = "uom_id";
                        cmd.ExecuteNonQuery();

                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddItems.cboItemTax.DataSource = dtTax;
                        AddItems.cboItemTax.DisplayMember = "tax_desc";
                        AddItems.cboItemTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();
                        con.Close();
                        AddItems.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddItems.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddItems.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            dgvOrderListRefresh();
                            refreshTotal();
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }

                
            }
            else
            {
                try
                {
                    using (Add_Service AddServices = new Add_Service())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddServices.Owner = newFormDialog;
                        AddServices.lblBomCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
                        AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[4].Value.ToString();
                        con.Open();
                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddServices.cboServiceTax.DataSource = dtTax;
                        AddServices.cboServiceTax.DisplayMember = "tax_desc";
                        AddServices.cboServiceTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();

                        con.Close();
                        AddServices.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddServices.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddServices.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            conn.OpenConnection();
                            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_estimate_view");
                            refreshTotal();
                            conn.CloseConnection();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }
            }
            Add_Items add_Items = new Add_Items();
            Add_Service add_Service = new Add_Service();


        }

        private void dgvServiceList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*string type = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
            
            if ("Services" == type)
            {
                Job_Estimate_Add_Service AddServices = new Job_Estimate_Add_Service();
                AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();
                AddServices.ShowDialog();

            }
            else if ("Item"== type)
            {
                Job_Estimate_Add_Items AddItems = new Job_Estimate_Add_Items();
                
               
                AddItems.txtItemName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                AddItems.txtItemCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                AddItems.txtPrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();
                AddItems.ShowDialog();
            }*/
        }

        private void txtCustomerName_Click(object sender, EventArgs e)
        {

        }

        private void txtCustomerName_MouseClick(object sender, MouseEventArgs e)
        {
            con.Open();
            Form newFormDialog = new Form();
            try
            {
                using (Customer_List cusList = new Customer_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    cusList.Owner = newFormDialog;
                    try
                    {
                        if (cusList.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            cusID = cusList.CusID;


                            SqlCommand cmdVehicle = new SqlCommand("select * from vehicles_of_customer where [Customer ID] = '" + cusID + "'", con);
                            SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                            DataTable dtVehicle = new DataTable();
                            dtVehicle.Load(drVehicle);
                            cboVehicleModel.DataSource = dtVehicle;
                            cboVehicleModel.DisplayMember = "Vehicle Model";
                            cboVehicleModel.ValueMember = "ID";
                            cmdVehicle.ExecuteNonQuery();

                            SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                            SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                            DataTable dtCategory = new DataTable();
                            dtCategory.Load(drCategory);
                            cboCategory.DataSource = dtCategory;
                            cboCategory.DisplayMember = "vehicle_cat_desc";
                            cboCategory.ValueMember = "vehicle_cat_id";
                            cmdVeCat.ExecuteNonQuery();

                            if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                            {
                                vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());
                                txtCustomerName.Text = cusList.CusName;
                                txtContactNumber.Text = cusList.CusNo;
                                txtAddress.Text = cusList.CusAddress;

                                if (vehicleID != 0 && cusID != 0)
                                {

                                    conn.OpenConnection();
                                    /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                    SqlDataReader dr = cmdV.ExecuteReader();*/

                                    SqlDataReader dr = conn.DataReader("select * from vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                                    if (dr.Read())
                                    {
                                        cboVehicleModel.Enabled = true;
                                        txtVehicleMake.Text = dr["Auto"].ToString();
                                        txtVehicleSize.Text = dr["Size"].ToString();
                                        txtVehicleColor.Text = dr["color"].ToString();
                                        txtPlateNumber.Text = dr["Platenumber"].ToString();
                                        cboCategory.Text = dr["Vehicle Category"].ToString();
                                        conn.CloseConnection();
                                    }
                                    else
                                    {
                                        MessageBox.Show(vehicleID + " " + cusID);
                                    }
                                }
                            }

                            else
                            {

                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                    con.Close();
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

                newFormDialog.Dispose();
            }       
        }

        private void cboVehicleModel_SelectedIndexChanged(object sender, EventArgs e)
        {

            conn.OpenConnection();

            string model = this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem);
            string owner = txtCustomerName.Text;

            SqlDataReader drCBOV = conn.DataReader("select * from dbo.vehicles_of_customer where [Customer ID] = '" + cusID + "'and  [Vehicle Model] = '" + model + "'");
            if (drCBOV.Read())
            {

                txtVehicleMake.Text = drCBOV["Auto"].ToString();
                txtVehicleSize.Text = drCBOV["Size"].ToString();
                txtVehicleColor.Text = drCBOV["color"].ToString();
                txtPlateNumber.Text = drCBOV["Platenumber"].ToString();
                vehicleID = Int32.Parse(drCBOV["ID"].ToString());
                cboCategory.Text = drCBOV["Vehicle Category"].ToString();
                conn.CloseConnection();
            }
            /*else
            {
            MessageBox.Show(vehicleID.ToString() + "cbo");
            }*/

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void dgvServiceOrderList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string item_or_service_name = this.dgvServiceOrderList.CurrentRow.Cells[1].Value.ToString();
            int iD = Int32.Parse(this.dgvServiceOrderList.CurrentRow.Cells[10].Value.ToString());
            string bom_code = this.dgvServiceOrderList.CurrentRow.Cells[11].Value.ToString();

            if(bom_code == "")
            {
                if (MessageBox.Show("Do you wan't to void this item?" + "\n" + item_or_service_name, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    conn.OpenConnection();
                    conn.ExecuteQueries("delete from dbo.draft_estimate where draft_estimate_id = '" + iD + "'");
                    conn.CloseConnection();
                    dgvOrderListRefresh();
                    refreshTotal();
                }
                else
                {

                }
            }
            else
            {
                if(MessageBox.Show("Do you want to void this BOM?" + "\n" + bom_code, "",MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    conn.OpenConnection();
                    conn.ExecuteQueries("delete from draft_estimate where bom_code = '"+bom_code+"'");
                    conn.CloseConnection();
                    dgvOrderListRefresh();
                    refreshTotal();
                }
                else
                {

                }
            }
            

        }
        private void refreshTotal()
        {

            conn.OpenConnection();
            SqlDataReader totalReader = conn.DataReader("select format(SUM(total_price), '#,#.00')as Price_total, format(SUM(tax_price), '#,#.00')as Tax_total, format(SUM(total_price + tax_price),'#,#.00') as Total_amount from draft_estimate");
            totalReader.Read();
            TotalItem = totalReader["Price_total"].ToString();
            TotalPaymentDue = totalReader["Total_amount"].ToString();
            TotalTax = totalReader["Tax_total"].ToString();
            conn.CloseConnection();

            lblTotalPaymentDue.Text = TotalPaymentDue;
            lblTotalItem.Text = TotalItem;
            lblTaxValue.Text = TotalTax;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

       
        }

        private void cboEstimatedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //int EmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());
            //MessageBox.Show(EmployeeID.ToString());
        }

        private void btnViewJobEstimateList_Click(object sender, EventArgs e)
        {

        }

        private void btnViewJobEstimateList_Click_1(object sender, EventArgs e)
        {
            
        }

        private void txtCustomerName_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btnViewJobEstimateList_Click_2(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try { 
                using(Job_Estimate_List JEL = new Job_Estimate_List())
                {
                    JEL.lblJobEstimate.Text = "Job Estimate View";
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    JEL.Owner = newFormDialog;

                    if (JEL.ShowDialog() != DialogResult.OK && JEL.CusID != 0 && JEL.CarID != 0)
                    {
                        newFormDialog.Dispose();

                        btnSave.Enabled = false;
                        conn.OpenConnection();
                        SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + JEL.CusID + "'");
                        cusDR.Read();
                        txtCustomerName.Text = cusDR["name"].ToString();

                        txtContactNumber.Text = cusDR["Contact Number"].ToString();
                        txtAddress.Text = cusDR["address"].ToString();
                        conn.CloseConnection();

                        cusID = JEL.CusID;
                        con.Open();
                        SqlCommand vehicleCMD = new SqlCommand("select * from vehicles_of_customer where [ID] = '" + JEL.CarID + "'", con);
                        SqlDataReader vehicleDR = vehicleCMD.ExecuteReader();
                        DataTable vehicleDT = new DataTable();
                        vehicleDT.Load(vehicleDR);
                        cboVehicleModel.DataSource = vehicleDT;
                        cboVehicleModel.DisplayMember = "Vehicle Model";
                        cboVehicleModel.ValueMember = "ID";
                        con.Close();

                        con.Open();
                        SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                        SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                        DataTable dtCategory = new DataTable();
                        dtCategory.Load(drCategory);
                        cboCategory.DataSource = dtCategory;
                        cboCategory.DisplayMember = "vehicle_cat_desc";
                        cboCategory.ValueMember = "vehicle_cat_id";
                        cmdVeCat.ExecuteNonQuery();
                        con.Close();


                        cboCategory.Enabled = false;
                        rdoWithMaterial.Enabled = false;
                        rdoWithoutMaterial.Enabled = false;
                        dgvServiceList.Enabled = false;
                        cboSearchBy.Enabled = false;
                        txtCustomerName.Enabled = false;
                        txtAddress.Enabled = false;
                        txtContactNumber.Enabled = false;
                        txtSearch.Enabled = false;
                        cboEstimatedBy.Enabled = false;
                        dtpDateCreated.Enabled = false;
                        cboVehicleModel.Enabled = false;
                        dgvServiceOrderList.Enabled = false;

                        if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                        {
                            vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                            if (vehicleID != 0 && cusID != 0)
                            {

                                conn.OpenConnection();
                                /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                SqlDataReader dr = cmdV.ExecuteReader();*/

                                SqlDataReader dr = conn.DataReader("select * from vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                                if (dr.Read())
                                {
                                    cboVehicleModel.Enabled = true;
                                    txtVehicleMake.Text = dr["Auto"].ToString();
                                    txtVehicleSize.Text = dr["Size"].ToString();
                                    txtVehicleColor.Text = dr["color"].ToString();
                                    txtPlateNumber.Text = dr["Platenumber"].ToString();
                                    cboCategory.Text = dr["Vehicle Category"].ToString();
                                    conn.CloseConnection();
                                }
                                else
                                {
                                    MessageBox.Show(vehicleID + " " + cusID);
                                }
                            }
                        }

                        else
                        {

                        }

                        con.Open();
                        SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + JEL.empID + "'", con);
                        SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                        DataTable employeeDT = new DataTable();
                        employeeDT.Load(employeeDR);
                        cboEstimatedBy.DataSource = employeeDT;
                        cboEstimatedBy.DisplayMember = "Name";
                        cboEstimatedBy.ValueMember = "ID";
                        con.Close();

                        
                        lblDocumentNumber.Text = JEL.DocNum;
                        dtpDateCreated.Text = JEL.Date;

                        conn.OpenConnection();
                        dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from job_estimate_view where [Job Estimate Number] = '" + JEL.DocNum + "' ");
                        conn.CloseConnection();


                        conn.OpenConnection();
                        SqlDataReader drTotal = conn.DataReader("select format(SUM(Price), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from job_estimate_view where [Job Estimate Number] = '"+JEL.DocNum+"'");
                        if(drTotal.Read())
                        {
                            lblTaxValue.Text = drTotal["Tax_total"].ToString();
                            lblTotalItem.Text = drTotal["Price_total"].ToString();
                            lblTotalPaymentDue.Text = drTotal["Total_amount"].ToString();
                        }
                        conn.CloseConnection();
                    }
                    else
                    {

                    }
                }
            } 
            catch (Exception ex){
                MessageBox.Show(ex.Message);

            }finally
            {
                newFormDialog.Dispose();
            }
            
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            nEmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());
            /*nTechnician = Int32.Parse(cboTechnicianName.SelectedValue.ToString());*/

            conn.OpenConnection();
            SqlDataReader drDraft = conn.DataReader("select * from draft_estimate");

            if (drDraft.Read() && cusID != 0 && nEmployeeID != 0 && txtCustomerName.Text != "" && vehicleID != 0 || rdoWithMaterial.Checked || rdoWithoutMaterial.Checked)
            {
                conn.CloseConnection();
                double nTotalPaymentDue = double.Parse(TotalPaymentDue);
                conn.OpenConnection();
                conn.ExecuteQueries("insert into job_estimate (job_estimate_number, customer_id, car_id, [date], estimated_by, status_id, total_price)" +
                    " values" +
                    " ('" + nDocumentNumber + "'" +
                    ", '" + cusID + "'" +
                    ", '" + vehicleID + "'" +
                    ", '" + nDateCreated + "'" +
                    ", '" + nEmployeeID + "'" +
                    ", '" + 5 + "'" +
                    ", '" + nTotalPaymentDue + "')");
                conn.CloseConnection();
                conn.OpenConnection();
                conn.ExecuteQueries("Insert into job_estimate_body (job_estimate_number,inventory_code,price,quantity,tax_id,tax_price,uom_id,total_price, bom_code) Select job_estimate_number,inventory_code,price,quantity,tax_id,tax_price,uom_id,total_price,bom_code from draft_estimate");
                conn.ExecuteQueries("Truncate table draft_estimate");
                conn.CloseConnection();
                MessageBox.Show("Job estimation saved", "Sucessfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please Fill up all fields");
            }
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCustomerName_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void rdoWithoutMaterial_CheckedChanged(object sender, EventArgs e)
        {
            dgvItemlistrefresh();
        }

        private void rdoWithMaterial_CheckedChanged(object sender, EventArgs e)
        {
            dgvItemlistrefresh();
        }

        private void cboVehicleModel_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCustomerName_Click_1(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
