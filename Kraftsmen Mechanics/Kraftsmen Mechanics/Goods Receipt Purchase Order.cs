﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Goods_Receipt_Purchase_Order : Form
    {
        public static string uom = "";

        public string dateCreated, datePosted, dateUpdated, createdBy, updatedBy, docNum, grpo_docno;
        public int xdocNum;

        private string code, created, updated, po_doc_num;
        private int contactPerson, status, doc_Num;
        private double totalAmount;

        public Goods_Receipt_Purchase_Order()
        {
            InitializeComponent();
        }

        Connection grpoCon = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");



        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            grpoCon.OpenConn();
            dgvItemList.DataSource = grpoCon.ShowDataDGV("Select * From Item_List where [Product Code]  like '%" + txtSearch.Text + "%' or [Product Name] like '%" + txtSearch.Text + "%'");
            grpoCon.CloseConn();
        }
        #region trash
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        #endregion

        private void Goods_Receipt_Purchase_Order_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null)
            {

            }
            else
            {
                DialogResult msg = MessageBox.Show("Closing this window will lose all your unsave data.\n Are you sure you want to close this window?", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (msg == DialogResult.OK)
                {
                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("truncate table dbo.draft_grpo");
                    grpoCon.CloseConn();
                }
                else if (msg == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSupplierCode_TextChanged(object sender, EventArgs e)
        {
            conn.Open();
            try
            {
                string supcode = txtSupplierCode.Text;

                SqlCommand cmdCode = new SqlCommand("select * from dbo.contact_person where supplier_code = '" + supcode + "'", conn);
                SqlDataReader drCode = cmdCode.ExecuteReader();
                DataTable dtCode = new DataTable();
                dtCode.Load(drCode);
                cboContactPerson.DataSource = dtCode;
                cboContactPerson.DisplayMember = "name";
                cboContactPerson.ValueMember = "contact_person_id";
                cmdCode.ExecuteNonQuery();

            }
            catch (Exception)
            {

            }
            conn.Close();
        }

        private void dgvItemList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtSupplierCode.Text == "" || txtSupplierCode.Text == null || txtSupplierName.Text == "" || txtSupplierName.Text == null || txtSupplierContact.Text == "" || txtSupplierContact.Text == null)
            {
                MessageBox.Show("Please fill up the details first.", "Alert!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Form newFormDialog = new Form();
                try
                {
                    using (GRPO_Item_Details grpo_ItmList = new GRPO_Item_Details())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        grpo_ItmList.Owner = newFormDialog;

                        uom = this.dgvItemList.CurrentRow.Cells[5].Value.ToString();
                        grpo_ItmList.txtProductCode.Text = this.dgvItemList.CurrentRow.Cells[0].Value.ToString();
                        grpo_ItmList.txtProductName.Text = this.dgvItemList.CurrentRow.Cells[1].Value.ToString();
                        grpo_ItmList.txtProductDescription.Text = this.dgvItemList.CurrentRow.Cells[2].Value.ToString();
                        grpo_ItmList.txtProductPrice.Text = this.dgvItemList.CurrentRow.Cells[4].Value.ToString();
                        grpo_ItmList.lblhidden.Text = txtDocumentNumber.Text;

                        if (grpo_ItmList.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            grpoCon.OpenConn();
                            dgvOrderList.DataSource = grpoCon.ShowDataDGV("Select * from draft_GRPO_view");
                            grpoCon.CloseConn();

                            lblTotalBeforeDiscount.Text = grpo_ItmList.T_Price;
                            lblTaxValue.Text = grpo_ItmList.T_Tax;
                            lblTotalPaymentDue.Text = grpo_ItmList.T_Value;
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }  
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cboContactPerson.Text == "" || cboContactPerson.Text == null)
            {
                contactPerson = Int32.Parse(0.ToString());
            }
            else
            { 
                contactPerson = Int32.Parse(cboContactPerson.SelectedValue.ToString());
            }

            totalAmount = double.Parse(lblTotalPaymentDue.Text);
            po_doc_num = lblPo_Doc_Num.Text;

            grpoCon.OpenConn();
            SqlDataReader drDraft = grpoCon.DataReader("select * from draft_grpo");

            

                if ((lblPo_Doc_Num.Text == "" || lblPo_Doc_Num.Text == null))
                {

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into grpo (grpo_doc_num, supplier_code, contact_person_id, date_posted, date_created, date_upload, employee_id, status_id, total_price)" +
                        "Values('" + txtDocumentNumber.Text + "', '" + txtSupplierCode.Text + "', '" + contactPerson + "', '" + datePosted + "', '" + dateCreated + "', '" + dateUpdated + "', '" + 0 + "', '" + 5 + "', '" + totalAmount + "' )");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into grpo_body (grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price)" +
                        "select grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price from draft_grpo");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("Update inventory set quantity  = a.quantity + b.quantity from inventory a left join draft_grpo b on a.inventory_code = b.inventory_code where b.grpo_doc_num = '" + txtDocumentNumber.Text + "'");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into stock_in " +
                        "select a.inventory_code, b.quantity, '" + dateCreated + "', a.quantity, a.grpo_doc_num from draft_grpo a left join inventory b on a.inventory_code = b.inventory_code ");
                    grpoCon.CloseConn();

                    MessageBox.Show("Goods Receipt Purchase Order Saved!", "Save Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("truncate table draft_grpo");
                    grpoCon.CloseConn();

                    txtSupplierCode.Text = "";

                    this.Close();
                }

                else if ((txtSupplierCode.Text != "" || txtSupplierCode.Text != null || txtSupplierName.Text == "" || txtSupplierName.Text == null || txtSupplierContact.Text == "" || txtSupplierContact.Text == null) && drDraft.Read())
                {
                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into grpo (grpo_doc_num, po_document_number, supplier_code, contact_person_id, date_posted, date_created, date_upload, employee_id, status_id, total_price)" +
                        "Values('" + txtDocumentNumber.Text + "', '" + po_doc_num + "', '" + txtSupplierCode.Text + "', '" + contactPerson + "', '" + datePosted + "', '" + dateCreated + "', '" + dateUpdated + "', '" + 0 + "', '" + 5 + "', '" + totalAmount + "' )");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into grpo_body (grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price)" +
                        "select grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price from draft_grpo");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("update inventory set quantity  = a.quantity + b.quantity from inventory a left join draft_grpo b on a.inventory_code = b.inventory_code where b.grpo_doc_num = '" + txtDocumentNumber.Text + "'");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into stock_in " +
                        "select a.inventory_code, b.quantity, '" + dateCreated + "', a.quantity, a.grpo_doc_num from draft_grpo a left join inventory b on a.inventory_code = b.inventory_code ");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("Update po set status_id = '" + 6 + "' where po_document_number = '" + po_doc_num + "'");
                    grpoCon.CloseConn();

                    MessageBox.Show("Goods Receipt Purchase Order Saved!", "Save Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("truncate table draft_grpo");
                    grpoCon.CloseConn();

                    clear();
                    txtSupplierCode.Text = "";
                    this.Close();
                }
            
                else
                { 
                    MessageBox.Show("Please fill up all the fields above.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                grpoCon.CloseConn();
        }

        public void clear()
        {
            txtSupplierCode.Text = "";
            txtSupplierName.Text = "";
            txtSupplierAddress.Text = "";
            txtSupplierContact.Text = "";
            cboContactPerson.Text = "";
        }

        private void dgvOrderList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string itm_ID = this.dgvOrderList.CurrentRow.Cells[1].Value.ToString();
            string itm_name = this.dgvOrderList.CurrentRow.Cells[2].Value.ToString();
            if (dgvOrderList.Columns[e.ColumnIndex].Index == 0)
            {
                if (MessageBox.Show("Do you want to void " + itm_name + "?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("delete from draft_grpo where draft_grpo_body_id = '" + itm_ID + "'");
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    dgvOrderList.DataSource = grpoCon.ShowDataDGV("Select * from draft_GRPO_view");
                    grpoCon.CloseConn();
                    showTotal();
                }
            }
            else
            {
                Form newFormDialog = new Form();
                try
                {
                    using (GRPO_Item_Quantity grpo_itm_qty = new GRPO_Item_Quantity())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        grpo_itm_qty.Owner = newFormDialog;

                        grpo_itm_qty.txtProductCode.Text = this.dgvOrderList.CurrentRow.Cells[2].Value.ToString();
                        grpo_itm_qty.txtProductQuantity.Text = this.dgvOrderList.CurrentRow.Cells[5].Value.ToString();
                        grpo_itm_qty.lblhidden.Text = txtDocumentNumber.Text;
                        if (grpo_itm_qty.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            grpoCon.OpenConn();
                            dgvOrderList.DataSource = grpoCon.ShowDataDGV("Select * from draft_GRPO_view");
                            grpoCon.CloseConn();
                            showTotal();
                        }
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    newFormDialog.Dispose();
                }
            }
        }

        private void btnViewDocument_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (PO_List poList = new PO_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    poList.Owner = newFormDialog;

                    if (poList.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        lblPo_Doc_Num.Text = poList.DocNum;
                        lblPO_Status.Text = poList.Status;

                        grpoCon.OpenConn();
                        grpoCon.ExecuteQuery("truncate table draft_grpo");
                        grpoCon.ExecuteQuery("insert into draft_grpo (grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, discount, total_price)" +
                            "(select '" + txtDocumentNumber.Text + "', inventory_code, quantity, uom_id, price, tax_id, tax_price, discount, total_price from po_body where po_document_number = '" + lblPo_Doc_Num.Text + "')");
                        grpoCon.CloseConn();

                        txtSupplierCode.Text = poList.SupCode;
                        txtSupplierName.Text = poList.SupName;
                        cboContactPerson.Text = poList.SupPerson;
                        txtSupplierContact.Text = poList.SupContact;
                        txtSupplierAddress.Text = poList.SupAddress;

                        

                        /*dtpDateCreated.Text = poList.DateCreated;
                        dtpDatePosted.Text = poList.DatePosted;
                        dtpDateUploaded.Text = poList.DateUpdated;*/

                        txtCreatedBy.Text = poList.CreatedBy;

                        grpoCon.OpenConn();
                        dgvOrderList.DataSource = grpoCon.ShowDataDGV("select * from draft_grpo_view where '" + poList.DocNum + "' = '" + lblPo_Doc_Num.Text + "'");
                        grpoCon.CloseConn();

                        groupBox1.Enabled = false;
                        groupBox3.Enabled = true;

                        cboStatus.Enabled = false;
                        txtDocumentNumber.Enabled = false;
                        dtpDateCreated.Enabled = true;
                        dtpDatePosted.Enabled = true;
                        dtpDateUploaded.Enabled = true;

                        btnSave.Enabled = true;
                        txtSearch.Enabled = true;
                        dgvItemList.Enabled = true;

                        if(lblPO_Status.Text == "CLOSED")
                        {
                            btnSave.Enabled = false;
                            groupBox3.Enabled = false;
                        }



                    }
                    showTotal();
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnViewSupplier_Click(object sender, EventArgs e)
        {

            Form newFormDialog = new Form();

            try
            {
                using (Suppliers_List suplist = new Suppliers_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;

                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        txtSupplierCode.Text = suplist.SupCode;
                        txtSupplierName.Text = suplist.SupName;
                        txtSupplierAddress.Text = suplist.SupAdd;
                        txtSupplierContact.Text = suplist.SupCon;

                        cboContactPerson.Text = "";

                        groupBox3.Enabled = true;
                    }
                    groupBox1.Enabled = true;

                    txtSupplierCode.Enabled = false;
                    txtSupplierName.Enabled = false;
                    txtSupplierAddress.Enabled = false;
                    cboContactPerson.Enabled = false;
                    txtSupplierContact.Enabled = false;
                    cboContactPerson.Enabled = true;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        
        private void Goods_Receipt_Purchase_Order_Load(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmdStat = new SqlCommand("Select * from dbo.status where status_id between 5 AND 6", conn);
            SqlDataReader drStat = cmdStat.ExecuteReader();
            DataTable dtStat = new DataTable();
            dtStat.Load(drStat);
            cboStatus.DataSource = dtStat;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStat.ExecuteNonQuery();
            conn.Close();

            showItemList();

            dateCreated = dtpDateCreated.Value.ToString("yyyy-MM-dd");
            datePosted = dtpDatePosted.Value.ToString("yyyy-MM-dd");
            dateUpdated = dtpDateUploaded.Value.ToString("yyyy-MM-dd");

            code = txtSupplierCode.Text;

            grpoCon.OpenConn();
            dgvOrderList.DataSource = grpoCon.ShowDataDGV("select * from draft_grpo_view");
            grpoCon.CloseConn();

            grpoCon.OpenConn();

            SqlDataReader drHeader = grpoCon.DataReader("select MAX(grpo_doc_num) as grpo_docnum from grpo_body");
            drHeader.Read();
            if (drHeader["grpo_docnum"].ToString() != "")
            {
                docNum = drHeader["grpo_docnum"].ToString();
                xdocNum = Int32.Parse(docNum) + 1;

                grpo_docno = xdocNum.ToString().PadLeft(7,'0');
                txtDocumentNumber.Text = grpo_docno;
            }
            else
            {
                docNum = "0000001";
                grpo_docno = docNum;
                xdocNum = Int32.Parse(docNum);
                txtDocumentNumber.Text = grpo_docno;
            }

            grpoCon.CloseConn();

            groupBox3.Enabled = true;

            txtSupplierCode.Enabled = false;
            txtSupplierName.Enabled = false;
            txtSupplierAddress.Enabled = false;
            cboContactPerson.Enabled = false;
            txtSupplierContact.Enabled = false;
            cboContactPerson.Enabled = false;
        }
        public void showItemList()
        {
            grpoCon.OpenConn();
            dgvItemList.DataSource = grpoCon.ShowDataDGV("select * from Item_List");
            grpoCon.CloseConn();
        }
        public void showTotal()
        {
            //-----------------------------------------//
            grpoCon.OpenConn();

            SqlDataReader drTotal = grpoCon.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from draft_grpo where grpo_doc_num = '" + txtDocumentNumber.Text + "'");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            lblTotalBeforeDiscount.Text = total_price;
            lblTaxValue.Text = total_tax;
            lblTotalPaymentDue.Text = total_amount;
            grpoCon.CloseConn();
            //--------------------------------------------------------------------//
        }
    }
}
