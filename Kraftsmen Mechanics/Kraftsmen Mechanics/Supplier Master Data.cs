﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Supplier_Master_Data : Form
    {
        public int supCode;
        public Supplier_Master_Data()
        {
            InitializeComponent();
        }

        Connection supMasData = new Connection();


        private void Supplier_Master_Data_Load(object sender, EventArgs e)
        {
            showDgvList();
            showContactPerson();
        }

        private void showDgvList()
        {
            supMasData.OpenConn();
            dgvSupplierList.DataSource = supMasData.ShowDataDGV("select * from supplier ");
            supMasData.CloseConn();
        }

        private void showContactPerson()
        {
            supMasData.OpenConn();
            dgvContactPersonList.DataSource = supMasData.ShowDataDGV("select * from Contact_Person_Info where [Supplier Code] = '" + txtContactPersonSupplier.Text + "'");
            supMasData.CloseConn();
        }
        private void Clear()
        {
            txtSupCode.Text = "";
            txtSupName.Text = "";
            txtSupConNo.Text = "";
            txtSupplierEmail.Text = "";
            txtSupAdd.Text = "";
            cboSupplierCity.Text = "";
            txtSupPostalCode.Text = "";
        }

        private void clearContact()
        {
            txtContactPersonSupplier.Text = "";
            txtContactPersonName.Text = "";
            txtContactPersonNumber.Text = "";
            txtContactPersonEmail.Text = "";
            txtContactPersonAddress.Text = "";
            cboContactPersonCity.Text = "";
            txtContactPersonPostalCode.Text = "";
        }
        private void txtSupCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSupplierSearch_TextChanged(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            dgvSupplierList.DataSource = supMasData.ShowDataDGV("Select * from supplier where [supplier_code] like '%" + txtSearchSupplier.Text + "%' or [Name] like '%" + txtSearchSupplier.Text + "%'");
            supMasData.CloseConn();
        }
        #region dgv click event
        private void dgvSupplierList_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvSupplierList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvSupplierList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvSupplierList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvSupplierList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dgvSupplierList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dgvSupplierList_MouseClick(object sender, MouseEventArgs e)
        {

        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {

        }

        private void btnName_Click(object sender, EventArgs e)
        {

        }

        private void btnViewSupplierList_Click(object sender, EventArgs e)
        {

        }

        private void txtSearchSupplier_TextChanged(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            dgvSupplierList.DataSource = supMasData.ShowDataDGV("Select * from supplier where [supplier_code] like '%" + txtSearchSupplier.Text + "%' or [Name] like '%" + txtSearchSupplier.Text + "%'");
            supMasData.CloseConn();
        }

        private void btnSaveSupplier_Click(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            if (txtSupCode.Text == "" || txtSupName.Text == "" || txtSupConNo.Text == "" || txtSupConNo.Text == "" || txtSupAdd.Text == "" || txtContactPersonSupplier.Text == "" || txtContactPersonName.Text == "")
            {
                MessageBox.Show("Please fill up the required field first.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                supMasData.ExecuteQuery("Insert into dbo.supplier(supplier_code, name, contact_number, address, email_add, city, postal_code)" +
                    "Values('" + txtSupCode.Text + "' , '" + txtSupName.Text + "', '" + txtSupConNo.Text + "', '" + txtSupAdd.Text + "', '" + txtSupplierEmail.Text + "' , '" + cboSupplierCity.Text + "' , '" + txtSupPostalCode.Text + "')");
                supMasData.CloseConn();

                MessageBox.Show("Data Saved!", "Save!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                showDgvList();
            }

        }

        private void btnViewSupplierList_Click_1(object sender, EventArgs e)
        {
            Suppliers_List supList = new Suppliers_List();

            if (supList.ShowDialog() != DialogResult.OK)
            {
                txtContactPersonSupplier.Text = supList.SupCode;
                txtContactPersonName.Text = "";
                txtContactPersonNumber.Text = "";
                txtContactPersonEmail.Text = "";
                txtContactPersonAddress.Text = "";
                txtContactPersonPostalCode.Text = "";
                cboContactPersonCity.Text = "";
            }
        }

        private void dgvSupplierList_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtSupCode.Text = dgvSupplierList.SelectedRows[0].Cells[1].Value.ToString();
                txtSupName.Text = dgvSupplierList.SelectedRows[0].Cells[2].Value.ToString();
                txtSupConNo.Text = dgvSupplierList.SelectedRows[0].Cells[7].Value.ToString();
                txtSupAdd.Text = dgvSupplierList.SelectedRows[0].Cells[4].Value.ToString();
                cboSupplierCity.Text = dgvSupplierList.SelectedRows[0].Cells[5].Value.ToString();
                txtSupPostalCode.Text = dgvSupplierList.SelectedRows[0].Cells[6].Value.ToString();
                txtSupplierEmail.Text = dgvSupplierList.SelectedRows[0].Cells[3].Value.ToString();
            }
        }

        private void dgvSupplierList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            //supCode = this.dgvSupplierList.CurrentRow.Cells[1].Value.ToString();
            //supName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            //supAdd = this.dgvSupplierList.CurrentRow.Cells[4].Value.ToString();
            //supCon = this.dgvSupplierList.CurrentRow.Cells[7].Value.ToString();

            //this.Close();
        }

        private void btnUpdateSupplier_Click(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            if (txtSupCode.Text == "" || txtSupName.Text == "" || txtSupConNo.Text == "" || txtSupConNo.Text == "" || txtSupAdd.Text == "")
            {
                MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                supMasData.ExecuteQuery("Update supplier set supplier_code = '" + txtSupCode.Text + "' , name = '" + txtSupName.Text + "' , email_add = '" + txtSupAdd.Text + "' , address = '" + txtSupAdd.Text + "', city = '" + cboSupplierCity.Text + "' , postal_code = '" + txtSupPostalCode.Text + "', contact_number = '" + txtSupConNo.Text + "' where supplier_code = '" + txtSupCode.Text + "'");
                supMasData.CloseConn();

                MessageBox.Show("Data Updated!", "Updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                showDgvList();
            }
        }

        private void dgvContactPersonList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txtContactPersonSupplier.Text = dgvContactPersonList.SelectedRows[0].Cells[0].Value.ToString();
                txtContactPersonName.Text = dgvContactPersonList.SelectedRows[0].Cells[2].Value.ToString();
                txtContactPersonNumber.Text = dgvContactPersonList.SelectedRows[0].Cells[3].Value.ToString();
                txtContactPersonEmail.Text = dgvContactPersonList.SelectedRows[0].Cells[4].Value.ToString();
                txtContactPersonAddress.Text = dgvContactPersonList.SelectedRows[0].Cells[5].Value.ToString();
                cboContactPersonCity.Text = dgvContactPersonList.SelectedRows[0].Cells[6].Value.ToString();
                txtContactPersonPostalCode.Text = dgvContactPersonList.SelectedRows[0].Cells[7].Value.ToString();
                txtHidden.Text = dgvContactPersonList.SelectedRows[0].Cells[8].Value.ToString();
            }
        }

        private void txtContactPersonSupplier_TextChanged(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            dgvContactPersonList.DataSource = supMasData.ShowDataDGV("select * from Contact_Person_Info where [Supplier Code] = '" + txtContactPersonSupplier.Text + "'");
            supMasData.CloseConn();
        }

        private void btnSaveContactPerson_Click(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            if (txtContactPersonSupplier.Text == "" || txtContactPersonName.Text == "" || txtContactPersonNumber.Text == "" || txtContactPersonAddress.Text == "")
            {
                MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                supMasData.ExecuteQuery("insert into contact_person(name, address, contact_number, city, postal_code, email, supplier_code)" +
                    "VALUES('" + txtContactPersonName.Text + "' , '" + txtContactPersonAddress.Text + "' , '" + txtContactPersonNumber.Text + "' , '" + cboContactPersonCity.Text + "' , '" + txtContactPersonPostalCode.Text + "', '" + txtContactPersonEmail.Text + "', '" + txtContactPersonSupplier.Text + "')");
                
                supMasData.CloseConn();
                MessageBox.Show("Data Saved!", "Save!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clearContact();
                showContactPerson();
            }
        }

        private void btnUpdateContactPerson_Click(object sender, EventArgs e)
        {
            supMasData.OpenConn();
            if (txtContactPersonSupplier.Text == "" || txtContactPersonName.Text == "" || txtContactPersonNumber.Text == "" || txtContactPersonAddress.Text == "")
            {
                MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                supMasData.ExecuteQuery("Update contact_person set name = '" + txtContactPersonName.Text + "' , address = '" + txtContactPersonAddress.Text + "' , contact_number = '" + txtContactPersonNumber.Text + "' , city = '" + cboContactPersonCity.Text + "', postal_code = '" + txtContactPersonPostalCode.Text + "' , email = '" + txtContactPersonEmail.Text + "', supplier_code = '" + txtContactPersonSupplier.Text + "' where [contact_person_id] = '"+txtHidden.Text+"'");
                supMasData.CloseConn();

                MessageBox.Show("Data Updated!", "Updated!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clearContact();
                showContactPerson();
            }
        }
    }
}
