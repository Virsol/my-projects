﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Customer_List : Form
    {
        DBConnection dbcon = new DBConnection();
        public int nCustomerID;
        private string cusName, cusNo, cusAddress,cusPostal, cusCity, sSearch;
        private int cusID;

        public string CusName
        {
            get { return cusName; }
            set { cusName = value; }
        }
        public string CusNo
        {
            get { return cusNo; }
            set { cusNo = value; }
        }

        public int CusID
        {
            get { return cusID; }
            set { cusID = value; }
        }
        public string CusAddress
        {
            get { return cusAddress; }
            set { cusAddress = value; }
        }
        public string CusCity
        {
            get { return cusCity; }
            set { cusCity = value; }
        }
        public string CusPortal
        {
            get { return cusPostal; }
            set { cusPostal = value; }
        }
        public Customer_List()
        {
            InitializeComponent();
        }

        private void dgvSupplierList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (lblreport.Text == "Sales Report")
            {
                PO_Report.custName_Report = this.dgvCustomerList.CurrentRow.Cells[1].Value.ToString();
            }
            else
            {
                cusID = Int32.Parse(this.dgvCustomerList.CurrentRow.Cells[0].Value.ToString());

                dbcon.OpenConnection();
                SqlDataReader dr = dbcon.DataReader("select * from customer where customer_id = '" + cusID + "'");
                if (dr.Read())
                {
                    cusName = dr["name"].ToString();
                    cusCity = dr["city"].ToString();
                    cusPostal = dr["postal_code"].ToString();
                    cusNo = dr["number"].ToString();
                    CusAddress = dr["address"].ToString();
                }
                dbcon.CloseConnection();
            }


            this.Close();

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Customer_List_Load(object sender, EventArgs e)
        {
            refreshDgvCusList();
        }

        public void dgvCusList()
        {
            dbcon.OpenConnection();
            dgvCustomerList.DataSource = dbcon.ShowDataInGridView("select * from dbo.Customer_list");
            dbcon.CloseConnection();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            sSearch = txtSearch.Text;
            refreshDgvCusList();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void refreshDgvCusList()
        {
            dbcon.OpenConnection();
            dgvCustomerList.DataSource = dbcon.ShowDataInGridView("select * from Customer_list where [name] like '%"+sSearch+"%'");
            dbcon.CloseConnection();
        }
        private void btnAddNewCustomer_Click(object sender, EventArgs e)
        {

            Form newFormDialog = new Form();
            try
            {
                using(Add_New_Customer newCus = new Add_New_Customer())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    newCus.Owner = newFormDialog;

                    newCus.ShowDialog();

                    newFormDialog.Dispose();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            dgvCusList();
        }

        private void dgvCustomerList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            /*cusName = this.dgvCustomerList.CurrentRow.Cells[1].Value.ToString();
            cusNo = this.dgvCustomerList.CurrentRow.Cells[4].Value.ToString();
            cusID = Int32.Parse(this.dgvCustomerList.CurrentRow.Cells[0].Value.ToString());
            this.Close();*/
        }
    }
}
