﻿
namespace Kraftsmen_Mechanics
{
    partial class Purchasing_List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Purchasing_List));
            this.dgvPurchasingOrderList = new System.Windows.Forms.DataGridView();
            this.APInvoice = new System.Windows.Forms.TabPage();
            this.cboSearchByAPInvoiceStatus = new System.Windows.Forms.ComboBox();
            this.txtSearchAPInvoiceList = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvAPInvoiceList = new System.Windows.Forms.DataGridView();
            this.GRPO = new System.Windows.Forms.TabPage();
            this.cboSearchByGRPOStatus = new System.Windows.Forms.ComboBox();
            this.txtSearchGRPOList = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvGRPOList = new System.Windows.Forms.DataGridView();
            this.tctrlSalesReports = new System.Windows.Forms.TabControl();
            this.PO = new System.Windows.Forms.TabPage();
            this.txtSearchPurchasingList = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OutgoingPayment = new System.Windows.Forms.TabPage();
            this.txtSearchOutgoingPayment = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvOutgoingPaymentList = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.cboSearchByPurchasingStatus = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpOPTo = new System.Windows.Forms.DateTimePicker();
            this.dtpOPFrom = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpInvoiceTo = new System.Windows.Forms.DateTimePicker();
            this.dtpInvoiceFrom = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpGRPOTo = new System.Windows.Forms.DateTimePicker();
            this.dtpGRPOFrom = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpPOTo = new System.Windows.Forms.DateTimePicker();
            this.dtpPOFrom = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchasingOrderList)).BeginInit();
            this.APInvoice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAPInvoiceList)).BeginInit();
            this.GRPO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRPOList)).BeginInit();
            this.tctrlSalesReports.SuspendLayout();
            this.PO.SuspendLayout();
            this.OutgoingPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutgoingPaymentList)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPurchasingOrderList
            // 
            this.dgvPurchasingOrderList.AllowUserToAddRows = false;
            this.dgvPurchasingOrderList.AllowUserToDeleteRows = false;
            this.dgvPurchasingOrderList.AllowUserToResizeColumns = false;
            this.dgvPurchasingOrderList.AllowUserToResizeRows = false;
            this.dgvPurchasingOrderList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPurchasingOrderList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPurchasingOrderList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPurchasingOrderList.BackgroundColor = System.Drawing.Color.White;
            this.dgvPurchasingOrderList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchasingOrderList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvPurchasingOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPurchasingOrderList.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvPurchasingOrderList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvPurchasingOrderList.Location = new System.Drawing.Point(9, 74);
            this.dgvPurchasingOrderList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvPurchasingOrderList.MultiSelect = false;
            this.dgvPurchasingOrderList.Name = "dgvPurchasingOrderList";
            this.dgvPurchasingOrderList.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPurchasingOrderList.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvPurchasingOrderList.RowHeadersWidth = 51;
            this.dgvPurchasingOrderList.RowTemplate.Height = 24;
            this.dgvPurchasingOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPurchasingOrderList.Size = new System.Drawing.Size(1341, 586);
            this.dgvPurchasingOrderList.TabIndex = 61;
            this.dgvPurchasingOrderList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPurchasingOrderList_CellDoubleClick);
            // 
            // APInvoice
            // 
            this.APInvoice.BackColor = System.Drawing.Color.White;
            this.APInvoice.Controls.Add(this.label7);
            this.APInvoice.Controls.Add(this.dtpInvoiceTo);
            this.APInvoice.Controls.Add(this.dtpInvoiceFrom);
            this.APInvoice.Controls.Add(this.label8);
            this.APInvoice.Controls.Add(this.cboSearchByAPInvoiceStatus);
            this.APInvoice.Controls.Add(this.txtSearchAPInvoiceList);
            this.APInvoice.Controls.Add(this.label3);
            this.APInvoice.Controls.Add(this.dgvAPInvoiceList);
            this.APInvoice.Location = new System.Drawing.Point(4, 34);
            this.APInvoice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.APInvoice.Name = "APInvoice";
            this.APInvoice.Size = new System.Drawing.Size(1363, 719);
            this.APInvoice.TabIndex = 2;
            this.APInvoice.Text = "A/P Invoice";
            // 
            // cboSearchByAPInvoiceStatus
            // 
            this.cboSearchByAPInvoiceStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchByAPInvoiceStatus.FormattingEnabled = true;
            this.cboSearchByAPInvoiceStatus.Items.AddRange(new object[] {
            "OPEN",
            "CLOSED"});
            this.cboSearchByAPInvoiceStatus.Location = new System.Drawing.Point(87, 27);
            this.cboSearchByAPInvoiceStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboSearchByAPInvoiceStatus.Name = "cboSearchByAPInvoiceStatus";
            this.cboSearchByAPInvoiceStatus.Size = new System.Drawing.Size(160, 26);
            this.cboSearchByAPInvoiceStatus.TabIndex = 72;
            this.cboSearchByAPInvoiceStatus.SelectedIndexChanged += new System.EventHandler(this.cboSearchByAPInvoiceStatus_SelectedIndexChanged);
            // 
            // txtSearchAPInvoiceList
            // 
            this.txtSearchAPInvoiceList.Location = new System.Drawing.Point(256, 28);
            this.txtSearchAPInvoiceList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSearchAPInvoiceList.Name = "txtSearchAPInvoiceList";
            this.txtSearchAPInvoiceList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchAPInvoiceList.TabIndex = 71;
            this.txtSearchAPInvoiceList.TextChanged += new System.EventHandler(this.txtSearchAPInvoiceList_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 70;
            this.label3.Text = "Search:";
            // 
            // dgvAPInvoiceList
            // 
            this.dgvAPInvoiceList.AllowUserToAddRows = false;
            this.dgvAPInvoiceList.AllowUserToDeleteRows = false;
            this.dgvAPInvoiceList.AllowUserToResizeColumns = false;
            this.dgvAPInvoiceList.AllowUserToResizeRows = false;
            this.dgvAPInvoiceList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAPInvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAPInvoiceList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAPInvoiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvAPInvoiceList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAPInvoiceList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvAPInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAPInvoiceList.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvAPInvoiceList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvAPInvoiceList.Location = new System.Drawing.Point(9, 74);
            this.dgvAPInvoiceList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvAPInvoiceList.MultiSelect = false;
            this.dgvAPInvoiceList.Name = "dgvAPInvoiceList";
            this.dgvAPInvoiceList.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAPInvoiceList.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvAPInvoiceList.RowHeadersWidth = 51;
            this.dgvAPInvoiceList.RowTemplate.Height = 24;
            this.dgvAPInvoiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAPInvoiceList.Size = new System.Drawing.Size(1341, 586);
            this.dgvAPInvoiceList.TabIndex = 69;
            this.dgvAPInvoiceList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAPInvoiceList_CellDoubleClick);
            // 
            // GRPO
            // 
            this.GRPO.BackColor = System.Drawing.Color.White;
            this.GRPO.Controls.Add(this.label9);
            this.GRPO.Controls.Add(this.dtpGRPOTo);
            this.GRPO.Controls.Add(this.dtpGRPOFrom);
            this.GRPO.Controls.Add(this.label10);
            this.GRPO.Controls.Add(this.cboSearchByGRPOStatus);
            this.GRPO.Controls.Add(this.txtSearchGRPOList);
            this.GRPO.Controls.Add(this.label2);
            this.GRPO.Controls.Add(this.dgvGRPOList);
            this.GRPO.ForeColor = System.Drawing.Color.Black;
            this.GRPO.Location = new System.Drawing.Point(4, 34);
            this.GRPO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GRPO.Name = "GRPO";
            this.GRPO.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GRPO.Size = new System.Drawing.Size(1363, 719);
            this.GRPO.TabIndex = 1;
            this.GRPO.Text = "Goods Receipt Purchasing Order";
            // 
            // cboSearchByGRPOStatus
            // 
            this.cboSearchByGRPOStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchByGRPOStatus.FormattingEnabled = true;
            this.cboSearchByGRPOStatus.Items.AddRange(new object[] {
            "OPEN",
            "CLOSED"});
            this.cboSearchByGRPOStatus.Location = new System.Drawing.Point(87, 27);
            this.cboSearchByGRPOStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboSearchByGRPOStatus.Name = "cboSearchByGRPOStatus";
            this.cboSearchByGRPOStatus.Size = new System.Drawing.Size(160, 26);
            this.cboSearchByGRPOStatus.TabIndex = 68;
            this.cboSearchByGRPOStatus.SelectedIndexChanged += new System.EventHandler(this.cboSearchByGRPOStatus_SelectedIndexChanged);
            // 
            // txtSearchGRPOList
            // 
            this.txtSearchGRPOList.Location = new System.Drawing.Point(256, 28);
            this.txtSearchGRPOList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSearchGRPOList.Name = "txtSearchGRPOList";
            this.txtSearchGRPOList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchGRPOList.TabIndex = 67;
            this.txtSearchGRPOList.TextChanged += new System.EventHandler(this.txtSearchGRPOList_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 66;
            this.label2.Text = "Search:";
            // 
            // dgvGRPOList
            // 
            this.dgvGRPOList.AllowUserToAddRows = false;
            this.dgvGRPOList.AllowUserToDeleteRows = false;
            this.dgvGRPOList.AllowUserToResizeColumns = false;
            this.dgvGRPOList.AllowUserToResizeRows = false;
            this.dgvGRPOList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGRPOList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGRPOList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvGRPOList.BackgroundColor = System.Drawing.Color.White;
            this.dgvGRPOList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGRPOList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvGRPOList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGRPOList.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgvGRPOList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvGRPOList.Location = new System.Drawing.Point(9, 74);
            this.dgvGRPOList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvGRPOList.MultiSelect = false;
            this.dgvGRPOList.Name = "dgvGRPOList";
            this.dgvGRPOList.ReadOnly = true;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGRPOList.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvGRPOList.RowHeadersWidth = 51;
            this.dgvGRPOList.RowTemplate.Height = 24;
            this.dgvGRPOList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGRPOList.Size = new System.Drawing.Size(1341, 586);
            this.dgvGRPOList.TabIndex = 65;
            this.dgvGRPOList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGRPOList_CellDoubleClick);
            // 
            // tctrlSalesReports
            // 
            this.tctrlSalesReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctrlSalesReports.Controls.Add(this.PO);
            this.tctrlSalesReports.Controls.Add(this.GRPO);
            this.tctrlSalesReports.Controls.Add(this.APInvoice);
            this.tctrlSalesReports.Controls.Add(this.OutgoingPayment);
            this.tctrlSalesReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tctrlSalesReports.ItemSize = new System.Drawing.Size(80, 30);
            this.tctrlSalesReports.Location = new System.Drawing.Point(0, 52);
            this.tctrlSalesReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tctrlSalesReports.Name = "tctrlSalesReports";
            this.tctrlSalesReports.Padding = new System.Drawing.Point(25, 3);
            this.tctrlSalesReports.SelectedIndex = 0;
            this.tctrlSalesReports.Size = new System.Drawing.Size(1371, 757);
            this.tctrlSalesReports.TabIndex = 66;
            // 
            // PO
            // 
            this.PO.BackColor = System.Drawing.Color.White;
            this.PO.Controls.Add(this.label11);
            this.PO.Controls.Add(this.dtpPOTo);
            this.PO.Controls.Add(this.dtpPOFrom);
            this.PO.Controls.Add(this.label12);
            this.PO.Controls.Add(this.cboSearchByPurchasingStatus);
            this.PO.Controls.Add(this.txtSearchPurchasingList);
            this.PO.Controls.Add(this.label1);
            this.PO.Controls.Add(this.dgvPurchasingOrderList);
            this.PO.ForeColor = System.Drawing.Color.Black;
            this.PO.Location = new System.Drawing.Point(4, 34);
            this.PO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PO.Name = "PO";
            this.PO.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.PO.Size = new System.Drawing.Size(1363, 719);
            this.PO.TabIndex = 0;
            this.PO.Text = "Purchasing Order";
            // 
            // txtSearchPurchasingList
            // 
            this.txtSearchPurchasingList.Location = new System.Drawing.Point(256, 28);
            this.txtSearchPurchasingList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSearchPurchasingList.Name = "txtSearchPurchasingList";
            this.txtSearchPurchasingList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchPurchasingList.TabIndex = 63;
            this.txtSearchPurchasingList.TextChanged += new System.EventHandler(this.txtSearchPurchasingList_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 62;
            this.label1.Text = "Search:";
            // 
            // OutgoingPayment
            // 
            this.OutgoingPayment.Controls.Add(this.label5);
            this.OutgoingPayment.Controls.Add(this.dtpOPTo);
            this.OutgoingPayment.Controls.Add(this.dtpOPFrom);
            this.OutgoingPayment.Controls.Add(this.label6);
            this.OutgoingPayment.Controls.Add(this.txtSearchOutgoingPayment);
            this.OutgoingPayment.Controls.Add(this.label4);
            this.OutgoingPayment.Controls.Add(this.dgvOutgoingPaymentList);
            this.OutgoingPayment.Location = new System.Drawing.Point(4, 34);
            this.OutgoingPayment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OutgoingPayment.Name = "OutgoingPayment";
            this.OutgoingPayment.Size = new System.Drawing.Size(1363, 719);
            this.OutgoingPayment.TabIndex = 3;
            this.OutgoingPayment.Text = "Outgoing Payments";
            this.OutgoingPayment.UseVisualStyleBackColor = true;
            // 
            // txtSearchOutgoingPayment
            // 
            this.txtSearchOutgoingPayment.Location = new System.Drawing.Point(80, 28);
            this.txtSearchOutgoingPayment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSearchOutgoingPayment.Name = "txtSearchOutgoingPayment";
            this.txtSearchOutgoingPayment.Size = new System.Drawing.Size(413, 24);
            this.txtSearchOutgoingPayment.TabIndex = 75;
            this.txtSearchOutgoingPayment.TextChanged += new System.EventHandler(this.txtSearchOutgoingPayment_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 18);
            this.label4.TabIndex = 74;
            this.label4.Text = "Search:";
            // 
            // dgvOutgoingPaymentList
            // 
            this.dgvOutgoingPaymentList.AllowUserToAddRows = false;
            this.dgvOutgoingPaymentList.AllowUserToDeleteRows = false;
            this.dgvOutgoingPaymentList.AllowUserToResizeColumns = false;
            this.dgvOutgoingPaymentList.AllowUserToResizeRows = false;
            this.dgvOutgoingPaymentList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOutgoingPaymentList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOutgoingPaymentList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvOutgoingPaymentList.BackgroundColor = System.Drawing.Color.White;
            this.dgvOutgoingPaymentList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOutgoingPaymentList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvOutgoingPaymentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvOutgoingPaymentList.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgvOutgoingPaymentList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvOutgoingPaymentList.Location = new System.Drawing.Point(9, 74);
            this.dgvOutgoingPaymentList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvOutgoingPaymentList.MultiSelect = false;
            this.dgvOutgoingPaymentList.Name = "dgvOutgoingPaymentList";
            this.dgvOutgoingPaymentList.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOutgoingPaymentList.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvOutgoingPaymentList.RowHeadersWidth = 51;
            this.dgvOutgoingPaymentList.RowTemplate.Height = 24;
            this.dgvOutgoingPaymentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOutgoingPaymentList.Size = new System.Drawing.Size(1341, 586);
            this.dgvOutgoingPaymentList.TabIndex = 73;
            this.dgvOutgoingPaymentList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOutgoingPaymentList_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1371, 52);
            this.panel1.TabIndex = 63;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 25);
            this.label17.TabIndex = 24;
            this.label17.Text = "Purchasing List";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(1323, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 31);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cboSearchByPurchasingStatus
            // 
            this.cboSearchByPurchasingStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchByPurchasingStatus.FormattingEnabled = true;
            this.cboSearchByPurchasingStatus.Items.AddRange(new object[] {
            "OPEN",
            "CLOSED"});
            this.cboSearchByPurchasingStatus.Location = new System.Drawing.Point(87, 27);
            this.cboSearchByPurchasingStatus.Margin = new System.Windows.Forms.Padding(4);
            this.cboSearchByPurchasingStatus.Name = "cboSearchByPurchasingStatus";
            this.cboSearchByPurchasingStatus.Size = new System.Drawing.Size(160, 26);
            this.cboSearchByPurchasingStatus.TabIndex = 64;
            this.cboSearchByPurchasingStatus.SelectedIndexChanged += new System.EventHandler(this.cboSearchByPurchasingStatus_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(1045, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 18);
            this.label5.TabIndex = 79;
            this.label5.Text = "To:";
            // 
            // dtpOPTo
            // 
            this.dtpOPTo.Location = new System.Drawing.Point(1082, 28);
            this.dtpOPTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpOPTo.Name = "dtpOPTo";
            this.dtpOPTo.Size = new System.Drawing.Size(268, 24);
            this.dtpOPTo.TabIndex = 78;
            // 
            // dtpOPFrom
            // 
            this.dtpOPFrom.Location = new System.Drawing.Point(770, 28);
            this.dtpOPFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpOPFrom.Name = "dtpOPFrom";
            this.dtpOPFrom.Size = new System.Drawing.Size(268, 24);
            this.dtpOPFrom.TabIndex = 77;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(715, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 18);
            this.label6.TabIndex = 76;
            this.label6.Text = "From:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(1045, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 83;
            this.label7.Text = "To:";
            // 
            // dtpInvoiceTo
            // 
            this.dtpInvoiceTo.Location = new System.Drawing.Point(1082, 28);
            this.dtpInvoiceTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpInvoiceTo.Name = "dtpInvoiceTo";
            this.dtpInvoiceTo.Size = new System.Drawing.Size(268, 24);
            this.dtpInvoiceTo.TabIndex = 82;
            // 
            // dtpInvoiceFrom
            // 
            this.dtpInvoiceFrom.Location = new System.Drawing.Point(770, 28);
            this.dtpInvoiceFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpInvoiceFrom.Name = "dtpInvoiceFrom";
            this.dtpInvoiceFrom.Size = new System.Drawing.Size(268, 24);
            this.dtpInvoiceFrom.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(715, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 18);
            this.label8.TabIndex = 80;
            this.label8.Text = "From:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(1045, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 18);
            this.label9.TabIndex = 83;
            this.label9.Text = "To:";
            // 
            // dtpGRPOTo
            // 
            this.dtpGRPOTo.Location = new System.Drawing.Point(1082, 28);
            this.dtpGRPOTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpGRPOTo.Name = "dtpGRPOTo";
            this.dtpGRPOTo.Size = new System.Drawing.Size(268, 24);
            this.dtpGRPOTo.TabIndex = 82;
            // 
            // dtpGRPOFrom
            // 
            this.dtpGRPOFrom.Location = new System.Drawing.Point(770, 28);
            this.dtpGRPOFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpGRPOFrom.Name = "dtpGRPOFrom";
            this.dtpGRPOFrom.Size = new System.Drawing.Size(268, 24);
            this.dtpGRPOFrom.TabIndex = 81;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(715, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 18);
            this.label10.TabIndex = 80;
            this.label10.Text = "From:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label11.Location = new System.Drawing.Point(1045, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 18);
            this.label11.TabIndex = 83;
            this.label11.Text = "To:";
            // 
            // dtpPOTo
            // 
            this.dtpPOTo.Location = new System.Drawing.Point(1082, 28);
            this.dtpPOTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpPOTo.Name = "dtpPOTo";
            this.dtpPOTo.Size = new System.Drawing.Size(268, 24);
            this.dtpPOTo.TabIndex = 82;
            // 
            // dtpPOFrom
            // 
            this.dtpPOFrom.Location = new System.Drawing.Point(770, 28);
            this.dtpPOFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpPOFrom.Name = "dtpPOFrom";
            this.dtpPOFrom.Size = new System.Drawing.Size(268, 24);
            this.dtpPOFrom.TabIndex = 81;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label12.Location = new System.Drawing.Point(715, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 18);
            this.label12.TabIndex = 80;
            this.label12.Text = "From:";
            // 
            // Purchasing_List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 809);
            this.Controls.Add(this.tctrlSalesReports);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Purchasing_List";
            this.Text = "Purchasing_List";
            this.Load += new System.EventHandler(this.Purchasing_List_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchasingOrderList)).EndInit();
            this.APInvoice.ResumeLayout(false);
            this.APInvoice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAPInvoiceList)).EndInit();
            this.GRPO.ResumeLayout(false);
            this.GRPO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGRPOList)).EndInit();
            this.tctrlSalesReports.ResumeLayout(false);
            this.PO.ResumeLayout(false);
            this.PO.PerformLayout();
            this.OutgoingPayment.ResumeLayout(false);
            this.OutgoingPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutgoingPaymentList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dgvPurchasingOrderList;
        private System.Windows.Forms.TabPage APInvoice;
        private System.Windows.Forms.TabPage GRPO;
        private System.Windows.Forms.TabControl tctrlSalesReports;
        private System.Windows.Forms.TabPage PO;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuImageButton btnExit;
        private System.Windows.Forms.TabPage OutgoingPayment;
        private System.Windows.Forms.ComboBox cboSearchByAPInvoiceStatus;
        private System.Windows.Forms.TextBox txtSearchAPInvoiceList;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dgvAPInvoiceList;
        private System.Windows.Forms.ComboBox cboSearchByGRPOStatus;
        private System.Windows.Forms.TextBox txtSearchGRPOList;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView dgvGRPOList;
        private System.Windows.Forms.TextBox txtSearchPurchasingList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearchOutgoingPayment;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dgvOutgoingPaymentList;
        private System.Windows.Forms.ComboBox cboSearchByPurchasingStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpInvoiceTo;
        private System.Windows.Forms.DateTimePicker dtpInvoiceFrom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpGRPOTo;
        private System.Windows.Forms.DateTimePicker dtpGRPOFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpPOTo;
        private System.Windows.Forms.DateTimePicker dtpPOFrom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpOPTo;
        private System.Windows.Forms.DateTimePicker dtpOPFrom;
        private System.Windows.Forms.Label label6;
    }
}