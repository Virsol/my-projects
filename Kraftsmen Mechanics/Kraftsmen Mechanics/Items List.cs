﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Items_List : Form
    {
        public static string xUom = "";

        private string invCode;
        private int  invQty;

        public string InvCode
        {
            get { return invCode; }
            set { invCode = value; }
        }

        public int InvQty
        {
            get { return invQty; }
            set { invQty = value; }
        }


        public Items_List()
        {
            InitializeComponent();
        }

        Connection iList = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void dgvItemsList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


        }
        #endregion
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       

        private void Items_List_Load_1(object sender, EventArgs e)
        {
            iList.OpenConn();
            dgvItemsList.DataSource = iList.ShowDataDGV("select * from Item_List");
            iList.CloseConn();
        }

        private void dgvItemsList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (label1.Text != "Items Report")
            {
                xUom = this.dgvItemsList.CurrentRow.Cells[5].Value.ToString();

                Add_BOM_Item addItem = new Add_BOM_Item();
                Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();

                addItem.lblhidden.Text = this.dgvItemsList.CurrentRow.Cells[0].Value.ToString();

                iList.OpenConn();
                SqlDataReader drInv = iList.DataReader("select inventory_code, inventory_name, inventory_description, item_price, uom_id from inventory where inventory_code = '" + addItem.lblhidden.Text + "'");
                drInv.Read();

                string code = drInv["inventory_code"].ToString();
                string name = drInv["inventory_name"].ToString();
                string desc = drInv["inventory_description"].ToString();
                double price = double.Parse(drInv["item_price"].ToString());
                int oum = Int32.Parse(drInv["uom_id"].ToString());

                addItem.txtItemCode.Text = code;
                addItem.txtItemName.Text = name;
                addItem.txtItemDescription.Text = desc;
                addItem.txtItemPrice.Text = price.ToString("n2");

                if (addItem.ShowDialog() == DialogResult.OK)
                {
                    this.Close();
                }

                iList.CloseConn();

                /*invCode = this.dgvItemsList.CurrentRow.Cells[0].Value.ToString();
                invQty = Int32.Parse(this.dgvItemsList.CurrentRow.Cells[3].Value.ToString());
                Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();

                iList.OpenConn();
                iList.ExecuteQuery("insert into bom_draft(bom_code, inventory_code, item_quantity, [P/C], status)" +
                    "values('"+addBOM.txtBOMCode.Text+"', '"+invCode+"', '"+1+"', '"+0+"', '"+1+"')");
                iList.CloseConn();*/
                this.Close();
            }
            else
            {

                PO_Report.Item_list = this.dgvItemsList.CurrentRow.Cells[1].Value.ToString();
                this.Close();

            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            iList.OpenConn();
            dgvItemsList.DataSource = iList.ShowDataDGV("Select * From Item_List where [Product Code]  like '%" + txtSearch.Text + "%' or [Product Name] like '%" + txtSearch.Text + "%'");
            iList.CloseConn();
        }

        private void Items_List_Load_2(object sender, EventArgs e)
        {
            iList.OpenConn();
            dgvItemsList.DataSource = iList.ShowDataDGV("select * from Item_List");
            iList.CloseConn();
        }

        private void dgvItemsList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvItemsList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvItemsList_CellDoubleClick_2(object sender, DataGridViewCellEventArgs e)
        {
            if (label1.Text != "Items Report")
            {
                xUom = this.dgvItemsList.CurrentRow.Cells[5].Value.ToString();

                Add_BOM_Item addItem = new Add_BOM_Item();
                Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();

                addItem.lblhidden.Text = this.dgvItemsList.CurrentRow.Cells[0].Value.ToString();

                iList.OpenConn();
                SqlDataReader drInv = iList.DataReader("select inventory_code, inventory_name, inventory_description, item_price, uom_id from inventory where inventory_code = '" + addItem.lblhidden.Text + "'");
                drInv.Read();

                string code = drInv["inventory_code"].ToString();
                string name = drInv["inventory_name"].ToString();
                string desc = drInv["inventory_description"].ToString();
                double price = double.Parse(drInv["item_price"].ToString());
                int oum = Int32.Parse(drInv["uom_id"].ToString());

                addItem.txtItemCode.Text = code;
                addItem.txtItemName.Text = name;
                addItem.txtItemDescription.Text = desc;
                addItem.txtItemPrice.Text = price.ToString("n2");

                if (addItem.ShowDialog() == DialogResult.OK)
                {
                    this.Close();
                }

                iList.CloseConn();

                /*invCode = this.dgvItemsList.CurrentRow.Cells[0].Value.ToString();
                invQty = Int32.Parse(this.dgvItemsList.CurrentRow.Cells[3].Value.ToString());
                Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();

                iList.OpenConn();
                iList.ExecuteQuery("insert into bom_draft(bom_code, inventory_code, item_quantity, [P/C], status)" +
                    "values('"+addBOM.txtBOMCode.Text+"', '"+invCode+"', '"+1+"', '"+0+"', '"+1+"')");
                iList.CloseConn();*/
                this.Close();
            }
            else
            {

                PO_Report.Item_list = this.dgvItemsList.CurrentRow.Cells[1].Value.ToString();
                this.Close();

            }
        }
    }
    }

