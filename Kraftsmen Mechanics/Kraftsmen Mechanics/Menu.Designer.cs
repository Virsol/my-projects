﻿
namespace Kraftsmen_Mechanics
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPurchasing = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlPurchasing = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.btnInvoice = new System.Windows.Forms.Button();
            this.btnGoodsReceiptPurchaseOrder = new System.Windows.Forms.Button();
            this.btnPurchasingOrder = new System.Windows.Forms.Button();
            this.pnlJobOrder = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btnJobEstimate = new System.Windows.Forms.Button();
            this.btnJobOrder = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.pnlPurchasing.SuspendLayout();
            this.pnlJobOrder.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPurchasing
            // 
            this.btnPurchasing.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPurchasing.FlatAppearance.BorderSize = 0;
            this.btnPurchasing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchasing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPurchasing.Location = new System.Drawing.Point(0, 0);
            this.btnPurchasing.Name = "btnPurchasing";
            this.btnPurchasing.Size = new System.Drawing.Size(270, 48);
            this.btnPurchasing.TabIndex = 0;
            this.btnPurchasing.Text = "Purchasing";
            this.btnPurchasing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurchasing.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 71);
            this.panel1.TabIndex = 1;
            // 
            // pnlPurchasing
            // 
            this.pnlPurchasing.Controls.Add(this.button5);
            this.pnlPurchasing.Controls.Add(this.btnInvoice);
            this.pnlPurchasing.Controls.Add(this.btnGoodsReceiptPurchaseOrder);
            this.pnlPurchasing.Controls.Add(this.btnPurchasingOrder);
            this.pnlPurchasing.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPurchasing.Location = new System.Drawing.Point(0, 48);
            this.pnlPurchasing.Name = "pnlPurchasing";
            this.pnlPurchasing.Size = new System.Drawing.Size(270, 140);
            this.pnlPurchasing.TabIndex = 2;
            this.pnlPurchasing.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPurchasing_Paint);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(0, 99);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(270, 33);
            this.button5.TabIndex = 3;
            this.button5.Text = "Outgoing Payment";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnInvoice
            // 
            this.btnInvoice.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInvoice.Location = new System.Drawing.Point(0, 66);
            this.btnInvoice.Name = "btnInvoice";
            this.btnInvoice.Size = new System.Drawing.Size(270, 33);
            this.btnInvoice.TabIndex = 2;
            this.btnInvoice.Text = "Invoice";
            this.btnInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInvoice.UseVisualStyleBackColor = true;
            this.btnInvoice.Click += new System.EventHandler(this.btnInvoice_Click);
            // 
            // btnGoodsReceiptPurchaseOrder
            // 
            this.btnGoodsReceiptPurchaseOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGoodsReceiptPurchaseOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoodsReceiptPurchaseOrder.Location = new System.Drawing.Point(0, 33);
            this.btnGoodsReceiptPurchaseOrder.Name = "btnGoodsReceiptPurchaseOrder";
            this.btnGoodsReceiptPurchaseOrder.Size = new System.Drawing.Size(270, 33);
            this.btnGoodsReceiptPurchaseOrder.TabIndex = 1;
            this.btnGoodsReceiptPurchaseOrder.Text = "Goods Receipt Purchase Order";
            this.btnGoodsReceiptPurchaseOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGoodsReceiptPurchaseOrder.UseVisualStyleBackColor = true;
            this.btnGoodsReceiptPurchaseOrder.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnPurchasingOrder
            // 
            this.btnPurchasingOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPurchasingOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPurchasingOrder.Location = new System.Drawing.Point(0, 0);
            this.btnPurchasingOrder.Name = "btnPurchasingOrder";
            this.btnPurchasingOrder.Size = new System.Drawing.Size(270, 33);
            this.btnPurchasingOrder.TabIndex = 0;
            this.btnPurchasingOrder.Text = "Purchasing Order";
            this.btnPurchasingOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurchasingOrder.UseVisualStyleBackColor = true;
            this.btnPurchasingOrder.Click += new System.EventHandler(this.button2_Click);
            // 
            // pnlJobOrder
            // 
            this.pnlJobOrder.Controls.Add(this.button8);
            this.pnlJobOrder.Controls.Add(this.btnJobEstimate);
            this.pnlJobOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlJobOrder.Location = new System.Drawing.Point(0, 236);
            this.pnlJobOrder.Name = "pnlJobOrder";
            this.pnlJobOrder.Size = new System.Drawing.Size(270, 76);
            this.pnlJobOrder.TabIndex = 4;
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Top;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(0, 33);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(270, 33);
            this.button8.TabIndex = 1;
            this.button8.Text = "Job Order";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // btnJobEstimate
            // 
            this.btnJobEstimate.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnJobEstimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJobEstimate.Location = new System.Drawing.Point(0, 0);
            this.btnJobEstimate.Name = "btnJobEstimate";
            this.btnJobEstimate.Size = new System.Drawing.Size(270, 33);
            this.btnJobEstimate.TabIndex = 0;
            this.btnJobEstimate.Text = "Job Estimate";
            this.btnJobEstimate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJobEstimate.UseVisualStyleBackColor = true;
            this.btnJobEstimate.Click += new System.EventHandler(this.btnJobEstimate_Click);
            // 
            // btnJobOrder
            // 
            this.btnJobOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnJobOrder.FlatAppearance.BorderSize = 0;
            this.btnJobOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJobOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJobOrder.Location = new System.Drawing.Point(0, 188);
            this.btnJobOrder.Name = "btnJobOrder";
            this.btnJobOrder.Size = new System.Drawing.Size(270, 48);
            this.btnJobOrder.TabIndex = 3;
            this.btnJobOrder.Text = "Job Order";
            this.btnJobOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJobOrder.UseVisualStyleBackColor = true;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.Controls.Add(this.panel3);
            this.pnlMenu.Controls.Add(this.button7);
            this.pnlMenu.Controls.Add(this.panel2);
            this.pnlMenu.Controls.Add(this.button11);
            this.pnlMenu.Controls.Add(this.pnlJobOrder);
            this.pnlMenu.Controls.Add(this.btnJobOrder);
            this.pnlMenu.Controls.Add(this.pnlPurchasing);
            this.pnlMenu.Controls.Add(this.btnPurchasing);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu.Location = new System.Drawing.Point(0, 71);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(287, 470);
            this.pnlMenu.TabIndex = 0;
            this.pnlMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMenu_Paint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 452);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(270, 73);
            this.panel3.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(270, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Sales Report";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(0, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(270, 33);
            this.button6.TabIndex = 0;
            this.button6.Text = "Purchasing Report";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(0, 404);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(270, 48);
            this.button7.TabIndex = 7;
            this.button7.Text = "Reports";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 360);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 44);
            this.panel2.TabIndex = 6;
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Top;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(0, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(270, 33);
            this.button10.TabIndex = 0;
            this.button10.Text = "Inventory";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Dock = System.Windows.Forms.DockStyle.Top;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(0, 312);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(270, 48);
            this.button11.TabIndex = 5;
            this.button11.Text = "Inventory";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(287, 541);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Menu";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.pnlPurchasing.ResumeLayout(false);
            this.pnlJobOrder.ResumeLayout(false);
            this.pnlMenu.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPurchasing;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlPurchasing;
        private System.Windows.Forms.Button btnPurchasingOrder;
        private System.Windows.Forms.Button btnGoodsReceiptPurchaseOrder;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnInvoice;
        private System.Windows.Forms.Panel pnlJobOrder;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnJobEstimate;
        private System.Windows.Forms.Button btnJobOrder;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}