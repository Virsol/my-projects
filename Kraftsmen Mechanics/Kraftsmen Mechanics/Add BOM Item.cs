﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Add_BOM_Item : Form
    {

        private double t_tax, t_price;

         public double T_Tax
        {
            get { return t_tax; }
            set { t_tax = value; }
        }

        public double T_Price
        {
            get { return t_price; }
            set { t_price = value; }
        }
        public Add_BOM_Item()
        {
            InitializeComponent();
        }

        Connection addBOMItm = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void Add_BOM_Item_Load(object sender, EventArgs e)
        {

        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void Add_BOM_Item_Load_1(object sender, EventArgs e)
        {
            string uom = Items_List.xUom;

            Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();


            //lblhidden.Visible = true;
            lblhidden.Text = Add_Bill_of_Materials.BCode;

            conn.Open();
            SqlCommand cmdtax = new SqlCommand("Select * from dbo.tax where tax_id = '2'", conn);
            SqlDataReader drtax = cmdtax.ExecuteReader();
            DataTable dttax = new DataTable();
            dttax.Load(drtax);
            cboItemTax.DataSource = dttax;
            cboItemTax.DisplayMember = "tax_desc";
            cboItemTax.ValueMember = "tax_id";
            cmdtax.ExecuteNonQuery();

            SqlCommand cmduom = new SqlCommand("Select * from dbo.uom", conn);
            SqlDataReader dr = cmduom.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            cboUOM.DataSource = dt;
            cboUOM.DisplayMember = "uom_desc";
            cboUOM.ValueMember = "uom_id";
            cmduom.ExecuteNonQuery();

            cboUOM.Text = uom;

            conn.Close();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            

            //string bom_Code = addBOM.txtBOMCode.Text;
            string code = txtItemCode.Text;
            string name = txtItemName.Text;
            string desc = txtItemDescription.Text;
            double price = double.Parse(txtItemPrice.Text);
            int quantity = Int32.Parse(txtItemQuantity.Text);
            int tax = Int32.Parse(cboItemTax.SelectedValue.ToString());
            int uom = Int32.Parse(cboUOM.SelectedValue.ToString());

            addBOMItm.OpenConn();
            SqlDataReader drtax = addBOMItm.DataReader("select tax_rate from dbo.tax where tax_id = '" + tax + "'");
            drtax.Read();
            int prod_tax_rate = Int32.Parse(drtax["tax_rate"].ToString());
            int per = 100;

            T_Price = price * quantity;
            T_Tax = T_Price * prod_tax_rate / per;

            addBOMItm.OpenConn();
            addBOMItm.ExecuteQuery("insert into bom_draft(bom_code, inventory_code, item_quantity, [P/C], status, total_tax, total_price)" +
                "values('" + lblhidden.Text + "', '" + code + "','" + quantity + "', '" + 0 + "', '" + 1 + "', '"+T_Tax+"', '"+T_Price+"')");
            addBOMItm.CloseConn();

            addBOMItm.CloseConn();

            this.Close();
        }
    }
}
