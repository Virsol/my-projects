﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Kraftsmen_Mechanics
{
    class Sales_receipt
    {
        public static string docNumReceipt = "";
        public void SI()
        {
            /*frmparameter cbo = new frmparameter();*/
            Form1 frm = new Form1();
            ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load(@"C:\Users\Ronnie\Documents\leobytes_main\Kraftsmen Mechanics\Kraftsmen Mechanics\CrystalReport1.rpt");
            cryRpt.SetDatabaseLogon("sa", "15249898", "THUNDER\\LEOBYTES", "service");

            ParameterFieldDefinitions crParameterFieldDefinitions;

            ParameterFieldDefinition crParameterFieldDefinition;

            ParameterValues crParameterValues = new ParameterValues();

            ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

            crParameterDiscreteValue.Value = docNumReceipt;
            /* crParameterDiscreteValue.Value = frmparameter.cboSelected;*/

            crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;

            crParameterFieldDefinition = crParameterFieldDefinitions["DocNum"];

            crParameterValues = crParameterFieldDefinition.CurrentValues;

            crParameterValues.Add(crParameterDiscreteValue);

            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

            frm.crystalReportViewer1.ReportSource = cryRpt;

            frm.crystalReportViewer1.Refresh();
          
            frm.ShowDialog();


        }
    }
}
