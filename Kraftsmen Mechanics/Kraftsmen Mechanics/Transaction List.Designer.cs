﻿
namespace Kraftsmen_Mechanics
{
    partial class Transaction_List
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Transaction_List));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSearchIncomingPayments = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvIncomingPaymentList = new System.Windows.Forms.DataGridView();
            this.IP = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearchJOList = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchJEList = new System.Windows.Forms.TextBox();
            this.dgvJobOrderList = new System.Windows.Forms.DataGridView();
            this.JO = new System.Windows.Forms.TabPage();
            this.txtSearchServiceInvoiceList = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvServiceInvoiceList = new System.Windows.Forms.DataGridView();
            this.SI = new System.Windows.Forms.TabPage();
            this.dgvJobEstimateList = new System.Windows.Forms.DataGridView();
            this.tctrlSalesReports = new System.Windows.Forms.TabControl();
            this.JE = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncomingPaymentList)).BeginInit();
            this.IP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobOrderList)).BeginInit();
            this.JO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceInvoiceList)).BeginInit();
            this.SI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobEstimateList)).BeginInit();
            this.tctrlSalesReports.SuspendLayout();
            this.JE.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(1323, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 31);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1371, 52);
            this.panel1.TabIndex = 67;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(150, 25);
            this.label17.TabIndex = 24;
            this.label17.Text = "Transaction List";
            // 
            // txtSearchIncomingPayments
            // 
            this.txtSearchIncomingPayments.Location = new System.Drawing.Point(87, 27);
            this.txtSearchIncomingPayments.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchIncomingPayments.Name = "txtSearchIncomingPayments";
            this.txtSearchIncomingPayments.Size = new System.Drawing.Size(413, 24);
            this.txtSearchIncomingPayments.TabIndex = 75;
            this.txtSearchIncomingPayments.TextChanged += new System.EventHandler(this.txtSearchIncomingPayments_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 18);
            this.label4.TabIndex = 74;
            this.label4.Text = "Search:";
            // 
            // dgvIncomingPaymentList
            // 
            this.dgvIncomingPaymentList.AllowUserToAddRows = false;
            this.dgvIncomingPaymentList.AllowUserToDeleteRows = false;
            this.dgvIncomingPaymentList.AllowUserToResizeColumns = false;
            this.dgvIncomingPaymentList.AllowUserToResizeRows = false;
            this.dgvIncomingPaymentList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvIncomingPaymentList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvIncomingPaymentList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvIncomingPaymentList.BackgroundColor = System.Drawing.Color.White;
            this.dgvIncomingPaymentList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncomingPaymentList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIncomingPaymentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIncomingPaymentList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvIncomingPaymentList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvIncomingPaymentList.Location = new System.Drawing.Point(9, 74);
            this.dgvIncomingPaymentList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvIncomingPaymentList.MultiSelect = false;
            this.dgvIncomingPaymentList.Name = "dgvIncomingPaymentList";
            this.dgvIncomingPaymentList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncomingPaymentList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvIncomingPaymentList.RowHeadersWidth = 51;
            this.dgvIncomingPaymentList.RowTemplate.Height = 24;
            this.dgvIncomingPaymentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIncomingPaymentList.Size = new System.Drawing.Size(1341, 586);
            this.dgvIncomingPaymentList.TabIndex = 73;
            // 
            // IP
            // 
            this.IP.Controls.Add(this.txtSearchIncomingPayments);
            this.IP.Controls.Add(this.label4);
            this.IP.Controls.Add(this.dgvIncomingPaymentList);
            this.IP.Location = new System.Drawing.Point(4, 34);
            this.IP.Margin = new System.Windows.Forms.Padding(4);
            this.IP.Name = "IP";
            this.IP.Size = new System.Drawing.Size(1363, 719);
            this.IP.TabIndex = 3;
            this.IP.Text = "Incoming Payments";
            this.IP.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 62;
            this.label1.Text = "Search:";
            // 
            // txtSearchJOList
            // 
            this.txtSearchJOList.Location = new System.Drawing.Point(87, 27);
            this.txtSearchJOList.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchJOList.Name = "txtSearchJOList";
            this.txtSearchJOList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchJOList.TabIndex = 67;
            this.txtSearchJOList.TextChanged += new System.EventHandler(this.txtSearchJOList_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 66;
            this.label2.Text = "Search:";
            // 
            // txtSearchJEList
            // 
            this.txtSearchJEList.Location = new System.Drawing.Point(87, 27);
            this.txtSearchJEList.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchJEList.Name = "txtSearchJEList";
            this.txtSearchJEList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchJEList.TabIndex = 63;
            this.txtSearchJEList.TextChanged += new System.EventHandler(this.txtSearchJEList_TextChanged);
            // 
            // dgvJobOrderList
            // 
            this.dgvJobOrderList.AllowUserToAddRows = false;
            this.dgvJobOrderList.AllowUserToDeleteRows = false;
            this.dgvJobOrderList.AllowUserToResizeColumns = false;
            this.dgvJobOrderList.AllowUserToResizeRows = false;
            this.dgvJobOrderList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvJobOrderList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvJobOrderList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvJobOrderList.BackgroundColor = System.Drawing.Color.White;
            this.dgvJobOrderList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobOrderList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvJobOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvJobOrderList.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvJobOrderList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvJobOrderList.Location = new System.Drawing.Point(9, 74);
            this.dgvJobOrderList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvJobOrderList.MultiSelect = false;
            this.dgvJobOrderList.Name = "dgvJobOrderList";
            this.dgvJobOrderList.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobOrderList.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvJobOrderList.RowHeadersWidth = 51;
            this.dgvJobOrderList.RowTemplate.Height = 24;
            this.dgvJobOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobOrderList.Size = new System.Drawing.Size(1341, 586);
            this.dgvJobOrderList.TabIndex = 65;
            this.dgvJobOrderList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvJobOrderList_CellContentClick_1);
            // 
            // JO
            // 
            this.JO.BackColor = System.Drawing.Color.White;
            this.JO.Controls.Add(this.txtSearchJOList);
            this.JO.Controls.Add(this.label2);
            this.JO.Controls.Add(this.dgvJobOrderList);
            this.JO.ForeColor = System.Drawing.Color.Black;
            this.JO.Location = new System.Drawing.Point(4, 34);
            this.JO.Margin = new System.Windows.Forms.Padding(4);
            this.JO.Name = "JO";
            this.JO.Padding = new System.Windows.Forms.Padding(4);
            this.JO.Size = new System.Drawing.Size(1363, 719);
            this.JO.TabIndex = 1;
            this.JO.Text = "Job Order";
            this.JO.Click += new System.EventHandler(this.JO_Click);
            // 
            // txtSearchServiceInvoiceList
            // 
            this.txtSearchServiceInvoiceList.Location = new System.Drawing.Point(87, 27);
            this.txtSearchServiceInvoiceList.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchServiceInvoiceList.Name = "txtSearchServiceInvoiceList";
            this.txtSearchServiceInvoiceList.Size = new System.Drawing.Size(413, 24);
            this.txtSearchServiceInvoiceList.TabIndex = 71;
            this.txtSearchServiceInvoiceList.TextChanged += new System.EventHandler(this.txtSearchServiceInvoiceList_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 70;
            this.label3.Text = "Search:";
            // 
            // dgvServiceInvoiceList
            // 
            this.dgvServiceInvoiceList.AllowUserToAddRows = false;
            this.dgvServiceInvoiceList.AllowUserToDeleteRows = false;
            this.dgvServiceInvoiceList.AllowUserToResizeColumns = false;
            this.dgvServiceInvoiceList.AllowUserToResizeRows = false;
            this.dgvServiceInvoiceList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServiceInvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvServiceInvoiceList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvServiceInvoiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvServiceInvoiceList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceInvoiceList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvServiceInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvServiceInvoiceList.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvServiceInvoiceList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvServiceInvoiceList.Location = new System.Drawing.Point(9, 74);
            this.dgvServiceInvoiceList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvServiceInvoiceList.MultiSelect = false;
            this.dgvServiceInvoiceList.Name = "dgvServiceInvoiceList";
            this.dgvServiceInvoiceList.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceInvoiceList.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvServiceInvoiceList.RowHeadersWidth = 51;
            this.dgvServiceInvoiceList.RowTemplate.Height = 24;
            this.dgvServiceInvoiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServiceInvoiceList.Size = new System.Drawing.Size(1341, 586);
            this.dgvServiceInvoiceList.TabIndex = 69;
            this.dgvServiceInvoiceList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServiceInvoiceList_CellContentClick_1);
            // 
            // SI
            // 
            this.SI.BackColor = System.Drawing.Color.White;
            this.SI.Controls.Add(this.txtSearchServiceInvoiceList);
            this.SI.Controls.Add(this.label3);
            this.SI.Controls.Add(this.dgvServiceInvoiceList);
            this.SI.Location = new System.Drawing.Point(4, 34);
            this.SI.Margin = new System.Windows.Forms.Padding(4);
            this.SI.Name = "SI";
            this.SI.Size = new System.Drawing.Size(1363, 719);
            this.SI.TabIndex = 2;
            this.SI.Text = "Service Invoice";
            this.SI.Click += new System.EventHandler(this.SI_Click);
            // 
            // dgvJobEstimateList
            // 
            this.dgvJobEstimateList.AllowUserToAddRows = false;
            this.dgvJobEstimateList.AllowUserToDeleteRows = false;
            this.dgvJobEstimateList.AllowUserToResizeColumns = false;
            this.dgvJobEstimateList.AllowUserToResizeRows = false;
            this.dgvJobEstimateList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvJobEstimateList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvJobEstimateList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvJobEstimateList.BackgroundColor = System.Drawing.Color.White;
            this.dgvJobEstimateList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobEstimateList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvJobEstimateList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvJobEstimateList.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvJobEstimateList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvJobEstimateList.Location = new System.Drawing.Point(9, 74);
            this.dgvJobEstimateList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvJobEstimateList.MultiSelect = false;
            this.dgvJobEstimateList.Name = "dgvJobEstimateList";
            this.dgvJobEstimateList.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobEstimateList.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvJobEstimateList.RowHeadersWidth = 51;
            this.dgvJobEstimateList.RowTemplate.Height = 24;
            this.dgvJobEstimateList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobEstimateList.Size = new System.Drawing.Size(1341, 586);
            this.dgvJobEstimateList.TabIndex = 61;
            this.dgvJobEstimateList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvJobEstimateList_CellContentClick_1);
            // 
            // tctrlSalesReports
            // 
            this.tctrlSalesReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctrlSalesReports.Controls.Add(this.JE);
            this.tctrlSalesReports.Controls.Add(this.JO);
            this.tctrlSalesReports.Controls.Add(this.SI);
            this.tctrlSalesReports.Controls.Add(this.IP);
            this.tctrlSalesReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tctrlSalesReports.ItemSize = new System.Drawing.Size(80, 30);
            this.tctrlSalesReports.Location = new System.Drawing.Point(0, 52);
            this.tctrlSalesReports.Margin = new System.Windows.Forms.Padding(4);
            this.tctrlSalesReports.Name = "tctrlSalesReports";
            this.tctrlSalesReports.Padding = new System.Drawing.Point(25, 3);
            this.tctrlSalesReports.SelectedIndex = 0;
            this.tctrlSalesReports.Size = new System.Drawing.Size(1371, 757);
            this.tctrlSalesReports.TabIndex = 68;
            // 
            // JE
            // 
            this.JE.BackColor = System.Drawing.Color.White;
            this.JE.Controls.Add(this.txtSearchJEList);
            this.JE.Controls.Add(this.label1);
            this.JE.Controls.Add(this.dgvJobEstimateList);
            this.JE.ForeColor = System.Drawing.Color.Black;
            this.JE.Location = new System.Drawing.Point(4, 34);
            this.JE.Margin = new System.Windows.Forms.Padding(4);
            this.JE.Name = "JE";
            this.JE.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.JE.Size = new System.Drawing.Size(1363, 719);
            this.JE.TabIndex = 0;
            this.JE.Text = "Job Estimate";
            this.JE.Click += new System.EventHandler(this.JE_Click);
            // 
            // Transaction_List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 788);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tctrlSalesReports);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Transaction_List";
            this.Text = "Transaction_List";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Transaction_List_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncomingPaymentList)).EndInit();
            this.IP.ResumeLayout(false);
            this.IP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobOrderList)).EndInit();
            this.JO.ResumeLayout(false);
            this.JO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceInvoiceList)).EndInit();
            this.SI.ResumeLayout(false);
            this.SI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobEstimateList)).EndInit();
            this.tctrlSalesReports.ResumeLayout(false);
            this.JE.ResumeLayout(false);
            this.JE.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuImageButton btnExit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSearchIncomingPayments;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dgvIncomingPaymentList;
        private System.Windows.Forms.TabPage IP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearchJOList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearchJEList;
        public System.Windows.Forms.DataGridView dgvJobOrderList;
        private System.Windows.Forms.TabPage JO;
        private System.Windows.Forms.TextBox txtSearchServiceInvoiceList;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dgvServiceInvoiceList;
        private System.Windows.Forms.TabPage SI;
        public System.Windows.Forms.DataGridView dgvJobEstimateList;
        private System.Windows.Forms.TabControl tctrlSalesReports;
        private System.Windows.Forms.TabPage JE;
    }
}