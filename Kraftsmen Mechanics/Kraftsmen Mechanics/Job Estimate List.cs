﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Estimate_List : Form
    {
        DBConnection dbcon = new DBConnection();
        private int cusID;
        private int carID;
        private string nDate;
        private int estimatedBy;
        private string docNum;
        private string jedocnum;
        private string jodocnum;

        public Job_Estimate_List()
        {
            InitializeComponent();
        }

        private void Job_Estimate_List_Load(object sender, EventArgs e)
        
        {

            
                dbcon.OpenConnection();
                dgvJobEstimateList.DataSource = dbcon.ShowDataInGridView("select * from job_estimate_list_view");
                dbcon.CloseConnection();
            
           
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgvJobEstimateList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*jedocnum = dgvJobEstimateList.CurrentRow.Cells[0].Value.ToString();
            dbcon.OpenConnection();
            SqlDataReader data =  dbcon.DataReader("Select * from job_estimate where job_estimate_number = (Select Right([Job Estimate Number],7) from Job_estimate_list_view where [Job Estimate Number] = '"+jedocnum+"');");
            data.Read();
            cusID = Int32.Parse(data["customer_id"].ToString());
            carID = Int32.Parse(data["car_id"].ToString());
            nDate = data["date"].ToString();
            estimatedBy = Int32.Parse(data["estimated_by"].ToString());
            dbcon.CloseConnection();
            this.Close();*/
        }
        public int CusID
        {
            get { return cusID; }
            set { cusID = value; }
        }
        public int CarID
        {
            get { return carID; }
            set { carID = value; }
        }
        public string Date
        {
            get { return nDate; }
            set { nDate = value; }
        }
        public int empID
        {
            get { return estimatedBy; }
            set { estimatedBy = value; }
        }
        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
           
        }

        private void panel1_Paint_2(object sender, PaintEventArgs e)
        {

        }

        private void dgvJobEstimateList_CellContentClick_3(object sender, DataGridViewCellEventArgs e)
        {
            jedocnum = dgvJobEstimateList.CurrentRow.Cells[0].Value.ToString();
            dbcon.OpenConnection();
            SqlDataReader data = dbcon.DataReader("Select * from job_estimate where job_estimate_number = (Select Right([Job Estimate Number],7) from Job_estimate_list_view where [Job Estimate Number] = '" + jedocnum + "');");
            data.Read();
            docNum = data["job_estimate_number"].ToString();
            cusID = Int32.Parse(data["customer_id"].ToString());
            carID = Int32.Parse(data["car_id"].ToString());
            nDate = data["date"].ToString();
            estimatedBy = Int32.Parse(data["estimated_by"].ToString());
            dbcon.CloseConnection();
            this.Close();
        }


        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string sSearch = txtSearch.Text;
            dbcon.OpenConnection();
            dgvJobEstimateList.DataSource = dbcon.ShowDataInGridView("select * from job_estimate_list_view where [Job Estimate Number] like '%" + sSearch + "%' or [Customer Name] like '%" + sSearch + "%'");
            dbcon.CloseConnection();
        }
    }
}
