﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public partial class Add_Product : Form
    {
        public Add_Product()
        {
            InitializeComponent();
        }

        Connection addProd = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region trash
        private void Add_Product_Load_1(object sender, EventArgs e)
        {

        }
        private void btnSave_Click_1(object sender, EventArgs e)
        {

        }
        private void Add_Product_Load(object sender, EventArgs e)
        {
            
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

        }
        #endregion

        private void btnSave_Click_2(object sender, EventArgs e)
        {
            string invCode = txtProductCode.Text;
            string invName = txtProductName.Text;
            string invDesc = txtProductDescription.Text;
            int uomId = Int32.Parse(cboUOM.SelectedValue.ToString());
            double itmPrice = double.Parse(txtProductPrice.Text);


            addProd.OpenConn();
            SqlDataReader drCount = addProd.DataReader("Select count(inventory_code) as 'Code' from inventory where inventory_code = '" + invCode + "' ");
            drCount.Read();
            int xCode = Int32.Parse(drCount["Code"].ToString());

            if (xCode == 1)
            {
                MessageBox.Show("Item code is already existed.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (txtProductCode.Text == "" || txtProductName.Text == "" || txtProductDescription.Text == "" || txtProductPrice.Text == "" || txtProductPrice.Text == "0")
                {
                    MessageBox.Show("Please fill up the fields first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    addProd.OpenConn();
                    addProd.ExecuteQuery("insert into inventory(inventory_code, inventory_name, inventory_description, inventory_type, quantity, item_price, inventory_status, uom_id)" +
                        "VALUES('" + invCode + "', '" + invName + "', '" + invDesc + "', '" + 1 + "', '" + 0 + "', '" + itmPrice + "', '" + 1 + "', '" + uomId + "')");
                    addProd.CloseConn();

                    MessageBox.Show("Item Added.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            addProd.CloseConn();
        }

        private void Add_Product_Load_2(object sender, EventArgs e)
        {
            conn.Open();

            SqlCommand cmdUom = new SqlCommand("Select * from uom", conn);
            SqlDataReader drUom = cmdUom.ExecuteReader();
            DataTable dtUom = new DataTable();
            dtUom.Load(drUom);
            cboUOM.DataSource = dtUom;
            cboUOM.DisplayMember = "uom_desc";
            cboUOM.ValueMember = "uom_id";
            cmdUom.ExecuteNonQuery();

            conn.Close();
        }
    }
}
