﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Bill_of_Material : Form
    {
        public Bill_of_Material()
        {
            InitializeComponent();
        }
        Connection bom = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void Bill_of_Material_Load(object sender, EventArgs e)
        {

        }
        private void Bill_of_Material_Load_1(object sender, EventArgs e)
        {

        }
        private void Bill_of_Material_Load_2(object sender, EventArgs e)
        {

        }
        #endregion

        private void btnAddBillOfMaterial_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_Bill_of_Materials add_Bill_Of_Materials = new Add_Bill_of_Materials()) 
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_Bill_Of_Materials.Owner = newFormDialog;

                    if (add_Bill_Of_Materials.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        showItem();
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void showItem()
        {
            bom.OpenConn();
            dgvBillOfMaterialList.DataSource = bom.ShowDataDGV("select * from Parent_View");
            bom.CloseConn();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void Bill_of_Material_Load_3(object sender, EventArgs e)
        {
            showItem();
        }
    }
}
