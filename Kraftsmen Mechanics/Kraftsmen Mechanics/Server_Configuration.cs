﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Server_Configuration : Form
    {
        public Server_Configuration()
        {
            InitializeComponent();
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            String constring = "Data Source= " + txtServerName.Text + ";Initial Catalog=master;User ID=" + txtServerUsername.Text + ";Password=" + txtServerPassword.Text + " ";
            string Query = "Select * from master.sys.databases";

            SqlConnection conDatabase = new SqlConnection(constring);

            SqlCommand cmdDatabse = new SqlCommand(Query, conDatabase);
            SqlDataReader myReader;
            conDatabase.Open();
            myReader = cmdDatabse.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(myReader);


            try
            {
                SqlHelper helper = new SqlHelper(constring);

                if (helper.IsConnection)
                    MessageBox.Show("Server Connected", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                /* cbodatabase.Items.Add("db");
                 cbodatabase.Items.Add("temptes");
                 cbodatabase.Items.Add("service");
                 cbodatabase.Items.Add("master");
                 cbodatabase.SelectedIndex=0;*/

                /*String valueOfItem = drv["name"].ToString();*/
                cboSelectDatabase.DataSource = dt;
                cboSelectDatabase.DisplayMember = "name";
                cboSelectDatabase.ValueMember = "name";
                cmdDatabse.ExecuteNonQuery();
                conDatabase.Close();





                /*cbodatabase.ValueMember = ["name"];*/
                /* cmdDatabse.ExecuteNonQuery();*/
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.server = txtServerName.Text;
            Properties.Settings.Default.username = txtServerUsername.Text;
            Properties.Settings.Default.password = txtServerPassword.Text;
            Properties.Settings.Default.database = cboSelectDatabase.SelectedValue.ToString();
            Properties.Settings.Default.Save();
           /* Properties.Settings.Default.Reload();*/
            MessageBox.Show("Save Successfully");
        }

        private void Server_Configuration_Load(object sender, EventArgs e)
        {
            txtServerName.Text = Properties.Settings.Default.server;
            txtServerUsername.Text = Properties.Settings.Default.username;
            txtServerPassword.Text = Properties.Settings.Default.password;
            /*label1.Text = Properties.Settings.Default.database;*/ 

        }
    }
}
