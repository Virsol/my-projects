﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class GRPO_Item_Details : Form
    {
        private string t_Price, t_Tax, t_Value;

        public string T_Price
        {
            get { return t_Price; }
            set { t_Price = value; }
        }

        public string T_Tax
        {
            get { return t_Tax; }
            set { t_Tax = value; }
        }

        public string T_Value
        {
            get { return t_Value; }
            set { t_Value = value; }
        }


        public GRPO_Item_Details()
        {
            InitializeComponent();
        }

        Connection grpoCon = new Connection();
        SqlConnection Conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }
        #endregion
        private void btnCancel_Click(object sender, EventArgs e)
        {
            grpoCon.OpenConn();

            SqlDataReader drTotal = grpoCon.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax from draft_grpo");
            drTotal.Read();

            double totalPrice = double.Parse(drTotal["Total_Price"].ToString());
            double totalTax = double.Parse(drTotal["Total_Tax"].ToString());

            double totalValue = (totalPrice + totalTax);

            T_Price = totalPrice.ToString("n2");
            T_Tax = totalTax.ToString("n2");
            T_Value = totalValue.ToString("n2");

            grpoCon.CloseConn();

            this.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            grpoCon.OpenConn();
            Goods_Receipt_Purchase_Order grpo = new Goods_Receipt_Purchase_Order();
            string prodNum = lblhidden.Text;
            string prodCode = txtProductCode.Text;
            int prodQty = Int32.Parse(txtProductQuantity.Text);
            int prodUom = Int32.Parse(cboProductUoM.SelectedValue.ToString());
            int prodTax = Int32.Parse(cboProductTax.SelectedValue.ToString());
            double prodPrice = double.Parse(txtProductPrice.Text);

                if (txtProductCode.Text == "" || txtProductName.Text == "" || txtProductPrice.Text == "" || txtProductQuantity.Text == "")
                {
                    MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    SqlDataReader drTax = grpoCon.DataReader("select tax_rate from dbo.tax where tax_id = '" + prodTax + "'");
                    drTax.Read();
                    int prodTaxRate = Int32.Parse(drTax["tax_rate"].ToString());
                    int per = 100;
                    double taxPrice;
                    double prodTotal = prodPrice * prodQty;

                    taxPrice = prodTotal * prodTaxRate / per;
                    grpoCon.CloseConn();

                    grpoCon.OpenConn();
                    grpoCon.ExecuteQuery("insert into dbo.draft_grpo (grpo_doc_num, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price)" +
                        "values('" + prodNum + "', '" + prodCode + "','" + prodQty + "', '" + prodUom + "', '" + prodPrice + "', '" + prodTax + "', '" + taxPrice + "', '" + prodTotal + "')");
                    grpoCon.CloseConn();

                    this.Close();
                }

            //----------------------------------------------------------//
            grpoCon.OpenConn();

            SqlDataReader drTotal = grpoCon.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax from draft_grpo");
            drTotal.Read();

            double totalPrice = double.Parse(drTotal["Total_Price"].ToString());
            double totalTax = double.Parse(drTotal["Total_Tax"].ToString());

            double totalValue = (totalPrice + totalTax);

            T_Price = totalPrice.ToString("n2");
            T_Tax = totalTax.ToString("n2");
            T_Value = totalValue.ToString("n2");

            grpoCon.CloseConn();
            //----------------------------------------------------------//



            grpoCon.CloseConn();
        }

        private void GRPO_Item_Details_Load(object sender, EventArgs e)
        {
            string xUom = Goods_Receipt_Purchase_Order.uom;
            Conn.Open();

            SqlCommand cmdUom = new SqlCommand("select * from dbo.uom",Conn);
            SqlDataReader drUom = cmdUom.ExecuteReader();
            DataTable dtUom = new DataTable();
            dtUom.Load(drUom);
            cboProductUoM.DataSource = dtUom;
            cboProductUoM.ValueMember = "uom_id";
            cboProductUoM.DisplayMember = "uom_desc";
            cmdUom.ExecuteNonQuery();

            SqlCommand cmdTax = new SqlCommand("select * from dbo.tax where tax_id = '2'",Conn);
            SqlDataReader drTax = cmdTax.ExecuteReader();
            DataTable dtTax = new DataTable();
            dtTax.Load(drTax);
            cboProductTax.DataSource = dtTax;
            cboProductTax.ValueMember = "tax_id";
            cboProductTax.DisplayMember = "tax_desc";
            cmdTax.ExecuteNonQuery();

            Conn.Close();

            cboProductUoM.Text = xUom;
        }
    }
}
