﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Kraftsmen_Mechanics
{
    class Connection
    {
        
        string dbserver = Properties.Settings.Default.server;
        string dbpassword = Properties.Settings.Default.password;
        string dbdatabase = Properties.Settings.Default.database;
        string ConString = "Data Source="+ Properties.Settings.Default.server + ";Initial Catalog="+ Properties.Settings.Default.database + ";User ID="+ Properties.Settings.Default.username + ";Password="+ Properties.Settings.Default.password + "";
        SqlConnection conn;

        public void OpenConn()
        {
            conn = new SqlConnection(ConString);
            conn.Open();
        }

        public void CloseConn()
        {
            conn.Close();
        }

        public void ExecuteQuery(string Query)
        {
            SqlCommand cmd = new SqlCommand(Query, conn);
            cmd.ExecuteNonQuery();
        }

        public SqlDataReader DataReader(string Query)
        {
            SqlCommand cmd = new SqlCommand(Query, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public object ShowDataDGV(string Query)
        {
            SqlDataAdapter da = new SqlDataAdapter(Query, ConString);
            DataSet ds = new DataSet();
            da.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;

        }
    }
}
