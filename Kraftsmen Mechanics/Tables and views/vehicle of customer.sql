USE [service]
GO

/****** Object:  View [dbo].[Vehicles_of_customer]    Script Date: 2/25/2021 12:11:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Vehicles_of_customer] as 

SELECT a.customer_vehicle_id AS ID, a.customer_id AS [Customer ID], d.auto_make_desc AS Auto, c.model AS [Vehicle Model], e.size_desc AS Size, a.plate_number AS Platenumber, a.color
FROM     dbo.customer_vehicle AS a LEFT OUTER JOIN
                  dbo.customer AS b ON a.customer_id = b.customer_id LEFT OUTER JOIN
                  dbo.vehicle AS c ON a.vehicle_id = c.vehicle_id LEFT OUTER JOIN
                  dbo.auto_make AS d ON c.auto_make_id = d.auto_make_id LEFT OUTER JOIN
                  dbo.size AS e ON c.size = e.size_id
GO

