USE [service]
GO

/****** Object:  Table [dbo].[draft_order]    Script Date: 3/2/2021 5:15:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[draft_order](
	[draft_order_id] [int] IDENTITY(1,1) NOT NULL,
	[job_estimate_number] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[price] [decimal](11, 2) NULL,
	[quantity] [int] NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
	[uom_id] [int] NULL,
	[discount_id] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[bom_code] [varchar](30) NULL,
 CONSTRAINT [PK_draft_order] PRIMARY KEY CLUSTERED 
(
	[draft_order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

