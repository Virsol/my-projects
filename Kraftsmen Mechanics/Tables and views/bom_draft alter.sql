USE [service]
GO

/****** Object:  Table [dbo].[bom_draft]    Script Date: 2/26/2021 5:25:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom_draft](
	[bom_code] [varchar](20) NULL,
	[inventory_code] [varchar](30) NULL,
	[item_quantity] [int] NULL,
	[P/C] [int] NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO

