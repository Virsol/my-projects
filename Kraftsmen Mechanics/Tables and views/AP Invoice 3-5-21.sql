USE [service]
GO

/****** Object:  Table [dbo].[ap_invoice]    Script Date: 3/5/2021 4:29:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ap_invoice](
	[ap_invoice_id] [int] IDENTITY(1,1) NOT NULL,
	[ap_invoice_doc_num] [varchar](30) NULL,
	[grpo_docnum] [varchar](30) NULL,
	[supplier_code] [varchar](30) NULL,
	[contact_person_id] [int] NULL,
	[date_posted] [date] NULL,
	[date_created] [date] NULL,
	[date_upload] [date] NULL,
	[employee_id] [int] NULL,
	[status_id] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[remaining_balance] [decimal](11, 2) NULL,
	[Installment] [int] NULL,
 CONSTRAINT [PK_ap_invoice] PRIMARY KEY CLUSTERED 
(
	[ap_invoice_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

