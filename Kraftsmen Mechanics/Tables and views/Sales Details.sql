USE [service]
GO

/****** Object:  View [dbo].[sales_invoice_Details_view]    Script Date: 2/18/2021 5:14:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[sales_invoice_Details_view] as
Select 
a.sales_invoice_doc_num 'Document Number',
b.inventory_name 'Item/Service Name',
a.quantity 'Quantity',
a.price 'Price',
tax_price 'Tax Price',
a.total_price 'Total Price'

from

sales_invoice_body a 
left join inventory b on a.inventory_code = b.inventory_code
GO

