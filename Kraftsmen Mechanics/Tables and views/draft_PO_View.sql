USE [service]
GO

/****** Object:  View [dbo].[draft_PO_view]    Script Date: 2/16/2021 11:11:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[draft_PO_view] as

SELECT a.po_body_id AS ID, a.inventory_code AS [Item Code], b.inventory_name AS [Item Name], b.inventory_description AS [Item Description], a.quantity, FORMAT(a.price, '#,#.00') AS Price, d.uom_desc AS UOM, c.tax_desc AS Tax, 
                 CONCAT(c.tax_rate, '%')  AS [Tax Rate], a.tax_price AS Vat, a.discount, FORMAT(a.total_price, '#,#.00') AS [Total Price], a.po_document_number 'PO Document Number'
FROM     dbo.draft_po AS a LEFT OUTER JOIN
                  dbo.inventory AS b ON a.inventory_code = b.inventory_code LEFT OUTER JOIN
                  dbo.tax AS c ON a.tax_id = c.tax_id LEFT OUTER JOIN
                  dbo.uom AS d ON a.uom_id = d.uom_id
GO

