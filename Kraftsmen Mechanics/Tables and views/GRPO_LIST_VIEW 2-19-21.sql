USE [service]
GO

/****** Object:  View [dbo].[GRPO_LIST_VIEW]    Script Date: 2/19/2021 4:00:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[GRPO_LIST_VIEW] as

SELECT CASE WHEN
                      (SELECT a.[on/off]
                       FROM      document_series a LEFT JOIN
                                         Document_tag b ON a.doc_tag_id = b.doc_tag_id
                       WHERE   doc_tag_desc = 'GRPO' OR
                                         doc_tag_desc = 'Goods Receipt PO') = 1 THEN CONCAT
                      ((SELECT doc_tag_desc
                        FROM      document_series a LEFT JOIN
                                          Document_tag b ON a.doc_tag_id = b.doc_tag_id
                        WHERE   doc_tag_desc = 'GRPO' OR
                                          doc_tag_desc = 'Goods Receipt PO'), '-', a.grpo_doc_num) WHEN
                      (SELECT a.[on/off]
                       FROM      document_series a LEFT JOIN
                                         Document_tag b ON a.doc_tag_id = b.doc_tag_id
                       WHERE   doc_tag_desc = 'GRPO' OR
                                         doc_tag_desc = 'Goods Receipt PO') = 0 THEN a.grpo_doc_num END AS [GRPO Document], b.name AS Supplier, a.date_created AS [Date Created], a.date_posted AS [Date Posted], a.date_upload AS [Date Upload], 
                  a.total_price AS [Total Price],
				  d.status_desc 'Status'
FROM     dbo.grpo AS a LEFT OUTER JOIN
                  dbo.supplier AS b ON a.supplier_code = b.supplier_code LEFT OUTER JOIN
                  dbo.contact_person AS c ON a.contact_person_id = c.contact_person_id LEFT OUTER JOIN
                  dbo.status AS d ON a.status_id = d.status_id
GO

