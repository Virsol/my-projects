USE [service]
GO

/****** Object:  Table [dbo].[document_series]    Script Date: 2/17/2021 4:41:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[document_series](
	[series_id] [int] IDENTITY(1,1) NOT NULL,
	[series_desc] [varchar](30) NULL,
	[doc_tag_id] [int] NULL,
	[on/off] [int] NULL,
 CONSTRAINT [PK_document_series] PRIMARY KEY CLUSTERED 
(
	[series_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[document_series] ADD  CONSTRAINT [DF_document_series_on/off]  DEFAULT ((0)) FOR [on/off]
GO

