USE [service]
GO

/****** Object:  View [dbo].[Ap_Inv_list_view]    Script Date: 2/19/2021 3:56:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[Ap_Inv_list_view] as
Select 
Case when
(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Ap Invoice') = 1
then 
concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Ap Invoice'),'-',a.ap_invoice_doc_num)
else
a.grpo_docnum
end as 'Document Number',
b.name 'Supplier',
format(a.date_created, 'MMMM dd, yyyy') 'Date Created',
a.date_posted 'Date Posted',
a.date_upload 'Date Upload',
a.total_price 'Total Price',
c.status_desc 'Status'

from ap_invoice a
left join supplier b on a.supplier_code = b.supplier_code
left join status c on a.status_id = c.status_id

GO

