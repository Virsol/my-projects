USE [service]
GO

/****** Object:  View [dbo].[Work_progress_view]    Script Date: 2/17/2021 5:39:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Work_progress_view] as 

Select 
a.work_prog_id 'ID',
b.name 'Customer Name',
c.name 'Technician',
f.auto_make_desc 'Auto Make',
d.model 'Model',
g.size_desc 'Size',
case when
(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Job ORDER') = 1

then 
Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Job ORDER'),'-',a.job_number) 
else
a.job_number
end as
'Job Number',
a.start_date 'Start Date',
e.status_desc 'Status'


from 

work_progress a 
left join customer b on a.customer_id = b.customer_id
left join employee c on a.employee_id = c.employee_id
left join vehicle d on a.car_id = d.vehicle_id
left join status e on a.status = e.status_id
left join auto_make f on d.auto_make_id = f.auto_make_id
left join size g on d.size = g.size_id
where status_desc = 'ONGOING'
GO

