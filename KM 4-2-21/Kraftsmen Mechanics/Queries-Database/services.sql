USE [master]
GO
/****** Object:  Database [service]    Script Date: 1/14/2021 10:25:43 PM ******/
CREATE DATABASE [service]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'service', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.VIRSOL_DRIN\MSSQL\DATA\service.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'service_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.VIRSOL_DRIN\MSSQL\DATA\service_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [service] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [service].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [service] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [service] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [service] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [service] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [service] SET ARITHABORT OFF 
GO
ALTER DATABASE [service] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [service] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [service] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [service] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [service] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [service] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [service] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [service] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [service] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [service] SET  DISABLE_BROKER 
GO
ALTER DATABASE [service] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [service] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [service] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [service] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [service] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [service] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [service] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [service] SET RECOVERY FULL 
GO
ALTER DATABASE [service] SET  MULTI_USER 
GO
ALTER DATABASE [service] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [service] SET DB_CHAINING OFF 
GO
ALTER DATABASE [service] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [service] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [service] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'service', N'ON'
GO
ALTER DATABASE [service] SET QUERY_STORE = OFF
GO
USE [service]
GO
/****** Object:  Table [dbo].[ap_inv_body]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ap_inv_body](
	[ap_inv_body_id] [int] IDENTITY(1,1) NOT NULL,
	[ap_inv_doc_num] [int] NULL,
	[inventory_code] [varchar](30) NULL,
	[quantity] [int] NULL,
	[uom_id] [int] NULL,
	[price] [decimal](11, 2) NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
 CONSTRAINT [PK_ap_inv_body] PRIMARY KEY CLUSTERED 
(
	[ap_inv_body_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ap_invoice]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ap_invoice](
	[ap_invoice_id] [int] IDENTITY(1,1) NOT NULL,
	[ap_invoice_doc_num] [varchar](30) NULL,
	[po_document_number] [varchar](30) NULL,
	[grpo_docnum] [varchar](30) NULL,
	[supplier_code] [int] NULL,
	[contact_person_id] [int] NULL,
	[date_posted] [date] NULL,
	[date_created] [date] NULL,
	[date_upload] [date] NULL,
	[employee_id] [int] NULL,
	[status_id] [nchar](10) NULL,
 CONSTRAINT [PK_ap_invoice] PRIMARY KEY CLUSTERED 
(
	[ap_invoice_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[contact_person]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact_person](
	[contact_person_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](125) NULL,
	[address] [text] NULL,
	[contact_number] [int] NULL,
	[city] [varchar](70) NULL,
	[postal_code] [varchar](20) NULL,
	[email] [varchar](125) NULL,
	[supplier_code] [varchar](20) NULL,
 CONSTRAINT [PK_contact_person] PRIMARY KEY CLUSTERED 
(
	[contact_person_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[customer]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[customer_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](120) NULL,
	[birthdate] [date] NULL,
	[address] [text] NULL,
	[email_add] [varchar](120) NULL,
	[number] [int] NULL,
	[city] [varchar](50) NULL,
	[postal_code] [varchar](20) NULL,
	[height] [varchar](30) NULL,
	[weight] [varchar](30) NULL,
 CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[draft]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[draft](
	[draft_id] [int] IDENTITY(1,1) NOT NULL,
	[po_document_number] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[status_id] [int] NULL,
 CONSTRAINT [PK_draft] PRIMARY KEY CLUSTERED 
(
	[draft_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee](
	[employee_id] [int] NOT NULL,
	[name] [varchar](120) NULL,
	[birthdate] [date] NULL,
	[address] [text] NULL,
	[contact_number] [int] NULL,
	[city] [varchar](30) NULL,
	[postal_code] [varchar](20) NULL,
	[height] [varchar](30) NULL,
	[weight] [varchar](30) NULL,
	[email] [varchar](120) NULL,
	[position] [varchar](75) NULL,
	[userid] [int] NULL,
	[image_id] [int] NULL,
	[status_id] [int] NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grpo]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grpo](
	[grpo_id] [int] IDENTITY(1,1) NOT NULL,
	[grpo_doc_num] [varchar](30) NULL,
	[po_document_number] [varchar](30) NULL,
	[supplier_code] [int] NULL,
	[contact_person_id] [int] NULL,
	[date_posted] [date] NULL,
	[date_created] [date] NULL,
	[date_upload] [nchar](10) NULL,
	[employee_id] [int] NULL,
	[status_id] [int] NULL,
 CONSTRAINT [PK_grpo] PRIMARY KEY CLUSTERED 
(
	[grpo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[grpo_body]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grpo_body](
	[grpo_body_id] [int] IDENTITY(1,1) NOT NULL,
	[grpo_doc_num] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[quantity] [int] NULL,
	[uom_id] [int] NULL,
	[price] [decimal](11, 2) NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
 CONSTRAINT [PK_grpo_body] PRIMARY KEY CLUSTERED 
(
	[grpo_body_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[image]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[image](
	[image_id] [int] IDENTITY(1,1) NOT NULL,
	[image_name] [varchar](50) NULL,
	[image_directory] [text] NULL,
 CONSTRAINT [PK_image] PRIMARY KEY CLUSTERED 
(
	[image_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[inventory]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory](
	[inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_code] [varchar](30) NULL,
	[inventory_name] [varchar](120) NULL,
	[inventory_type] [int] NULL,
	[service_type_id] [int] NULL,
	[quantity] [int] NULL,
	[item_price] [decimal](11, 2) NULL,
	[inventory_status] [int] NULL,
 CONSTRAINT [PK_inventory] PRIMARY KEY CLUSTERED 
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[inventory_type]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_type](
	[inventory_type_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_desc] [varchar](75) NULL,
 CONSTRAINT [PK_inventory_type] PRIMARY KEY CLUSTERED 
(
	[inventory_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_estimate]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_estimate](
	[job_estimate_id] [int] NOT NULL,
	[job_estimate_number] [varchar](30) NULL,
	[customer_id] [int] NULL,
	[car_id] [int] NULL,
	[date] [date] NULL,
	[estimated_by] [int] NULL,
	[status_id] [int] NULL,
 CONSTRAINT [PK_job_estimate] PRIMARY KEY CLUSTERED 
(
	[job_estimate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_estimate_body]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_estimate_body](
	[job_estimate_body] [int] IDENTITY(1,1) NOT NULL,
	[job_estimate_number] [varchar](30) NULL,
	[inventory_code] [int] NULL,
	[price] [decimal](11, 2) NULL,
 CONSTRAINT [PK_job_estimate_body] PRIMARY KEY CLUSTERED 
(
	[job_estimate_body] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_order]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_order](
	[job_order_id] [int] IDENTITY(1,1) NOT NULL,
	[job_number] [varchar](30) NULL,
	[job_estimate_number] [varchar](30) NULL,
	[customer_id] [int] NULL,
	[car_id] [int] NULL,
	[technician] [int] NULL,
	[checked_by] [int] NULL,
	[approved_by] [int] NULL,
	[status_id] [int] NULL,
 CONSTRAINT [PK_job_order] PRIMARY KEY CLUSTERED 
(
	[job_order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_order_body]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_order_body](
	[job_order_body_id] [int] IDENTITY(1,1) NOT NULL,
	[job_number] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[price] [decimal](11, 2) NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
 CONSTRAINT [PK_job_order_body] PRIMARY KEY CLUSTERED 
(
	[job_order_body_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[po_body]    Script Date: 1/14/2021 10:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[po_body](
	[po_body_id] [int] IDENTITY(1,1) NOT NULL,
	[po_document_number] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[quantity] [int] NULL,
	[uom_id] [int] NULL,
	[price] [decimal](11, 2) NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
 CONSTRAINT [PK_po_body] PRIMARY KEY CLUSTERED 
(
	[po_body_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [service] SET  READ_WRITE 
GO
