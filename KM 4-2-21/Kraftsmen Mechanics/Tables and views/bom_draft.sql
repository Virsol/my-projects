USE [service]
GO

/****** Object:  Table [dbo].[bom_draft]    Script Date: 2/25/2021 3:41:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom_draft](
	[service_code] [varchar](20) NULL,
	[item_code] [varchar](20) NULL,
	[item_quantity] [int] NULL,
	[price] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

