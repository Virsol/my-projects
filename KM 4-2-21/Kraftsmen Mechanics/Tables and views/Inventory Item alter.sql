USE [service]
GO

/****** Object:  View [dbo].[Inventory_Item]    Script Date: 2/24/2021 4:44:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Inventory_Item] as

Select  
a.inventory_code 'Item Code',
a.inventory_name 'Item Name',
a.inventory_description 'Item Description',
a.quantity 'Quantity',
a.item_price 'Item Price',
c.status_desc 'Status',
b.uom_desc 'UOM',
case when quantity < 50 then 'Critical'
else
'Stable'
end as 'Critical Level'

from inventory a 
left join uom b on a.uom_id = b.uom_id
left join status c on a.inventory_status = c.status_id
where inventory_type = 1
GO

