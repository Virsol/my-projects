USE [service]
GO

/****** Object:  View [dbo].[bom_parent]    Script Date: 3/1/2021 5:58:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[bom_parent] as
Select 
a.bom_code 'BOM Code',
a.inventory_code 'Service Code',
b.inventory_name 'Service Name',
c.status_desc 'Status'

from bom a
left join inventory b on a.inventory_code = b.inventory_code
left join status c on a.status = c.status_id
where a.[P/C] = 1
GO

