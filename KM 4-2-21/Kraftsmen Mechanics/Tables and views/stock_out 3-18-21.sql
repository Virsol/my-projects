USE [service]
GO

/****** Object:  Table [dbo].[stock_out]    Script Date: 3/18/2021 10:52:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stock_out](
	[stock_out_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_code] [varchar](30) NULL,
	[quantity] [int] NULL,
	[date] [date] NULL,
	[used_quantity] [int] NULL,
	[quantity_bt] [int] NULL,
	[job_number] [varchar](30) NULL,
 CONSTRAINT [PK_stock_out] PRIMARY KEY CLUSTERED 
(
	[stock_out_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

