USE [service]
GO

/****** Object:  Table [dbo].[customer_vehicle]    Script Date: 2/25/2021 4:30:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[customer_vehicle](
	[customer_vehicle_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_id] [int] NULL,
	[vehicle_id] [int] NULL,
	[plate_number] [varchar](20) NULL,
	[color] [varchar](30) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_customer_vehicle] PRIMARY KEY CLUSTERED 
(
	[customer_vehicle_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

