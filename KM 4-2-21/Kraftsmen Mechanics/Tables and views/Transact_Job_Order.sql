USE [service]
GO

/****** Object:  View [dbo].[Transact_Job_Order]    Script Date: 3/13/2021 7:34:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[Transact_Job_Order] as
Select 
a.job_number 'Job Number',
b.name 'Customer',
a.date 'Date',
c.name 'Approved By',
d.name 'Technician',
a.total_price 'Total Price',
e.status_desc 'Status'
from 
Job_order a left join customer b on a.customer_id = b.customer_id
left join employee c on a.approved_by = c.employee_id
left join employee d on a.technician = d.employee_id
left join status e on a.status_id = e.status_id
GO

