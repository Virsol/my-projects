USE [service]
GO

/****** Object:  View [dbo].[Job_estimate_view]    Script Date: 2/11/2021 4:46:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Job_estimate_view] as

Select
a.job_estimate_number 'Job Estimate Number',
b.inventory_name 'Item/Service Description',
a.quantity 'Quantity',
a.price 'Price',
c.tax_desc 'TAX',
a.tax_price 'Tax Price',
a.total_price 'Total Price'
from 

job_estimate_body a 
left join inventory b on a.inventory_code = b.inventory_code
left join tax c on a.tax_id = c.tax_id 
GO

