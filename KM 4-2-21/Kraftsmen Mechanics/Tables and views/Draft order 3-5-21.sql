USE [service]
GO

/****** Object:  View [dbo].[draft_order_view]    Script Date: 3/5/2021 2:24:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[draft_order_view] as

SELECT a.inventory_code AS [Service/Item Code], b.inventory_name AS [Service/Item Name], b.inventory_description AS [Service/Item Description], a.quantity, format(a.price, '#,#.00') AS Price, d.uom_desc AS UOM, c.tax_desc AS Tax, 
                  CONCAT(c.tax_rate, '%') AS [Tax Rate], a.tax_price AS Vat, FORMAT(a.total_price, '#,#.00') AS [Total Price], a.draft_order_id AS ID,a.bom_code 'BOM CODE'
FROM     dbo.draft_order AS a LEFT OUTER JOIN
                  dbo.inventory AS b ON a.inventory_code = b.inventory_code LEFT OUTER JOIN
                  dbo.tax AS c ON a.tax_id = c.tax_id LEFT OUTER JOIN
                  dbo.uom AS d ON a.uom_id = d.uom_id
GO

