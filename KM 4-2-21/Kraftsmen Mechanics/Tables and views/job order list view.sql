USE [service]
GO

/****** Object:  View [dbo].[Job_order_list_view]    Script Date: 2/17/2021 6:02:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Job_order_list_view] as

Select
a.job_number 'Job Number',

b.name 'Customer',
a.date 'Date',
c.name 'Approved By',
d.name 'Technician',
a.total_price 'Total Price',
e.status_desc

from


job_order a 
left join customer b on a.customer_id = b.customer_id
left join employee c on a.approved_by = c.employee_id
left join employee d on a.technician = d.employee_id
left join status e on a.status_id = e.status_id

where e.status_desc = 'OPEN'
GO

