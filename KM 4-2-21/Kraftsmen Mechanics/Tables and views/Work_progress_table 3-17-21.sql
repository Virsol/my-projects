USE [service]
GO

/****** Object:  Table [dbo].[work_progress]    Script Date: 3/17/2021 4:03:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[work_progress](
	[work_prog_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_id] [int] NULL,
	[employee_id] [int] NULL,
	[car_id] [int] NULL,
	[job_number] [varchar](30) NULL,
	[start_date] [date] NULL,
	[comment] [text] NULL,
	[status] [int] NULL,
	[time_started] [time](0) NULL,
	[time_end] [time](0) NULL,
	[end_date] [date] NULL,
 CONSTRAINT [PK_work_progress] PRIMARY KEY CLUSTERED 
(
	[work_prog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

