USE [service]
GO

/****** Object:  View [dbo].[transact_Job_Estimate]    Script Date: 3/13/2021 7:34:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[transact_Job_Estimate] as
Select 
case when 
(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Estimate')  = 1 then
CONCAT((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Estimate'),' - ',a.job_estimate_number)
else a.job_estimate_number end as 'Job Estimate Number',
b.name 'Customer Name',
a.date 'Date',
a.total_price 'Total Price',
c.name 'Estimated By',
d.status_desc 'Status'


from 
job_estimate a left join customer b on a.customer_id = b.customer_id
left join employee c on a.estimated_by = c.employee_id
left join status d on a.status_id = d.status_id
GO

