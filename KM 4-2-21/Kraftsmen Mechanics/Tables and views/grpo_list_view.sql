USE [service]
GO

/****** Object:  View [dbo].[GRPO_LIST_VIEW]    Script Date: 2/17/2021 4:15:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[GRPO_LIST_VIEW] as

Select 
case when (select a.[on/off] from document_series a 
left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO' or doc_tag_desc = 'Goods Receipt PO' ) = 1
then
CONCAT((Select doc_tag_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO' or doc_tag_desc = 'Goods Receipt PO'),'-',a.grpo_doc_num) 
when (select a.[on/off] from document_series a 
left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO' or doc_tag_desc = 'Goods Receipt PO' ) = 0
then a.grpo_doc_num
end as'GRPO Document',
b.name 'Supplier',
a.date_created 'Date Created',
a.date_posted 'Date Posted',
a.date_upload 'Date Upload',
a.total_price 'Total Price'




from

grpo a left join supplier b on a.supplier_code = b.supplier_code
left join contact_person c on a.contact_person_id = c.contact_person_id
left join status d on a.status_id = d.status_id

where d.status_desc = 'OPEN'
GO

