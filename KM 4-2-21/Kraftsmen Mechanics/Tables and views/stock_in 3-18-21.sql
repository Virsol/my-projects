USE [service]
GO

/****** Object:  Table [dbo].[stock_in]    Script Date: 3/18/2021 10:52:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stock_in](
	[stock_in_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_code] [varchar](30) NULL,
	[quantity] [int] NULL,
	[date_of_delivered] [date] NULL,
	[quantity_of_added_stock] [int] NULL,
	[grpo_doc_num] [varchar](30) NULL,
	[quantity_bt] [int] NULL,
 CONSTRAINT [PK_stock_in] PRIMARY KEY CLUSTERED 
(
	[stock_in_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

