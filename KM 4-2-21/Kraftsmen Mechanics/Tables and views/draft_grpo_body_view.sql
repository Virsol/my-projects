USE [service]
GO

/****** Object:  View [dbo].[draft_GRPO_view]    Script Date: 2/15/2021 2:50:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[draft_GRPO_view] as

SELECT a.draft_grpo_body_id 'ID',
a.inventory_code AS [Item Code], b.inventory_name AS [Item Name], b.inventory_description AS [Item Description], a.quantity, FORMAT(a.price, '#,#.00') AS Price, d.uom_desc AS UOM, c.tax_desc AS Tax,  CONCAT(c.tax_rate, '%') 
               AS [Tax Rate], a.tax_price AS Vat, a.discount, FORMAT(a.total_price, '#,#.00') AS [Total Price]
FROM     dbo.draft_grpo AS a LEFT OUTER JOIN
                  dbo.inventory AS b ON a.inventory_code = b.inventory_code LEFT OUTER JOIN
                  dbo.tax AS c ON a.tax_id = c.tax_id LEFT OUTER JOIN
                  dbo.uom AS d ON a.uom_id = d.uom_id
GO

