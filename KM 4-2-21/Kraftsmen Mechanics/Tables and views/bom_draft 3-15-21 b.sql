USE [service]
GO

/****** Object:  Table [dbo].[bom_draft]    Script Date: 3/15/2021 5:02:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom_draft](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[bom_code] [varchar](20) NULL,
	[inventory_code] [varchar](30) NULL,
	[item_quantity] [int] NULL,
	[P/C] [int] NULL,
	[status] [int] NULL,
	[total_tax] [decimal](11, 2) NULL,
	[total_price] [decimal](11, 2) NULL,
	[price_pl] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

