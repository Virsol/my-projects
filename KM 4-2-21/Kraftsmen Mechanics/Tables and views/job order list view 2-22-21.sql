USE [service]
GO

/****** Object:  View [dbo].[Job_order_list_view]    Script Date: 2/22/2021 10:00:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Job_order_list_view] as
SELECT distinct a.job_number AS [Job Number], b.name AS Customer, a.date, c.name AS [Approved By], d.name AS Technician, a.total_price AS [Total Price], e.status_desc
FROM     dbo.job_order AS a LEFT OUTER JOIN
                  dbo.customer AS b ON a.customer_id = b.customer_id LEFT OUTER JOIN
                  dbo.employee AS c ON a.approved_by = c.employee_id LEFT OUTER JOIN
                  dbo.employee AS d ON a.technician = d.employee_id LEFT OUTER JOIN
                  dbo.status AS e ON a.status_id = e.status_id left OUTER JOIN
                  dbo.work_progress AS f ON a.job_number = f.job_number
WHERE  
(e.status_desc = 'OPEN')AND(f.status = 4)
GO

