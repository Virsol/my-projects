USE [service]
GO

/****** Object:  View [dbo].[Bom_child]    Script Date: 2/26/2021 5:32:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Bom_child] as
Select
a.inventory_code 'Item Code',
b.inventory_name 'Item Name',
b.inventory_description 'Item Description',
a.item_quantity 'Quantity'



from bom_draft a left join inventory b  on a.inventory_code = b.inventory_code
GO

