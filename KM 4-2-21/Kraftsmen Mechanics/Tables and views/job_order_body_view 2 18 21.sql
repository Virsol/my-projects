USE [service]
GO

/****** Object:  View [dbo].[Job_order_body_view]    Script Date: 2/18/2021 6:38:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Job_order_body_view] as

SELECT a.job_number AS [Job Number], b.inventory_name AS [Item/Service Name], a.quantity,a.price 'Price' ,c.tax_desc AS TAX, a.tax_price AS [Tax Price], a.total_price AS [Total Price]
FROM     dbo.job_order_body AS a LEFT OUTER JOIN
                  dbo.inventory AS b ON a.inventory_code = b.inventory_code LEFT OUTER JOIN
                  dbo.tax AS c ON c.tax_id = a.tax_id
GO

