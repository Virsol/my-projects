USE [service]
GO

/****** Object:  Table [dbo].[bom_per_vehicle]    Script Date: 3/22/2021 10:09:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom_per_vehicle](
	[bom_code] [varchar](30) NULL,
	[vehicle_id] [int] NULL
) ON [PRIMARY]
GO

