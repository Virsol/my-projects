USE [service]
GO

/****** Object:  Table [dbo].[bom_draft]    Script Date: 2/26/2021 1:21:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom_draft](
	[bom_code] [varchar](20) NULL,
	[inventory_code] [varchar](30) NULL,
	[item_quantity] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[total_tax] [decimal](11, 2) NULL,
	[P/C] [int] NULL
) ON [PRIMARY]
GO

