USE [service]
GO

/****** Object:  View [dbo].[List_of_Vehicle]    Script Date: 2/24/2021 3:48:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[List_of_Vehicle] as

Select 
a.vehicle_id 'ID',
b.auto_make_desc 'Auto Make',
a.model 'Model',
c.size_desc 'Size',
d.vehicle_cat_desc 'Vehicle Category'

from 

vehicle a 

left join auto_make b on a.auto_make_id = b.auto_make_id
left join size c on a.size = c.size_id 
left join vehicle_category d on a.vehicle_cat_id = d.vehicle_cat_id
GO

