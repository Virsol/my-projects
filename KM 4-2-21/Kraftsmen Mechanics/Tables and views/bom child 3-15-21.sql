USE [service]
GO

/****** Object:  View [dbo].[Bom_child]    Script Date: 3/15/2021 4:40:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Bom_child] as

SELECT a.ID 'ID',a.inventory_code AS [Item Code], b.inventory_name AS [Item Name], b.inventory_description AS [Item Description], a.item_quantity AS Quantity
FROM     dbo.bom_draft AS a LEFT OUTER JOIN
                  dbo.inventory AS b ON a.inventory_code = b.inventory_code
GO

