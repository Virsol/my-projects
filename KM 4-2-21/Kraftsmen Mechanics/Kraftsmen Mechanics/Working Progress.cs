﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Working_Progress : Form
    {
        DBConnection conn = new DBConnection();
        public Working_Progress()
        {
            InitializeComponent();
        }

        private void Working_Progress_Load(object sender, EventArgs e)
        {
            refreshDgvWorkingProgress();
        }
        private void refreshDgvWorkingProgress()
        {
            
            conn.OpenConnection();
            dgvWorkingProgress.DataSource = conn.ShowDataInGridView("select * from work_progress_view where [Customer Name] like '%"+txtSearch.Text+"%' or [Job Number] like '%"+txtSearch.Text+"%'");
            conn.CloseConnection();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvWorkingProgress_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = Int32.Parse(dgvWorkingProgress.CurrentRow.Cells[0].Value.ToString());
            string time_end = DateTime.Now.ToString("HH:mm:ss");
            string dateNow = DateTime.Now.ToString("yyyy-MM-dd");
            if (MessageBox.Show("Is this work progress done?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                conn.OpenConnection();
                conn.ExecuteQueries("update work_progress set status = 4, time_end = '"+time_end+"' ,end_date = '"+dateNow+"' where work_prog_id = '" + id + "'");
                conn.CloseConnection();
                refreshDgvWorkingProgress();
            }
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            refreshDgvWorkingProgress();
        }
    }
}
