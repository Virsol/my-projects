﻿
namespace Kraftsmen_Mechanics
{
    partial class Job_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Job_Order));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.txtVehicleSize = new System.Windows.Forms.TextBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboVehicleModel = new System.Windows.Forms.ComboBox();
            this.cboEstimatedBy = new System.Windows.Forms.ComboBox();
            this.cboTechnicianName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtVehicleColor = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPlateNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtVehicleMake = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDateCreated = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDocumentSeries = new System.Windows.Forms.Label();
            this.lblDocumentNumber = new System.Windows.Forms.Label();
            this.btnViewJobEstimateList = new System.Windows.Forms.Button();
            this.dgvServiceList = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.Label();
            this.btnDecline = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdoWithoutMaterial = new System.Windows.Forms.RadioButton();
            this.rdoWithMaterial = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblTotalItem = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTotalPaymentDue = new System.Windows.Forms.Label();
            this.lblTaxValue = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnAddNewService = new System.Windows.Forms.Button();
            this.dgvServiceOrderList = new System.Windows.Forms.DataGridView();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboCategory = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtContactNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceList)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceOrderList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSearchBy.BackColor = System.Drawing.Color.White;
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Location = new System.Drawing.Point(109, 49);
            this.cboSearchBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(143, 26);
            this.cboSearchBy.TabIndex = 12;
            this.cboSearchBy.SelectedIndexChanged += new System.EventHandler(this.cboSearchBy_SelectedIndexChanged_1);
            // 
            // txtVehicleSize
            // 
            this.txtVehicleSize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVehicleSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtVehicleSize.Location = new System.Drawing.Point(157, 130);
            this.txtVehicleSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVehicleSize.Name = "txtVehicleSize";
            this.txtVehicleSize.ReadOnly = true;
            this.txtVehicleSize.Size = new System.Drawing.Size(193, 24);
            this.txtVehicleSize.TabIndex = 6;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSearch.Location = new System.Drawing.Point(259, 52);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(356, 24);
            this.txtSearch.TabIndex = 13;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged_1);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label12.Location = new System.Drawing.Point(39, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 18);
            this.label12.TabIndex = 22;
            this.label12.Text = "Search:";
            // 
            // cboVehicleModel
            // 
            this.cboVehicleModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboVehicleModel.BackColor = System.Drawing.Color.White;
            this.cboVehicleModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVehicleModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboVehicleModel.FormattingEnabled = true;
            this.cboVehicleModel.Location = new System.Drawing.Point(157, 42);
            this.cboVehicleModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboVehicleModel.Name = "cboVehicleModel";
            this.cboVehicleModel.Size = new System.Drawing.Size(433, 26);
            this.cboVehicleModel.TabIndex = 4;
            this.cboVehicleModel.SelectedIndexChanged += new System.EventHandler(this.cboVehicleModel_SelectedIndexChanged_2);
            // 
            // cboEstimatedBy
            // 
            this.cboEstimatedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboEstimatedBy.BackColor = System.Drawing.Color.White;
            this.cboEstimatedBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEstimatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboEstimatedBy.FormattingEnabled = true;
            this.cboEstimatedBy.Location = new System.Drawing.Point(128, 166);
            this.cboEstimatedBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboEstimatedBy.Name = "cboEstimatedBy";
            this.cboEstimatedBy.Size = new System.Drawing.Size(400, 26);
            this.cboEstimatedBy.TabIndex = 10;
            // 
            // cboTechnicianName
            // 
            this.cboTechnicianName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTechnicianName.BackColor = System.Drawing.Color.White;
            this.cboTechnicianName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTechnicianName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboTechnicianName.FormattingEnabled = true;
            this.cboTechnicianName.Location = new System.Drawing.Point(128, 210);
            this.cboTechnicianName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboTechnicianName.Name = "cboTechnicianName";
            this.cboTechnicianName.Size = new System.Drawing.Size(400, 26);
            this.cboTechnicianName.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label11.Location = new System.Drawing.Point(25, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Technician :";
            // 
            // txtVehicleColor
            // 
            this.txtVehicleColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVehicleColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtVehicleColor.Location = new System.Drawing.Point(157, 172);
            this.txtVehicleColor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVehicleColor.Name = "txtVehicleColor";
            this.txtVehicleColor.ReadOnly = true;
            this.txtVehicleColor.Size = new System.Drawing.Size(433, 24);
            this.txtVehicleColor.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(39, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 18);
            this.label9.TabIndex = 15;
            this.label9.Text = "Vehicle Color:";
            // 
            // txtPlateNumber
            // 
            this.txtPlateNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPlateNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtPlateNumber.Location = new System.Drawing.Point(157, 214);
            this.txtPlateNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPlateNumber.Name = "txtPlateNumber";
            this.txtPlateNumber.ReadOnly = true;
            this.txtPlateNumber.Size = new System.Drawing.Size(433, 24);
            this.txtPlateNumber.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(45, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 11;
            this.label7.Text = "Vehicle Size:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(36, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 18);
            this.label8.TabIndex = 12;
            this.label8.Text = "Plate Number:";
            // 
            // txtVehicleMake
            // 
            this.txtVehicleMake.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVehicleMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtVehicleMake.Location = new System.Drawing.Point(157, 86);
            this.txtVehicleMake.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVehicleMake.Name = "txtVehicleMake";
            this.txtVehicleMake.ReadOnly = true;
            this.txtVehicleMake.Size = new System.Drawing.Size(433, 24);
            this.txtVehicleMake.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(39, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Vehicle Make:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(15, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "Estimated By:";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.White;
            this.groupBox5.Controls.Add(this.cboEstimatedBy);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.dtpDateCreated);
            this.groupBox5.Controls.Add(this.groupBox3);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.cboTechnicianName);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox5.Location = new System.Drawing.Point(1257, 60);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(557, 257);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Document Details";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(13, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date Created:";
            // 
            // dtpDateCreated
            // 
            this.dtpDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDateCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpDateCreated.Location = new System.Drawing.Point(128, 126);
            this.dtpDateCreated.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpDateCreated.Name = "dtpDateCreated";
            this.dtpDateCreated.Size = new System.Drawing.Size(400, 23);
            this.dtpDateCreated.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lblDocumentSeries);
            this.groupBox3.Controls.Add(this.lblDocumentNumber);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox3.Location = new System.Drawing.Point(39, 33);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(491, 73);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Document No.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label5.Location = new System.Drawing.Point(244, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 26);
            this.label5.TabIndex = 2;
            this.label5.Text = "-";
            // 
            // lblDocumentSeries
            // 
            this.lblDocumentSeries.AutoSize = true;
            this.lblDocumentSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.lblDocumentSeries.Location = new System.Drawing.Point(177, 28);
            this.lblDocumentSeries.Name = "lblDocumentSeries";
            this.lblDocumentSeries.Size = new System.Drawing.Size(56, 26);
            this.lblDocumentSeries.TabIndex = 1;
            this.lblDocumentSeries.Text = "xxxx";
            // 
            // lblDocumentNumber
            // 
            this.lblDocumentNumber.AutoSize = true;
            this.lblDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.lblDocumentNumber.Location = new System.Drawing.Point(271, 30);
            this.lblDocumentNumber.Name = "lblDocumentNumber";
            this.lblDocumentNumber.Size = new System.Drawing.Size(60, 26);
            this.lblDocumentNumber.TabIndex = 0;
            this.lblDocumentNumber.Text = "0000";
            // 
            // btnViewJobEstimateList
            // 
            this.btnViewJobEstimateList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewJobEstimateList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(207)))));
            this.btnViewJobEstimateList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewJobEstimateList.FlatAppearance.BorderSize = 0;
            this.btnViewJobEstimateList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewJobEstimateList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnViewJobEstimateList.ForeColor = System.Drawing.Color.White;
            this.btnViewJobEstimateList.Image = ((System.Drawing.Image)(resources.GetObject("btnViewJobEstimateList.Image")));
            this.btnViewJobEstimateList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewJobEstimateList.Location = new System.Drawing.Point(1416, 43);
            this.btnViewJobEstimateList.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewJobEstimateList.Name = "btnViewJobEstimateList";
            this.btnViewJobEstimateList.Size = new System.Drawing.Size(219, 37);
            this.btnViewJobEstimateList.TabIndex = 14;
            this.btnViewJobEstimateList.Text = "View Job Estimate List";
            this.btnViewJobEstimateList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnViewJobEstimateList.UseVisualStyleBackColor = false;
            this.btnViewJobEstimateList.Click += new System.EventHandler(this.btnViewJobEstimateList_Click);
            // 
            // dgvServiceList
            // 
            this.dgvServiceList.AllowUserToAddRows = false;
            this.dgvServiceList.AllowUserToDeleteRows = false;
            this.dgvServiceList.AllowUserToResizeColumns = false;
            this.dgvServiceList.AllowUserToResizeRows = false;
            this.dgvServiceList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvServiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvServiceList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvServiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvServiceList.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvServiceList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvServiceList.Location = new System.Drawing.Point(43, 89);
            this.dgvServiceList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvServiceList.MultiSelect = false;
            this.dgvServiceList.Name = "dgvServiceList";
            this.dgvServiceList.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceList.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvServiceList.RowHeadersWidth = 51;
            this.dgvServiceList.RowTemplate.Height = 24;
            this.dgvServiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServiceList.Size = new System.Drawing.Size(919, 474);
            this.dgvServiceList.TabIndex = 16;
            this.dgvServiceList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServiceList_CellContentClick_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(83, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 20);
            this.label4.TabIndex = 22;
            this.label4.Text = "Address:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVehicleModel.AutoSize = true;
            this.txtVehicleModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtVehicleModel.Location = new System.Drawing.Point(33, 46);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.Size = new System.Drawing.Size(104, 18);
            this.txtVehicleModel.TabIndex = 8;
            this.txtVehicleModel.Text = "Vehicle Model:";
            // 
            // btnDecline
            // 
            this.btnDecline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDecline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(66)))));
            this.btnDecline.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDecline.FlatAppearance.BorderSize = 0;
            this.btnDecline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDecline.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnDecline.ForeColor = System.Drawing.Color.White;
            this.btnDecline.Image = ((System.Drawing.Image)(resources.GetObject("btnDecline.Image")));
            this.btnDecline.Location = new System.Drawing.Point(1611, 575);
            this.btnDecline.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDecline.Name = "btnDecline";
            this.btnDecline.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.btnDecline.Size = new System.Drawing.Size(161, 39);
            this.btnDecline.TabIndex = 20;
            this.btnDecline.Text = "Decline";
            this.btnDecline.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDecline.UseVisualStyleBackColor = false;
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnAccept.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Image = ((System.Drawing.Image)(resources.GetObject("btnAccept.Image")));
            this.btnAccept.Location = new System.Drawing.Point(1427, 575);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.btnAccept.Size = new System.Drawing.Size(161, 39);
            this.btnAccept.TabIndex = 19;
            this.btnAccept.Text = "Accept";
            this.btnAccept.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.rdoWithoutMaterial);
            this.groupBox4.Controls.Add(this.rdoWithMaterial);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.btnCancel);
            this.groupBox4.Controls.Add(this.lblTotalItem);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.lblTotalPaymentDue);
            this.groupBox4.Controls.Add(this.lblTaxValue);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.btnPrint);
            this.groupBox4.Controls.Add(this.btnAddNewService);
            this.groupBox4.Controls.Add(this.btnViewJobEstimateList);
            this.groupBox4.Controls.Add(this.btnDecline);
            this.groupBox4.Controls.Add(this.btnAccept);
            this.groupBox4.Controls.Add(this.dgvServiceOrderList);
            this.groupBox4.Controls.Add(this.cboSearchBy);
            this.groupBox4.Controls.Add(this.txtSearch);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.dgvServiceList);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox4.Location = new System.Drawing.Point(15, 321);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(1800, 634);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Job Order Details";
            // 
            // rdoWithoutMaterial
            // 
            this.rdoWithoutMaterial.AutoSize = true;
            this.rdoWithoutMaterial.Enabled = false;
            this.rdoWithoutMaterial.Location = new System.Drawing.Point(909, 53);
            this.rdoWithoutMaterial.Margin = new System.Windows.Forms.Padding(4);
            this.rdoWithoutMaterial.Name = "rdoWithoutMaterial";
            this.rdoWithoutMaterial.Size = new System.Drawing.Size(49, 22);
            this.rdoWithoutMaterial.TabIndex = 48;
            this.rdoWithoutMaterial.TabStop = true;
            this.rdoWithoutMaterial.Text = "No";
            this.rdoWithoutMaterial.UseVisualStyleBackColor = true;
            // 
            // rdoWithMaterial
            // 
            this.rdoWithMaterial.AutoSize = true;
            this.rdoWithMaterial.Enabled = false;
            this.rdoWithMaterial.Location = new System.Drawing.Point(841, 52);
            this.rdoWithMaterial.Margin = new System.Windows.Forms.Padding(4);
            this.rdoWithMaterial.Name = "rdoWithMaterial";
            this.rdoWithMaterial.Size = new System.Drawing.Size(54, 22);
            this.rdoWithMaterial.TabIndex = 47;
            this.rdoWithMaterial.TabStop = true;
            this.rdoWithMaterial.Text = "Yes";
            this.rdoWithMaterial.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label18.Location = new System.Drawing.Point(655, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(164, 18);
            this.label18.TabIndex = 46;
            this.label18.Text = "Customer with Material:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(66)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(983, 42);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.btnCancel.Size = new System.Drawing.Size(161, 39);
            this.btnCancel.TabIndex = 45;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_2);
            // 
            // lblTotalItem
            // 
            this.lblTotalItem.AutoSize = true;
            this.lblTotalItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblTotalItem.Location = new System.Drawing.Point(1576, 447);
            this.lblTotalItem.Name = "lblTotalItem";
            this.lblTotalItem.Size = new System.Drawing.Size(18, 20);
            this.lblTotalItem.TabIndex = 44;
            this.lblTotalItem.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.Location = new System.Drawing.Point(1203, 447);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 20);
            this.label14.TabIndex = 43;
            this.label14.Text = "Total Items:";
            // 
            // lblTotalPaymentDue
            // 
            this.lblTotalPaymentDue.AutoSize = true;
            this.lblTotalPaymentDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblTotalPaymentDue.Location = new System.Drawing.Point(1575, 513);
            this.lblTotalPaymentDue.Name = "lblTotalPaymentDue";
            this.lblTotalPaymentDue.Size = new System.Drawing.Size(58, 29);
            this.lblTotalPaymentDue.TabIndex = 42;
            this.lblTotalPaymentDue.Text = "0.00";
            // 
            // lblTaxValue
            // 
            this.lblTaxValue.AutoSize = true;
            this.lblTaxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblTaxValue.Location = new System.Drawing.Point(1576, 480);
            this.lblTaxValue.Name = "lblTaxValue";
            this.lblTaxValue.Size = new System.Drawing.Size(40, 20);
            this.lblTaxValue.TabIndex = 41;
            this.lblTaxValue.Text = "0.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label15.Location = new System.Drawing.Point(1079, 513);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(223, 29);
            this.label15.TabIndex = 40;
            this.label15.Text = "Total Payment Due:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label16.Location = new System.Drawing.Point(1216, 480);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 20);
            this.label16.TabIndex = 39;
            this.label16.Text = "Total Tax:";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(167)))), ((int)(((byte)(43)))));
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrint.ForeColor = System.Drawing.Color.White;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(1643, 43);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPrint.Size = new System.Drawing.Size(129, 37);
            this.btnPrint.TabIndex = 38;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            // 
            // btnAddNewService
            // 
            this.btnAddNewService.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnAddNewService.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewService.FlatAppearance.BorderSize = 0;
            this.btnAddNewService.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnAddNewService.ForeColor = System.Drawing.Color.White;
            this.btnAddNewService.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewService.Image")));
            this.btnAddNewService.Location = new System.Drawing.Point(983, 382);
            this.btnAddNewService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddNewService.Name = "btnAddNewService";
            this.btnAddNewService.Size = new System.Drawing.Size(789, 39);
            this.btnAddNewService.TabIndex = 18;
            this.btnAddNewService.Text = " Add New Service / Item";
            this.btnAddNewService.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddNewService.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddNewService.UseVisualStyleBackColor = false;
            this.btnAddNewService.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvServiceOrderList
            // 
            this.dgvServiceOrderList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServiceOrderList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvServiceOrderList.BackgroundColor = System.Drawing.Color.White;
            this.dgvServiceOrderList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServiceOrderList.Location = new System.Drawing.Point(983, 89);
            this.dgvServiceOrderList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvServiceOrderList.MultiSelect = false;
            this.dgvServiceOrderList.Name = "dgvServiceOrderList";
            this.dgvServiceOrderList.ReadOnly = true;
            this.dgvServiceOrderList.RowHeadersWidth = 51;
            this.dgvServiceOrderList.RowTemplate.Height = 24;
            this.dgvServiceOrderList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServiceOrderList.Size = new System.Drawing.Size(789, 288);
            this.dgvServiceOrderList.TabIndex = 17;
            this.dgvServiceOrderList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServiceOrderList_CellContentClick_3);
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtAddress.Location = new System.Drawing.Point(177, 126);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(408, 115);
            this.txtAddress.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.cboCategory);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtVehicleSize);
            this.groupBox2.Controls.Add(this.cboVehicleModel);
            this.groupBox2.Controls.Add(this.txtVehicleColor);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtPlateNumber);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtVehicleMake);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtVehicleModel);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(636, 60);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(616, 256);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vehicle Details";
            // 
            // cboCategory
            // 
            this.cboCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCategory.BackColor = System.Drawing.Color.White;
            this.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.Location = new System.Drawing.Point(440, 129);
            this.cboCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(151, 26);
            this.cboCategory.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label17.Location = new System.Drawing.Point(357, 134);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 18);
            this.label17.TabIndex = 21;
            this.label17.Text = "Category:";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtCustomerName.Location = new System.Drawing.Point(177, 38);
            this.txtCustomerName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(408, 24);
            this.txtCustomerName.TabIndex = 1;
            this.txtCustomerName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtCustomerName_MouseClick);
            this.txtCustomerName.TextChanged += new System.EventHandler(this.txtCustomerName_TextChanged_1);
            // 
            // txtContactNumber
            // 
            this.txtContactNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtContactNumber.Location = new System.Drawing.Point(177, 82);
            this.txtContactNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtContactNumber.Name = "txtContactNumber";
            this.txtContactNumber.Size = new System.Drawing.Size(408, 24);
            this.txtContactNumber.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(53, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Contact No.:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(17, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.txtContactNumber);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(15, 59);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(616, 256);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer Details";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(15, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 25);
            this.label13.TabIndex = 24;
            this.label13.Text = "Job Order";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1848, 52);
            this.panel1.TabIndex = 46;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(1800, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 31);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Job_Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1848, 970);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Job_Order";
            this.Text = "Job Order";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Job_Order_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceList)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceOrderList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboSearchBy;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnViewJobEstimateList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtVehicleModel;
        private System.Windows.Forms.Button btnDecline;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtVehicleSize;
        public System.Windows.Forms.ComboBox cboVehicleModel;
        public System.Windows.Forms.ComboBox cboEstimatedBy;
        public System.Windows.Forms.ComboBox cboTechnicianName;
        public System.Windows.Forms.TextBox txtVehicleColor;
        public System.Windows.Forms.TextBox txtPlateNumber;
        public System.Windows.Forms.TextBox txtVehicleMake;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.DateTimePicker dtpDateCreated;
        public System.Windows.Forms.DataGridView dgvServiceList;
        public System.Windows.Forms.DataGridView dgvServiceOrderList;
        public System.Windows.Forms.TextBox txtAddress;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtCustomerName;
        public System.Windows.Forms.TextBox txtContactNumber;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lblDocumentSeries;
        public System.Windows.Forms.Label lblDocumentNumber;
        private System.Windows.Forms.Label lblTotalItem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblTotalPaymentDue;
        private System.Windows.Forms.Label lblTaxValue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Button btnAddNewService;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuImageButton btnExit;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox cboCategory;
        private System.Windows.Forms.RadioButton rdoWithoutMaterial;
        private System.Windows.Forms.RadioButton rdoWithMaterial;
        private System.Windows.Forms.Label label18;
    }
}