﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Edit_Service : Form
    {
        public Edit_Service()
        {
            InitializeComponent();
        }
        Connection eSer = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            string eCode = txtServiceCode.Text;
            string eName = txtServicetName.Text;
            string eDesc = txtServiceDescription.Text;
            double ePrice = double.Parse(txtServicePrice.Text);
            int eType = Int32.Parse(cboServiceType.SelectedValue.ToString());
            int eStat = Int32.Parse(cboStatus.SelectedValue.ToString());

            if (txtServiceCode.Text == "" || txtServicetName.Text == "" || txtServiceDescription.Text == "" || txtServicePrice.Text == "" || txtServicePrice.Text == "0")
            {
                MessageBox.Show("Please fill up the fields first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                eSer.OpenConn();
                eSer.ExecuteQuery("Update inventory set inventory_name = '" + eName + "', inventory_description = '" + eDesc + "', item_price = '" + ePrice + "', service_type_id = '" + eType + "', inventory_status = '" + eStat + "' where inventory_code = '" + eCode + "'");
                eSer.CloseConn();

                this.Close();
            }
        }
        private void Edit_Service_Load_1(object sender, EventArgs e)
        {
            string xStat = Services.serStat;
            string xType = Services.serType;
            //-------------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdType = new SqlCommand("Select * from service_type", conn);
            SqlDataReader drType = cmdType.ExecuteReader();
            DataTable dtType = new DataTable();
            dtType.Load(drType);
            cboServiceType.DataSource = dtType;
            cboServiceType.DisplayMember = "service_desc";
            cboServiceType.ValueMember = "service_type_id";
            cmdType.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdStatus = new SqlCommand("Select * from status where  status_id = '1' or status_id = '2'", conn);
            SqlDataReader drStatus = cmdStatus.ExecuteReader();
            DataTable dtStatus = new DataTable();
            dtStatus.Load(drStatus);
            cboStatus.DataSource = dtStatus;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStatus.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------------//

            cboServiceType.Text = xType;
            cboServiceType.Text = xStat;
        }
    }
}
