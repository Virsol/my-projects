﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public class MainMenu
    {
        public void OpenForm(Form form)
        {
        Leobytes leobytes = new Leobytes();
            form.TopLevel = false;
            form.ControlBox = true;
            form.Dock = DockStyle.None;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            /*I assume this code is in your ParentForm and so 'this' points to ParentForm that contains ContainerPanel*/
/*            leobytes.PanelContainer.Controls.Add(form);*/
            form.Show();
        }
    }
}
