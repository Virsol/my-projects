﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    
    public partial class PO_Item_Details : Form
    {

        private string TPrice, TTax, TValue;

        public string t_Price
        {
            get { return TPrice; }
            set { TPrice = value; }
        }

        public string t_Tax
        {
            get { return TTax; }
            set { TTax = value; }
        }

        public string t_Value
        {
            get { return TValue; }
            set { TValue = value; }
        }
        public PO_Item_Details()
        {
            InitializeComponent();
        }

        Connection podet = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private void btnAdd_Click(object sender, EventArgs e)
        {
            podet.OpenConn();

            Purchasing_Order po = new Purchasing_Order();

            string prod_num = lblhidden.Text;
            string prod_code = txtProductCode.Text;
            int prod_quantiy = Int32.Parse(txtProductQuantity.Text);
            int prod_uom = Int32.Parse(cboProductUoM.SelectedValue.ToString());
            double prod_price = double.Parse(txtProductPrice.Text);
            int prod_tax = Int32.Parse(cboProductTax.SelectedValue.ToString());

            if (txtProductCode.Text == "" || txtProductName.Text == "" || txtProductPrice.Text == "" || txtProductQuantity.Text == "")
            {
                MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {

                SqlDataReader drtax = podet.DataReader("select tax_rate from dbo.tax where tax_id = '" + prod_tax + "'");
                drtax.Read();
                int prod_tax_rate = Int32.Parse(drtax["tax_rate"].ToString());
                int per = 100;
                double tax_price;
                double prod_total = prod_price * prod_quantiy;

                tax_price = prod_total * prod_tax_rate / per;
                podet.CloseConn();

                podet.OpenConn();
                podet.ExecuteQuery("Insert into dbo.draft_po (po_document_number, inventory_code, quantity, uom_id, price, tax_id, tax_price, total_price) " +
                    "VALUES('" + prod_num + "' , '" + prod_code + "', '" + prod_quantiy + "' , '" + prod_uom + "' , '" + prod_price + "' , '" + prod_tax + "' , '" + tax_price + "' , '" + prod_total + "' )");
                podet.CloseConn();

                // MessageBox.Show("Save Successfull!", "Save Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Clear();
                this.Close();

            }

            //-----------------//
            podet.OpenConn();

            SqlDataReader drTotal = podet.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax from draft_PO");
            drTotal.Read();

            double total_price = double.Parse(drTotal["Total_Price"].ToString());
            double total_tax = double.Parse(drTotal["Total_Tax"].ToString());
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            double total_value = (total_price + total_tax); // - total_discount;

            t_Price = total_price.ToString("n2");
            t_Tax = total_tax.ToString("n2");
            t_Value = total_value.ToString("n2");

            podet.CloseConn();

            //------------------//
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            podet.OpenConn();

            SqlDataReader drTotal = podet.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax from draft_PO");
            drTotal.Read();

            double total_price = double.Parse(drTotal["Total_Price"].ToString());
            double total_tax = double.Parse(drTotal["Total_Tax"].ToString());
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            double total_value = (total_price + total_tax); // - total_discount;

            t_Price = total_price.ToString("n2");
            t_Tax = total_tax.ToString("n2");
            t_Value = total_value.ToString("n2");

            podet.CloseConn();
            this.Close();
        }

        private void Clear()
        {
            txtProductDescription.Text = "";
            txtProductCode.Text = "";
            txtProductName.Text = "";
            txtProductPrice.Text = "";
            txtProductDiscount.Text = "";
            txtProductQuantity.Text = "";
        }

        private void PO_Item_Details_Load(object sender, EventArgs e)
        {
            string xUom = Purchasing_Order.uom;

            conn.Open();

            SqlCommand cmduom = new SqlCommand("Select * from dbo.uom", conn);
            SqlDataReader dr = cmduom.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            cboProductUoM.DataSource = dt;
            cboProductUoM.DisplayMember = "uom_desc";
            cboProductUoM.ValueMember = "uom_id";
            cmduom.ExecuteNonQuery();

            SqlCommand cmdtax = new SqlCommand("Select * from dbo.tax where tax_id = '2'", conn);
            SqlDataReader drtax = cmdtax.ExecuteReader();
            DataTable dttax = new DataTable();
            dttax.Load(drtax);
            cboProductTax.DataSource = dttax;
            cboProductTax.DisplayMember = "tax_desc";
            cboProductTax.ValueMember = "tax_id";
            cmdtax.ExecuteNonQuery();

            conn.Close();

            cboProductUoM.Text = xUom;

        }
    }
}
