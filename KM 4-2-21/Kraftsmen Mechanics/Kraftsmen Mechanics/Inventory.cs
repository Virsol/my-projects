﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Inventory : Form
    {

        public static string co = "";
        public static string status = "";


        public Inventory()
        {
            InitializeComponent();
        }

        Connection inv = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void dgvProductList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        private void Inventory_Load(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void dgvProductList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bunifuCustomLabel2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        #endregion
        private void btnAddNewProduct_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_Product add_Product = new Add_Product())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_Product.Owner = newFormDialog;

                    if (add_Product.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        showTable();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            inv.OpenConn();
            dgvProductList.DataSource = inv.ShowDataDGV("Select * From inventory_item where [Item Code]  like '%" + txtSearch.Text + "%' or [Item Name] like '%" + txtSearch.Text + "%'");
            inv.CloseConn();
        }

        

        public void showTable()
        {
            inv.OpenConn();
            dgvProductList.DataSource = inv.ShowDataDGV("select * from inventory_item");
            inv.CloseConn();
        }

        private void Inventory_Load_1(object sender, EventArgs e)
        {
            showTable();
        }

        private void dgvProductList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            status = dgvProductList.CurrentRow.Cells[5].Value.ToString();
            co = dgvProductList.CurrentRow.Cells[6].Value.ToString();
            Edit_Product e_prod = new Edit_Product();

            e_prod.txtProductCode.Text = dgvProductList.CurrentRow.Cells[0].Value.ToString();
            e_prod.txtProductName.Text = dgvProductList.CurrentRow.Cells[1].Value.ToString();
            e_prod.txtProductDescription.Text = dgvProductList.CurrentRow.Cells[2].Value.ToString();
            e_prod.txtProductPrice.Text = dgvProductList.CurrentRow.Cells[4].Value.ToString();


            if (e_prod.ShowDialog() != DialogResult.OK)
            {
                showTable();
            }
        }
    }
}
