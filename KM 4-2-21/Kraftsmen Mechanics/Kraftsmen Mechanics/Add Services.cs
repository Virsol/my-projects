﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Add_Services : Form
    {
        public Add_Services()
        {
            InitializeComponent();
        }
        
        Connection addSer = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #region hide
        private void Add_Services_Load(object sender, EventArgs e)
        {
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
        }
        #endregion
        private void Add_Services_Load_1(object sender, EventArgs e)
        {
            conn.Open();

            SqlCommand cmdSer = new SqlCommand("Select * from service_type", conn);
            SqlDataReader drSer = cmdSer.ExecuteReader();
            DataTable dtSer = new DataTable();
            dtSer.Load(drSer);
            cboServiceType.DataSource = dtSer;
            cboServiceType.DisplayMember = "service_desc";
            cboServiceType.ValueMember = "service_type_id";
            cmdSer.ExecuteNonQuery();

            conn.Close();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            string sCode = txtServiceCode.Text;
            string sName = txtServiceName.Text;
            string sDesc = txtServiceDescription.Text;
            double sPrice = double.Parse(txtServicePrice.Text.ToString());
            int sType = Int32.Parse(cboServiceType.SelectedValue.ToString());

            addSer.OpenConn();
            SqlDataReader drCount = addSer.DataReader("select count (inventory_code) as 'Code' from inventory where inventory_code = '" + sCode + "' ");
            drCount.Read();

            int xCode = Int32.Parse(drCount["Code"].ToString());

            if (xCode == 1)
            {
                MessageBox.Show("Item code is already existed.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (txtServiceCode.Text == "" || txtServiceName.Text == "" || txtServiceDescription.Text == "" || txtServicePrice.Text == "")
                {
                    MessageBox.Show("Please fill up the fields first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    addSer.OpenConn();
                    addSer.ExecuteQuery("insert into inventory(inventory_code, inventory_name,inventory_description, inventory_type, service_type_id, item_price, inventory_status)" +
                        "VALUES('" + sCode + "', '" + sName + "', '" + sDesc + "', '" + 2 + "','" + sType + "', '" + sPrice + "','" + 1 + "')");
                    addSer.CloseConn();

                    MessageBox.Show("Service added.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            addSer.CloseConn();
        }
    }
}
