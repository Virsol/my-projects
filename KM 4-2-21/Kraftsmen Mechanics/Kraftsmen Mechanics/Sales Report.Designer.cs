﻿
namespace Kraftsmen_Mechanics
{
    partial class Sales_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sales_Report));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.lblResults = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tctrlSalesReports = new System.Windows.Forms.TabControl();
            this.Services = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGenerateServiceReport = new FontAwesome.Sharp.IconButton();
            this.label5 = new System.Windows.Forms.Label();
            this.txtServiceReportTotalSales = new System.Windows.Forms.TextBox();
            this.btnServiceList = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpServiceReportTo = new System.Windows.Forms.DateTimePicker();
            this.dtpServiceReportFrom = new System.Windows.Forms.DateTimePicker();
            this.txtServiceName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvServiceSalesList = new System.Windows.Forms.DataGridView();
            this.Items = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGenerateItemReport = new FontAwesome.Sharp.IconButton();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemsReportTotalSales = new System.Windows.Forms.TextBox();
            this.btnItemList = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpItemsSalesReportTo = new System.Windows.Forms.DateTimePicker();
            this.dtpItemsSalesReportFrom = new System.Windows.Forms.DateTimePicker();
            this.txtItemsSalesReport = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvItemsReportList = new System.Windows.Forms.DataGridView();
            this.Transaction = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_doc_to = new System.Windows.Forms.Button();
            this.btn_doc_from = new System.Windows.Forms.Button();
            this.txtDocNoFrom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDocNoTo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGenerateTransactionReport = new FontAwesome.Sharp.IconButton();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTransactionReportTotalSales = new System.Windows.Forms.TextBox();
            this.btnCustomerList = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpTransactionReportTo = new System.Windows.Forms.DateTimePicker();
            this.dtpTransactionReportFrom = new System.Windows.Forms.DateTimePicker();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgvTransactionReportList = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.tctrlSalesReports.SuspendLayout();
            this.Services.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceSalesList)).BeginInit();
            this.Items.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemsReportList)).BeginInit();
            this.Transaction.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransactionReportList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1371, 52);
            this.panel1.TabIndex = 57;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 25);
            this.label17.TabIndex = 24;
            this.label17.Text = "Sales Report";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(1323, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 31);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblResults
            // 
            this.lblResults.AutoSize = true;
            this.lblResults.Location = new System.Drawing.Point(84, 786);
            this.lblResults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(44, 17);
            this.lblResults.TabIndex = 61;
            this.lblResults.Text = "0 of 0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 786);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 60;
            this.label4.Text = "Results:";
            // 
            // tctrlSalesReports
            // 
            this.tctrlSalesReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctrlSalesReports.Controls.Add(this.Services);
            this.tctrlSalesReports.Controls.Add(this.Items);
            this.tctrlSalesReports.Controls.Add(this.Transaction);
            this.tctrlSalesReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tctrlSalesReports.ItemSize = new System.Drawing.Size(80, 30);
            this.tctrlSalesReports.Location = new System.Drawing.Point(0, 52);
            this.tctrlSalesReports.Margin = new System.Windows.Forms.Padding(4);
            this.tctrlSalesReports.Name = "tctrlSalesReports";
            this.tctrlSalesReports.Padding = new System.Drawing.Point(25, 3);
            this.tctrlSalesReports.SelectedIndex = 0;
            this.tctrlSalesReports.Size = new System.Drawing.Size(1371, 757);
            this.tctrlSalesReports.TabIndex = 62;
            // 
            // Services
            // 
            this.Services.BackColor = System.Drawing.Color.White;
            this.Services.Controls.Add(this.groupBox1);
            this.Services.Controls.Add(this.dgvServiceSalesList);
            this.Services.ForeColor = System.Drawing.Color.Black;
            this.Services.Location = new System.Drawing.Point(4, 34);
            this.Services.Margin = new System.Windows.Forms.Padding(4);
            this.Services.Name = "Services";
            this.Services.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Services.Size = new System.Drawing.Size(1363, 719);
            this.Services.TabIndex = 0;
            this.Services.Text = "Services";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.btnGenerateServiceReport);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtServiceReportTotalSales);
            this.groupBox1.Controls.Add(this.btnServiceList);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpServiceReportTo);
            this.groupBox1.Controls.Add(this.dtpServiceReportFrom);
            this.groupBox1.Controls.Add(this.txtServiceName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(1341, 236);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Report Details";
            // 
            // btnGenerateServiceReport
            // 
            this.btnGenerateServiceReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnGenerateServiceReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateServiceReport.FlatAppearance.BorderSize = 0;
            this.btnGenerateServiceReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateServiceReport.ForeColor = System.Drawing.Color.White;
            this.btnGenerateServiceReport.IconChar = FontAwesome.Sharp.IconChar.ChartBar;
            this.btnGenerateServiceReport.IconColor = System.Drawing.Color.White;
            this.btnGenerateServiceReport.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnGenerateServiceReport.IconSize = 20;
            this.btnGenerateServiceReport.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerateServiceReport.Location = new System.Drawing.Point(177, 180);
            this.btnGenerateServiceReport.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateServiceReport.Name = "btnGenerateServiceReport";
            this.btnGenerateServiceReport.Padding = new System.Windows.Forms.Padding(7, 5, 7, 6);
            this.btnGenerateServiceReport.Size = new System.Drawing.Size(183, 39);
            this.btnGenerateServiceReport.TabIndex = 67;
            this.btnGenerateServiceReport.Text = "Generate Report";
            this.btnGenerateServiceReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerateServiceReport.UseVisualStyleBackColor = false;
            this.btnGenerateServiceReport.Click += new System.EventHandler(this.btnGenerateServiceReport_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(67, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "Total Sales:";
            // 
            // txtServiceReportTotalSales
            // 
            this.txtServiceReportTotalSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtServiceReportTotalSales.Location = new System.Drawing.Point(177, 142);
            this.txtServiceReportTotalSales.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtServiceReportTotalSales.Name = "txtServiceReportTotalSales";
            this.txtServiceReportTotalSales.ReadOnly = true;
            this.txtServiceReportTotalSales.Size = new System.Drawing.Size(396, 24);
            this.txtServiceReportTotalSales.TabIndex = 13;
            // 
            // btnServiceList
            // 
            this.btnServiceList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnServiceList.BackgroundImage")));
            this.btnServiceList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnServiceList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnServiceList.FlatAppearance.BorderSize = 0;
            this.btnServiceList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnServiceList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnServiceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServiceList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServiceList.Location = new System.Drawing.Point(544, 38);
            this.btnServiceList.Margin = new System.Windows.Forms.Padding(4);
            this.btnServiceList.Name = "btnServiceList";
            this.btnServiceList.Size = new System.Drawing.Size(20, 18);
            this.btnServiceList.TabIndex = 12;
            this.btnServiceList.UseVisualStyleBackColor = true;
            this.btnServiceList.Click += new System.EventHandler(this.btnServiceList_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(129, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "To:";
            // 
            // dtpServiceReportTo
            // 
            this.dtpServiceReportTo.Location = new System.Drawing.Point(177, 105);
            this.dtpServiceReportTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpServiceReportTo.Name = "dtpServiceReportTo";
            this.dtpServiceReportTo.Size = new System.Drawing.Size(396, 24);
            this.dtpServiceReportTo.TabIndex = 3;
            // 
            // dtpServiceReportFrom
            // 
            this.dtpServiceReportFrom.Location = new System.Drawing.Point(177, 69);
            this.dtpServiceReportFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpServiceReportFrom.Name = "dtpServiceReportFrom";
            this.dtpServiceReportFrom.Size = new System.Drawing.Size(396, 24);
            this.dtpServiceReportFrom.TabIndex = 2;
            // 
            // txtServiceName
            // 
            this.txtServiceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtServiceName.Location = new System.Drawing.Point(177, 34);
            this.txtServiceName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtServiceName.Name = "txtServiceName";
            this.txtServiceName.Size = new System.Drawing.Size(356, 24);
            this.txtServiceName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(109, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "From:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(45, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Service Name:";
            // 
            // dgvServiceSalesList
            // 
            this.dgvServiceSalesList.AllowUserToAddRows = false;
            this.dgvServiceSalesList.AllowUserToDeleteRows = false;
            this.dgvServiceSalesList.AllowUserToResizeColumns = false;
            this.dgvServiceSalesList.AllowUserToResizeRows = false;
            this.dgvServiceSalesList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServiceSalesList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvServiceSalesList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvServiceSalesList.BackgroundColor = System.Drawing.Color.White;
            this.dgvServiceSalesList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceSalesList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvServiceSalesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvServiceSalesList.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvServiceSalesList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvServiceSalesList.Location = new System.Drawing.Point(9, 250);
            this.dgvServiceSalesList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvServiceSalesList.MultiSelect = false;
            this.dgvServiceSalesList.Name = "dgvServiceSalesList";
            this.dgvServiceSalesList.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServiceSalesList.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvServiceSalesList.RowHeadersWidth = 51;
            this.dgvServiceSalesList.RowTemplate.Height = 24;
            this.dgvServiceSalesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServiceSalesList.Size = new System.Drawing.Size(1341, 452);
            this.dgvServiceSalesList.TabIndex = 61;
            // 
            // Items
            // 
            this.Items.BackColor = System.Drawing.Color.White;
            this.Items.Controls.Add(this.groupBox2);
            this.Items.Controls.Add(this.dgvItemsReportList);
            this.Items.ForeColor = System.Drawing.Color.Black;
            this.Items.Location = new System.Drawing.Point(4, 34);
            this.Items.Margin = new System.Windows.Forms.Padding(4);
            this.Items.Name = "Items";
            this.Items.Padding = new System.Windows.Forms.Padding(4);
            this.Items.Size = new System.Drawing.Size(1363, 719);
            this.Items.TabIndex = 1;
            this.Items.Text = "Items";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.btnGenerateItemReport);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtItemsReportTotalSales);
            this.groupBox2.Controls.Add(this.btnItemList);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.dtpItemsSalesReportTo);
            this.groupBox2.Controls.Add(this.dtpItemsSalesReportFrom);
            this.groupBox2.Controls.Add(this.txtItemsSalesReport);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(9, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(1340, 236);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Report Details";
            // 
            // btnGenerateItemReport
            // 
            this.btnGenerateItemReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnGenerateItemReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateItemReport.FlatAppearance.BorderSize = 0;
            this.btnGenerateItemReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateItemReport.ForeColor = System.Drawing.Color.White;
            this.btnGenerateItemReport.IconChar = FontAwesome.Sharp.IconChar.ChartBar;
            this.btnGenerateItemReport.IconColor = System.Drawing.Color.White;
            this.btnGenerateItemReport.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnGenerateItemReport.IconSize = 20;
            this.btnGenerateItemReport.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerateItemReport.Location = new System.Drawing.Point(177, 180);
            this.btnGenerateItemReport.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateItemReport.Name = "btnGenerateItemReport";
            this.btnGenerateItemReport.Padding = new System.Windows.Forms.Padding(7, 5, 7, 6);
            this.btnGenerateItemReport.Size = new System.Drawing.Size(183, 39);
            this.btnGenerateItemReport.TabIndex = 68;
            this.btnGenerateItemReport.Text = "Generate Report";
            this.btnGenerateItemReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerateItemReport.UseVisualStyleBackColor = false;
            this.btnGenerateItemReport.Click += new System.EventHandler(this.btnGenerateItemReport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(67, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Total Sales:";
            // 
            // txtItemsReportTotalSales
            // 
            this.txtItemsReportTotalSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtItemsReportTotalSales.Location = new System.Drawing.Point(177, 142);
            this.txtItemsReportTotalSales.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtItemsReportTotalSales.Name = "txtItemsReportTotalSales";
            this.txtItemsReportTotalSales.ReadOnly = true;
            this.txtItemsReportTotalSales.Size = new System.Drawing.Size(396, 24);
            this.txtItemsReportTotalSales.TabIndex = 13;
            // 
            // btnItemList
            // 
            this.btnItemList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemList.BackgroundImage")));
            this.btnItemList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnItemList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnItemList.FlatAppearance.BorderSize = 0;
            this.btnItemList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnItemList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnItemList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemList.Location = new System.Drawing.Point(544, 38);
            this.btnItemList.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemList.Name = "btnItemList";
            this.btnItemList.Size = new System.Drawing.Size(20, 18);
            this.btnItemList.TabIndex = 12;
            this.btnItemList.UseVisualStyleBackColor = true;
            this.btnItemList.Click += new System.EventHandler(this.btnItemList_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(129, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "To:";
            // 
            // dtpItemsSalesReportTo
            // 
            this.dtpItemsSalesReportTo.Location = new System.Drawing.Point(177, 105);
            this.dtpItemsSalesReportTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpItemsSalesReportTo.Name = "dtpItemsSalesReportTo";
            this.dtpItemsSalesReportTo.Size = new System.Drawing.Size(396, 24);
            this.dtpItemsSalesReportTo.TabIndex = 3;
            // 
            // dtpItemsSalesReportFrom
            // 
            this.dtpItemsSalesReportFrom.Location = new System.Drawing.Point(177, 69);
            this.dtpItemsSalesReportFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpItemsSalesReportFrom.Name = "dtpItemsSalesReportFrom";
            this.dtpItemsSalesReportFrom.Size = new System.Drawing.Size(396, 24);
            this.dtpItemsSalesReportFrom.TabIndex = 2;
            // 
            // txtItemsSalesReport
            // 
            this.txtItemsSalesReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtItemsSalesReport.Location = new System.Drawing.Point(177, 34);
            this.txtItemsSalesReport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtItemsSalesReport.Name = "txtItemsSalesReport";
            this.txtItemsSalesReport.Size = new System.Drawing.Size(356, 24);
            this.txtItemsSalesReport.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(109, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 18);
            this.label8.TabIndex = 1;
            this.label8.Text = "From:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label9.Location = new System.Drawing.Point(67, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 18);
            this.label9.TabIndex = 0;
            this.label9.Text = "Item Name:";
            // 
            // dgvItemsReportList
            // 
            this.dgvItemsReportList.AllowUserToAddRows = false;
            this.dgvItemsReportList.AllowUserToDeleteRows = false;
            this.dgvItemsReportList.AllowUserToResizeColumns = false;
            this.dgvItemsReportList.AllowUserToResizeRows = false;
            this.dgvItemsReportList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItemsReportList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvItemsReportList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvItemsReportList.BackgroundColor = System.Drawing.Color.White;
            this.dgvItemsReportList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemsReportList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvItemsReportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemsReportList.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvItemsReportList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvItemsReportList.Location = new System.Drawing.Point(9, 251);
            this.dgvItemsReportList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvItemsReportList.MultiSelect = false;
            this.dgvItemsReportList.Name = "dgvItemsReportList";
            this.dgvItemsReportList.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemsReportList.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvItemsReportList.RowHeadersWidth = 51;
            this.dgvItemsReportList.RowTemplate.Height = 24;
            this.dgvItemsReportList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItemsReportList.Size = new System.Drawing.Size(1340, 452);
            this.dgvItemsReportList.TabIndex = 63;
            // 
            // Transaction
            // 
            this.Transaction.BackColor = System.Drawing.Color.White;
            this.Transaction.Controls.Add(this.groupBox3);
            this.Transaction.Controls.Add(this.dgvTransactionReportList);
            this.Transaction.Location = new System.Drawing.Point(4, 34);
            this.Transaction.Margin = new System.Windows.Forms.Padding(4);
            this.Transaction.Name = "Transaction";
            this.Transaction.Size = new System.Drawing.Size(1363, 719);
            this.Transaction.TabIndex = 2;
            this.Transaction.Text = "Transactions";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.btn_doc_to);
            this.groupBox3.Controls.Add(this.btn_doc_from);
            this.groupBox3.Controls.Add(this.txtDocNoFrom);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtDocNoTo);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.btnGenerateTransactionReport);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtTransactionReportTotalSales);
            this.groupBox3.Controls.Add(this.btnCustomerList);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.dtpTransactionReportTo);
            this.groupBox3.Controls.Add(this.dtpTransactionReportFrom);
            this.groupBox3.Controls.Add(this.txtCustomerName);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox3.Location = new System.Drawing.Point(11, 9);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(1340, 236);
            this.groupBox3.TabIndex = 64;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Report Details";
            //this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // btn_doc_to
            // 
            this.btn_doc_to.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_doc_to.BackgroundImage")));
            this.btn_doc_to.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_doc_to.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_doc_to.FlatAppearance.BorderSize = 0;
            this.btn_doc_to.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_doc_to.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_doc_to.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_doc_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_doc_to.Location = new System.Drawing.Point(1030, 78);
            this.btn_doc_to.Margin = new System.Windows.Forms.Padding(4);
            this.btn_doc_to.Name = "btn_doc_to";
            this.btn_doc_to.Size = new System.Drawing.Size(20, 18);
            this.btn_doc_to.TabIndex = 74;
            this.btn_doc_to.UseVisualStyleBackColor = true;
            this.btn_doc_to.Click += new System.EventHandler(this.btn_doc_to_Click);
            // 
            // btn_doc_from
            // 
            this.btn_doc_from.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_doc_from.BackgroundImage")));
            this.btn_doc_from.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_doc_from.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_doc_from.FlatAppearance.BorderSize = 0;
            this.btn_doc_from.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_doc_from.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_doc_from.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_doc_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_doc_from.Location = new System.Drawing.Point(1030, 39);
            this.btn_doc_from.Margin = new System.Windows.Forms.Padding(4);
            this.btn_doc_from.Name = "btn_doc_from";
            this.btn_doc_from.Size = new System.Drawing.Size(20, 18);
            this.btn_doc_from.TabIndex = 73;
            this.btn_doc_from.UseVisualStyleBackColor = true;
            this.btn_doc_from.Click += new System.EventHandler(this.btn_doc_from_Click);
            // 
            // txtDocNoFrom
            // 
            this.txtDocNoFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtDocNoFrom.Location = new System.Drawing.Point(728, 34);
            this.txtDocNoFrom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDocNoFrom.Name = "txtDocNoFrom";
            this.txtDocNoFrom.ReadOnly = true;
            this.txtDocNoFrom.Size = new System.Drawing.Size(292, 24);
            this.txtDocNoFrom.TabIndex = 72;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label11.Location = new System.Drawing.Point(612, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 18);
            this.label11.TabIndex = 71;
            this.label11.Text = "Doc No.From:";
            // 
            // txtDocNoTo
            // 
            this.txtDocNoTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtDocNoTo.Location = new System.Drawing.Point(728, 71);
            this.txtDocNoTo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDocNoTo.Name = "txtDocNoTo";
            this.txtDocNoTo.ReadOnly = true;
            this.txtDocNoTo.Size = new System.Drawing.Size(292, 24);
            this.txtDocNoTo.TabIndex = 70;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.Location = new System.Drawing.Point(628, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 18);
            this.label10.TabIndex = 69;
            this.label10.Text = "Doc No. To:";
            // 
            // btnGenerateTransactionReport
            // 
            this.btnGenerateTransactionReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnGenerateTransactionReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateTransactionReport.FlatAppearance.BorderSize = 0;
            this.btnGenerateTransactionReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateTransactionReport.ForeColor = System.Drawing.Color.White;
            this.btnGenerateTransactionReport.IconChar = FontAwesome.Sharp.IconChar.ChartBar;
            this.btnGenerateTransactionReport.IconColor = System.Drawing.Color.White;
            this.btnGenerateTransactionReport.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnGenerateTransactionReport.IconSize = 20;
            this.btnGenerateTransactionReport.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerateTransactionReport.Location = new System.Drawing.Point(177, 181);
            this.btnGenerateTransactionReport.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateTransactionReport.Name = "btnGenerateTransactionReport";
            this.btnGenerateTransactionReport.Padding = new System.Windows.Forms.Padding(7, 5, 7, 6);
            this.btnGenerateTransactionReport.Size = new System.Drawing.Size(183, 39);
            this.btnGenerateTransactionReport.TabIndex = 68;
            this.btnGenerateTransactionReport.Text = "Generate Report";
            this.btnGenerateTransactionReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerateTransactionReport.UseVisualStyleBackColor = false;
            this.btnGenerateTransactionReport.Click += new System.EventHandler(this.btnGenerateTransactionReport_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label12.Location = new System.Drawing.Point(65, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 18);
            this.label12.TabIndex = 14;
            this.label12.Text = "Total Sales:";
            // 
            // txtTransactionReportTotalSales
            // 
            this.txtTransactionReportTotalSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtTransactionReportTotalSales.Location = new System.Drawing.Point(167, 143);
            this.txtTransactionReportTotalSales.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTransactionReportTotalSales.Name = "txtTransactionReportTotalSales";
            this.txtTransactionReportTotalSales.ReadOnly = true;
            this.txtTransactionReportTotalSales.Size = new System.Drawing.Size(356, 24);
            this.txtTransactionReportTotalSales.TabIndex = 13;
            // 
            // btnCustomerList
            // 
            this.btnCustomerList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCustomerList.BackgroundImage")));
            this.btnCustomerList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCustomerList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCustomerList.FlatAppearance.BorderSize = 0;
            this.btnCustomerList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCustomerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomerList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomerList.Location = new System.Drawing.Point(531, 39);
            this.btnCustomerList.Margin = new System.Windows.Forms.Padding(4);
            this.btnCustomerList.Name = "btnCustomerList";
            this.btnCustomerList.Size = new System.Drawing.Size(20, 18);
            this.btnCustomerList.TabIndex = 12;
            this.btnCustomerList.UseVisualStyleBackColor = true;
            this.btnCustomerList.Click += new System.EventHandler(this.btnCustomerList_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label13.Location = new System.Drawing.Point(128, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 18);
            this.label13.TabIndex = 4;
            this.label13.Text = "To:";
            // 
            // dtpTransactionReportTo
            // 
            this.dtpTransactionReportTo.Location = new System.Drawing.Point(167, 106);
            this.dtpTransactionReportTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpTransactionReportTo.Name = "dtpTransactionReportTo";
            this.dtpTransactionReportTo.Size = new System.Drawing.Size(356, 24);
            this.dtpTransactionReportTo.TabIndex = 3;
            // 
            // dtpTransactionReportFrom
            // 
            this.dtpTransactionReportFrom.Location = new System.Drawing.Point(167, 71);
            this.dtpTransactionReportFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpTransactionReportFrom.Name = "dtpTransactionReportFrom";
            this.dtpTransactionReportFrom.Size = new System.Drawing.Size(356, 24);
            this.dtpTransactionReportFrom.TabIndex = 2;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtCustomerName.Location = new System.Drawing.Point(167, 34);
            this.txtCustomerName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(356, 24);
            this.txtCustomerName.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label14.Location = new System.Drawing.Point(108, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 18);
            this.label14.TabIndex = 1;
            this.label14.Text = "From:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label15.Location = new System.Drawing.Point(28, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 18);
            this.label15.TabIndex = 0;
            this.label15.Text = "Customer Name:";
            // 
            // dgvTransactionReportList
            // 
            this.dgvTransactionReportList.AllowUserToAddRows = false;
            this.dgvTransactionReportList.AllowUserToDeleteRows = false;
            this.dgvTransactionReportList.AllowUserToResizeColumns = false;
            this.dgvTransactionReportList.AllowUserToResizeRows = false;
            this.dgvTransactionReportList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTransactionReportList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTransactionReportList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTransactionReportList.BackgroundColor = System.Drawing.Color.White;
            this.dgvTransactionReportList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(57)))), ((int)(((byte)(3)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(157)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTransactionReportList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvTransactionReportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTransactionReportList.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvTransactionReportList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(177)))), ((int)(((byte)(91)))));
            this.dgvTransactionReportList.Location = new System.Drawing.Point(11, 250);
            this.dgvTransactionReportList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvTransactionReportList.MultiSelect = false;
            this.dgvTransactionReportList.Name = "dgvTransactionReportList";
            this.dgvTransactionReportList.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTransactionReportList.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvTransactionReportList.RowHeadersWidth = 51;
            this.dgvTransactionReportList.RowTemplate.Height = 24;
            this.dgvTransactionReportList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTransactionReportList.Size = new System.Drawing.Size(1340, 452);
            this.dgvTransactionReportList.TabIndex = 65;
            // 
            // Sales_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1371, 809);
            this.Controls.Add(this.tctrlSalesReports);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblResults);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Sales_Report";
            this.Text = "Sales_Report";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.tctrlSalesReports.ResumeLayout(false);
            this.Services.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceSalesList)).EndInit();
            this.Items.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemsReportList)).EndInit();
            this.Transaction.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransactionReportList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuImageButton btnExit;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tctrlSalesReports;
        private System.Windows.Forms.TabPage Services;
        private System.Windows.Forms.TabPage Items;
        private System.Windows.Forms.TabPage Transaction;
        public System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtServiceReportTotalSales;
        private System.Windows.Forms.Button btnServiceList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpServiceReportTo;
        private System.Windows.Forms.DateTimePicker dtpServiceReportFrom;
        public System.Windows.Forms.TextBox txtServiceName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView dgvServiceSalesList;
        public System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtItemsReportTotalSales;
        private System.Windows.Forms.Button btnItemList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpItemsSalesReportTo;
        private System.Windows.Forms.DateTimePicker dtpItemsSalesReportFrom;
        public System.Windows.Forms.TextBox txtItemsSalesReport;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.DataGridView dgvItemsReportList;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtTransactionReportTotalSales;
        private System.Windows.Forms.Button btnCustomerList;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpTransactionReportTo;
        private System.Windows.Forms.DateTimePicker dtpTransactionReportFrom;
        public System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.DataGridView dgvTransactionReportList;
        private FontAwesome.Sharp.IconButton btnGenerateServiceReport;
        private FontAwesome.Sharp.IconButton btnGenerateItemReport;
        private FontAwesome.Sharp.IconButton btnGenerateTransactionReport;
        public System.Windows.Forms.TextBox txtDocNoFrom;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtDocNoTo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_doc_to;
        private System.Windows.Forms.Button btn_doc_from;
    }
}