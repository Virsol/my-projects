﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public partial class Customers : Form
    {
        public Customers()
        {
            InitializeComponent();
        }

        private void btnAddNewCustomer_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_New_Customers add_New_Customers = new Add_New_Customers())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_New_Customers.Owner = newFormDialog;

                    if (add_New_Customers.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnAddCustomerVehicle_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_New_Customer add_New_Customer = new Add_New_Customer())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_New_Customer.Owner = newFormDialog;

                    if (add_New_Customer.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
