﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Edit_Product : Form
    {
        private int nVehicleID = 0;
        private string eCode, eName, eDesc;
        private double ePrice;
        public Edit_Product()
        {
            InitializeComponent();
        }

        Connection e_Prod = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region trash
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void btnSave_Click(object sender, EventArgs e)
        {

        }
        private void Edit_Product_Load_1(object sender, EventArgs e)
        {

        }
        private void Edit_Product_Load(object sender, EventArgs e)
        {
            
        }
        #endregion
        

        private void Edit_Product_Load_2(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------//
            string xUom = Inventory.co;
            string xStat = Inventory.status;


            //-------------------------------------------------------------------------//

            Inventory inven = new Inventory();
            //-------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmduom = new SqlCommand("Select * from dbo.uom", conn);
            SqlDataReader dr = cmduom.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            cboUOM.DataSource = dt;
            cboUOM.DisplayMember = "uom_desc";
            cboUOM.ValueMember = "uom_id";
            cmduom.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdStatus = new SqlCommand("Select * from status where  status_id = '1' or status_id = '2'", conn);
            SqlDataReader drStatus = cmdStatus.ExecuteReader();
            DataTable dtStatus = new DataTable();
            dtStatus.Load(drStatus);
            cboStatus.DataSource = dtStatus;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStatus.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------//

            cboUOM.Text = xUom;
            cboStatus.Text = xStat;



            //Created by Jayar-Pogi
            conn.Open();
            SqlCommand cmdMake = new SqlCommand("select * from dbo.auto_make", conn);
            SqlDataReader drMake = cmdMake.ExecuteReader();
            DataTable dtMake = new DataTable();
            dtMake.Load(drMake);

            cboMake.DataSource = dtMake;
            cboMake.DisplayMember = "auto_make_desc";
            cboMake.ValueMember = "auto_make_id";
            cmdMake.ExecuteNonQuery();


            SqlCommand cmdSize = new SqlCommand("select * from dbo.size", conn);
            SqlDataReader drSize = cmdSize.ExecuteReader();
            DataTable dtSize = new DataTable();
            dtSize.Load(drSize);

            cboSize.DataSource = dtSize;
            cboSize.DisplayMember = "size_desc";
            cboSize.ValueMember = "size_id";
            cmdSize.ExecuteNonQuery();
            conn.Close();

            conn.Open();
            SqlCommand cmdCat = new SqlCommand("select * from vehicle_category", conn);
            SqlDataReader drCat = cmdCat.ExecuteReader();
            DataTable dtCat = new DataTable();
            dtCat.Load(drCat);

            cboCategory.DataSource = dtCat;
            cboCategory.DisplayMember = "vehicle_cat_desc";
            cboCategory.ValueMember = "vehicle_cat_id";
            cmdCat.ExecuteNonQuery();
            conn.Close();

            eCode = txtProductCode.Text;
            eName = txtProductName.Text;
            eDesc = txtProductDescription.Text;
            vDgvItemVehicleRefresh();
            //end - jayar
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            eCode = txtProductCode.Text;
            eName = txtProductName.Text;
            eDesc = txtProductDescription.Text;
            ePrice = double.Parse(txtProductPrice.Text);
            int eUom = Int32.Parse(cboUOM.SelectedValue.ToString());
            int eStat = Int32.Parse(cboStatus.SelectedValue.ToString());

            if (txtProductCode.Text == "" || txtProductName.Text == "" || txtProductDescription.Text == "" || txtProductPrice.Text == "" || txtProductPrice.Text == "0" || nVehicleID == 0)
            {
                MessageBox.Show("Please fill up the fields first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                e_Prod.OpenConn();
                e_Prod.ExecuteQuery("Update inventory set inventory_name = '" + eName + "', inventory_description = '" + eDesc + "', item_price = '" + ePrice + "', uom_id = '" + eUom + "', inventory_status = '" + eStat + "' where inventory_code = '" + eCode + "'");
                e_Prod.CloseConn();

                e_Prod.OpenConn();
                e_Prod.ExecuteQuery("Insert into item_car_application (inventory_code,vehicle_id) values ('"+eCode+"', '"+nVehicleID+"')");
                e_Prod.CloseConn();
                MessageBox.Show("Vehicle Added!");
                vDgvItemVehicleRefresh();
            }

        }
        private void vDgvItemVehicleRefresh()
        {
            e_Prod.OpenConn();
            dgvItemVehicle.DataSource = e_Prod.ShowDataDGV("select * from vehicles_of_item where [Item Code] = '"+eCode+"'");
            dgvItemVehicle.Columns["Item Code"].Visible = false;
            e_Prod.CloseConn();
        }

        private void dgvItemVehicle_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string model = dgvItemVehicle.CurrentRow.Cells[3].Value.ToString();
            string itemCode = dgvItemVehicle.CurrentRow.Cells[0].Value.ToString();
            int vID = Int32.Parse(dgvItemVehicle.CurrentRow.Cells[1].Value.ToString());
            if (MessageBox.Show("Do you want to remove this Vehicle?" + "\n" + model, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                e_Prod.OpenConn();
                e_Prod.ExecuteQuery("Delete from item_car_application where inventory_code = '" + itemCode + "' and vehicle_id = '" + vID + "'");
                e_Prod.CloseConn();
                vDgvItemVehicleRefresh();
            }

        }

        private void btnVehicleList_Click(object sender, EventArgs e)
        {

        }

        private void btnVehicleList_Click_1(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Vehicle_List vehicle_List = new Vehicle_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    vehicle_List.Owner = newFormDialog;

                    if (vehicle_List.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        nVehicleID = vehicle_List.vehicleID;
                        cboCategory.SelectedValue = vehicle_List.vehicleCategory.ToString();
                        cboSize.SelectedValue = vehicle_List.vehicleSize.ToString();
                        cboMake.SelectedValue = vehicle_List.vehicleMake.ToString();
                        txtModel.Text = vehicle_List.vehicleName;
                        
                        cboMake.Enabled = false;
                        cboCategory.Enabled = false;
                        cboSize.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
    }
}
