﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Services : Form
    {
        public static string serType = "";
        public static string serStat = "";

        public Services()
        {
            InitializeComponent();
        }

        Connection ser = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        #region hide
        private void Services_Load(object sender, EventArgs e)
        {

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgvServiceList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        #endregion
        private void btnAddNewService_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_Services add_Services = new Add_Services())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_Services.Owner = newFormDialog;

                    if (add_Services.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        showTable();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        private void dgvServiceList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            serType = this.dgvServiceList.CurrentRow.Cells[4].Value.ToString();
            serStat = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();

            Edit_Service eService = new Edit_Service();

            eService.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
            eService.txtServicetName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
            eService.txtServiceDescription.Text = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
            eService.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[3].Value.ToString();

            if(eService.ShowDialog() != DialogResult.OK)
            {
                showTable();
            }
        }

        public void showTable()
        {
            ser.OpenConn();
            dgvServiceList.DataSource = ser.ShowDataDGV("select * from service_list_view");
            ser.CloseConn();
        }

        private void Services_Load_1(object sender, EventArgs e)
        {
            showTable();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            ser.OpenConn();
            dgvServiceList.DataSource = ser.ShowDataDGV("Select * From service_list_view where [Service Code]  like '%" + txtSearch.Text + "%' or [Service Name] like '%" + txtSearch.Text + "%'");
            ser.CloseConn();
        }
    }
}
