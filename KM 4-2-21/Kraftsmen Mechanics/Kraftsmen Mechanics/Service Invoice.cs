﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Drawing;

namespace Kraftsmen_Mechanics
{
    public partial class Service_Invoice : Form
    {
        DBConnection conn = new DBConnection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        Job_Order_List JOL = new Job_Order_List();
        Transaction_List transaction_List;
        public string ntxtSearch;
        public string ncboSearchBy;
        public string ntxtCustomerName;
        public string ncboVehicleModel;
        public string ntxtContactNumber;
        public string ntxtVehicleMake;
        public string ntxtVehicleColor;
        public string ntxtVehiclePlateNo;
        public string nDocumentNumber;
        public string docSeries;
        public string docNum;
        public string nDocumentSeries;
        public string nDateCreated;
        private int nEmployeeID;
        private int cusID;
        private int iDocNum;
        private string sDocNum;
        private int vehicleID;
        private int nTechnician;
        private string TotalItem;
        private string TotalPaymentDue;
        private string TotalTax;
        private string JolDocNum;
        public string vehicleName;
        public string nInvoiceNumber;
        public string Sildocnum;
        public Service_Invoice()
        {
            InitializeComponent();
        }
        public Service_Invoice(Transaction_List trans)
        {
            InitializeComponent();
            transaction_List = trans;
        }
        private void btnViewJobEstimateList_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Job_Order_List JOL = new Job_Order_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    JOL.Owner = newFormDialog;

                    if (JOL.ShowDialog() != DialogResult.OK && JOL.CusID != 0 && JOL.CarID != 0)
                    {
                        newFormDialog.Dispose();
                        conn.OpenConnection();
                        SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + JOL.CusID + "'");
                        cusDR.Read();
                        txtCustomerName.Text = cusDR["name"].ToString();
                        ntxtCustomerName = txtCustomerName.Text;
                        txtContactNumber.Text = cusDR["Contact Number"].ToString();
                        txtAddress.Text = cusDR["address"].ToString();
                        conn.CloseConnection();


                        nEmployeeID = JOL.empID;
                        nTechnician = JOL.techID;
                        cusID = JOL.CusID;
                        /*MessageBox.Show(cusID.ToString());*/
                        dtpDateCreated.Text = JOL.Date;
                        JolDocNum = JOL.DocNum;
                        conn.OpenConnection();
                        dgvItemList.DataSource = conn.ShowDataInGridView("select * from job_order_body_view where [Job Number] = '" + JolDocNum + "'");
                        conn.CloseConnection();
                        conn.OpenConnection();
                        SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
                        if (drDT.Read())
                        {
                            docSeries = drDT["series_desc"].ToString();
                            conn.CloseConnection();
                        }
                        lblDocumentSeries.Text = docSeries;
                        lblDocumentNumber.Text = JolDocNum;
                        btnAccept.Enabled = true;

                        con.Open();
                        SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + JOL.empID + "'", con);
                        SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                        DataTable employeeDT = new DataTable();
                        employeeDT.Load(employeeDR);
                        cboEstimatedBy.DataSource = employeeDT;
                        cboEstimatedBy.DisplayMember = "Name";
                        cboEstimatedBy.ValueMember = "ID";
                        con.Close();

                        con.Open();
                        SqlCommand technicianCMD = new SqlCommand("select * from Technician where ID ='" + JOL.techID + "'", con);
                        SqlDataReader technicianDR = technicianCMD.ExecuteReader();
                        DataTable technicianDT = new DataTable();
                        technicianDT.Load(technicianDR);
                        cboTechnicianName.DataSource = technicianDT;
                        cboTechnicianName.DisplayMember = "Name";
                        cboTechnicianName.ValueMember = "ID";
                        con.Close();


                        con.Open();
                        SqlCommand cmdVehicle = new SqlCommand("select * from dbo.Vehicles_of_customer where [Customer ID] = '" + JOL.CusID + "'and [ID] = '"+JOL.CarID+"'", con);
                        SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                        DataTable dtVehicle = new DataTable();
                        dtVehicle.Load(drVehicle);
                        cboVehicleModel.DataSource = dtVehicle;
                        cboVehicleModel.DisplayMember = "Vehicle Model";
                        cboVehicleModel.ValueMember = "ID";
                        cmdVehicle.ExecuteNonQuery();
                        con.Close();


                        con.Open();
                        SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                        SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                        DataTable dtCategory = new DataTable();
                        dtCategory.Load(drCategory);
                        cboCategory.DataSource = dtCategory;
                        cboCategory.DisplayMember = "vehicle_cat_desc";
                        cboCategory.ValueMember = "vehicle_cat_id";
                        cmdVeCat.ExecuteNonQuery();
                        con.Close();

                        if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                        {
                            vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                            if (JOL.CusID != 0 && JOL.CarID != 0)
                            {

                                conn.OpenConnection();
                                /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                SqlDataReader dr = cmdV.ExecuteReader();*/

                                SqlDataReader dr = conn.DataReader("select * from dbo.Vehicles_of_customer where  [ID] = '" + JOL.CarID + "'");
                                if (dr.Read())
                                {
                                    cboVehicleModel.Enabled = true;
                                    txtVehicleMake.Text = dr["Auto"].ToString();
                                    txtVehicleSize.Text = dr["Size"].ToString();
                                    txtVehicleColor.Text = dr["color"].ToString();
                                    txtPlateNumber.Text = dr["Platenumber"].ToString();
                                    cboCategory.Text = dr["Vehicle Category"].ToString();
                                    vehicleID = JOL.CarID;
                                    conn.CloseConnection();
                                }
                                else
                                {
                                    MessageBox.Show(vehicleID + " " + cusID);
                                }
                            }
                        }
                        else
                        {

                        }
                        
                        refreshTotal();
                        btnCancel.Visible = true;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        private void refreshTotal()
        {
            
            conn.OpenConnection();
            SqlDataReader totalReader = conn.DataReader("select format(SUM([Price] * [Quantity]), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from Job_order_body_view where [Job Number] = '" + JolDocNum + "' ");
            totalReader.Read();
            TotalItem = totalReader["Price_total"].ToString();
            TotalPaymentDue = totalReader["Total_amount"].ToString();
            TotalTax = totalReader["Tax_total"].ToString();
            conn.CloseConnection();

            lblTotalPaymentDue.Text = TotalPaymentDue;
            lblTotalItem.Text = TotalItem;
            lblTaxValue.Text = TotalTax;
        }

        private void Service_Invoice_Load(object sender, EventArgs e)
        {
            conn.OpenConnection();
            SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Sales Invoice' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
            if (drDT.Read())
            {
                string invoiceSeries = drDT["series_desc"].ToString();
                conn.CloseConnection();
                lblInvoiceSeries.Text = invoiceSeries;
            }
            conn.OpenConnection();
            SqlDataReader drDN = conn.DataReader("select max(sales_invoice_doc_num) as current_doc_num from dbo.sales_invoice");
            drDN.Read();
            if (drDN["current_doc_num"].ToString() != "")
            {
                docNum = drDN["current_doc_num"].ToString();
                iDocNum = Int32.Parse(docNum) + 1;

                nInvoiceNumber = iDocNum.ToString().PadLeft(7, '0');
                nDocumentSeries = docSeries;
                lblInvoiceNumber.Text = nInvoiceNumber;
                

            }
            else
            {
                docNum = "0000001";
                nInvoiceNumber = docNum;
                iDocNum = Int32.Parse(docNum);
                lblInvoiceNumber.Text = nInvoiceNumber;
                conn.CloseConnection();
            }
            label11.Visible = false;
            cboTechnicianName.Visible = false;
            if(transaction_List != null)
            {
                conn.OpenConnection();
                SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + transaction_List.CusID + "'");
                cusDR.Read();
                txtCustomerName.Text = cusDR["name"].ToString();
                ntxtCustomerName = txtCustomerName.Text;
                txtContactNumber.Text = cusDR["Contact Number"].ToString();
                txtAddress.Text = cusDR["address"].ToString();
                conn.CloseConnection();

                btnPrint.Enabled = false;

                groupBox4.Enabled = false;
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox5.Enabled = false;

                nEmployeeID = transaction_List.empID;
                nTechnician = transaction_List.techID;
                cusID = transaction_List.CusID;
                /*MessageBox.Show(cusID.ToString());*/
                dtpDateCreated.Text = JOL.Date;

                conn.OpenConnection();
                dgvItemList.DataSource = conn.ShowDataInGridView("select * from sales_invoice_Details_view where [Document Number] = '" + transaction_List.SIDocNum + "'");
                conn.CloseConnection();
                conn.OpenConnection();
                SqlDataReader sidt = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
                if (sidt.Read())
                {
                    docSeries = sidt["series_desc"].ToString();
                    conn.CloseConnection();
                }
                lblDocumentSeries.Text = docSeries;
                lblInvoiceNumber.Text = transaction_List.SIDocNum;
                con.Open();

                SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + transaction_List.empID + "'", con);
                SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                DataTable employeeDT = new DataTable();
                employeeDT.Load(employeeDR);
                cboEstimatedBy.DataSource = employeeDT;
                cboEstimatedBy.DisplayMember = "Name";
                cboEstimatedBy.ValueMember = "ID";
                con.Close();

                con.Open();
                SqlCommand technicianCMD = new SqlCommand("select * from Technician where ID ='" + transaction_List.techID + "'", con);
                SqlDataReader technicianDR = technicianCMD.ExecuteReader();
                DataTable technicianDT = new DataTable();
                technicianDT.Load(technicianDR);
                cboTechnicianName.DataSource = technicianDT;
                cboTechnicianName.DisplayMember = "Name";
                cboTechnicianName.ValueMember = "ID";
                con.Close();

                conn.OpenConnection();
                SqlDataReader vdr = conn.DataReader("select * from job_order where job_number = '" + transaction_List.DocNum + "'");
                vdr.Read();
                vehicleID = Int32.Parse(vdr["car_id"].ToString());
                conn.CloseConnection();

                con.Open();
                SqlCommand cmdVehicle = new SqlCommand("select * from dbo.vehicles_of_customer where [Customer ID] = '" + transaction_List.CusID + "' and [ID] = '" + vehicleID + "' ", con);
                SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                DataTable dtVehicle = new DataTable();
                dtVehicle.Load(drVehicle);
                cboVehicleModel.DataSource = dtVehicle;
                cboVehicleModel.DisplayMember = "Vehicle Model";
                cboVehicleModel.ValueMember = "ID";
                cmdVehicle.ExecuteNonQuery();
                con.Close();


                con.Open();
                SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                DataTable dtCategory = new DataTable();
                dtCategory.Load(drCategory);
                cboCategory.DataSource = dtCategory;
                cboCategory.DisplayMember = "vehicle_cat_desc";
                cboCategory.ValueMember = "vehicle_cat_id";
                cmdVeCat.ExecuteNonQuery();
                con.Close();


                if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                {
                    vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                    if (transaction_List.CusID != 0)
                    {

                        conn.OpenConnection();
                        /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                        SqlDataReader dr = cmdV.ExecuteReader();*/

                        SqlDataReader dr = conn.DataReader("select * from dbo.vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                        if (dr.Read())
                        {
                            cboVehicleModel.Enabled = true;
                            txtVehicleMake.Text = dr["Auto"].ToString();
                            txtVehicleSize.Text = dr["Size"].ToString();
                            txtVehicleColor.Text = dr["color"].ToString();
                            txtPlateNumber.Text = dr["Platenumber"].ToString();
                            cboCategory.Text = dr["Vehicle Category"].ToString();
                            
                            conn.CloseConnection();
                        }
                        else
                        {
                            MessageBox.Show(vehicleID + " " + cusID);
                        }
                    }

                    conn.OpenConnection();
                    SqlDataReader totalReader = conn.DataReader("select format(SUM([Price]), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from sales_invoice_Details_view where [Document Number] = '" + transaction_List.SIDocNum + "' ");
                    totalReader.Read();
                    TotalItem = totalReader["Price_total"].ToString();
                    TotalPaymentDue = totalReader["Total_amount"].ToString();
                    TotalTax = totalReader["Tax_total"].ToString();
                    conn.CloseConnection();

                    lblTotalPaymentDue.Text = TotalPaymentDue;
                    lblTotalItem.Text = TotalItem;
                    lblTaxValue.Text = TotalTax;
                    btnCancel.Visible = true;
                    btnAccept.Enabled = false;
                }
                else
                {

                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            
/*            nEmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());
            nTechnician = Int32.Parse(cboTechnicianName.SelectedValue.ToString());*/

            conn.OpenConnection();
            
            if (cusID != 0 || nEmployeeID != 0 || nTechnician != 0)
            {
                string dateNow = DateTime.Now.ToString("yyyy-MM-dd");
                double nTotalPaymentDue = double.Parse(TotalPaymentDue);
                conn.OpenConnection();
                conn.ExecuteQueries("insert into sales_invoice (sales_invoice_doc_num,job_number, customer_id,[date],employee_id, status_id, total_price,date_posted,date_created)" +
                    " values" +
                    " ('" + nInvoiceNumber + "'" +
                    ", '" + JolDocNum + "'" +
                    ", '" + cusID + "'" +
                    ", '" + dateNow + "'" +
                    ", '" + nEmployeeID + "'" +
                    ", '" + 5 + "'" +
                    ", '" + nTotalPaymentDue + "'" +
                    ", '" + dateNow + "'" +
                    ", '" + dateNow +"')");
                conn.CloseConnection();

                conn.OpenConnection();
                conn.ExecuteQueries("insert into sales_invoice_body (sales_invoice_doc_num, inventory_code, quantity, price, uom_id, tax_id, tax_price, total_price) select '"+nInvoiceNumber+"', inventory_code, quantity, price, uom_id, tax_id, tax_price, total_price from job_order_body where job_number = '"+JolDocNum+"'");
                conn.CloseConnection();

                conn.OpenConnection();
                conn.ExecuteQueries("update job_order set status_id = 6 where job_number = '"+JolDocNum+"'");
                conn.CloseConnection();

                MessageBox.Show("Sales Invoice Saved!", "Notify", MessageBoxButtons.OK,MessageBoxIcon.Information );
                this.Close();
                
            }
            else
            {
                MessageBox.Show("code error");
            }
        }

        private void btnServiceInvoiceList_Click(object sender, EventArgs e)
        {
           
        }

        private void btnServiceInvoiceList_Click_1(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Service_Invoice_List SIL = new Service_Invoice_List())
                {

                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    SIL.Owner = newFormDialog;

                    if (SIL.ShowDialog() != DialogResult.OK && SIL.CusID != 0)
                    {
                        newFormDialog.Dispose();
                        conn.OpenConnection();
                        SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + SIL.CusID + "'");
                        cusDR.Read();
                        txtCustomerName.Text = cusDR["name"].ToString();
                        ntxtCustomerName = txtCustomerName.Text;
                        txtContactNumber.Text = cusDR["Contact Number"].ToString();
                        txtAddress.Text = cusDR["address"].ToString();
                        conn.CloseConnection();
                        if (SIL.Status == "OPEN")
                        {
                            btnPrint.Enabled = false;
                        }
                        else
                        {
                            btnPrint.Enabled = true;
                        }


                        nEmployeeID = SIL.empID;
                        nTechnician = SIL.techID;
                        cusID = SIL.CusID;
                        /*MessageBox.Show(cusID.ToString());*/
                        dtpDateCreated.Text = JOL.Date;
                        Sildocnum = SIL.DocNum;
                        conn.OpenConnection();
                        dgvItemList.DataSource = conn.ShowDataInGridView("select * from sales_invoice_Details_view where [Document Number] = '" + SIL.SIDocNum + "'");
                        conn.CloseConnection();
                        conn.OpenConnection();
                        SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
                        if (drDT.Read())
                        {
                            docSeries = drDT["series_desc"].ToString();
                            conn.CloseConnection();
                        }
                        lblDocumentSeries.Text = docSeries;
                        lblDocumentNumber.Text = Sildocnum;
                        lblInvoiceNumber.Text = SIL.SIDocNum;

                        con.Open();
                        SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + SIL.empID + "'", con);
                        SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                        DataTable employeeDT = new DataTable();
                        employeeDT.Load(employeeDR);
                        cboEstimatedBy.DataSource = employeeDT;
                        cboEstimatedBy.DisplayMember = "Name";
                        cboEstimatedBy.ValueMember = "ID";
                        con.Close();

                        con.Open();
                        SqlCommand technicianCMD = new SqlCommand("select * from Technician where ID ='" + SIL.techID + "'", con);
                        SqlDataReader technicianDR = technicianCMD.ExecuteReader();
                        DataTable technicianDT = new DataTable();
                        technicianDT.Load(technicianDR);
                        cboTechnicianName.DataSource = technicianDT;
                        cboTechnicianName.DisplayMember = "Name";
                        cboTechnicianName.ValueMember = "ID";
                        con.Close();

                        conn.OpenConnection();
                        SqlDataReader vdr = conn.DataReader("select * from job_order where job_number = '"+SIL.DocNum+"'");
                        vdr.Read();
                        vehicleID = Int32.Parse(vdr["car_id"].ToString());
                        conn.CloseConnection();

                        con.Open();
                        SqlCommand cmdVehicle = new SqlCommand("select * from dbo.vehicles_of_customer where [Customer ID] = '" + SIL.CusID + "' and [ID] = '"+vehicleID+"' ", con);
                        SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                        DataTable dtVehicle = new DataTable();
                        dtVehicle.Load(drVehicle);
                        cboVehicleModel.DataSource = dtVehicle;
                        cboVehicleModel.DisplayMember = "Vehicle Model";
                        cboVehicleModel.ValueMember = "ID";
                        cmdVehicle.ExecuteNonQuery();
                        con.Close();


                        con.Open();
                        SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                        SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                        DataTable dtCategory = new DataTable();
                        dtCategory.Load(drCategory);
                        cboCategory.DataSource = dtCategory;
                        cboCategory.DisplayMember = "vehicle_cat_desc";
                        cboCategory.ValueMember = "vehicle_cat_id";
                        cmdVeCat.ExecuteNonQuery();
                        con.Close();


                        if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                        {
                            vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                            if (SIL.CusID != 0)
                            {

                                conn.OpenConnection();
                                /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                SqlDataReader dr = cmdV.ExecuteReader();*/

                                SqlDataReader dr = conn.DataReader("select * from dbo.vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                                if (dr.Read())
                                {
                                    cboVehicleModel.Enabled = true;
                                    txtVehicleMake.Text = dr["Auto"].ToString();
                                    txtVehicleSize.Text = dr["Size"].ToString();
                                    txtVehicleColor.Text = dr["color"].ToString();
                                    txtPlateNumber.Text = dr["Platenumber"].ToString();
                                    cboCategory.Text = dr["Vehicle Category"].ToString();
                                    vehicleID = SIL.CarID;
                                    conn.CloseConnection();
                                }
                                else
                                {
                                    MessageBox.Show(vehicleID + " " + cusID);
                                }
                            }


                        }
                        else
                        {

                        }
                        conn.OpenConnection();
                        SqlDataReader totalReader = conn.DataReader("select format(SUM([Price]), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from sales_invoice_Details_view where [Document Number] = '" + SIL.SIDocNum + "' ");
                        totalReader.Read();
                        TotalItem = totalReader["Price_total"].ToString();
                        TotalPaymentDue = totalReader["Total_amount"].ToString();
                        TotalTax = totalReader["Tax_total"].ToString();
                        conn.CloseConnection();

                        lblTotalPaymentDue.Text = TotalPaymentDue;
                        lblTotalItem.Text = TotalItem;
                        lblTaxValue.Text = TotalTax;
                        btnCancel.Visible = true;
                        btnAccept.Enabled = false;
                        con.Close();
                        btnAccept.Enabled = false;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
