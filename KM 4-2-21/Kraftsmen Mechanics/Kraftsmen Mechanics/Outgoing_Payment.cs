﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Outgoing_Payment : Form
    {
        public string docNum, supCode, supName, Contact, Address,totalTax,cash, remainingBalance;
        public int inst;
        public double totalAmount,Balance,RBalance;
        public string akin;

        public Outgoing_Payment()
        {
            InitializeComponent();
        }
        Connection outgoing = new Connection();

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            outgoing.OpenConn();
            dgvServiceInvoiceList.DataSource = outgoing.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view where [Document Number]  like '%" + txtSearch.Text + "%'");
            outgoing.CloseConn();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            string xDate = DateTime.Now.ToShortDateString();
            string xCode = supCode;



            if (double.Parse(txtCash.Text) == 0)
            {
                MessageBox.Show("Cash value is invalid", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                double xCash = double.Parse(txtCash.Text);
                double xRBalance = double.Parse(remainingBalance) - xCash;


                outgoing.OpenConn();
                outgoing.ExecuteQuery("insert into outgoing_payment(supplier_code, ap_inv_doc_num,date,amount, remaining_balance)" +
                    "VALUES('" + xCode + "', '" + lblInvoiceNumber.Text + "', '" + xDate + "', '" + double.Parse(txtCash.Text) + "', '"+xRBalance+"')");
                outgoing.CloseConn();

                if (txtCash.Text == lblRemainingBalance.Text)
                {
                    outgoing.OpenConn();
                    outgoing.ExecuteQuery("update ap_invoice set status_id = '" + 6 + "' where ap_invoice_doc_num = '" + lblInvoiceNumber.Text + "'");
                    outgoing.CloseConn();

                    MessageBox.Show("Payment Saved", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    txtCustomerName.Text = "";
                    this.Close();
                }
                else
                {

                    outgoing.OpenConn();
                    outgoing.ExecuteQuery("update ap_invoice set remaining_balance = '" + xRBalance + "'");
                    outgoing.CloseConn();

                    if (xRBalance == 0)
                    {
                        outgoing.OpenConn();
                        outgoing.ExecuteQuery("update ap_invoice set status_id = '" + 6 + "' where ap_invoice_doc_num = '" + lblInvoiceNumber.Text + "'");
                        outgoing.CloseConn();
                    }
                    MessageBox.Show("Payment Saved", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    txtCustomerName.Text = "";
                    this.Close();
                }
            }
        }

        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            cash = txtCash.Text;

            if (txtCash.Text == "" || txtCash.Text == null)
            {
                txtCash.Text = "0";
            }

            if (double.Parse(cash.ToString()) > RBalance)
            {
                txtCash.Text = RBalance.ToString();
            }

            
            Balance = RBalance - double.Parse(cash.ToString());
            lblRemainingBalance.Text = Balance.ToString("n2");

            
        }

       
        private void Outgoing_Payment_Load(object sender, EventArgs e)
        {
            outgoing.OpenConn();
            dgvServiceInvoiceList.DataSource = outgoing.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view");
            outgoing.CloseConn();

            txtCash.Text = "0";
            txtCash.Enabled = false;

            this.dgvServiceInvoiceList.Columns["Docnum"].Visible = false;
        }

        #region trash
        private void dgvServiceInvoiceList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        #endregion
        private void dgvServiceInvoiceList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
                docNum = this.dgvServiceInvoiceList.CurrentRow.Cells[7].Value.ToString();
                totalAmount = double.Parse(this.dgvServiceInvoiceList.CurrentRow.Cells[5].Value.ToString());

                lblInvoiceNumber.Text = docNum;

                outgoing.OpenConn();
                SqlDataReader drInv = outgoing.DataReader("Select a.supplier_code 'Supplier Code', b.name 'Supplier Name', b.contact_number 'Contact Number', b.address 'Address', (Select format(SUM(tax_price), '#,#.00') from ap_inv_body where ap_inv_doc_num = '" + docNum + "') 'Total Tax',a.remaining_balance 'Remaining balance',a.Installment 'Installment' from ap_invoice a left join supplier b on a.supplier_code = b.supplier_code where a.ap_invoice_doc_num = '" + docNum + "'");
                drInv.Read();

                supCode = drInv["Supplier Code"].ToString();
                supName = drInv["Supplier Name"].ToString();
                Contact = drInv["Contact Number"].ToString();
                Address = drInv["Address"].ToString();
                totalTax = drInv["Total Tax"].ToString();
                remainingBalance = drInv["Remaining Balance"].ToString();
                RBalance = double.Parse(drInv["Remaining Balance"].ToString());
                inst = Int32.Parse(drInv["Installment"].ToString());

                outgoing.CloseConn();

                if(inst == 1)
                {
                  lblInstallment.Visible = true;
                  txtCash.Text = "0";
                  txtCash.ReadOnly = false;
                  lblRemainingBalance.Text = remainingBalance;
                }
                else
                {
                    lblInstallment.Visible = false;
                    txtCash.Text = totalAmount.ToString();
                    txtCash.ReadOnly = true;
                    Balance = totalAmount - double.Parse(cash.ToString());
                    lblRemainingBalance.Text = Balance.ToString("n2");
                }
                
                txtCustomerName.Text = supName;
                txtContactNumber.Text = Contact;
                txtAddress.Text = Address;
                lblTaxValue.Text = totalTax;
                

                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                groupBox6.Enabled = true;
                txtCash.Enabled = true;

                txtCustomerName.ReadOnly = true;
                txtAddress.ReadOnly = true;
                txtContactNumber.ReadOnly = true;


                outgoing.OpenConn();
                SqlDataReader drCount = outgoing.DataReader("select COUNT(*) 'Item Count' from ap_inv_body where ap_inv_doc_num = '" + docNum + "'");
                drCount.Read();
                lblTotalItem.Text = drCount["Item Count"].ToString();
                lblTotalPaymentDue.Text = totalAmount.ToString();
                outgoing.CloseConn();

/*
                outgoing.OpenConn();
                SqlDataReader red = outgoing.DataReader("Select Right([Document Number],7) 'Docnum'from Ap_Inv_List_View");
                red.Read();
                akin = red["Docnum"].ToString();
                // lblTotalPaymentDue.Text = totalAmount.ToString();

                outgoing.CloseConn();*/
            
          
        }
    }
}
