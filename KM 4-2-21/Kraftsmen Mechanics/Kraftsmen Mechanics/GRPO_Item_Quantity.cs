﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class GRPO_Item_Quantity : Form
    {
        public GRPO_Item_Quantity()
        {
            InitializeComponent();
        }
        Connection itmQty = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void txtProductCode_TextChanged(object sender, EventArgs e)
        {

        }

        public void showTotal()
        {

            Goods_Receipt_Purchase_Order grpo = new Goods_Receipt_Purchase_Order();
            //-----------------------------------------//
            itmQty.OpenConn();

            SqlDataReader drTotal = itmQty.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from po_body where po_document_number = '" + lblhidden.Text + "'");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            grpo.lblTotalBeforeDiscount.Text = total_price;
            grpo.lblTaxValue.Text = total_tax;
            grpo.lblTotalPaymentDue.Text = total_amount;
            itmQty.CloseConn();
            //--------------------------------------------------------------------//
        }

        private void GRPO_Item_Quantity_Load(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            itmQty.OpenConn();
            itmQty.ExecuteQuery("Update draft_grpo set quantity = '" + txtProductQuantity.Text + "' ,total_price = '" + txtProductQuantity.Text + "' * price where inventory_code = '" + txtProductCode.Text + "'");
            itmQty.CloseConn();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
