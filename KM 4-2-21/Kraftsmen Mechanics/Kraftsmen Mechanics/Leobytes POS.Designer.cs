﻿
namespace Kraftsmen_Mechanics
{
    partial class Leobytes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Leobytes));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TransactionTimer = new System.Windows.Forms.Timer(this.components);
            this.ReportsTimer = new System.Windows.Forms.Timer(this.components);
            this.panelTitleBar = new System.Windows.Forms.Panel();
            this.btnMinimize = new FontAwesome.Sharp.IconPictureBox();
            this.btnMaximize = new FontAwesome.Sharp.IconPictureBox();
            this.btnCloseForm = new FontAwesome.Sharp.IconPictureBox();
            this.btnMenu = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.menuAnimation = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.PanelContainer = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnServices = new FontAwesome.Sharp.IconButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlPurchasing = new System.Windows.Forms.Panel();
            this.btnPurchasingList = new System.Windows.Forms.Button();
            this.btnSortPurchasing = new FontAwesome.Sharp.IconPictureBox();
            this.btnOutgoingPayment = new System.Windows.Forms.Button();
            this.btnInvoice = new System.Windows.Forms.Button();
            this.btnGRPO = new System.Windows.Forms.Button();
            this.btnPurchasingOrder = new System.Windows.Forms.Button();
            this.btnDropdownPurchasing = new FontAwesome.Sharp.IconButton();
            this.pnlTransaction = new System.Windows.Forms.Panel();
            this.btnTransactionList = new System.Windows.Forms.Button();
            this.lblServicesNotificationCount = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnSortTransaction = new FontAwesome.Sharp.IconPictureBox();
            this.lblIncomingPaymentNotification = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblWorkingProgressNumber = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnIncomingPayment = new System.Windows.Forms.Button();
            this.btnServiceInvoice = new System.Windows.Forms.Button();
            this.btnWorkingProgress = new System.Windows.Forms.Button();
            this.btnJobOrder = new System.Windows.Forms.Button();
            this.btnJobEstimate = new System.Windows.Forms.Button();
            this.btnDropdownTransaction = new FontAwesome.Sharp.IconButton();
            this.pnlReports = new System.Windows.Forms.Panel();
            this.btnSortReports = new FontAwesome.Sharp.IconPictureBox();
            this.btnInventoryReport = new System.Windows.Forms.Button();
            this.btnSalesReport = new System.Windows.Forms.Button();
            this.btnPurchasingOrderReport = new System.Windows.Forms.Button();
            this.btnDropdownReports = new FontAwesome.Sharp.IconButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnSettings = new FontAwesome.Sharp.IconButton();
            this.pnlCategories = new System.Windows.Forms.Panel();
            this.btnSortCategories = new FontAwesome.Sharp.IconPictureBox();
            this.btnVehicleCategory = new System.Windows.Forms.Button();
            this.btnProductCategory = new System.Windows.Forms.Button();
            this.btnDropdownCategories = new FontAwesome.Sharp.IconButton();
            this.btnProducts = new FontAwesome.Sharp.IconButton();
            this.pnlServices = new System.Windows.Forms.Panel();
            this.btnSortServices = new FontAwesome.Sharp.IconPictureBox();
            this.btnBillOfMaterials = new System.Windows.Forms.Button();
            this.btnManageServices = new System.Windows.Forms.Button();
            this.btnVehicles = new FontAwesome.Sharp.IconButton();
            this.btnCustomers = new FontAwesome.Sharp.IconButton();
            this.btnDashboard = new FontAwesome.Sharp.IconButton();
            this.RefreshServiceNotification = new System.Windows.Forms.Timer(this.components);
            this.CategoriesDropdown = new System.Windows.Forms.Timer(this.components);
            this.datetime = new System.Windows.Forms.Timer(this.components);
            this.ServicesTimer = new System.Windows.Forms.Timer(this.components);
            this.panelTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlPurchasing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPurchasing)).BeginInit();
            this.pnlTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortTransaction)).BeginInit();
            this.pnlReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortReports)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.pnlCategories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortCategories)).BeginInit();
            this.pnlServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortServices)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TransactionTimer
            // 
            this.TransactionTimer.Interval = 10;
            this.TransactionTimer.Tick += new System.EventHandler(this.TransactionTimer_Tick);
            // 
            // ReportsTimer
            // 
            this.ReportsTimer.Interval = 10;
            this.ReportsTimer.Tick += new System.EventHandler(this.ReportsTimer_Tick);
            // 
            // panelTitleBar
            // 
            this.panelTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(88)))));
            this.panelTitleBar.Controls.Add(this.btnMinimize);
            this.panelTitleBar.Controls.Add(this.btnMaximize);
            this.panelTitleBar.Controls.Add(this.btnCloseForm);
            this.panelTitleBar.Controls.Add(this.btnMenu);
            this.panelTitleBar.Controls.Add(this.bunifuCustomLabel1);
            this.panelTitleBar.Controls.Add(this.bunifuImageButton1);
            this.menuAnimation.SetDecoration(this.panelTitleBar, BunifuAnimatorNS.DecorationType.None);
            this.panelTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleBar.Location = new System.Drawing.Point(275, 0);
            this.panelTitleBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelTitleBar.Name = "panelTitleBar";
            this.panelTitleBar.Size = new System.Drawing.Size(1281, 47);
            this.panelTitleBar.TabIndex = 3;
            this.panelTitleBar.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTitleBar_Paint);
            // 
            // btnMinimize
            // 
            this.btnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(88)))));
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnMinimize, BunifuAnimatorNS.DecorationType.None);
            this.btnMinimize.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.btnMinimize.IconColor = System.Drawing.Color.White;
            this.btnMinimize.IconFont = FontAwesome.Sharp.IconFont.Solid;
            this.btnMinimize.IconSize = 20;
            this.btnMinimize.Location = new System.Drawing.Point(1152, 14);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnMinimize.Size = new System.Drawing.Size(23, 20);
            this.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnMinimize.TabIndex = 3;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            this.btnMinimize.MouseLeave += new System.EventHandler(this.btnMinimize_MouseLeave);
            this.btnMinimize.MouseHover += new System.EventHandler(this.btnMinimize_MouseHover);
            // 
            // btnMaximize
            // 
            this.btnMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(88)))));
            this.btnMaximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnMaximize, BunifuAnimatorNS.DecorationType.None);
            this.btnMaximize.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize;
            this.btnMaximize.IconColor = System.Drawing.Color.White;
            this.btnMaximize.IconFont = FontAwesome.Sharp.IconFont.Solid;
            this.btnMaximize.IconSize = 28;
            this.btnMaximize.Location = new System.Drawing.Point(1197, 11);
            this.btnMaximize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMaximize.Name = "btnMaximize";
            this.btnMaximize.Padding = new System.Windows.Forms.Padding(3, 4, 0, 0);
            this.btnMaximize.Size = new System.Drawing.Size(33, 28);
            this.btnMaximize.TabIndex = 2;
            this.btnMaximize.TabStop = false;
            this.btnMaximize.Click += new System.EventHandler(this.btnMaximize_Click);
            this.btnMaximize.MouseLeave += new System.EventHandler(this.btnMaximize_MouseLeave);
            this.btnMaximize.MouseHover += new System.EventHandler(this.btnMaximize_MouseHover);
            // 
            // btnCloseForm
            // 
            this.btnCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(88)))));
            this.btnCloseForm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnCloseForm, BunifuAnimatorNS.DecorationType.None);
            this.btnCloseForm.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.btnCloseForm.IconColor = System.Drawing.Color.White;
            this.btnCloseForm.IconFont = FontAwesome.Sharp.IconFont.Solid;
            this.btnCloseForm.IconSize = 28;
            this.btnCloseForm.Location = new System.Drawing.Point(1239, 12);
            this.btnCloseForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCloseForm.Name = "btnCloseForm";
            this.btnCloseForm.Padding = new System.Windows.Forms.Padding(3, 4, 0, 0);
            this.btnCloseForm.Size = new System.Drawing.Size(31, 28);
            this.btnCloseForm.TabIndex = 1;
            this.btnCloseForm.TabStop = false;
            this.btnCloseForm.Click += new System.EventHandler(this.btnCloseForm_Click);
            this.btnCloseForm.MouseLeave += new System.EventHandler(this.btnCloseForm_MouseLeave);
            this.btnCloseForm.MouseHover += new System.EventHandler(this.btnCloseForm_MouseHover);
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.Color.Transparent;
            this.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
            this.btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Image")));
            this.btnMenu.ImageActive = null;
            this.btnMenu.Location = new System.Drawing.Point(20, 12);
            this.btnMenu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(27, 25);
            this.btnMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMenu.TabIndex = 0;
            this.btnMenu.TabStop = false;
            this.btnMenu.Zoom = 10;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.menuAnimation.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(116, 14);
            this.bunifuCustomLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(97, 20);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "LEOBYTES";
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.menuAnimation.SetDecoration(this.bunifuImageButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(55, 0);
            this.bunifuImageButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(53, 47);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 0;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            // 
            // menuAnimation
            // 
            this.menuAnimation.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.menuAnimation.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.menuAnimation.DefaultAnimation = animation1;
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = true;
            this.PanelContainer.AutoSize = true;
            this.PanelContainer.Controls.Add(this.lblDate);
            this.PanelContainer.Controls.Add(this.lblTime);
            this.PanelContainer.Controls.Add(this.pictureBox1);
            this.menuAnimation.SetDecoration(this.PanelContainer, BunifuAnimatorNS.DecorationType.None);
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContainer.Location = new System.Drawing.Point(275, 47);
            this.PanelContainer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PanelContainer.Name = "PanelContainer";
            this.PanelContainer.Size = new System.Drawing.Size(1281, 837);
            this.PanelContainer.TabIndex = 2;
            this.PanelContainer.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelContainer_Paint);
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.AutoSize = true;
            this.menuAnimation.SetDecoration(this.lblDate, BunifuAnimatorNS.DecorationType.None);
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(455, 597);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(370, 36);
            this.lblDate.TabIndex = 2;
            this.lblDate.Text = "Monday, January 26, 2021";
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.AutoSize = true;
            this.menuAnimation.SetDecoration(this.lblTime, BunifuAnimatorNS.DecorationType.None);
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 64F);
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(352, 478);
            this.lblTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(658, 120);
            this.lblTime.TabIndex = 1;
            this.lblTime.Text = "00:00:00 AM";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuAnimation.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(236, -57);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(851, 650);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnServices
            // 
            this.btnServices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnServices.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnServices, BunifuAnimatorNS.DecorationType.None);
            this.btnServices.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnServices.FlatAppearance.BorderSize = 0;
            this.btnServices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServices.ForeColor = System.Drawing.Color.White;
            this.btnServices.IconChar = FontAwesome.Sharp.IconChar.ClipboardCheck;
            this.btnServices.IconColor = System.Drawing.Color.White;
            this.btnServices.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnServices.IconSize = 18;
            this.btnServices.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnServices.Location = new System.Drawing.Point(0, 0);
            this.btnServices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnServices.Name = "btnServices";
            this.btnServices.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnServices.Size = new System.Drawing.Size(275, 52);
            this.btnServices.TabIndex = 12;
            this.btnServices.Text = "Services";
            this.btnServices.UseVisualStyleBackColor = false;
            this.btnServices.Click += new System.EventHandler(this.btnServices_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(119)))), ((int)(((byte)(147)))));
            this.menuAnimation.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(275, 50);
            this.panel2.TabIndex = 2;
            // 
            // pnlPurchasing
            // 
            this.pnlPurchasing.Controls.Add(this.btnPurchasingList);
            this.pnlPurchasing.Controls.Add(this.btnSortPurchasing);
            this.pnlPurchasing.Controls.Add(this.btnOutgoingPayment);
            this.pnlPurchasing.Controls.Add(this.btnInvoice);
            this.pnlPurchasing.Controls.Add(this.btnGRPO);
            this.pnlPurchasing.Controls.Add(this.btnPurchasingOrder);
            this.pnlPurchasing.Controls.Add(this.btnDropdownPurchasing);
            this.menuAnimation.SetDecoration(this.pnlPurchasing, BunifuAnimatorNS.DecorationType.None);
            this.pnlPurchasing.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPurchasing.Location = new System.Drawing.Point(0, 102);
            this.pnlPurchasing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlPurchasing.MaximumSize = new System.Drawing.Size(277, 229);
            this.pnlPurchasing.MinimumSize = new System.Drawing.Size(277, 50);
            this.pnlPurchasing.Name = "pnlPurchasing";
            this.pnlPurchasing.Size = new System.Drawing.Size(277, 50);
            this.pnlPurchasing.TabIndex = 3;
            // 
            // btnPurchasingList
            // 
            this.btnPurchasingList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPurchasingList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnPurchasingList, BunifuAnimatorNS.DecorationType.None);
            this.btnPurchasingList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPurchasingList.FlatAppearance.BorderSize = 0;
            this.btnPurchasingList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchasingList.Location = new System.Drawing.Point(0, 194);
            this.btnPurchasingList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPurchasingList.Name = "btnPurchasingList";
            this.btnPurchasingList.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPurchasingList.Size = new System.Drawing.Size(277, 34);
            this.btnPurchasingList.TabIndex = 7;
            this.btnPurchasingList.Text = "Purchasing List";
            this.btnPurchasingList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurchasingList.UseVisualStyleBackColor = false;
            this.btnPurchasingList.Click += new System.EventHandler(this.btnPurchasingList_Click);
            // 
            // btnSortPurchasing
            // 
            this.btnSortPurchasing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSortPurchasing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSortPurchasing, BunifuAnimatorNS.DecorationType.None);
            this.btnSortPurchasing.IconChar = FontAwesome.Sharp.IconChar.SortDown;
            this.btnSortPurchasing.IconColor = System.Drawing.Color.White;
            this.btnSortPurchasing.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSortPurchasing.IconSize = 15;
            this.btnSortPurchasing.Location = new System.Drawing.Point(239, 17);
            this.btnSortPurchasing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortPurchasing.Name = "btnSortPurchasing";
            this.btnSortPurchasing.Size = new System.Drawing.Size(15, 15);
            this.btnSortPurchasing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnSortPurchasing.TabIndex = 3;
            this.btnSortPurchasing.TabStop = false;
            this.btnSortPurchasing.Click += new System.EventHandler(this.btnSortPurchasing_Click);
            // 
            // btnOutgoingPayment
            // 
            this.btnOutgoingPayment.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOutgoingPayment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnOutgoingPayment, BunifuAnimatorNS.DecorationType.None);
            this.btnOutgoingPayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOutgoingPayment.FlatAppearance.BorderSize = 0;
            this.btnOutgoingPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOutgoingPayment.Location = new System.Drawing.Point(0, 160);
            this.btnOutgoingPayment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOutgoingPayment.Name = "btnOutgoingPayment";
            this.btnOutgoingPayment.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnOutgoingPayment.Size = new System.Drawing.Size(277, 34);
            this.btnOutgoingPayment.TabIndex = 4;
            this.btnOutgoingPayment.Text = "Outgoing Payment";
            this.btnOutgoingPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOutgoingPayment.UseVisualStyleBackColor = false;
            this.btnOutgoingPayment.Click += new System.EventHandler(this.btnOutgoingPayment_Click);
            // 
            // btnInvoice
            // 
            this.btnInvoice.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnInvoice, BunifuAnimatorNS.DecorationType.None);
            this.btnInvoice.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInvoice.FlatAppearance.BorderSize = 0;
            this.btnInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInvoice.Location = new System.Drawing.Point(0, 126);
            this.btnInvoice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnInvoice.Name = "btnInvoice";
            this.btnInvoice.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnInvoice.Size = new System.Drawing.Size(277, 34);
            this.btnInvoice.TabIndex = 3;
            this.btnInvoice.Text = "Invoice";
            this.btnInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInvoice.UseVisualStyleBackColor = false;
            this.btnInvoice.Click += new System.EventHandler(this.btnInvoice_Click);
            // 
            // btnGRPO
            // 
            this.btnGRPO.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGRPO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnGRPO, BunifuAnimatorNS.DecorationType.None);
            this.btnGRPO.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGRPO.FlatAppearance.BorderSize = 0;
            this.btnGRPO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGRPO.Location = new System.Drawing.Point(0, 90);
            this.btnGRPO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGRPO.Name = "btnGRPO";
            this.btnGRPO.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnGRPO.Size = new System.Drawing.Size(277, 36);
            this.btnGRPO.TabIndex = 2;
            this.btnGRPO.Text = "Goods Receipt Purchasing Order";
            this.btnGRPO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGRPO.UseVisualStyleBackColor = false;
            this.btnGRPO.Click += new System.EventHandler(this.btnGRPO_Click);
            // 
            // btnPurchasingOrder
            // 
            this.btnPurchasingOrder.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPurchasingOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnPurchasingOrder, BunifuAnimatorNS.DecorationType.None);
            this.btnPurchasingOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPurchasingOrder.FlatAppearance.BorderSize = 0;
            this.btnPurchasingOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchasingOrder.Location = new System.Drawing.Point(0, 52);
            this.btnPurchasingOrder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPurchasingOrder.Name = "btnPurchasingOrder";
            this.btnPurchasingOrder.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPurchasingOrder.Size = new System.Drawing.Size(277, 38);
            this.btnPurchasingOrder.TabIndex = 1;
            this.btnPurchasingOrder.Text = "Purchasing Order";
            this.btnPurchasingOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurchasingOrder.UseVisualStyleBackColor = false;
            this.btnPurchasingOrder.Click += new System.EventHandler(this.btnPurchasingOrder_Click);
            // 
            // btnDropdownPurchasing
            // 
            this.btnDropdownPurchasing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnDropdownPurchasing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnDropdownPurchasing, BunifuAnimatorNS.DecorationType.None);
            this.btnDropdownPurchasing.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDropdownPurchasing.FlatAppearance.BorderSize = 0;
            this.btnDropdownPurchasing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDropdownPurchasing.ForeColor = System.Drawing.Color.White;
            this.btnDropdownPurchasing.IconChar = FontAwesome.Sharp.IconChar.ClipboardList;
            this.btnDropdownPurchasing.IconColor = System.Drawing.Color.White;
            this.btnDropdownPurchasing.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDropdownPurchasing.IconSize = 20;
            this.btnDropdownPurchasing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDropdownPurchasing.Location = new System.Drawing.Point(0, 0);
            this.btnDropdownPurchasing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDropdownPurchasing.Name = "btnDropdownPurchasing";
            this.btnDropdownPurchasing.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnDropdownPurchasing.Size = new System.Drawing.Size(277, 52);
            this.btnDropdownPurchasing.TabIndex = 6;
            this.btnDropdownPurchasing.Text = "Purchasing";
            this.btnDropdownPurchasing.UseVisualStyleBackColor = false;
            this.btnDropdownPurchasing.Click += new System.EventHandler(this.btnDropdownPurchasing_Click);
            // 
            // pnlTransaction
            // 
            this.pnlTransaction.Controls.Add(this.btnTransactionList);
            this.pnlTransaction.Controls.Add(this.lblServicesNotificationCount);
            this.pnlTransaction.Controls.Add(this.btnSortTransaction);
            this.pnlTransaction.Controls.Add(this.lblIncomingPaymentNotification);
            this.pnlTransaction.Controls.Add(this.lblWorkingProgressNumber);
            this.pnlTransaction.Controls.Add(this.btnIncomingPayment);
            this.pnlTransaction.Controls.Add(this.btnServiceInvoice);
            this.pnlTransaction.Controls.Add(this.btnWorkingProgress);
            this.pnlTransaction.Controls.Add(this.btnJobOrder);
            this.pnlTransaction.Controls.Add(this.btnJobEstimate);
            this.pnlTransaction.Controls.Add(this.btnDropdownTransaction);
            this.menuAnimation.SetDecoration(this.pnlTransaction, BunifuAnimatorNS.DecorationType.None);
            this.pnlTransaction.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTransaction.Location = new System.Drawing.Point(0, 152);
            this.pnlTransaction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlTransaction.MaximumSize = new System.Drawing.Size(277, 279);
            this.pnlTransaction.MinimumSize = new System.Drawing.Size(277, 50);
            this.pnlTransaction.Name = "pnlTransaction";
            this.pnlTransaction.Size = new System.Drawing.Size(277, 50);
            this.pnlTransaction.TabIndex = 4;
            // 
            // btnTransactionList
            // 
            this.btnTransactionList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnTransactionList, BunifuAnimatorNS.DecorationType.None);
            this.btnTransactionList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTransactionList.FlatAppearance.BorderSize = 0;
            this.btnTransactionList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransactionList.Location = new System.Drawing.Point(0, 242);
            this.btnTransactionList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTransactionList.Name = "btnTransactionList";
            this.btnTransactionList.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnTransactionList.Size = new System.Drawing.Size(277, 38);
            this.btnTransactionList.TabIndex = 10;
            this.btnTransactionList.Text = "Transaction List";
            this.btnTransactionList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransactionList.UseVisualStyleBackColor = false;
            this.btnTransactionList.Click += new System.EventHandler(this.btnTransactionList_Click);
            // 
            // lblServicesNotificationCount
            // 
            this.lblServicesNotificationCount.AutoSize = true;
            this.lblServicesNotificationCount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.menuAnimation.SetDecoration(this.lblServicesNotificationCount, BunifuAnimatorNS.DecorationType.None);
            this.lblServicesNotificationCount.ForeColor = System.Drawing.Color.White;
            this.lblServicesNotificationCount.Location = new System.Drawing.Point(232, 12);
            this.lblServicesNotificationCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServicesNotificationCount.Name = "lblServicesNotificationCount";
            this.lblServicesNotificationCount.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.lblServicesNotificationCount.Size = new System.Drawing.Size(30, 29);
            this.lblServicesNotificationCount.TabIndex = 6;
            this.lblServicesNotificationCount.Text = "0";
            this.lblServicesNotificationCount.Visible = false;
            // 
            // btnSortTransaction
            // 
            this.btnSortTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSortTransaction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSortTransaction, BunifuAnimatorNS.DecorationType.None);
            this.btnSortTransaction.IconChar = FontAwesome.Sharp.IconChar.SortDown;
            this.btnSortTransaction.IconColor = System.Drawing.Color.White;
            this.btnSortTransaction.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSortTransaction.IconSize = 15;
            this.btnSortTransaction.Location = new System.Drawing.Point(239, 17);
            this.btnSortTransaction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortTransaction.Name = "btnSortTransaction";
            this.btnSortTransaction.Size = new System.Drawing.Size(15, 15);
            this.btnSortTransaction.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnSortTransaction.TabIndex = 7;
            this.btnSortTransaction.TabStop = false;
            this.btnSortTransaction.Click += new System.EventHandler(this.btnSortTransaction_Click);
            // 
            // lblIncomingPaymentNotification
            // 
            this.lblIncomingPaymentNotification.AutoSize = true;
            this.lblIncomingPaymentNotification.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.menuAnimation.SetDecoration(this.lblIncomingPaymentNotification, BunifuAnimatorNS.DecorationType.None);
            this.lblIncomingPaymentNotification.ForeColor = System.Drawing.Color.White;
            this.lblIncomingPaymentNotification.Location = new System.Drawing.Point(232, 207);
            this.lblIncomingPaymentNotification.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIncomingPaymentNotification.Name = "lblIncomingPaymentNotification";
            this.lblIncomingPaymentNotification.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.lblIncomingPaymentNotification.Size = new System.Drawing.Size(30, 29);
            this.lblIncomingPaymentNotification.TabIndex = 9;
            this.lblIncomingPaymentNotification.Text = "0";
            this.lblIncomingPaymentNotification.Visible = false;
            // 
            // lblWorkingProgressNumber
            // 
            this.lblWorkingProgressNumber.AutoSize = true;
            this.lblWorkingProgressNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.menuAnimation.SetDecoration(this.lblWorkingProgressNumber, BunifuAnimatorNS.DecorationType.None);
            this.lblWorkingProgressNumber.ForeColor = System.Drawing.Color.White;
            this.lblWorkingProgressNumber.Location = new System.Drawing.Point(232, 134);
            this.lblWorkingProgressNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWorkingProgressNumber.Name = "lblWorkingProgressNumber";
            this.lblWorkingProgressNumber.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.lblWorkingProgressNumber.Size = new System.Drawing.Size(30, 29);
            this.lblWorkingProgressNumber.TabIndex = 5;
            this.lblWorkingProgressNumber.Text = "0";
            this.lblWorkingProgressNumber.Visible = false;
            // 
            // btnIncomingPayment
            // 
            this.btnIncomingPayment.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnIncomingPayment, BunifuAnimatorNS.DecorationType.None);
            this.btnIncomingPayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIncomingPayment.FlatAppearance.BorderSize = 0;
            this.btnIncomingPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncomingPayment.Location = new System.Drawing.Point(0, 204);
            this.btnIncomingPayment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnIncomingPayment.Name = "btnIncomingPayment";
            this.btnIncomingPayment.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnIncomingPayment.Size = new System.Drawing.Size(277, 38);
            this.btnIncomingPayment.TabIndex = 8;
            this.btnIncomingPayment.Text = "Incoming Payments";
            this.btnIncomingPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncomingPayment.UseVisualStyleBackColor = false;
            this.btnIncomingPayment.Click += new System.EventHandler(this.btnIncomingPayment_Click);
            // 
            // btnServiceInvoice
            // 
            this.btnServiceInvoice.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnServiceInvoice, BunifuAnimatorNS.DecorationType.None);
            this.btnServiceInvoice.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnServiceInvoice.FlatAppearance.BorderSize = 0;
            this.btnServiceInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServiceInvoice.Location = new System.Drawing.Point(0, 166);
            this.btnServiceInvoice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnServiceInvoice.Name = "btnServiceInvoice";
            this.btnServiceInvoice.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnServiceInvoice.Size = new System.Drawing.Size(277, 38);
            this.btnServiceInvoice.TabIndex = 7;
            this.btnServiceInvoice.Text = "Service Invoice";
            this.btnServiceInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnServiceInvoice.UseVisualStyleBackColor = false;
            this.btnServiceInvoice.Click += new System.EventHandler(this.btnServiceInvoice_Click);
            // 
            // btnWorkingProgress
            // 
            this.btnWorkingProgress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnWorkingProgress, BunifuAnimatorNS.DecorationType.None);
            this.btnWorkingProgress.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnWorkingProgress.FlatAppearance.BorderSize = 0;
            this.btnWorkingProgress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWorkingProgress.Location = new System.Drawing.Point(0, 128);
            this.btnWorkingProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnWorkingProgress.Name = "btnWorkingProgress";
            this.btnWorkingProgress.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnWorkingProgress.Size = new System.Drawing.Size(277, 38);
            this.btnWorkingProgress.TabIndex = 4;
            this.btnWorkingProgress.Text = "Working Progress";
            this.btnWorkingProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWorkingProgress.UseVisualStyleBackColor = false;
            this.btnWorkingProgress.Click += new System.EventHandler(this.btnWorkingProgress_Click);
            // 
            // btnJobOrder
            // 
            this.btnJobOrder.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnJobOrder, BunifuAnimatorNS.DecorationType.None);
            this.btnJobOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnJobOrder.FlatAppearance.BorderSize = 0;
            this.btnJobOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJobOrder.Location = new System.Drawing.Point(0, 90);
            this.btnJobOrder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnJobOrder.Name = "btnJobOrder";
            this.btnJobOrder.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnJobOrder.Size = new System.Drawing.Size(277, 38);
            this.btnJobOrder.TabIndex = 3;
            this.btnJobOrder.Text = "Job Order";
            this.btnJobOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJobOrder.UseVisualStyleBackColor = false;
            this.btnJobOrder.Click += new System.EventHandler(this.btnJobOrder_Click);
            // 
            // btnJobEstimate
            // 
            this.btnJobEstimate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnJobEstimate, BunifuAnimatorNS.DecorationType.None);
            this.btnJobEstimate.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnJobEstimate.FlatAppearance.BorderSize = 0;
            this.btnJobEstimate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJobEstimate.Location = new System.Drawing.Point(0, 52);
            this.btnJobEstimate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnJobEstimate.Name = "btnJobEstimate";
            this.btnJobEstimate.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnJobEstimate.Size = new System.Drawing.Size(277, 38);
            this.btnJobEstimate.TabIndex = 2;
            this.btnJobEstimate.Text = "Job Estimate";
            this.btnJobEstimate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJobEstimate.UseVisualStyleBackColor = false;
            this.btnJobEstimate.Click += new System.EventHandler(this.btnJobEstimate_Click);
            // 
            // btnDropdownTransaction
            // 
            this.btnDropdownTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnDropdownTransaction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnDropdownTransaction, BunifuAnimatorNS.DecorationType.None);
            this.btnDropdownTransaction.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDropdownTransaction.FlatAppearance.BorderSize = 0;
            this.btnDropdownTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDropdownTransaction.ForeColor = System.Drawing.Color.White;
            this.btnDropdownTransaction.IconChar = FontAwesome.Sharp.IconChar.Clipboard;
            this.btnDropdownTransaction.IconColor = System.Drawing.Color.White;
            this.btnDropdownTransaction.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDropdownTransaction.IconSize = 18;
            this.btnDropdownTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDropdownTransaction.Location = new System.Drawing.Point(0, 0);
            this.btnDropdownTransaction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDropdownTransaction.Name = "btnDropdownTransaction";
            this.btnDropdownTransaction.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnDropdownTransaction.Size = new System.Drawing.Size(277, 52);
            this.btnDropdownTransaction.TabIndex = 7;
            this.btnDropdownTransaction.Text = "Transaction";
            this.btnDropdownTransaction.UseVisualStyleBackColor = false;
            this.btnDropdownTransaction.Click += new System.EventHandler(this.btnDropdownTransaction_Click);
            // 
            // pnlReports
            // 
            this.pnlReports.Controls.Add(this.btnSortReports);
            this.pnlReports.Controls.Add(this.btnInventoryReport);
            this.pnlReports.Controls.Add(this.btnSalesReport);
            this.pnlReports.Controls.Add(this.btnPurchasingOrderReport);
            this.pnlReports.Controls.Add(this.btnDropdownReports);
            this.menuAnimation.SetDecoration(this.pnlReports, BunifuAnimatorNS.DecorationType.None);
            this.pnlReports.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlReports.Location = new System.Drawing.Point(0, 410);
            this.pnlReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlReports.MaximumSize = new System.Drawing.Size(277, 169);
            this.pnlReports.MinimumSize = new System.Drawing.Size(277, 52);
            this.pnlReports.Name = "pnlReports";
            this.pnlReports.Size = new System.Drawing.Size(277, 52);
            this.pnlReports.TabIndex = 5;
            // 
            // btnSortReports
            // 
            this.btnSortReports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSortReports.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSortReports, BunifuAnimatorNS.DecorationType.None);
            this.btnSortReports.IconChar = FontAwesome.Sharp.IconChar.SortDown;
            this.btnSortReports.IconColor = System.Drawing.Color.White;
            this.btnSortReports.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSortReports.IconSize = 15;
            this.btnSortReports.Location = new System.Drawing.Point(239, 16);
            this.btnSortReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortReports.Name = "btnSortReports";
            this.btnSortReports.Size = new System.Drawing.Size(15, 15);
            this.btnSortReports.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnSortReports.TabIndex = 15;
            this.btnSortReports.TabStop = false;
            this.btnSortReports.Click += new System.EventHandler(this.btnSortReports_Click);
            // 
            // btnInventoryReport
            // 
            this.btnInventoryReport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnInventoryReport, BunifuAnimatorNS.DecorationType.None);
            this.btnInventoryReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInventoryReport.FlatAppearance.BorderSize = 0;
            this.btnInventoryReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInventoryReport.Location = new System.Drawing.Point(0, 128);
            this.btnInventoryReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnInventoryReport.Name = "btnInventoryReport";
            this.btnInventoryReport.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnInventoryReport.Size = new System.Drawing.Size(277, 38);
            this.btnInventoryReport.TabIndex = 15;
            this.btnInventoryReport.Text = "Inventory Report";
            this.btnInventoryReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInventoryReport.UseVisualStyleBackColor = false;
            this.btnInventoryReport.Click += new System.EventHandler(this.btnInventoryReport_Click);
            // 
            // btnSalesReport
            // 
            this.btnSalesReport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnSalesReport, BunifuAnimatorNS.DecorationType.None);
            this.btnSalesReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSalesReport.FlatAppearance.BorderSize = 0;
            this.btnSalesReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalesReport.Location = new System.Drawing.Point(0, 90);
            this.btnSalesReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSalesReport.Name = "btnSalesReport";
            this.btnSalesReport.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSalesReport.Size = new System.Drawing.Size(277, 38);
            this.btnSalesReport.TabIndex = 4;
            this.btnSalesReport.Text = "Sales Report";
            this.btnSalesReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalesReport.UseVisualStyleBackColor = false;
            this.btnSalesReport.Click += new System.EventHandler(this.btnSalesReport_Click);
            // 
            // btnPurchasingOrderReport
            // 
            this.btnPurchasingOrderReport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnPurchasingOrderReport, BunifuAnimatorNS.DecorationType.None);
            this.btnPurchasingOrderReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPurchasingOrderReport.FlatAppearance.BorderSize = 0;
            this.btnPurchasingOrderReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchasingOrderReport.Location = new System.Drawing.Point(0, 52);
            this.btnPurchasingOrderReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPurchasingOrderReport.Name = "btnPurchasingOrderReport";
            this.btnPurchasingOrderReport.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPurchasingOrderReport.Size = new System.Drawing.Size(277, 38);
            this.btnPurchasingOrderReport.TabIndex = 3;
            this.btnPurchasingOrderReport.Text = "Purchasing Order Report";
            this.btnPurchasingOrderReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurchasingOrderReport.UseVisualStyleBackColor = false;
            this.btnPurchasingOrderReport.Click += new System.EventHandler(this.btnPurchasingReport_Click);
            // 
            // btnDropdownReports
            // 
            this.btnDropdownReports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnDropdownReports.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnDropdownReports, BunifuAnimatorNS.DecorationType.None);
            this.btnDropdownReports.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDropdownReports.FlatAppearance.BorderSize = 0;
            this.btnDropdownReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDropdownReports.ForeColor = System.Drawing.Color.White;
            this.btnDropdownReports.IconChar = FontAwesome.Sharp.IconChar.ChartLine;
            this.btnDropdownReports.IconColor = System.Drawing.Color.White;
            this.btnDropdownReports.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDropdownReports.IconSize = 18;
            this.btnDropdownReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDropdownReports.Location = new System.Drawing.Point(0, 0);
            this.btnDropdownReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDropdownReports.Name = "btnDropdownReports";
            this.btnDropdownReports.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnDropdownReports.Size = new System.Drawing.Size(277, 52);
            this.btnDropdownReports.TabIndex = 12;
            this.btnDropdownReports.Text = "Reports";
            this.btnDropdownReports.UseVisualStyleBackColor = false;
            this.btnDropdownReports.Click += new System.EventHandler(this.btnDropdownReports_Click_1);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.menuAnimation.SetDecoration(this.btnExit, BunifuAnimatorNS.DecorationType.None);
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.SystemColors.Control;
            this.btnExit.Location = new System.Drawing.Point(0, 835);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(275, 49);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(88)))));
            this.pnlMenu.Controls.Add(this.btnSettings);
            this.pnlMenu.Controls.Add(this.pnlCategories);
            this.pnlMenu.Controls.Add(this.pnlReports);
            this.pnlMenu.Controls.Add(this.btnProducts);
            this.pnlMenu.Controls.Add(this.pnlServices);
            this.pnlMenu.Controls.Add(this.btnVehicles);
            this.pnlMenu.Controls.Add(this.btnCustomers);
            this.pnlMenu.Controls.Add(this.pnlTransaction);
            this.pnlMenu.Controls.Add(this.btnExit);
            this.pnlMenu.Controls.Add(this.pnlPurchasing);
            this.pnlMenu.Controls.Add(this.btnDashboard);
            this.pnlMenu.Controls.Add(this.panel2);
            this.menuAnimation.SetDecoration(this.pnlMenu, BunifuAnimatorNS.DecorationType.None);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(275, 884);
            this.pnlMenu.TabIndex = 1;
            this.pnlMenu.MouseHover += new System.EventHandler(this.pnlMenu_MouseHover);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSettings, BunifuAnimatorNS.DecorationType.None);
            this.btnSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.ForeColor = System.Drawing.Color.White;
            this.btnSettings.IconChar = FontAwesome.Sharp.IconChar.Cog;
            this.btnSettings.IconColor = System.Drawing.Color.White;
            this.btnSettings.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSettings.IconSize = 18;
            this.btnSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.Location = new System.Drawing.Point(0, 514);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnSettings.Size = new System.Drawing.Size(275, 52);
            this.btnSettings.TabIndex = 11;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = false;
            // 
            // pnlCategories
            // 
            this.pnlCategories.Controls.Add(this.btnSortCategories);
            this.pnlCategories.Controls.Add(this.btnVehicleCategory);
            this.pnlCategories.Controls.Add(this.btnProductCategory);
            this.pnlCategories.Controls.Add(this.btnDropdownCategories);
            this.menuAnimation.SetDecoration(this.pnlCategories, BunifuAnimatorNS.DecorationType.None);
            this.pnlCategories.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCategories.Location = new System.Drawing.Point(0, 462);
            this.pnlCategories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlCategories.MaximumSize = new System.Drawing.Size(275, 129);
            this.pnlCategories.MinimumSize = new System.Drawing.Size(275, 52);
            this.pnlCategories.Name = "pnlCategories";
            this.pnlCategories.Size = new System.Drawing.Size(275, 52);
            this.pnlCategories.TabIndex = 11;
            // 
            // btnSortCategories
            // 
            this.btnSortCategories.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSortCategories.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSortCategories, BunifuAnimatorNS.DecorationType.None);
            this.btnSortCategories.IconChar = FontAwesome.Sharp.IconChar.SortDown;
            this.btnSortCategories.IconColor = System.Drawing.Color.White;
            this.btnSortCategories.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSortCategories.IconSize = 15;
            this.btnSortCategories.Location = new System.Drawing.Point(239, 18);
            this.btnSortCategories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortCategories.Name = "btnSortCategories";
            this.btnSortCategories.Size = new System.Drawing.Size(15, 15);
            this.btnSortCategories.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnSortCategories.TabIndex = 16;
            this.btnSortCategories.TabStop = false;
            this.btnSortCategories.Click += new System.EventHandler(this.btnSortCategories_Click);
            // 
            // btnVehicleCategory
            // 
            this.btnVehicleCategory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnVehicleCategory, BunifuAnimatorNS.DecorationType.None);
            this.btnVehicleCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVehicleCategory.FlatAppearance.BorderSize = 0;
            this.btnVehicleCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVehicleCategory.Location = new System.Drawing.Point(0, 90);
            this.btnVehicleCategory.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVehicleCategory.Name = "btnVehicleCategory";
            this.btnVehicleCategory.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnVehicleCategory.Size = new System.Drawing.Size(275, 38);
            this.btnVehicleCategory.TabIndex = 12;
            this.btnVehicleCategory.Text = "Vehicle Category";
            this.btnVehicleCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVehicleCategory.UseVisualStyleBackColor = false;
            // 
            // btnProductCategory
            // 
            this.btnProductCategory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnProductCategory, BunifuAnimatorNS.DecorationType.None);
            this.btnProductCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductCategory.FlatAppearance.BorderSize = 0;
            this.btnProductCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductCategory.Location = new System.Drawing.Point(0, 52);
            this.btnProductCategory.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProductCategory.Name = "btnProductCategory";
            this.btnProductCategory.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnProductCategory.Size = new System.Drawing.Size(275, 38);
            this.btnProductCategory.TabIndex = 11;
            this.btnProductCategory.Text = "Product Category";
            this.btnProductCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductCategory.UseVisualStyleBackColor = false;
            // 
            // btnDropdownCategories
            // 
            this.btnDropdownCategories.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnDropdownCategories.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnDropdownCategories, BunifuAnimatorNS.DecorationType.None);
            this.btnDropdownCategories.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDropdownCategories.FlatAppearance.BorderSize = 0;
            this.btnDropdownCategories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDropdownCategories.ForeColor = System.Drawing.Color.White;
            this.btnDropdownCategories.IconChar = FontAwesome.Sharp.IconChar.List;
            this.btnDropdownCategories.IconColor = System.Drawing.Color.White;
            this.btnDropdownCategories.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDropdownCategories.IconSize = 18;
            this.btnDropdownCategories.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDropdownCategories.Location = new System.Drawing.Point(0, 0);
            this.btnDropdownCategories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDropdownCategories.Name = "btnDropdownCategories";
            this.btnDropdownCategories.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnDropdownCategories.Size = new System.Drawing.Size(275, 52);
            this.btnDropdownCategories.TabIndex = 10;
            this.btnDropdownCategories.Text = "Categories";
            this.btnDropdownCategories.UseVisualStyleBackColor = false;
            this.btnDropdownCategories.Click += new System.EventHandler(this.btnDropdownCategories_Click);
            // 
            // btnProducts
            // 
            this.btnProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnProducts, BunifuAnimatorNS.DecorationType.None);
            this.btnProducts.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProducts.FlatAppearance.BorderSize = 0;
            this.btnProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProducts.ForeColor = System.Drawing.Color.White;
            this.btnProducts.IconChar = FontAwesome.Sharp.IconChar.Boxes;
            this.btnProducts.IconColor = System.Drawing.Color.White;
            this.btnProducts.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnProducts.IconSize = 18;
            this.btnProducts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProducts.Location = new System.Drawing.Point(0, 358);
            this.btnProducts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProducts.Name = "btnProducts";
            this.btnProducts.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnProducts.Size = new System.Drawing.Size(275, 52);
            this.btnProducts.TabIndex = 10;
            this.btnProducts.Text = "Inventory";
            this.btnProducts.UseVisualStyleBackColor = false;
            this.btnProducts.Click += new System.EventHandler(this.btnProducts_Click);
            // 
            // pnlServices
            // 
            this.pnlServices.Controls.Add(this.btnSortServices);
            this.pnlServices.Controls.Add(this.btnBillOfMaterials);
            this.pnlServices.Controls.Add(this.btnManageServices);
            this.pnlServices.Controls.Add(this.btnServices);
            this.menuAnimation.SetDecoration(this.pnlServices, BunifuAnimatorNS.DecorationType.None);
            this.pnlServices.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServices.Location = new System.Drawing.Point(0, 306);
            this.pnlServices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlServices.MaximumSize = new System.Drawing.Size(275, 129);
            this.pnlServices.MinimumSize = new System.Drawing.Size(275, 52);
            this.pnlServices.Name = "pnlServices";
            this.pnlServices.Size = new System.Drawing.Size(275, 52);
            this.pnlServices.TabIndex = 13;
            // 
            // btnSortServices
            // 
            this.btnSortServices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnSortServices.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnSortServices, BunifuAnimatorNS.DecorationType.None);
            this.btnSortServices.IconChar = FontAwesome.Sharp.IconChar.SortDown;
            this.btnSortServices.IconColor = System.Drawing.Color.White;
            this.btnSortServices.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSortServices.IconSize = 15;
            this.btnSortServices.Location = new System.Drawing.Point(239, 17);
            this.btnSortServices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortServices.Name = "btnSortServices";
            this.btnSortServices.Size = new System.Drawing.Size(15, 15);
            this.btnSortServices.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btnSortServices.TabIndex = 7;
            this.btnSortServices.TabStop = false;
            this.btnSortServices.Click += new System.EventHandler(this.btnSortServices_Click);
            // 
            // btnBillOfMaterials
            // 
            this.btnBillOfMaterials.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnBillOfMaterials, BunifuAnimatorNS.DecorationType.None);
            this.btnBillOfMaterials.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBillOfMaterials.FlatAppearance.BorderSize = 0;
            this.btnBillOfMaterials.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBillOfMaterials.Location = new System.Drawing.Point(0, 90);
            this.btnBillOfMaterials.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBillOfMaterials.Name = "btnBillOfMaterials";
            this.btnBillOfMaterials.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnBillOfMaterials.Size = new System.Drawing.Size(275, 38);
            this.btnBillOfMaterials.TabIndex = 14;
            this.btnBillOfMaterials.Text = "Bill of Materials";
            this.btnBillOfMaterials.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBillOfMaterials.UseVisualStyleBackColor = false;
            this.btnBillOfMaterials.Click += new System.EventHandler(this.btnBillOfMaterials_Click);
            // 
            // btnManageServices
            // 
            this.btnManageServices.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuAnimation.SetDecoration(this.btnManageServices, BunifuAnimatorNS.DecorationType.None);
            this.btnManageServices.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageServices.FlatAppearance.BorderSize = 0;
            this.btnManageServices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageServices.Location = new System.Drawing.Point(0, 52);
            this.btnManageServices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnManageServices.Name = "btnManageServices";
            this.btnManageServices.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnManageServices.Size = new System.Drawing.Size(275, 38);
            this.btnManageServices.TabIndex = 13;
            this.btnManageServices.Text = "Manage Services";
            this.btnManageServices.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManageServices.UseVisualStyleBackColor = false;
            this.btnManageServices.Click += new System.EventHandler(this.btnManageServices_Click);
            // 
            // btnVehicles
            // 
            this.btnVehicles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnVehicles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnVehicles, BunifuAnimatorNS.DecorationType.None);
            this.btnVehicles.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVehicles.FlatAppearance.BorderSize = 0;
            this.btnVehicles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVehicles.ForeColor = System.Drawing.Color.White;
            this.btnVehicles.IconChar = FontAwesome.Sharp.IconChar.Car;
            this.btnVehicles.IconColor = System.Drawing.Color.White;
            this.btnVehicles.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnVehicles.IconSize = 18;
            this.btnVehicles.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVehicles.Location = new System.Drawing.Point(0, 254);
            this.btnVehicles.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVehicles.Name = "btnVehicles";
            this.btnVehicles.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnVehicles.Size = new System.Drawing.Size(275, 52);
            this.btnVehicles.TabIndex = 9;
            this.btnVehicles.Text = "Vehicles";
            this.btnVehicles.UseVisualStyleBackColor = false;
            this.btnVehicles.Click += new System.EventHandler(this.btnVehicles_Click);
            // 
            // btnCustomers
            // 
            this.btnCustomers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnCustomers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnCustomers, BunifuAnimatorNS.DecorationType.None);
            this.btnCustomers.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCustomers.FlatAppearance.BorderSize = 0;
            this.btnCustomers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomers.ForeColor = System.Drawing.Color.White;
            this.btnCustomers.IconChar = FontAwesome.Sharp.IconChar.User;
            this.btnCustomers.IconColor = System.Drawing.Color.White;
            this.btnCustomers.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCustomers.IconSize = 18;
            this.btnCustomers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCustomers.Location = new System.Drawing.Point(0, 202);
            this.btnCustomers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCustomers.Name = "btnCustomers";
            this.btnCustomers.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnCustomers.Size = new System.Drawing.Size(275, 52);
            this.btnCustomers.TabIndex = 8;
            this.btnCustomers.Text = "Customers";
            this.btnCustomers.UseVisualStyleBackColor = false;
            this.btnCustomers.Click += new System.EventHandler(this.btnCustomers_Click);
            // 
            // btnDashboard
            // 
            this.btnDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(69)))), ((int)(((byte)(92)))));
            this.btnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuAnimation.SetDecoration(this.btnDashboard, BunifuAnimatorNS.DecorationType.None);
            this.btnDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.ForeColor = System.Drawing.Color.White;
            this.btnDashboard.IconChar = FontAwesome.Sharp.IconChar.TachometerAlt;
            this.btnDashboard.IconColor = System.Drawing.Color.White;
            this.btnDashboard.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDashboard.IconSize = 18;
            this.btnDashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.Location = new System.Drawing.Point(0, 50);
            this.btnDashboard.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Padding = new System.Windows.Forms.Padding(20, 15, 13, 12);
            this.btnDashboard.Size = new System.Drawing.Size(275, 52);
            this.btnDashboard.TabIndex = 13;
            this.btnDashboard.Text = "Dashboard";
            this.btnDashboard.UseVisualStyleBackColor = false;
            // 
            // RefreshServiceNotification
            // 
            this.RefreshServiceNotification.Tick += new System.EventHandler(this.RefreshServiceNotification_Tick);
            // 
            // CategoriesDropdown
            // 
            this.CategoriesDropdown.Interval = 10;
            this.CategoriesDropdown.Tick += new System.EventHandler(this.CategoriesDropdown_Tick);
            // 
            // datetime
            // 
            this.datetime.Tick += new System.EventHandler(this.datetime_Tick);
            // 
            // ServicesTimer
            // 
            this.ServicesTimer.Interval = 10;
            this.ServicesTimer.Tick += new System.EventHandler(this.ServicesTimer_Tick);
            // 
            // Leobytes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(1556, 884);
            this.Controls.Add(this.PanelContainer);
            this.Controls.Add(this.panelTitleBar);
            this.Controls.Add(this.pnlMenu);
            this.menuAnimation.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Leobytes";
            this.Text = "Leobytes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelTitleBar.ResumeLayout(false);
            this.panelTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.PanelContainer.ResumeLayout(false);
            this.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlPurchasing.ResumeLayout(false);
            this.pnlPurchasing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPurchasing)).EndInit();
            this.pnlTransaction.ResumeLayout(false);
            this.pnlTransaction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortTransaction)).EndInit();
            this.pnlReports.ResumeLayout(false);
            this.pnlReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortReports)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.pnlCategories.ResumeLayout(false);
            this.pnlCategories.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortCategories)).EndInit();
            this.pnlServices.ResumeLayout(false);
            this.pnlServices.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer TransactionTimer;
        private System.Windows.Forms.Timer ReportsTimer;
        private System.Windows.Forms.Panel panelTitleBar;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private BunifuAnimatorNS.BunifuTransition menuAnimation;
        public System.Windows.Forms.Panel PanelContainer;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuImageButton btnMenu;
        private System.Windows.Forms.Panel pnlPurchasing;
        private System.Windows.Forms.Button btnOutgoingPayment;
        private System.Windows.Forms.Button btnInvoice;
        private System.Windows.Forms.Button btnGRPO;
        private System.Windows.Forms.Button btnPurchasingOrder;
        private System.Windows.Forms.Panel pnlTransaction;
        private System.Windows.Forms.Button btnJobOrder;
        private System.Windows.Forms.Button btnJobEstimate;
        private System.Windows.Forms.Panel pnlReports;
        private System.Windows.Forms.Button btnSalesReport;
        private System.Windows.Forms.Button btnPurchasingOrderReport;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlMenu;
        private Bunifu.Framework.UI.BunifuCustomLabel lblWorkingProgressNumber;
        private System.Windows.Forms.Button btnWorkingProgress;
        private System.Windows.Forms.Button btnIncomingPayment;
        private System.Windows.Forms.Button btnServiceInvoice;
        private Bunifu.Framework.UI.BunifuCustomLabel lblServicesNotificationCount;
        private FontAwesome.Sharp.IconPictureBox btnCloseForm;
        private FontAwesome.Sharp.IconPictureBox btnMinimize;
        private FontAwesome.Sharp.IconPictureBox btnMaximize;
        private System.Windows.Forms.Timer RefreshServiceNotification;
        private Bunifu.Framework.UI.BunifuCustomLabel lblIncomingPaymentNotification;
        private FontAwesome.Sharp.IconButton btnDropdownPurchasing;
        private FontAwesome.Sharp.IconButton btnDropdownTransaction;
        private FontAwesome.Sharp.IconButton btnSettings;
        private FontAwesome.Sharp.IconButton btnDropdownReports;
        private FontAwesome.Sharp.IconButton btnProducts;
        private FontAwesome.Sharp.IconButton btnVehicles;
        private FontAwesome.Sharp.IconButton btnCustomers;
        private System.Windows.Forms.Button btnInventoryReport;
        private System.Windows.Forms.Panel pnlCategories;
        private System.Windows.Forms.Button btnVehicleCategory;
        private System.Windows.Forms.Button btnProductCategory;
        private FontAwesome.Sharp.IconButton btnDropdownCategories;
        private System.Windows.Forms.Timer CategoriesDropdown;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer datetime;
        private FontAwesome.Sharp.IconButton btnServices;
        private FontAwesome.Sharp.IconButton btnDashboard;
        private System.Windows.Forms.Panel pnlServices;
        private System.Windows.Forms.Button btnBillOfMaterials;
        private System.Windows.Forms.Button btnManageServices;
        private System.Windows.Forms.Timer ServicesTimer;
        private FontAwesome.Sharp.IconPictureBox btnSortPurchasing;
        private FontAwesome.Sharp.IconPictureBox btnSortTransaction;
        private FontAwesome.Sharp.IconPictureBox btnSortReports;
        private FontAwesome.Sharp.IconPictureBox btnSortCategories;
        private FontAwesome.Sharp.IconPictureBox btnSortServices;
        private System.Windows.Forms.Button btnPurchasingList;
        private System.Windows.Forms.Button btnTransactionList;
    }
}

