﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace Kraftsmen_Mechanics
{
    public partial class Services_List : Form
    {

        private string serCode, serName, serDesc;
        private double tTax, tPrice;

        public double TTax
        {
            get { return tTax; }
            set { tTax = value; }
        }

        public double TPrice
        {
            get { return tPrice; }
            set { tPrice = value; }
        }

        public string SerCode
        {
            get { return  serCode; }
            set { serCode = value; }
        }

        public string SerName
        {
            get { return serName; }
            set { serName = value; }
        }

        public string SerDesc
        {
            get { return serDesc; }
            set { serDesc = value; }
        }
        public Services_List()
        {
            InitializeComponent();
        }

        Connection serv = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void dgvServicesList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Services_List_Load(object sender, EventArgs e)
        {

        }
        #endregion
        private void dgvServicesList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            serCode = this.dgvServicesList.CurrentRow.Cells[0].Value.ToString();
            serName = this.dgvServicesList.CurrentRow.Cells[1].Value.ToString();
            serDesc = this.dgvServicesList.CurrentRow.Cells[2].Value.ToString();
            PO_Report.service = this.dgvServicesList.CurrentRow.Cells[1].Value.ToString();

            /*serv.OpenConn();
            SqlDataReader drInv = serv.DataReader("select tax");
            serv.CloseConn();*/

            this.Close();
        }

        private void Services_List_Load_1(object sender, EventArgs e)
        {
            serv.OpenConn();
            dgvServicesList.DataSource = serv.ShowDataDGV("Select * from service_list_view");
            serv.CloseConn();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
    }
}
