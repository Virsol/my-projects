﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Sales_Report : Form
    {
        Connection conn = new Connection();
        public Sales_Report()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvItemsReportList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvServiceSalesList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }

        private void btnServiceList_Click(object sender, EventArgs e)
        {
            PO_Report report = new PO_Report();
            report.Service_Report();
            txtServiceName.Text = PO_Report.service;
        }

        private void btnGenerateServiceReport_Click(object sender, EventArgs e)
        {
            string service = txtServiceName.Text;
           
            string date_to = dtpServiceReportTo.Value.ToString("yyyy-MM-dd");
            string date_from = dtpServiceReportFrom.Value.ToString("yyyy-MM-dd");
            MessageBox.Show(date_to);
            
            try
            {
                if (service != "" && date_to != "" && date_from != "") {
                    conn.OpenConn();
                    dgvServiceSalesList.DataSource = conn.ShowDataDGV("Select *,(Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where b.inventory_name = '"+service+"' and [date] between '"+date_from+"' and '"+date_to+"' ) 'Sum' from Service_report where [Service Name] = '"+service+"' and [Date] between '"+date_from+"' and '"+date_to+"'");
                    conn.CloseConn();
                

                }
                else
                {
                    conn.OpenConn();
                    dgvServiceSalesList.DataSource = conn.ShowDataDGV("Select *,(Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where b.inventory_name = '" + service + "' or [date] between '" + date_from + "' and '" + date_to + "' ) 'Sum' from Service_report where [Service Name] = '"+service+"' or [Date] between '"+ date_from +"' and '"+ date_to +"'");
                    conn.CloseConn();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }

        private void btnServiceList_Click_1(object sender, EventArgs e)
        {
            PO_Report report = new PO_Report();
            report.Service_Report();
            txtServiceName.Text = PO_Report.service;
        }

        private void btnGenerateServiceReport_Click_1(object sender, EventArgs e)
        {
            string service = txtServiceName.Text;

            string date_to = dtpServiceReportTo.Value.ToString("yyyy-MM-dd");
            string date_from = dtpServiceReportFrom.Value.ToString("yyyy-MM-dd");
          

            try
            {
                if (service != "" && date_to != "" && date_from != "")
                {
                    conn.OpenConn();
                    dgvServiceSalesList.DataSource = conn.ShowDataDGV("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) else a.job_number end as 'Document Number', c.name 'Name', format(a.date, 'MMM-dd-yyyy') 'Date', d.name 'Technician', b.inventory_code 'Service Code', e.inventory_name 'Service Name', b.price 'Price', b.quantity 'Quantity', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where b.inventory_name = '"+service+"' and date between '"+date_from+"' and '"+date_to+ "' and b.inventory_type = 2 ) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join employee d on a.technician = d.employee_id left join inventory e on b.inventory_code = e.inventory_code where e.inventory_name = '" + service+"' and [Date] between '"+date_from+"' and '"+date_to+ "'and e.inventory_type = 2");
                    conn.CloseConn();
                    conn.OpenConn();
                    SqlDataReader ServiceValid = conn.DataReader("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) else a.job_number end as 'Document Number', c.name 'Name', format(a.date, 'MMM-dd-yyyy') 'Date', d.name 'Technician', b.inventory_code 'Service Code', e.inventory_name 'Service Name', b.price 'Price', b.quantity 'Quantity', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where b.inventory_name = '" + service + "' and date between '" + date_from + "' and '" + date_to + "' and b.inventory_type = 2 ) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join employee d on a.technician = d.employee_id left join inventory e on b.inventory_code = e.inventory_code where e.inventory_name = '" + service + "' and [Date] between '" + date_from + "' and '" + date_to + "'and e.inventory_type = 2");
                    if(ServiceValid.Read())
                    {
                        this.dgvServiceSalesList.Columns["Sum"].Visible = false;
                        this.dgvServiceSalesList.Columns["Service Name"].Visible = false;
                        txtServiceReportTotalSales.Text = this.dgvServiceSalesList.CurrentRow.Cells[10].Value.ToString();
                    }

                    conn.CloseConn();


                }
                else
                {
                    conn.OpenConn();
                    dgvServiceSalesList.DataSource = conn.ShowDataDGV("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) else a.job_number end as 'Document Number', c.name 'Name', format(a.date, 'MMM-dd-yyyy') 'Date', d.name 'Technician', b.inventory_code 'Service Code', e.inventory_name 'Service Name', b.price 'Price', b.quantity 'Quantity', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where (b.inventory_name = '" + service + "' or date between '" + date_from + "' and '" + date_to + "') and b.inventory_type = 2 ) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join employee d on a.technician = d.employee_id left join inventory e on b.inventory_code = e.inventory_code where (e.inventory_name = '" + service + "' or [Date] between '" + date_from + "' and '" + date_to + "')and e.inventory_type = 2");
                    conn.CloseConn();

                    conn.OpenConn();
                    SqlDataReader ServiceValid = conn.DataReader("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) else a.job_number end as 'Document Number', c.name 'Name', format(a.date, 'MMM-dd-yyyy') 'Date', d.name 'Technician', b.inventory_code 'Service Code', e.inventory_name 'Service Name', b.price 'Price', b.quantity 'Quantity', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join inventory b on a.inventory_code = b.inventory_code left join job_order c on a.job_number = c.job_number where (b.inventory_name = '" + service + "' or date between '" + date_from + "' and '" + date_to + "') and b.inventory_type = 2 ) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join employee d on a.technician = d.employee_id left join inventory e on b.inventory_code = e.inventory_code where (e.inventory_name = '" + service + "' or [Date] between '" + date_from + "' and '" + date_to + "')and e.inventory_type = 2");
                    if(ServiceValid.Read())
                    {
                        this.dgvServiceSalesList.Columns["Sum"].Visible = false;
                        this.dgvServiceSalesList.Columns["Service Name"].Visible = true;
                        txtServiceReportTotalSales.Text = this.dgvServiceSalesList.CurrentRow.Cells[10].Value.ToString();
                    }

                    conn.CloseConn();

                }
            }
            catch (Exception ex)
            {
                txtServiceReportTotalSales.Text = "";

            }
           
        }

        private void btnItemList_Click(object sender, EventArgs e)
        {
            
            PO_Report item_report = new PO_Report();
            item_report.Items_report();
            txtItemsSalesReport.Text = PO_Report.Item_list;
           
            

        }

        private void btnGenerateItemReport_Click(object sender, EventArgs e)
        {
            string Items = txtItemsSalesReport.Text;
            string Items_date_to = dtpItemsSalesReportTo.Value.ToString("yyyy-MM-dd");
            string Items_date_from = dtpItemsSalesReportFrom.Value.ToString("yyyy-MM-dd");
            try
            {
                if (Items != "" && Items_date_from != "" && Items_date_to != "")
                {
                    conn.OpenConn();
                    dgvItemsReportList.DataSource = conn.ShowDataDGV("Select case when(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),'-',a.job_number) else a.job_number end as 'Job Number', b.inventory_code 'Item Code', c.inventory_name 'Item Name', d.name 'Customer', Format(a.date,'MMM-dd-yyyy') 'Date', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price',b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join job_order b on a.job_number = b.job_number left join inventory c on a.inventory_code = c.inventory_code where (c.inventory_name = '" + Items + "' and b.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join inventory c on b.inventory_code = c.inventory_code left join customer d on a.customer_id = d.customer_id where (c.inventory_name =  '" + Items + "' and a.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1");
                    conn.CloseConn();
                    conn.OpenConn();
                    SqlDataReader ItemValid = conn.DataReader("Select case when(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),'-',a.job_number) else a.job_number end as 'Job Number', b.inventory_code 'Item Code', c.inventory_name 'Item Name', d.name 'Customer', Format(a.date,'MMM-dd-yyyy') 'Date', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price',b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join job_order b on a.job_number = b.job_number left join inventory c on a.inventory_code = c.inventory_code where (c.inventory_name = '" + Items + "' and b.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join inventory c on b.inventory_code = c.inventory_code left join customer d on a.customer_id = d.customer_id where (c.inventory_name =  '" + Items + "' and a.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1");
                    if(ItemValid.Read())
                    {
                        txtItemsReportTotalSales.Text = this.dgvItemsReportList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvItemsReportList.Columns["Sum"].Visible = false;
                        this.dgvItemsReportList.Columns["Item Name"].Visible = false;
                    }

                    conn.CloseConn();
                }
                else
                {

                    conn.OpenConn();
                    dgvItemsReportList.DataSource = conn.ShowDataDGV("Select case when(Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),'-',a.job_number) else a.job_number end as 'Job Number', b.inventory_code 'Item Code', c.inventory_name 'Item Name', d.name 'Customer', Format(a.date,'MMM-dd-yyyy') 'Date', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price',b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join job_order b on a.job_number = b.job_number left join inventory c on a.inventory_code = c.inventory_code where (c.inventory_name = '" + Items + "' or b.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join inventory c on b.inventory_code = c.inventory_code left join customer d on a.customer_id = d.customer_id where (c.inventory_name =  '" + Items + "' or a.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1");
                    conn.CloseConn();
                    conn.OpenConn();
                    SqlDataReader ItemValid = conn.DataReader("Select case when(Select a.[on / off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'), '-', a.job_number) else a.job_number end as 'Job Number', b.inventory_code 'Item Code', c.inventory_name 'Item Name', d.name 'Customer', Format(a.date, 'MMM-dd-yyyy') 'Date', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price',b.total_price 'Total Price', (Select sum(a.total_price) from job_order_body a left join job_order b on a.job_number = b.job_number left join inventory c on a.inventory_code = c.inventory_code where (c.inventory_name = '" + Items + "' or b.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1) 'Sum' from job_order a left join job_order_body b on a.job_number = b.job_number left join inventory c on b.inventory_code = c.inventory_code left join customer d on a.customer_id = d.customer_id where (c.inventory_name = '" + Items + "' or a.date between '" + Items_date_from + "' and '" + Items_date_to + "') and c.inventory_type = 1");
                    if(ItemValid.Read())
                    {
                        txtItemsReportTotalSales.Text = this.dgvItemsReportList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvItemsReportList.Columns["Item Name"].Visible = true;
                        this.dgvItemsReportList.Columns["Sum"].Visible = false;
                    }

                    conn.CloseConn();
                }
            }
            catch(Exception Error)
            {
                txtItemsReportTotalSales.Text = "";
            }
        }

        private void btnCustomerList_Click(object sender, EventArgs e)
        {
            PO_Report rep = new PO_Report();

            rep.sales_transaction();
            txtCustomerName.Text = PO_Report.custName_Report;
         
        }

        private void btn_doc_from_Click(object sender, EventArgs e)
        {
            PO_Report rep = new PO_Report();
            rep.doc_from_list();
            txtDocNoFrom.Text = PO_Report.doc_from;

        }

        private void btn_doc_to_Click(object sender, EventArgs e)
        {
            PO_Report rep = new PO_Report();
            rep.doc_from_list();
            txtDocNoTo.Text = PO_Report.doc_from;
        }

        private void btnGenerateTransactionReport_Click(object sender, EventArgs e)
        {
            string customer_report = txtCustomerName.Text;
            string date_from = dtpTransactionReportFrom.Value.ToString("yyyy-MM-dd");
            string date_to = dtpTransactionReportTo.Value.ToString("yyyy-MM-dd");
            string doc_from = "";
            string doc_to ="";
            if ( txtDocNoFrom.Text != "" && txtDocNoTo.Text =="")
            {
                doc_to = txtDocNoFrom.Text;
            }
            else
            {
                doc_to = txtDocNoTo.Text;
            }

            if (txtDocNoFrom.Text == "" && txtDocNoTo.Text != "")
            {
                doc_from = txtDocNoTo.Text;
            }
            else
            {
                doc_from = txtDocNoFrom.Text;
            }
            if ((txtDocNoFrom.Text == "" && txtDocNoTo.Text == "") || txtDocNoFrom.Text != "" && txtDocNoTo.Text != "")
            {
                doc_from = txtDocNoFrom.Text;
                doc_to = txtDocNoTo.Text;
            }
          /*  else
            {

            }*/
          
            /* else
             {*/


            /* doc_from = txtDocNoFrom.Text;
             doc_to = txtDocNoTo.Text;*/



            try
                {

                    if (customer_report != "" && (doc_from != "" || doc_to != ""))
                    {

                        try
                        {
                        conn.OpenConn();
                        dgvTransactionReportList.DataSource = conn.ShowDataDGV("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  and a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' and a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                        conn.OpenConn();
                        SqlDataReader TransValid = conn.DataReader("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  and a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' and a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "'");
                        if(TransValid.Read())
                        {
                            txtTransactionReportTotalSales.Text = this.dgvTransactionReportList.CurrentRow.Cells[9].Value.ToString();
                            this.dgvTransactionReportList.Columns["Sum"].Visible = false;
                        }

                        conn.CloseConn();
                        }
                        catch(Exception ex)
                        {
                            txtTransactionReportTotalSales.Text = this.dgvTransactionReportList.CurrentRow.Cells[9].Value.ToString();
                        }

                    }
                    else if ((customer_report == "" && (doc_from != "" || doc_to != "")) || (customer_report != "" && (doc_from == "" || doc_to == "")))

                    {
                        conn.OpenConn();
                        dgvTransactionReportList.DataSource = conn.ShowDataDGV("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  or a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' or a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "'");
                        conn.CloseConn();
                        conn.OpenConn();
                        SqlDataReader TransValid = conn.DataReader("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  or a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' or a.job_number between '" + doc_from + "' and '" + doc_to + "')  and a.date between '" + date_from + "' and '" + date_to + "'");
                        if(TransValid.Read())
                        {
                        txtTransactionReportTotalSales.Text = this.dgvTransactionReportList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvTransactionReportList.Columns["Sum"].Visible = false;
                        }

                        conn.CloseConn();
                    }
                    else if(customer_report == "" && doc_from == "" && doc_to == "")
                    {
                        conn.OpenConn();
                        dgvTransactionReportList.DataSource = conn.ShowDataDGV("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  or a.job_number between '" + doc_from + "' and '" + doc_to + "')  or a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' or a.job_number between '" + doc_from + "' and '" + doc_to + "')  or a.date between '" + date_from + "' and '" + date_to + "'");
                    conn.CloseConn();
                    conn.OpenConn();
                    SqlDataReader TransValid = conn.DataReader("Select c.name 'Customer', FORMAT(a.date, 'MMM-dd-yyyy') 'Date', case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order') = 1 then Concat ((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Job Order'),' - ',a.job_number) Else a.job_number end as'Job Number', b.inventory_code 'Item/Service Code', d.inventory_name 'Item/Service Name', b.quantity 'Quantity', b.price 'Price', b.tax_price 'Tax Price', b.total_price 'Total Price', (Select sum(b.total_price) from job_order a left join job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id where (c.name ='" + customer_report + "'  or a.job_number between '" + doc_from + "' and '" + doc_to + "')  or a.date between '" + date_from + "' and '" + date_to + "') 'Sum' from job_order a left join  job_order_body b on a.job_number = b.job_number left join customer c on a.customer_id = c.customer_id left join inventory d on b.inventory_code = d.inventory_code where (c.name ='" + customer_report + "' or a.job_number between '" + doc_from + "' and '" + doc_to + "')  or a.date between '" + date_from + "' and '" + date_to + "'");
                    if(TransValid.Read())
                    {
                        txtTransactionReportTotalSales.Text = this.dgvTransactionReportList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvTransactionReportList.Columns["Sum"].Visible = false;
                    }

                        conn.CloseConn();
                    }

                }
                catch (Exception ex)
                {
                    txtTransactionReportTotalSales.Text = "";
                //dgvTransactionReportList.Columns["Sum"].Visible = false;
                MessageBox.Show(ex.Message);
                }

           // }
            
               
            
        }
    }
}
