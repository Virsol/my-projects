﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Estimate_Add_Items : Form
    {
        DBConnection dbcon = new DBConnection();
        Job_Estimate je = new Job_Estimate();
        public Job_Estimate_Add_Items()
        {
            InitializeComponent();
        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click_2(object sender, EventArgs e)
        {
            dbcon.OpenConnection();
            string nItemName = txtItemName.Text;
            string nItemCode = txtItemCode.Text;
            double nItemPrice = double.Parse(txtPrice.Text);
            int nItemQuant = Int32.Parse(nudItemQuantity.Value.ToString());
            int nTax = Int32.Parse(cboItemTax.SelectedValue.ToString());
            int nUOM = Int32.Parse(cboUOM.SelectedValue.ToString());
            string nDocSeries = lblDocumentSeries.Text;
            string nDocumentNum = lblDocumentNumber.Text;

            SqlDataReader drTax = dbcon.DataReader("select tax_rate from dbo.tax where tax_id = '" + nTax + "'");
            drTax.Read();
            int nTaxRate = Int32.Parse(drTax["tax_rate"].ToString());
            int percent = 100;
            double ntaxPrice;
            double subTotal = nItemPrice * nItemQuant;
            ntaxPrice = subTotal * nTaxRate / percent;
            double total = subTotal + ntaxPrice;
            string iDocNum = je.lblDocNum;

            /*string iDocNum = je.DocNum;*/
            /*MessageBox.Show(je.DocNum);*/
            dbcon.CloseConnection();
            dbcon.OpenConnection();
            dbcon.ExecuteQueries("insert into dbo.draft_estimate (job_estimate_number, inventory_code, price, quantity, tax_id, tax_price, uom_id, total_price) values ('" + nDocumentNum + "', '" + nItemCode + "', '" + nItemPrice + "', '" + nItemQuant + "','" + nTax + "', '" + ntaxPrice + "', '" + nUOM + "', '" + total + "')");
            dbcon.CloseConnection();

            MessageBox.Show("Item Added", "information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Job_Estimate_Add_Items_Load(object sender, EventArgs e)
        {

        }
    }
}
