﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Incoming_Payment : Form
    {
        DBConnection conn = new DBConnection();
        Transaction_List transaction_List;
        private string ntxtCustomerName;
        private int cusID;
        private string TotalItem, TotalPaymentDue, TotalTax, docnum, payment, nChange, nStatus;
        public string datenow = DateTime.Now.ToString("yyyy-MM-dd");
        public Incoming_Payment()
        {
            InitializeComponent();
        }
        public Incoming_Payment(Transaction_List trans)
        {
            InitializeComponent();
            transaction_List = trans;
        }

        private void Incoming_Payment_Load(object sender, EventArgs e)
        {
            refreshDgvServiceInvoiceList();
            conn.OpenConnection();
            SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Sales Invoice' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
            if (drDT.Read())
            {
                string invoiceSeries = drDT["series_desc"].ToString();
                conn.CloseConnection();
                lblInvoiceSeries.Text = invoiceSeries;
            }
            txtCash.Enabled = false;
            if (transaction_List != null)
            {
                conn.OpenConnection();
                SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + transaction_List.CusID + "'");
                cusDR.Read();
                txtCustomerName.Text = cusDR["name"].ToString();
                ntxtCustomerName = txtCustomerName.Text;
                txtContactNumber.Text = cusDR["Contact Number"].ToString();
                txtAddress.Text = cusDR["address"].ToString();
                conn.CloseConnection();


                conn.OpenConnection();
                SqlDataReader totalReader = conn.DataReader("select format(SUM([Price] * [Quantity]), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from sales_invoice_Details_view where [Document Number] = '" + transaction_List.SIDocNum + "' ");
                totalReader.Read();
                TotalItem = totalReader["Price_total"].ToString();
                TotalPaymentDue = totalReader["Total_amount"].ToString();
                TotalTax = totalReader["Tax_total"].ToString();
                conn.CloseConnection();

                lblTotalPaymentDue.Text = TotalPaymentDue;
                lblTotalItem.Text = TotalItem;
                lblTaxValue.Text = TotalTax;
                groupBox4.Enabled = false;
                btnAccept.Enabled = false;
                dgvServiceInvoiceList.Enabled = false;

            }
        }
        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCash_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtCash_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtCash_TextChanged_1(object sender, EventArgs e)
        {
            string nCash = txtCash.Text;
            try
            {
                if (nCash != "")
                {
                    Double Cash = double.Parse(nCash);
                    Double PayDue = double.Parse(TotalPaymentDue);
                    if (Cash >= PayDue)
                    {
                        double Change = Cash - PayDue;
                        payment = Cash.ToString();
                        nChange = Change.ToString("n2");
                        lblChange.Text = nChange;
                    }
                    else
                    {
                        double Change = Cash - PayDue;
                        nChange = Change.ToString("n2");
                        payment = Cash.ToString();
                        lblChange.Text = "0.00";
                    }
                }
            }
            catch (Exception E)
            {
                MessageBox.Show("Input Amount is not valid", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            double dTotalPaymentDue = double.Parse(TotalPaymentDue);
            double dPayment = double.Parse(payment);
            
            if (txtCash.Text != "" && dPayment >= dTotalPaymentDue)
            {
                double dChange = double.Parse(nChange);
                conn.OpenConnection();
                conn.ExecuteQueries("insert into sales_payment (customer_id, sales_invoice_doc_num, [date], amount, amount_paid, change)" +
                    "values" +
                    "('" + cusID + "'" +
                    ",'" + docnum + "'" +
                    ",'" + datenow + "'" +
                    ",'" + dTotalPaymentDue + "'" +
                    ",'" + dPayment + "'" +
                    ",'" + dChange + "')");
                conn.CloseConnection();

                conn.OpenConnection();
                conn.ExecuteQueries("update sales_invoice set status_id = 6 where sales_invoice_id = '" + docnum + "'");
                conn.CloseConnection();

                MessageBox.Show("Transaction Complete", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please input the right amount", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void refreshDgvServiceInvoiceList()
        {
            conn.OpenConnection();
            dgvServiceInvoiceList.DataSource = conn.ShowDataInGridView("select * from Sales_invoice_view");
            conn.CloseConnection();
        }

        private void dgvServiceInvoiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            docnum = dgvServiceInvoiceList.CurrentRow.Cells[0].Value.ToString();
            nStatus = dgvServiceInvoiceList.CurrentRow.Cells[6].Value.ToString();


            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + docnum + "')");
            data.Read();
            cusID = Int32.Parse(data["customer_id"].ToString());
            lblInvoiceNumber.Text = data["sales_invoice_doc_num"].ToString();

            conn.CloseConnection();


            conn.OpenConnection();
            SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + cusID + "'");
            cusDR.Read();
            txtCustomerName.Text = cusDR["name"].ToString();
            ntxtCustomerName = txtCustomerName.Text;
            txtContactNumber.Text = cusDR["Contact Number"].ToString();
            txtAddress.Text = cusDR["address"].ToString();
            conn.CloseConnection();


            conn.OpenConnection();
            SqlDataReader totalReader = conn.DataReader("select format(SUM([Price] * [Quantity]), '#,#.00')as Price_total, format(SUM([Tax Price]), '#,#.00')as Tax_total, format(SUM([Total Price]),'#,#.00') as Total_amount from sales_invoice_Details_view where [Document Number] = '" + docnum + "' ");
            totalReader.Read();
            TotalItem = totalReader["Price_total"].ToString();
            TotalPaymentDue = totalReader["Total_amount"].ToString();
            TotalTax = totalReader["Tax_total"].ToString();
            conn.CloseConnection();

            lblTotalPaymentDue.Text = TotalPaymentDue;
            lblTotalItem.Text = TotalItem;
            lblTaxValue.Text = TotalTax;
            if (nStatus == "CLOSED")
            {
                txtCash.Enabled = false;
                btnAccept.Enabled = false;
            }
            else if (nStatus == "OPEN")
            {
                txtCash.Enabled = true;
                btnAccept.Enabled = true;
            }
            
        }
    }
}
