﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Purchasing_Order_Report : Form
    {
         Connection conn = new Connection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        PO_Report Report = new PO_Report();
        public Purchasing_Order_Report()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnViewSupplierList_Click(object sender, EventArgs e)
        {

        }

        private void btnViewSupplierList_Click_1(object sender, EventArgs e)
        {
            
        }

        private void txtSupplierName_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Purchasing_Order_Report_Load(object sender, EventArgs e)
        {

            /*  Suppliers_List suppliers = new Suppliers_List();

              txtSupplierName.Text = suppliers.SupplyName;*/
            /* PO_Report rep = new PO_Report();
             rep.PO_click_1();*/
            /*  try
              {
                  using (Suppliers_List supplier_list = new Suppliers_List())
                  {

                      if (DialogResult != DialogResult.OK)

                          txtSupplierName.Text = PO_Report.suphomies;
                  }
              }
              catch (Exception ex)
              {

              }

              *//*MessageBox.Show(PO_Report.suphomies);*/

            //Report.generate_report();

            /*  con.Open();
               dgvPurchasingOrderList.DataSource = conn.ShowDataDGV("Select * from Inventory");
              con.Close();*/
            
         
     
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
          
        }

        private void btnViewSupplierList_Click_2(object sender, EventArgs e)
        {
            Report.PO_click_1();
            txtSupplierName.Text = PO_Report.suphomies;
        }

        private void btnGenerateReport_Click_1(object sender, EventArgs e)
        {

            /*            Suppliers_List suppp = new Suppliers_List();
                        MessageBox.Show(PO_Report.suphomies);*/

            

            string name = txtSupplierName.Text;
            string date_to = dtpTo.Value.ToString("yyyy-MM-dd");
            string date_from = dtpFrom.Value.ToString("yyyy-MM-dd");
            try
            {
                if (name != "" && date_to != " " && date_from != " ")
                {
                    conn.OpenConn();
                    dgvPurchasingOrderList.DataSource = conn.ShowDataDGV("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Purchase Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO'),'-',a.po_document_number) else a.po_document_number end as'Document', format(a.date_created,'MMM-dd-yyyy') 'Date Created', format(a.date_posted,'MMM-dd-yyyy') 'Date Posted', b.inventory_code 'Item Code', c.inventory_name 'Item Name', b.quantity 'Quantity', b.price 'Price', b.total_price 'Total', d.name 'Supplier Name', (Select sum(b.total_price) from po a left join po_body b on a.po_document_number = b.po_document_number left join supplier c on a.supplier_code = c.supplier_code where c.name = '" + name + "' and a.date_created between '" + date_from + "' and '" + date_to + "' ) 'Sum' from PO a left join  po_body b on a.po_document_number = b.po_document_number left join inventory c on b.inventory_code = c.inventory_code left join supplier d on a.supplier_code = d.supplier_code where (d.name = '" + name + "' and a.date_created between '" + date_from + "' and '" + date_to + "')");
                    conn.CloseConn();

                    conn.OpenConn();
                    SqlDataReader dr = conn.DataReader("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Purchase Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO'),'-',a.po_document_number) else a.po_document_number end as'Document', format(a.date_created,'MMM-dd-yyyy') 'Date Created', format(a.date_posted,'MMM-dd-yyyy') 'Date Posted', b.inventory_code 'Item Code', c.inventory_name 'Item Name', b.quantity 'Quantity', b.price 'Price', b.total_price 'Total', d.name 'Supplier Name', (Select sum(b.total_price) from po a left join po_body b on a.po_document_number = b.po_document_number left join supplier c on a.supplier_code = c.supplier_code where c.name = '" + name + "' and a.date_created between '" + date_from + "' and '" + date_to + "' ) 'Sum' from PO a left join  po_body b on a.po_document_number = b.po_document_number left join inventory c on b.inventory_code = c.inventory_code left join supplier d on a.supplier_code = d.supplier_code where (d.name = '" + name + "' and a.date_created between '" + date_from + "' and '" + date_to + "')");
                    if(dr.Read())
                    {
                        txtServiceReportTotalSales.Text = this.dgvPurchasingOrderList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvPurchasingOrderList.Columns["Sum"].Visible = false;
                        this.dgvPurchasingOrderList.Columns["Supplier Name"].Visible = false;
                    }
                    conn.CloseConn();

                }
                else
                {
                    conn.OpenConn();
                    dgvPurchasingOrderList.DataSource = conn.ShowDataDGV("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Purchase Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO'),'-',a.po_document_number) else a.po_document_number end as'Document', format(a.date_created,'MMM-dd-yyyy') 'Date Created', format(a.date_posted,'MMM-dd-yyyy') 'Date Posted', b.inventory_code 'Item Code', c.inventory_name 'Item Name', b.quantity 'Quantity', b.price 'Price', b.total_price 'Total', d.name 'Supplier Name', (Select sum(b.total_price) from po a left join po_body b on a.po_document_number = b.po_document_number left join supplier c on a.supplier_code = c.supplier_code where c.name = '" + name + "' or a.date_created between '" + date_from + "' and '" + date_to + "' ) 'Sum' from PO a left join  po_body b on a.po_document_number = b.po_document_number left join inventory c on b.inventory_code = c.inventory_code left join supplier d on a.supplier_code = d.supplier_code where (d.name = '" + name + "' or a.date_created between '" + date_from + "' and '" + date_to + "')");
                    conn.CloseConn();

                    conn.OpenConn();
                    SqlDataReader dr = conn.DataReader("Select case when (Select a.[on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'Purchase Order') = 1 then Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where doc_tag_desc = 'GRPO'),'-',a.po_document_number) else a.po_document_number end as'Document', format(a.date_created,'MMM-dd-yyyy') 'Date Created', format(a.date_posted,'MMM-dd-yyyy') 'Date Posted', b.inventory_code 'Item Code', c.inventory_name 'Item Name', b.quantity 'Quantity', b.price 'Price', b.total_price 'Total', d.name 'Supplier Name', (Select sum(b.total_price) from po a left join po_body b on a.po_document_number = b.po_document_number left join supplier c on a.supplier_code = c.supplier_code where c.name = '" + name + "' or a.date_created between '" + date_from + "' and '" + date_to + "' ) 'Sum' from PO a left join  po_body b on a.po_document_number = b.po_document_number left join inventory c on b.inventory_code = c.inventory_code left join supplier d on a.supplier_code = d.supplier_code where (d.name = '" + name + "' or a.date_created between '" + date_from + "' and '" + date_to + "')");
                    if(dr.Read())
                    {
                        txtServiceReportTotalSales.Text = this.dgvPurchasingOrderList.CurrentRow.Cells[9].Value.ToString();
                        this.dgvPurchasingOrderList.Columns["Sum"].Visible = false;
                        this.dgvPurchasingOrderList.Columns["Supplier Name"].Visible = true;
                    }
                    conn.CloseConn();


                }
            }
            catch (Exception ex)
            {
                txtServiceReportTotalSales.Text = "";
                //MessageBox.Show("No Data Available","Warning!",MessageBoxButtons.OK,MessageBoxIcon.Warning);

            }




        }
    }
}

