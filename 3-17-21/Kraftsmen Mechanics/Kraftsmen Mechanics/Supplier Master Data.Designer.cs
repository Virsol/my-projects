﻿
namespace Kraftsmen_Mechanics
{
    partial class Supplier_Master_Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Supplier_Master_Data));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSupplier = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.txtSearchSupplier = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvSupplierList = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUpdateSupplier = new System.Windows.Forms.Button();
            this.btnSaveSupplier = new System.Windows.Forms.Button();
            this.cboSupplierCity = new System.Windows.Forms.ComboBox();
            this.txtSupPostalCode = new System.Windows.Forms.TextBox();
            this.txtSupplierEmail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSupConNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSupAdd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSupName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSupCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpContactPerson = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtSearchContactPerson = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dgvContactPersonList = new System.Windows.Forms.DataGridView();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtHidden = new System.Windows.Forms.TextBox();
            this.btnViewSupplierList = new System.Windows.Forms.Button();
            this.txtContactPersonName = new System.Windows.Forms.TextBox();
            this.btnUpdateContactPerson = new System.Windows.Forms.Button();
            this.btnSaveContactPerson = new System.Windows.Forms.Button();
            this.cboContactPersonCity = new System.Windows.Forms.ComboBox();
            this.txtContactPersonEmail = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtContactPersonPostalCode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtContactPersonNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtContactPersonAddress = new System.Windows.Forms.TextBox();
            this.txtContactPersonSupplier = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tpContactPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactPersonList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpSupplier);
            this.tabControl1.Controls.Add(this.tpContactPerson);
            this.tabControl1.ItemSize = new System.Drawing.Size(80, 30);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(25, 3);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 579);
            this.tabControl1.TabIndex = 0;
            // 
            // tpSupplier
            // 
            this.tpSupplier.Controls.Add(this.button5);
            this.tpSupplier.Controls.Add(this.label8);
            this.tpSupplier.Controls.Add(this.label10);
            this.tpSupplier.Controls.Add(this.button8);
            this.tpSupplier.Controls.Add(this.button7);
            this.tpSupplier.Controls.Add(this.button6);
            this.tpSupplier.Controls.Add(this.txtSearchSupplier);
            this.tpSupplier.Controls.Add(this.label9);
            this.tpSupplier.Controls.Add(this.dgvSupplierList);
            this.tpSupplier.Controls.Add(this.label7);
            this.tpSupplier.Controls.Add(this.groupBox1);
            this.tpSupplier.Location = new System.Drawing.Point(4, 34);
            this.tpSupplier.Name = "tpSupplier";
            this.tpSupplier.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupplier.Size = new System.Drawing.Size(768, 541);
            this.tpSupplier.TabIndex = 0;
            this.tpSupplier.Text = "Supplier";
            this.tpSupplier.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(94, 249);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(28, 27);
            this.button5.TabIndex = 36;
            this.button5.Text = "<<";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "of";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(429, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "Search:";
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(196, 249);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(28, 27);
            this.button8.TabIndex = 39;
            this.button8.Text = ">>";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(162, 248);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(28, 27);
            this.button7.TabIndex = 38;
            this.button7.Text = ">";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(128, 248);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(28, 27);
            this.button6.TabIndex = 37;
            this.button6.Text = "<";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // txtSearchSupplier
            // 
            this.txtSearchSupplier.Location = new System.Drawing.Point(479, 252);
            this.txtSearchSupplier.Name = "txtSearchSupplier";
            this.txtSearchSupplier.Size = new System.Drawing.Size(265, 20);
            this.txtSearchSupplier.TabIndex = 41;
            this.txtSearchSupplier.TextChanged += new System.EventHandler(this.txtSearchSupplier_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(75, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "0";
            // 
            // dgvSupplierList
            // 
            this.dgvSupplierList.AllowUserToAddRows = false;
            this.dgvSupplierList.AllowUserToDeleteRows = false;
            this.dgvSupplierList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplierList.Location = new System.Drawing.Point(24, 284);
            this.dgvSupplierList.Name = "dgvSupplierList";
            this.dgvSupplierList.ReadOnly = true;
            this.dgvSupplierList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplierList.Size = new System.Drawing.Size(720, 239);
            this.dgvSupplierList.TabIndex = 32;
            this.dgvSupplierList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplierList_CellClick_1);
            this.dgvSupplierList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplierList_CellDoubleClick_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnUpdateSupplier);
            this.groupBox1.Controls.Add(this.btnSaveSupplier);
            this.groupBox1.Controls.Add(this.cboSupplierCity);
            this.groupBox1.Controls.Add(this.txtSupPostalCode);
            this.groupBox1.Controls.Add(this.txtSupplierEmail);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtSupConNo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtSupAdd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSupName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtSupCode);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(744, 223);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Supplier Details";
            // 
            // btnUpdateSupplier
            // 
            this.btnUpdateSupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(207)))));
            this.btnUpdateSupplier.FlatAppearance.BorderSize = 0;
            this.btnUpdateSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateSupplier.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpdateSupplier.Location = new System.Drawing.Point(564, 164);
            this.btnUpdateSupplier.Name = "btnUpdateSupplier";
            this.btnUpdateSupplier.Size = new System.Drawing.Size(174, 32);
            this.btnUpdateSupplier.TabIndex = 38;
            this.btnUpdateSupplier.Text = "Update";
            this.btnUpdateSupplier.UseVisualStyleBackColor = false;
            this.btnUpdateSupplier.Click += new System.EventHandler(this.btnUpdateSupplier_Click);
            // 
            // btnSaveSupplier
            // 
            this.btnSaveSupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(255)))), ((int)(((byte)(66)))));
            this.btnSaveSupplier.FlatAppearance.BorderSize = 0;
            this.btnSaveSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSupplier.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSaveSupplier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveSupplier.Location = new System.Drawing.Point(378, 164);
            this.btnSaveSupplier.Name = "btnSaveSupplier";
            this.btnSaveSupplier.Size = new System.Drawing.Size(174, 32);
            this.btnSaveSupplier.TabIndex = 37;
            this.btnSaveSupplier.Text = "Save";
            this.btnSaveSupplier.UseVisualStyleBackColor = false;
            this.btnSaveSupplier.Click += new System.EventHandler(this.btnSaveSupplier_Click);
            // 
            // cboSupplierCity
            // 
            this.cboSupplierCity.FormattingEnabled = true;
            this.cboSupplierCity.Location = new System.Drawing.Point(447, 119);
            this.cboSupplierCity.Name = "cboSupplierCity";
            this.cboSupplierCity.Size = new System.Drawing.Size(95, 21);
            this.cboSupplierCity.TabIndex = 36;
            // 
            // txtSupPostalCode
            // 
            this.txtSupPostalCode.Location = new System.Drawing.Point(622, 119);
            this.txtSupPostalCode.Name = "txtSupPostalCode";
            this.txtSupPostalCode.Size = new System.Drawing.Size(116, 20);
            this.txtSupPostalCode.TabIndex = 35;
            // 
            // txtSupplierEmail
            // 
            this.txtSupplierEmail.Location = new System.Drawing.Point(104, 119);
            this.txtSupplierEmail.Name = "txtSupplierEmail";
            this.txtSupplierEmail.Size = new System.Drawing.Size(225, 20);
            this.txtSupplierEmail.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(549, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Postal Code:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(59, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Email:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(415, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "City:";
            // 
            // txtSupConNo
            // 
            this.txtSupConNo.Location = new System.Drawing.Point(104, 87);
            this.txtSupConNo.Name = "txtSupConNo";
            this.txtSupConNo.Size = new System.Drawing.Size(225, 20);
            this.txtSupConNo.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Contact No.:";
            // 
            // txtSupAdd
            // 
            this.txtSupAdd.Location = new System.Drawing.Point(447, 29);
            this.txtSupAdd.Multiline = true;
            this.txtSupAdd.Name = "txtSupAdd";
            this.txtSupAdd.Size = new System.Drawing.Size(291, 78);
            this.txtSupAdd.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(352, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Supplier Address:";
            // 
            // txtSupName
            // 
            this.txtSupName.Location = new System.Drawing.Point(104, 57);
            this.txtSupName.Name = "txtSupName";
            this.txtSupName.Size = new System.Drawing.Size(225, 20);
            this.txtSupName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Supplier Name:";
            // 
            // txtSupCode
            // 
            this.txtSupCode.Location = new System.Drawing.Point(104, 29);
            this.txtSupCode.Name = "txtSupCode";
            this.txtSupCode.Size = new System.Drawing.Size(225, 20);
            this.txtSupCode.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Supplier Code:";
            // 
            // tpContactPerson
            // 
            this.tpContactPerson.Controls.Add(this.button1);
            this.tpContactPerson.Controls.Add(this.label19);
            this.tpContactPerson.Controls.Add(this.label20);
            this.tpContactPerson.Controls.Add(this.button2);
            this.tpContactPerson.Controls.Add(this.button3);
            this.tpContactPerson.Controls.Add(this.button4);
            this.tpContactPerson.Controls.Add(this.txtSearchContactPerson);
            this.tpContactPerson.Controls.Add(this.label21);
            this.tpContactPerson.Controls.Add(this.dgvContactPersonList);
            this.tpContactPerson.Controls.Add(this.label22);
            this.tpContactPerson.Controls.Add(this.groupBox2);
            this.tpContactPerson.Location = new System.Drawing.Point(4, 34);
            this.tpContactPerson.Name = "tpContactPerson";
            this.tpContactPerson.Padding = new System.Windows.Forms.Padding(3);
            this.tpContactPerson.Size = new System.Drawing.Size(768, 541);
            this.tpContactPerson.TabIndex = 1;
            this.tpContactPerson.Text = "Contact Person";
            this.tpContactPerson.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(96, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 27);
            this.button1.TabIndex = 46;
            this.button1.Text = "<<";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(55, 268);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = "of";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(431, 267);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 50;
            this.label20.Text = "Search:";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(198, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 27);
            this.button2.TabIndex = 49;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(164, 260);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(28, 27);
            this.button3.TabIndex = 48;
            this.button3.Text = ">";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(130, 260);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(28, 27);
            this.button4.TabIndex = 47;
            this.button4.Text = "<";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // txtSearchContactPerson
            // 
            this.txtSearchContactPerson.Location = new System.Drawing.Point(481, 264);
            this.txtSearchContactPerson.Name = "txtSearchContactPerson";
            this.txtSearchContactPerson.Size = new System.Drawing.Size(265, 20);
            this.txtSearchContactPerson.TabIndex = 51;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(77, 268);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "0";
            // 
            // dgvContactPersonList
            // 
            this.dgvContactPersonList.AllowUserToAddRows = false;
            this.dgvContactPersonList.AllowUserToDeleteRows = false;
            this.dgvContactPersonList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvContactPersonList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContactPersonList.Location = new System.Drawing.Point(26, 296);
            this.dgvContactPersonList.Name = "dgvContactPersonList";
            this.dgvContactPersonList.ReadOnly = true;
            this.dgvContactPersonList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContactPersonList.Size = new System.Drawing.Size(720, 239);
            this.dgvContactPersonList.TabIndex = 42;
            this.dgvContactPersonList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContactPersonList_CellClick);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(36, 268);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 13);
            this.label22.TabIndex = 43;
            this.label22.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtHidden);
            this.groupBox2.Controls.Add(this.btnViewSupplierList);
            this.groupBox2.Controls.Add(this.txtContactPersonName);
            this.groupBox2.Controls.Add(this.btnUpdateContactPerson);
            this.groupBox2.Controls.Add(this.btnSaveContactPerson);
            this.groupBox2.Controls.Add(this.cboContactPersonCity);
            this.groupBox2.Controls.Add(this.txtContactPersonEmail);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtContactPersonPostalCode);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtContactPersonNumber);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtContactPersonAddress);
            this.groupBox2.Controls.Add(this.txtContactPersonSupplier);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(6, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(756, 199);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contact Person Details:";
            // 
            // txtHidden
            // 
            this.txtHidden.Location = new System.Drawing.Point(19, 162);
            this.txtHidden.Name = "txtHidden";
            this.txtHidden.Size = new System.Drawing.Size(192, 20);
            this.txtHidden.TabIndex = 45;
            this.txtHidden.Visible = false;
            // 
            // btnViewSupplierList
            // 
            this.btnViewSupplierList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViewSupplierList.BackgroundImage")));
            this.btnViewSupplierList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnViewSupplierList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewSupplierList.FlatAppearance.BorderSize = 0;
            this.btnViewSupplierList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnViewSupplierList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnViewSupplierList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewSupplierList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewSupplierList.Location = new System.Drawing.Point(320, 31);
            this.btnViewSupplierList.Name = "btnViewSupplierList";
            this.btnViewSupplierList.Size = new System.Drawing.Size(15, 15);
            this.btnViewSupplierList.TabIndex = 44;
            this.btnViewSupplierList.UseVisualStyleBackColor = true;
            this.btnViewSupplierList.Click += new System.EventHandler(this.btnViewSupplierList_Click_1);
            // 
            // txtContactPersonName
            // 
            this.txtContactPersonName.Location = new System.Drawing.Point(110, 55);
            this.txtContactPersonName.Name = "txtContactPersonName";
            this.txtContactPersonName.Size = new System.Drawing.Size(225, 20);
            this.txtContactPersonName.TabIndex = 43;
            // 
            // btnUpdateContactPerson
            // 
            this.btnUpdateContactPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(207)))));
            this.btnUpdateContactPerson.FlatAppearance.BorderSize = 0;
            this.btnUpdateContactPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateContactPerson.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpdateContactPerson.Location = new System.Drawing.Point(565, 150);
            this.btnUpdateContactPerson.Name = "btnUpdateContactPerson";
            this.btnUpdateContactPerson.Size = new System.Drawing.Size(174, 32);
            this.btnUpdateContactPerson.TabIndex = 42;
            this.btnUpdateContactPerson.Text = "Update";
            this.btnUpdateContactPerson.UseVisualStyleBackColor = false;
            this.btnUpdateContactPerson.Click += new System.EventHandler(this.btnUpdateContactPerson_Click);
            // 
            // btnSaveContactPerson
            // 
            this.btnSaveContactPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(255)))), ((int)(((byte)(66)))));
            this.btnSaveContactPerson.FlatAppearance.BorderSize = 0;
            this.btnSaveContactPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveContactPerson.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSaveContactPerson.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveContactPerson.Location = new System.Drawing.Point(379, 150);
            this.btnSaveContactPerson.Name = "btnSaveContactPerson";
            this.btnSaveContactPerson.Size = new System.Drawing.Size(174, 32);
            this.btnSaveContactPerson.TabIndex = 41;
            this.btnSaveContactPerson.Text = "Save";
            this.btnSaveContactPerson.UseVisualStyleBackColor = false;
            this.btnSaveContactPerson.Click += new System.EventHandler(this.btnSaveContactPerson_Click);
            // 
            // cboContactPersonCity
            // 
            this.cboContactPersonCity.FormattingEnabled = true;
            this.cboContactPersonCity.Location = new System.Drawing.Point(474, 113);
            this.cboContactPersonCity.Name = "cboContactPersonCity";
            this.cboContactPersonCity.Size = new System.Drawing.Size(95, 21);
            this.cboContactPersonCity.TabIndex = 17;
            // 
            // txtContactPersonEmail
            // 
            this.txtContactPersonEmail.Location = new System.Drawing.Point(110, 108);
            this.txtContactPersonEmail.Name = "txtContactPersonEmail";
            this.txtContactPersonEmail.Size = new System.Drawing.Size(225, 20);
            this.txtContactPersonEmail.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(65, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Email:";
            // 
            // txtContactPersonPostalCode
            // 
            this.txtContactPersonPostalCode.Location = new System.Drawing.Point(648, 113);
            this.txtContactPersonPostalCode.Name = "txtContactPersonPostalCode";
            this.txtContactPersonPostalCode.Size = new System.Drawing.Size(92, 20);
            this.txtContactPersonPostalCode.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(575, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Postal Code:";
            // 
            // txtContactPersonNumber
            // 
            this.txtContactPersonNumber.Location = new System.Drawing.Point(110, 82);
            this.txtContactPersonNumber.Name = "txtContactPersonNumber";
            this.txtContactPersonNumber.Size = new System.Drawing.Size(225, 20);
            this.txtContactPersonNumber.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "CP Contact No.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(437, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "City:";
            // 
            // txtContactPersonAddress
            // 
            this.txtContactPersonAddress.Location = new System.Drawing.Point(472, 32);
            this.txtContactPersonAddress.Multiline = true;
            this.txtContactPersonAddress.Name = "txtContactPersonAddress";
            this.txtContactPersonAddress.Size = new System.Drawing.Size(268, 70);
            this.txtContactPersonAddress.TabIndex = 4;
            // 
            // txtContactPersonSupplier
            // 
            this.txtContactPersonSupplier.Location = new System.Drawing.Point(110, 29);
            this.txtContactPersonSupplier.Name = "txtContactPersonSupplier";
            this.txtContactPersonSupplier.Size = new System.Drawing.Size(192, 20);
            this.txtContactPersonSupplier.TabIndex = 15;
            this.txtContactPersonSupplier.TextChanged += new System.EventHandler(this.txtContactPersonSupplier_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Supplier Code:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(376, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Contact Address:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Contact Name:";
            // 
            // Supplier_Master_Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 603);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Supplier_Master_Data";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Supplier Master Data";
            this.Load += new System.EventHandler(this.Supplier_Master_Data_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpSupplier.ResumeLayout(false);
            this.tpSupplier.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplierList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpContactPerson.ResumeLayout(false);
            this.tpContactPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactPersonList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpSupplier;
        private System.Windows.Forms.TabPage tpContactPerson;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboSupplierCity;
        private System.Windows.Forms.TextBox txtSupPostalCode;
        private System.Windows.Forms.TextBox txtSupplierEmail;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSupConNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSupAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSupName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSupCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtSearchSupplier;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvSupplierList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnUpdateSupplier;
        private System.Windows.Forms.Button btnSaveSupplier;
        private System.Windows.Forms.TextBox txtContactPersonSupplier;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboContactPersonCity;
        private System.Windows.Forms.TextBox txtContactPersonEmail;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtContactPersonPostalCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtContactPersonNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtContactPersonAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtSearchContactPerson;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView dgvContactPersonList;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnUpdateContactPerson;
        private System.Windows.Forms.Button btnSaveContactPerson;
        private System.Windows.Forms.TextBox txtContactPersonName;
        private System.Windows.Forms.Button btnViewSupplierList;
        private System.Windows.Forms.TextBox txtHidden;
    }
}