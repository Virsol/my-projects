﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Order : Form
    {
       
        DBConnection conn = new DBConnection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        Customer_List cusList = new Customer_List();
        Transaction_List transaction_List;
        public string ntxtSearch;
        public string ncboSearchBy;
        public string ntxtCustomerName;
        public string ncboVehicleModel;
        public string ntxtContactNumber;
        public string ntxtVehicleMake;
        public string ntxtVehicleColor;
        public string ntxtVehiclePlateNo;
        public string nDocumentNumber;
        public string docSeries;
        public string docNum;
        public string nDocumentSeries;
        public string nDateCreated;
        private int nEmployeeID;
        private int cusID;
        private int iDocNum;
        private string sDocNum;
        private int vehicleID;
        private int nTechnician;
        private string TotalItem;
        private string TotalPaymentDue;
        private string TotalTax;
        private string JelDocNum;
        public string vehicleName;
        private string walkin;

        public Job_Order()
        {
            InitializeComponent();
        }
        public Job_Order(Transaction_List trans)
        {
            InitializeComponent();
            transaction_List = trans;
        }
        public string DocNum
        {
            /*string num = iDocNum.ToString().PadLeft(4, '0');
            return num;*/
            get { return sDocNum; }
            set { sDocNum = value; }
        }
        private void btnViewJobEstimateList_Click(object sender, EventArgs e)
        {

            Form newFormDialog = new Form();
            try
            {
                using(Job_Estimate_List JEL = new Job_Estimate_List()) {

                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    JEL.Owner = newFormDialog;

                    if (JEL.ShowDialog() != DialogResult.OK && JEL.CusID != 0 && JEL.CarID != 0)
                    {
                        conn.OpenConnection();
                        conn.ExecuteQueries("Truncate table draft_order");
                        conn.CloseConnection();

                        btnCancel.Visible = true;
                        newFormDialog.Dispose();
                        conn.OpenConnection();
                        SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + JEL.CusID + "'");
                        cusDR.Read();
                        txtCustomerName.Text = cusDR["name"].ToString();

                        txtContactNumber.Text = cusDR["Contact Number"].ToString();
                        txtAddress.Text = cusDR["address"].ToString();
                        conn.CloseConnection();
                        btnAddNewService.Enabled = true;
                        cusID = JEL.CusID;
                        /*con.Open();
                        SqlCommand vehicleCMD = new SqlCommand("select * from Vehicle_list where [Vehicle ID] = '" + JEL.CarID + "'", con);
                        SqlDataReader vehicleDR = vehicleCMD.ExecuteReader();
                        DataTable vehicleDT = new DataTable();
                        vehicleDT.Load(vehicleDR);
                        cboVehicleModel.DataSource = vehicleDT;
                        cboVehicleModel.DisplayMember = "model";
                        cboVehicleModel.ValueMember = "Vehicle ID";
                        con.Close();*/

                        /*lblDocumentNumber.Text = JEL.DocNum;*/

                        con.Open();
                        SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                        SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                        DataTable dtCategory = new DataTable();
                        dtCategory.Load(drCategory);
                        cboCategory.DataSource = dtCategory;
                        cboCategory.DisplayMember = "vehicle_cat_desc";
                        cboCategory.ValueMember = "vehicle_cat_id";
                        cmdVeCat.ExecuteNonQuery();
                        con.Close();


                        dtpDateCreated.Text = JEL.Date;
                        JelDocNum = JEL.DocNum;


                        conn.OpenConnection();
                        conn.ExecuteQueries("Insert into draft_order SELECT '" + DocNum + "',inventory_code,price,quantity,tax_id,tax_price,uom_id,NULL,total_price,bom_code from job_estimate_body where job_estimate_number = '" + JelDocNum + "'");
                        conn.CloseConnection();


                        conn.OpenConnection();
                        dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_order_view");
                        conn.CloseConnection();

                        con.Open();
                        SqlCommand vehicleCMD = new SqlCommand("select * from vehicles_of_customer where [ID] = '" + JEL.CarID + "'", con);
                        SqlDataReader vehicleDR = vehicleCMD.ExecuteReader();
                        DataTable vehicleDT = new DataTable();
                        vehicleDT.Load(vehicleDR);
                        cboVehicleModel.DataSource = vehicleDT;
                        cboVehicleModel.DisplayMember = "Vehicle Model";
                        cboVehicleModel.ValueMember = "ID";
                        vehicleCMD.ExecuteNonQuery();
                        con.Close();

                        cboCategory.Enabled = false;
                        rdoWithMaterial.Enabled = false;
                        rdoWithoutMaterial.Enabled = false;
                        dgvServiceList.Enabled = false;
                        cboSearchBy.Enabled = false;
                        txtCustomerName.Enabled = false;
                        txtAddress.Enabled = false;
                        txtContactNumber.Enabled = false;
                        txtSearch.Enabled = false;
                        cboEstimatedBy.Enabled = false;
                        dtpDateCreated.Enabled = false;
                        cboVehicleModel.Enabled = false;
                        dgvServiceOrderList.Enabled = false;


                        if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                        {
                            vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                            if (JEL.CusID != 0 && JEL.CarID != 0)
                            {

                                conn.OpenConnection();
                                /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                SqlDataReader dr = cmdV.ExecuteReader();*/

                                SqlDataReader dr = conn.DataReader("select * from vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                                if (dr.Read())
                                {
                                    cboVehicleModel.Enabled = true;
                                    txtVehicleMake.Text = dr["Auto"].ToString();
                                    txtVehicleSize.Text = dr["Size"].ToString();
                                    txtVehicleColor.Text = dr["color"].ToString();
                                    txtPlateNumber.Text = dr["Platenumber"].ToString();
                                    cboCategory.Text = dr["Vehicle Category"].ToString();
                                    conn.CloseConnection();
                                }
                                else
                                {
                                    MessageBox.Show(vehicleID + " " + cusID);
                                }
                            }
                        }

                        else
                        {
                            
                        }
                        con.Close();
                        refreshTotal();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void Job_Order_Load(object sender, EventArgs e)
        {
            cboSearchBy.Enabled = false;
            txtSearch.Enabled = false;
            dgvServiceList.Enabled = false;
            btnAddNewService.Enabled = false;
            conn.OpenConnection();
            conn.ExecuteQueries("Truncate table draft_order");
            conn.CloseConnection();
            nDateCreated = dtpDateCreated.Value.ToString("yyyy-MM-dd");
            conn.OpenConnection();
            con.Open();
            
            SqlDataReader drDT = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id");
            if (drDT.Read())
            {
                docSeries = drDT["series_desc"].ToString();
                conn.CloseConnection();
            }

            conn.OpenConnection();
            SqlDataReader drDN = conn.DataReader("select max(job_number) as current_doc_num from dbo.job_order_body");
            drDN.Read();
            if (drDN["current_doc_num"].ToString() != "")
            {
                docNum = drDN["current_doc_num"].ToString();
                iDocNum = Int32.Parse(docNum) + 1;

                nDocumentNumber = iDocNum.ToString().PadLeft(7, '0');
                nDocumentSeries = docSeries;
                lblDocumentNumber.Text = nDocumentNumber;
                lblDocumentSeries.Text = nDocumentSeries;

            }
            else
            {
                docNum = "0000001";
                nDocumentNumber = docNum;
                nDocumentSeries = docSeries;
                iDocNum = Int32.Parse(docNum);
                lblDocumentSeries.Text = nDocumentSeries;
                lblDocumentNumber.Text = nDocumentNumber;
                conn.CloseConnection();
            }
            conn.OpenConnection();
            SqlCommand cmd = new SqlCommand("select * from dbo.inventory_type", con);
            SqlDataReader drSearch = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(drSearch);
            cboSearchBy.DataSource = dt;
            cboSearchBy.DisplayMember = "inventory_desc";
            cboSearchBy.ValueMember = "inventory_type_id";
            cmd.ExecuteNonQuery();

            SqlCommand cmdEmp = new SqlCommand("select * from dbo.Employees_List", con);
            SqlDataReader drEmp = cmdEmp.ExecuteReader();
            DataTable dtEmp = new DataTable();
            dtEmp.Load(drEmp);
            cboEstimatedBy.DataSource = dtEmp;
            cboEstimatedBy.DisplayMember = "Name";
            cboEstimatedBy.ValueMember = "ID";
            cmdEmp.ExecuteNonQuery();
            nEmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());

            SqlCommand cmdTech = new SqlCommand("select * from dbo.Technician", con);
            SqlDataReader drTech = cmdTech.ExecuteReader();
            DataTable dtTech = new DataTable();
            dtTech.Load(drTech);
            cboTechnicianName.DataSource = dtTech;
            cboTechnicianName.DisplayMember = "Name";
            cboTechnicianName.ValueMember = "ID";
            cmdTech.ExecuteNonQuery();
            nTechnician = Int32.Parse(cboTechnicianName.SelectedValue.ToString());

            sDocNum = iDocNum.ToString().PadLeft(7, '0');
            /*MessageBox.Show(DocNum);*/
            con.Close();
            conn.CloseConnection();


            /*conn.OpenConnection();
            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_estimate_view");
            conn.CloseConnection();*/

            if(transaction_List != null)
            {
                conn.OpenConnection();
                SqlDataReader cusDR = conn.DataReader("select * from customer_list where ID = '" + transaction_List.CusID + "'");
                cusDR.Read();
                txtCustomerName.Text = cusDR["name"].ToString();
                ntxtCustomerName = txtCustomerName.Text;
                txtContactNumber.Text = cusDR["Contact Number"].ToString();
                txtAddress.Text = cusDR["address"].ToString();
                conn.CloseConnection();


                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox4.Enabled = false;
                groupBox5.Enabled = false;

                nEmployeeID = transaction_List.empID;
                nTechnician = transaction_List.techID;
                cusID = transaction_List.CusID;
                /*MessageBox.Show(cusID.ToString());*/
                dtpDateCreated.Text = transaction_List.Date;
                string JolDocNum = transaction_List.DocNum;
                conn.OpenConnection();
                dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from job_order_body_view where [Job Number] = '" + JolDocNum + "'");
                conn.CloseConnection();
                conn.OpenConnection();
                SqlDataReader drDT1 = conn.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id and document_series.[on/off] = 1");
                if (drDT1.Read())
                {
                    docSeries = drDT1["series_desc"].ToString();
                    conn.CloseConnection();
                }
                lblDocumentSeries.Text = docSeries;



                con.Open();
                SqlCommand employeeCMD = new SqlCommand("select * from Employees_List where ID ='" + transaction_List.empID + "'", con);
                SqlDataReader employeeDR = employeeCMD.ExecuteReader();
                DataTable employeeDT = new DataTable();
                employeeDT.Load(employeeDR);
                cboEstimatedBy.DataSource = employeeDT;
                cboEstimatedBy.DisplayMember = "Name";
                cboEstimatedBy.ValueMember = "ID";
                con.Close();

                con.Open();
                SqlCommand technicianCMD = new SqlCommand("select * from Technician where ID ='" + transaction_List.techID + "'", con);
                SqlDataReader technicianDR = technicianCMD.ExecuteReader();
                DataTable technicianDT = new DataTable();
                technicianDT.Load(technicianDR);
                cboTechnicianName.DataSource = technicianDT;
                cboTechnicianName.DisplayMember = "Name";
                cboTechnicianName.ValueMember = "ID";
                con.Close();


                con.Open();
                SqlCommand cmdVehicle = new SqlCommand("select * from dbo.Vehicles_of_customer where [Customer ID] = '" + transaction_List.CusID + "' and [ID] = '"+transaction_List.CarID+"'", con);
                SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                DataTable dtVehicle = new DataTable();
                dtVehicle.Load(drVehicle);
                cboVehicleModel.DataSource = dtVehicle;
                cboVehicleModel.DisplayMember = "Vehicle Model";
                cboVehicleModel.ValueMember = "ID";
                cmdVehicle.ExecuteNonQuery();
                con.Close();


                con.Open();
                SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                DataTable dtCategory = new DataTable();
                dtCategory.Load(drCategory);
                cboCategory.DataSource = dtCategory;
                cboCategory.DisplayMember = "vehicle_cat_desc";
                cboCategory.ValueMember = "vehicle_cat_id";
                cmdVeCat.ExecuteNonQuery();
                con.Close();


                if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                {
                    vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());


                    if (transaction_List.CusID != 0 && transaction_List.CarID != 0)
                    {

                        conn.OpenConnection();
                        /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                        SqlDataReader dr = cmdV.ExecuteReader();*/

                        SqlDataReader dr = conn.DataReader("select * from dbo.Vehicles_of_customer where  [ID] = '" + transaction_List.CarID + "'");
                        if (dr.Read())
                        {
                            cboVehicleModel.Enabled = true;
                            txtVehicleMake.Text = dr["Auto"].ToString();
                            txtVehicleSize.Text = dr["Size"].ToString();
                            txtVehicleColor.Text = dr["color"].ToString();
                            txtPlateNumber.Text = dr["Platenumber"].ToString();
                            cboCategory.Text = dr["Vehicle Category"].ToString();
                            vehicleID = transaction_List.CarID;
                            conn.CloseConnection();
                        }
                        else
                        {
                            MessageBox.Show(vehicleID + " " + cusID);
                        }
                    }
                }
                
                else
                {

                }

                nDocumentNumber = transaction_List.DocNum;
                refreshTotal();
            }
            else
            {
                dgvServiceOrderListRefresh();
            }

            
        }
        private void dgvServiceOrderListRefresh()
        {
            conn.OpenConnection();
            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_order_view");
            conn.CloseConnection();
        }
        public string lblDocNum
        {
            get
            {
                return this.lblDocumentNumber.Text;
            }
            set
            {
                this.lblDocumentNumber.Text = value;
            }
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            ntxtSearch = txtSearch.Text;
            conn.OpenConnection();
            dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "' and ([Service/Item Name] like '%" + ntxtSearch + "%' or [Service/Item Code] like '%" + ntxtSearch + "%')");
            conn.CloseConnection();
        }
        private void cboSearchBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ncboSearchBy = this.cboSearchBy.GetItemText(this.cboSearchBy.SelectedItem);

            conn.OpenConnection();
            dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "'");
            conn.CloseConnection();
        }
        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {
            ntxtCustomerName = txtCustomerName.Text;
        }
        private void dgvServiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string type = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();

            if ("Services" == type)
            {
                Add_Service AddServices = new Add_Service();
                AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();
                con.Open();
                SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                SqlDataReader drTax = cmdTax.ExecuteReader();
                DataTable dtTax = new DataTable();
                dtTax.Load(drTax);
                AddServices.cboServiceTax.DataSource = dtTax;
                AddServices.cboServiceTax.DisplayMember = "tax_desc";
                AddServices.cboServiceTax.ValueMember = "tax_id";
                cmdTax.ExecuteNonQuery();

                con.Close();
                AddServices.lblDocumentNumber.Text = lblDocumentNumber.Text;
                AddServices.lblDocumentSeries.Text = lblDocumentSeries.Text;

                if (AddServices.ShowDialog() != DialogResult.OK)
                {
                    conn.OpenConnection();
                    dgvServiceOrderListRefresh();
                    refreshTotal();
                    conn.CloseConnection();
                }

            }
            else if ("Item" == type)
            {
                Add_Items AddItems = new Add_Items();


                AddItems.txtItemName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                AddItems.txtItemCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                AddItems.txtPrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();

                con.Open();
                SqlCommand cmd = new SqlCommand("select * from dbo.uom", con);
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                AddItems.cboUOM.DataSource = dt;
                AddItems.cboUOM.DisplayMember = "uom_desc";
                AddItems.cboUOM.ValueMember = "uom_id";
                cmd.ExecuteNonQuery();

                SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                SqlDataReader drTax = cmdTax.ExecuteReader();
                DataTable dtTax = new DataTable();
                dtTax.Load(drTax);
                AddItems.cboItemTax.DataSource = dtTax;
                AddItems.cboItemTax.DisplayMember = "tax_desc";
                AddItems.cboItemTax.ValueMember = "tax_id";
                cmdTax.ExecuteNonQuery();
                con.Close();
                AddItems.lblDocumentNumber.Text = lblDocumentNumber.Text;
                AddItems.lblDocumentSeries.Text = lblDocumentSeries.Text;

                if (AddItems.ShowDialog() != DialogResult.OK)
                {
                    dgvServiceOrderListRefresh();
                    refreshTotal();
                }
            }
            Add_Items add_Items = new Add_Items();
            Add_Service add_Service = new Add_Service();


        }
        private void txtCustomerName_MouseClick(object sender, MouseEventArgs e)
        {
            con.Open();
            Form newFormDialog = new Form();
            try
            {
                using (Customer_List cusList = new Customer_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    cusList.Owner = newFormDialog;
                    try
                    {
                        if (cusList.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            cusID = cusList.CusID;


                            SqlCommand cmdVehicle = new SqlCommand("select * from vehicles_of_customer where [Customer ID] = '" + cusID + "'", con);
                            SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                            DataTable dtVehicle = new DataTable();
                            dtVehicle.Load(drVehicle);
                            cboVehicleModel.DataSource = dtVehicle;
                            cboVehicleModel.DisplayMember = "Vehicle Model";
                            cboVehicleModel.ValueMember = "ID";
                            cmdVehicle.ExecuteNonQuery();


                            cboSearchBy.Enabled = true;
                            txtSearch.Enabled = true;
                            rdoWithMaterial.Enabled = true;
                            rdoWithoutMaterial.Enabled = true;
                            dgvServiceList.Enabled = true;

                            SqlCommand cmdVeCat = new SqlCommand("select * from vehicle_category", con);
                            SqlDataReader drCategory = cmdVeCat.ExecuteReader();
                            DataTable dtCategory = new DataTable();
                            dtCategory.Load(drCategory);
                            cboCategory.DataSource = dtCategory;
                            cboCategory.DisplayMember = "vehicle_cat_desc";
                            cboCategory.ValueMember = "vehicle_cat_id";
                            cmdVeCat.ExecuteNonQuery();

                            if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                            {
                                vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());
                                txtCustomerName.Text = cusList.CusName;
                                txtContactNumber.Text = cusList.CusNo;
                                txtAddress.Text = cusList.CusAddress;

                                if (vehicleID != 0 && cusID != 0)
                                {

                                    conn.OpenConnection();
                                    /*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                                    SqlDataReader dr = cmdV.ExecuteReader();*/

                                    SqlDataReader dr = conn.DataReader("select * from vehicles_of_customer where  [ID] = '" + vehicleID + "'");
                                    if (dr.Read())
                                    {
                                        cboVehicleModel.Enabled = true;
                                        txtVehicleMake.Text = dr["Auto"].ToString();
                                        txtVehicleSize.Text = dr["Size"].ToString();
                                        txtVehicleColor.Text = dr["color"].ToString();
                                        txtPlateNumber.Text = dr["Platenumber"].ToString();
                                        cboCategory.Text = dr["Vehicle Category"].ToString();
                                        conn.CloseConnection();
                                    }
                                    else
                                    {
                                        MessageBox.Show(vehicleID + " " + cusID);
                                    }
                                }
                            }

                            else
                            {

                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

                newFormDialog.Dispose();
            }

            con.Close();
        }
        private void cboVehicleModel_SelectedIndexChanged(object sender, EventArgs e)
        {

            conn.OpenConnection();

            string model = this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem);
            string owner = txtCustomerName.Text;

            SqlDataReader drCBOV = conn.DataReader("select * from dbo.vehicles_of_customer where [Customer ID] = '" + cusID + "'and  [Vehicle Model] = '" + model + "'");
            if (drCBOV.Read())
            {
                txtVehicleMake.Text = drCBOV["Auto"].ToString();
                txtVehicleSize.Text = drCBOV["Size"].ToString();
                txtVehicleColor.Text = drCBOV["color"].ToString();
                txtPlateNumber.Text = drCBOV["Plate Number"].ToString();
                vehicleID = Int32.Parse(drCBOV["Vehicle ID"].ToString());
                cboCategory.Text = drCBOV["Vehicle Category"].ToString();
                conn.CloseConnection();
            }
            /*else
            {
            MessageBox.Show(vehicleID.ToString() + "cbo");
            }*/

        }
        private void dgvServiceOrderList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string item_or_service_name = this.dgvServiceOrderList.CurrentRow.Cells[1].Value.ToString();
            int iD = Int32.Parse(this.dgvServiceOrderList.CurrentRow.Cells[10].Value.ToString());



            if (MessageBox.Show("Do you wan't to void this item?" + "\n" + item_or_service_name, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                conn.OpenConnection();
                conn.ExecuteQueries("delete from dbo.draft_estimate where draft_estimate_id = '" + iD + "'");
                conn.CloseConnection();
                dgvServiceOrderListRefresh();
                refreshTotal();
            }
            else
            {

            }

        }
        private void refreshTotal()
        {
            conn.OpenConnection();
            SqlDataReader totalReader = conn.DataReader("select format(SUM([price]), '#,#.00')as Price_total, format(SUM([tax_price]), '#,#.00')as Tax_total, format(SUM(total_price),'#,#.00') as Total_amount from draft_order where job_estimate_number = '" + nDocumentNumber + "' ");
            totalReader.Read();
            TotalItem = totalReader["Price_total"].ToString();
            TotalPaymentDue = totalReader["Total_amount"].ToString();
            TotalTax = totalReader["Tax_total"].ToString();
            conn.CloseConnection();

            lblTotalPaymentDue.Text = TotalPaymentDue;
            lblTotalItem.Text = TotalItem;
            lblTaxValue.Text = TotalTax;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCustomerName_TextChanged_1(object sender, EventArgs e)
        {
            /*con.Open();
            Customer_List cusList = new Customer_List();
            try
            {
                if (cusList.ShowDialog() != DialogResult.OK)
                {
                    cusID = cusList.CusID;
                    SqlCommand cmdVehicle = new SqlCommand("select * from dbo.Vehicle_List where [Customer ID] = '" + cusID + "'", con);
                    SqlDataReader drVehicle = cmdVehicle.ExecuteReader();
                    DataTable dtVehicle = new DataTable();
                    dtVehicle.Load(drVehicle);
                    cboVehicleModel.DataSource = dtVehicle;
                    cboVehicleModel.DisplayMember = "model";
                    cboVehicleModel.ValueMember = "Vehicle ID";
                    cmdVehicle.ExecuteNonQuery();
                    if (this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem).ToString() != "")
                    {
                        vehicleID = Int32.Parse(cboVehicleModel.SelectedValue.ToString());
                        txtCustomerName.Text = cusList.CusName;
                        txtContactNumber.Text = cusList.CusNo;
                        txtAddress.Text = cusList.CusAddress;

                        if (vehicleID != 0 && cusID != 0)
                        {

                            conn.OpenConnection();
                            *//*SqlCommand cmdV = new SqlCommand("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'", con);
                            SqlDataReader dr = cmdV.ExecuteReader();*//*

                            SqlDataReader dr = conn.DataReader("select * from dbo.Vehicle_List where  [Vehicle ID] = '" + vehicleID + "'");
                            if (dr.Read())
                            {
                                cboVehicleModel.Enabled = true;
                                txtVehicleMake.Text = dr["Auto"].ToString();
                                txtVehicleSize.Text = dr["Size"].ToString();
                                txtVehicleColor.Text = dr["color"].ToString();
                                txtPlateNumber.Text = dr["Plate Number"].ToString();

                                conn.CloseConnection();
                            }
                            else
                            {
                                MessageBox.Show(vehicleID + " " + cusID);
                            }
                        }
                    }

                    else
                    {

                    }
                }
            }
            catch (Exception)
            {

            }
            con.Close();*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cboSearchBy.Enabled = true;
            txtSearch.Enabled = true;
            dgvServiceList.Enabled = true;
            
            ncboSearchBy = this.cboSearchBy.GetItemText(this.cboSearchBy.SelectedItem);

            conn.OpenConnection();
            dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "'");
            conn.CloseConnection();

            
        }

        private void dgvServiceList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            string type = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
            Form newFormDialog = new Form();
            if ("Services" == type)
            {
                try
                {
                    using (Add_Service AddServices = new Add_Service()) {

                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddServices.Owner = newFormDialog;

                        AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();
                        con.Open();
                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddServices.cboServiceTax.DataSource = dtTax;
                        AddServices.cboServiceTax.DisplayMember = "tax_desc";
                        AddServices.cboServiceTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();

                        con.Close();
                        AddServices.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddServices.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddServices.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            conn.OpenConnection();
                            dgvServiceOrderListRefresh();
                            refreshTotal();
                            conn.CloseConnection();
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }
                
                

            }
            else if ("Item" == type)
            {
                try
                {
                    using (Add_Items AddItems = new Add_Items())
                    {

                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddItems.Owner = newFormDialog;

                        AddItems.txtItemName.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddItems.txtItemCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddItems.txtPrice.Text = this.dgvServiceList.CurrentRow.Cells[5].Value.ToString();

                        con.Open();
                        SqlCommand cmd = new SqlCommand("select * from dbo.uom", con);
                        SqlDataReader dr = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(dr);
                        AddItems.cboUOM.DataSource = dt;
                        AddItems.cboUOM.DisplayMember = "uom_desc";
                        AddItems.cboUOM.ValueMember = "uom_id";
                        cmd.ExecuteNonQuery();

                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddItems.cboItemTax.DataSource = dtTax;
                        AddItems.cboItemTax.DisplayMember = "tax_desc";
                        AddItems.cboItemTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();
                        con.Close();
                        AddItems.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddItems.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddItems.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            dgvServiceOrderListRefresh();
                            refreshTotal();
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }  
            }
            else
            {
                try
                {
                    using (Add_Service AddServices = new Add_Service())
                    {
                        newFormDialog.StartPosition = FormStartPosition.Manual;
                        newFormDialog.FormBorderStyle = FormBorderStyle.None;
                        newFormDialog.WindowState = FormWindowState.Maximized;
                        newFormDialog.Opacity = .70d;
                        newFormDialog.BackColor = Color.Black;
                        newFormDialog.TopMost = true;
                        newFormDialog.Location = this.Location;
                        newFormDialog.ShowInTaskbar = false;
                        newFormDialog.Show();

                        AddServices.Owner = newFormDialog;
                        AddServices.lblBomCode.Text = this.dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                        AddServices.txtServiceName.Text = this.dgvServiceList.CurrentRow.Cells[2].Value.ToString();
                        AddServices.txtServiceCode.Text = this.dgvServiceList.CurrentRow.Cells[1].Value.ToString();
                        AddServices.txtServicePrice.Text = this.dgvServiceList.CurrentRow.Cells[4].Value.ToString();
                        con.Open();
                        SqlCommand cmdTax = new SqlCommand("select * from dbo.tax", con);
                        SqlDataReader drTax = cmdTax.ExecuteReader();
                        DataTable dtTax = new DataTable();
                        dtTax.Load(drTax);
                        AddServices.cboServiceTax.DataSource = dtTax;
                        AddServices.cboServiceTax.DisplayMember = "tax_desc";
                        AddServices.cboServiceTax.ValueMember = "tax_id";
                        cmdTax.ExecuteNonQuery();

                        con.Close();
                        AddServices.lblDocumentNumber.Text = lblDocumentNumber.Text;
                        AddServices.lblDocumentSeries.Text = lblDocumentSeries.Text;

                        if (AddServices.ShowDialog() != DialogResult.OK)
                        {
                            newFormDialog.Dispose();
                            conn.OpenConnection();
                            dgvServiceOrderList.DataSource = conn.ShowDataInGridView("select * from draft_order_view");
                            refreshTotal();
                            conn.CloseConnection();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    newFormDialog.Dispose();
                }
            }

            Add_Items add_Items = new Add_Items();
            Add_Service add_Service = new Add_Service();
        }

        private void txtSearch_TextChanged_1(object sender, EventArgs e)
        {
            ntxtSearch = txtSearch.Text;
            dgvItemlistrefresh();
        }

        private void cboSearchBy_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ncboSearchBy = this.cboSearchBy.GetItemText(this.cboSearchBy.SelectedItem);

            dgvItemlistrefresh();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            nEmployeeID = Int32.Parse(cboEstimatedBy.SelectedValue.ToString());
            nTechnician = Int32.Parse(cboTechnicianName.SelectedValue.ToString());

            conn.OpenConnection();
            SqlDataReader drDraft = conn.DataReader("select * from draft_order");
            if (drDraft.Read() && cusID != 0 && nEmployeeID != 0 && nTechnician != 0 && txtCustomerName.Text != "" && vehicleID != 0)
            {

                conn.CloseConnection();

                conn.OpenConnection();
                SqlDataReader negativeQuantity = conn.DataReader("Select COUNT(*) from(Select a.inventory_code 'Code', Case when b.inventory_type = 1 then b.quantity else '1' end as 'Quantity' from draft_order a left join inventory b on a.inventory_code = b.inventory_code)a left join draft_order b on a.Code = b.inventory_code where(a.Quantity - b.quantity) > = 0 having COUNT(*) = (Select COUNT(*) from draft_order)");
                if (negativeQuantity.Read())
                {
                    conn.CloseConnection();
                    double nTotalPaymentDue = double.Parse(TotalPaymentDue);
                    conn.OpenConnection();
                    conn.ExecuteQueries("insert into job_order (job_number,job_estimate_number, customer_id, car_id, [date], technician,approved_by, status_id, total_price)" +
                        " values" +
                        " ('" + nDocumentNumber + "'" +
                        ", '" + JelDocNum + "'" +
                        ", '" + cusID + "'" +
                        ", '" + vehicleID + "'" +
                        ", '" + nDateCreated + "'" +
                        ", '" + nTechnician + "'" +
                        ", '" + nEmployeeID + "'" +
                        ", '" + 5 + "'" +
                        ", '" + nTotalPaymentDue + "')");
                    conn.CloseConnection();

                    conn.OpenConnection();
                    conn.ExecuteQueries("Insert into job_order_body (job_number,inventory_code,price,quantity,tax_id,tax_price,uom_id,total_price,bom_code) Select job_estimate_number,inventory_code,price,quantity,tax_id,tax_price,uom_id,total_price,bom_code from draft_order");
                    conn.CloseConnection();

                    conn.OpenConnection();
                    conn.ExecuteQueries("insert into work_progress (customer_id,employee_id,car_id,job_number,start_date,status)" +
                        "values" +
                        "('" + cusID + "'" +
                        ", '" + nTechnician + "'" +
                        ", '" + vehicleID + "'" +
                        ", '" + nDocumentNumber + "'" +
                        ", '" + nDateCreated + "'" +
                        ", '" + 7 + "')");
                    conn.CloseConnection();

                    /*conn.OpenConnection();
                    SqlDataReader stockDR = conn.DataReader("Select a.job_estimate_number 'Job Number',a.inventory_code 'Item Code',a.quantity 'quantity' from draft_order a left join inventory b on a.inventory_code = b.inventory_code left join inventory_type c on b.inventory_type = c.inventory_type_id where c.inventory_desc = 'Item'");

                        conn.CloseConnection();*/
                    conn.OpenConnection();
                    conn.ExecuteQueries("insert into stock_out (job_number,[date],inventory_code,quantity,used_quantity) Select a.job_estimate_number,'" + nDateCreated + "',a.inventory_code,(b.quantity - a.quantity),a.quantity  from draft_order a left join inventory b on a.inventory_code = b.inventory_code left join inventory_type c on b.inventory_type = c.inventory_type_id where c.inventory_desc = 'Item' and job_estimate_number = '" + nDocumentNumber + "'");
                    conn.CloseConnection();

                    conn.OpenConnection();
                    conn.ExecuteQueries("Update job_estimate set status_id = 6 where job_estimate_number = '" + JelDocNum + "'");
                    conn.CloseConnection();


                    conn.OpenConnection();
                    conn.ExecuteQueries("update inventory set quantity  = a.quantity - b.quantity from inventory a left join draft_order b on a.inventory_code = b.inventory_code where b.job_estimate_number = '" + nDocumentNumber + "'");
                    conn.ExecuteQueries("Truncate table draft_order");
                    conn.CloseConnection();

                    MessageBox.Show("Job Order saved", "Sucessfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Items in inventory is not sufficient","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Fill up all fields/Add Service or item");
            }
            }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvServiceOrderList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnCancel_Click_2(object sender, EventArgs e)
        {
            this.Close();
            conn.OpenConnection();
            conn.ExecuteQueries("Truncate table draft_order");
            conn.CloseConnection();
        }

        private void rdoWithMaterial_CheckedChanged(object sender, EventArgs e)
        {
            dgvItemlistrefresh();
        }
        private void dgvItemlistrefresh()
        {

            if (rdoWithMaterial.Checked)
            {
                conn.OpenConnection();
                dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "' and ([Service/Item Name] like '%" + ntxtSearch + "%' or [Service/Item Code] like '%" + ntxtSearch + "%')");
                conn.CloseConnection();
            }
            else if (rdoWithoutMaterial.Checked)
            {
                if (ncboSearchBy == "Item")
                {
                    conn.OpenConnection();
                    dgvServiceList.DataSource = conn.ShowDataInGridView("Select * from Inventory_table where [Inventory Type] = '" + ncboSearchBy + "' and ([Service/Item Name] like '%" + ntxtSearch + "%' or [Service/Item Code] like '%" + ntxtSearch + "%')");
                    conn.CloseConnection();
                }
                else if (ncboSearchBy == "Services")
                {
                    conn.OpenConnection();
                    dgvServiceList.DataSource = conn.ShowDataInGridView("select * from parent_view where [Service Code] like '%" + ntxtSearch + "%'");
                    conn.CloseConnection();
                }
            }
        }

        private void rdoWithoutMaterial_CheckedChanged(object sender, EventArgs e)
        {
            dgvItemlistrefresh();
        }

        private void cboVehicleModel_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            conn.OpenConnection();

            string model = this.cboVehicleModel.GetItemText(this.cboVehicleModel.SelectedItem);
            string owner = txtCustomerName.Text;

            SqlDataReader drCBOV = conn.DataReader("select * from dbo.vehicles_of_customer where [Customer ID] = '" + cusID + "'and  [Vehicle Model] = '" + model + "'");
            if (drCBOV.Read())
            {
                txtVehicleMake.Text = drCBOV["Auto"].ToString();
                txtVehicleSize.Text = drCBOV["Size"].ToString();
                txtVehicleColor.Text = drCBOV["color"].ToString();
                txtPlateNumber.Text = drCBOV["Platenumber"].ToString();
                vehicleID = Int32.Parse(drCBOV["Vehicle ID"].ToString());
                cboCategory.Text = drCBOV["Vehicle Category"].ToString();
                conn.CloseConnection();
            }
        }

        private void dgvServiceOrderList_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {
            string item_or_service_name = this.dgvServiceOrderList.CurrentRow.Cells[1].Value.ToString();
            int iD = Int32.Parse(this.dgvServiceOrderList.CurrentRow.Cells[10].Value.ToString());
            string bom_code = this.dgvServiceOrderList.CurrentRow.Cells[11].Value.ToString();

            if (bom_code == "")
            {
                if (MessageBox.Show("Do you wan't to void this item?" + "\n" + item_or_service_name, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    conn.OpenConnection();
                    conn.ExecuteQueries("delete from dbo.draft_order where draft_estimate_id = '" + iD + "'");
                    conn.CloseConnection();
                    dgvServiceOrderListRefresh();
                    refreshTotal();
                }
                else
                {

                }
            }
            else
            {
                if (MessageBox.Show("Do you want to void this BOM?" + "\n" + bom_code, "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    conn.OpenConnection();
                    conn.ExecuteQueries("delete from draft_order where bom_code = '" + bom_code + "'");
                    conn.CloseConnection();
                    dgvServiceOrderListRefresh();
                    refreshTotal();
                }
                else
                {

                }
            }
        }

        private void btnDecline_Click(object sender, EventArgs e)
        {
            conn.OpenConnection();
            SqlDataReader drDraft = conn.DataReader("select * from draft_order");

            if (drDraft.Read() && cusID != 0 && nEmployeeID != 0 && nTechnician != 0 && txtCustomerName.Text != "" && vehicleID != 0)
            {
                conn.CloseConnection();
                conn.OpenConnection();
                conn.ExecuteQueries("update job_estimate set status_id = 8 where job_estimate_number = '" + JelDocNum + "'");
                conn.CloseConnection();

                conn.OpenConnection();
                conn.ExecuteQueries("Truncate table draft_order");
                conn.CloseConnection();

                MessageBox.Show("Job Estimate Declined");
                this.Close();
            }
            else
            {
                MessageBox.Show("No data is presented","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
    }
}
