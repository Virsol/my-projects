﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Estimate_Add_Service : Form
    {
        DBConnection dbcon = new DBConnection();
        public Job_Estimate_Add_Service()
        {
            InitializeComponent();
        }

        private void Job_Estimate_Add_Service_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            string nDocSeries = lblDocumentSeries.Text;
            string nDOcNumber = lblDocumentNumber.Text;
            string nServiceName = txtServiceName.Text;
            string nServiceCode = txtServiceCode.Text;
            int nTax = Int32.Parse(cboServiceTax.SelectedValue.ToString());
            double nServicePrice = double.Parse(txtServicePrice.Text);
            dbcon.OpenConnection();
            SqlDataReader drTax = dbcon.DataReader("select tax_rate from dbo.tax where tax_id = '" + nTax + "'");
            drTax.Read();
            int nTaxRate = Int32.Parse(drTax["tax_rate"].ToString());
            dbcon.CloseConnection();

            int percent = 100;
            double nTaxPrice = nServicePrice * nTaxRate / percent;
            double total = nServicePrice + nTaxPrice;
            dbcon.OpenConnection();
            dbcon.ExecuteQueries("insert into draft_estimate (job_estimate_number, price, tax_id, tax_price, total_price, Inventory_code)values" +
                "('" + nDOcNumber + "'," +
                "'" + nServicePrice + "'," +
                "'" + nTax + "'," +
                "'" + nTaxPrice + "'," +
                "'" + total + "'," +
                "'" + nServiceCode + "')");
            dbcon.CloseConnection();
            MessageBox.Show("Service added", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Job_Estimate_Add_Service_Load_1(object sender, EventArgs e)
        {

        }

        private void cboServiceTax_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
