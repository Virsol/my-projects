﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kraftsmen_Mechanics
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Goods_Receipt_Purchase_Order GRPO = new Goods_Receipt_Purchase_Order();
            GRPO.MdiParent = Kraftsmen_Mechanics.Leobytes.ActiveForm;
            GRPO.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void pnlPurchasing_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            AP_Invoice API = new AP_Invoice();
            API.MdiParent = Kraftsmen_Mechanics.Leobytes.ActiveForm;
            API.Show();
        }

        private void btnJobEstimate_Click(object sender, EventArgs e)
        {
            Job_Estimate job_Estimate = new Job_Estimate();
            job_Estimate.MdiParent = Kraftsmen_Mechanics.Leobytes.ActiveForm;
            job_Estimate.Show();
        }
    }
 }

