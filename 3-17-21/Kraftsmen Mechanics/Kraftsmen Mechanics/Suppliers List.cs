﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Suppliers_List : Form
    {

        private string supCode, supName, supAdd, supCon, conCode;
        private string supplyName;
        public string SupplyName{
            get { return supplyName; }
            set { supplyName = value; }
            }
        public string SupCode
        { 
            get { return supCode; }
            set { supCode = value; }
        }
        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }
        public string SupAdd
        {
            get { return supAdd; }
            set { supAdd = value; }
        }
        public string SupCon
        {
            get { return supCon; }
            set { supCon = value; }
        }

        public string ConCode
        {
            get { return conCode; }
            set { conCode = value; }
        }

        public Suppliers_List()
        {
            InitializeComponent();
        }

        Connection supConn = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-BJQ45VQ\\LEOBYTES;Initial Catalog=service;Persist Security Info=True;User ID=sa;Password=15249898");

        

        #region hidden file
        private void btnAddNewSupplier_Click(object sender, EventArgs e)
        {
            Supplier_Master_Data sup_MasData = new Supplier_Master_Data();
            sup_MasData.ShowDialog();
        }
        private void dgvSupplierList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Suppliers_List_Load(object sender, EventArgs e)
        {

        }
        private void txtSearchContactPerson_TextChanged(object sender, EventArgs e)
        {

        }
        #endregion


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvSupplierList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            // PO_Report.suphomies = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            supplyName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            
            
         
        }

        private void dgvSupplierList_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {

            supCode = this.dgvSupplierList.CurrentRow.Cells[1].Value.ToString();
            supName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            supAdd = this.dgvSupplierList.CurrentRow.Cells[4].Value.ToString();
            supCon = this.dgvSupplierList.CurrentRow.Cells[7].Value.ToString();
            //supplyName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            supplyName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            PO_Report.suphomies = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
           
            this.Close();
        }

        private void Suppliers_List_Load_1(object sender, EventArgs e)
        {
            ShowDGV();
        }

        private void txtSearchContactPerson_TextChanged_1(object sender, EventArgs e)
        {
            supConn.OpenConn();
            dgvSupplierList.DataSource = supConn.ShowDataDGV("Select * from supplier where [supplier_code] like '%" + txtSearchContactPerson.Text + "%' or [Name] like '%" + txtSearchContactPerson.Text + "%'");
            supConn.CloseConn();
        }
        private void ShowDGV()
        {
            supConn.OpenConn();
            dgvSupplierList.DataSource = supConn.ShowDataDGV("select * from supplier");
            supConn.CloseConn();
        }

        
    }
}
