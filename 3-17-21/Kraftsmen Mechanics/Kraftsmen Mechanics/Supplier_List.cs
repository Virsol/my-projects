﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Supplier_List : Form
    {

        private string supCode, supName, supAdd, supCon, conCode;

        public string SupCode
        {
            get { return supCode; }
            set { supCode = value; }
        }
        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }
        public string SupAdd
        {
            get { return supAdd; }
            set { supAdd = value; }
        }
        public string SupCon
        {
            get { return supCon; }
            set { supCon = value; }
        }

        public string ConCode
        {
            get { return conCode; }
            set { conCode = value; }
        }


        public Supplier_List()
        {
            InitializeComponent();
        }

        Connection supConn = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-EQJE313\\LEOBYTES02;Initial Catalog=service;User ID=sa;Password=123456");

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            groupBox2.Enabled = true;
        }

        #region click event
        private void dgvSupplierList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvSupplierList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
        private void dgvSupplierList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
        #endregion

        private void dgvSupplierList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            groupBox1.Enabled = true;
            groupBox2.Enabled = true;

            if (e.RowIndex >= 0)
            {
                txtSupCode.Text = dgvSupplierList.SelectedRows[0].Cells[1].Value.ToString();
                txtSupName.Text = dgvSupplierList.SelectedRows[0].Cells[2].Value.ToString();
                txtSupConNo.Text = dgvSupplierList.SelectedRows[0].Cells[7].Value.ToString();
                txtSupAdd.Text = dgvSupplierList.SelectedRows[0].Cells[4].Value.ToString();
                cboSupplierCity.Text = dgvSupplierList.SelectedRows[0].Cells[5].Value.ToString();
                txtSupPostalCode.Text = dgvSupplierList.SelectedRows[0].Cells[6].Value.ToString();
                txtSupplierEmail.Text = dgvSupplierList.SelectedRows[0].Cells[3].Value.ToString();
            }
        }

        private void txtSupCode_TextChanged(object sender, EventArgs e)
        {
            conn.Open();
            try { 
                string conper = txtSupCode.Text;

                SqlCommand cmd = new SqlCommand("select * from dbo.contact_person where supplier_code = '" + conper + "'", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                cboContactName.DataSource = dt;
                cboContactName.DisplayMember = "name";
                cboContactName.ValueMember = "contact_person_id";
                cmd.ExecuteNonQuery();

                //SqlCommand supcmd = new SqlCommand("select * from dbo.supplier where supplier_code = '"+conper+"'",conn);
                //SqlDataReader supdr = supcmd.ExecuteReader();
                //DataTable supdt = new DataTable();
                //supdt.Load(supdr);
                //cboSupplierCity.DataSource = supdt;
                //cboSupplierCity.DisplayMember = ""



            }
            catch (Exception)
            {

            }

            conn.Close();

        }

        private void cboContactName_SelectedIndexChanged(object sender, EventArgs e)
        {


            supConn.OpenConn();
            string conname = this.cboContactName.GetItemText(this.cboContactName.SelectedItem);
            int convalue = Int32.Parse(this.cboContactName.SelectedValue.ToString());
            string supcode = txtSupCode.Text;

                SqlDataReader supdr = supConn.DataReader("select * from dbo.contact_person where[supplier_code] = '" + supcode + "' and [name] = '" + conname + "'");

                 if (supdr.Read())
                    {
                      txtConAdd.Text = supdr["address"].ToString();
                      txtConNo.Text = supdr["contact_number"].ToString();
                      txtConEmail.Text = supdr["email"].ToString();
                      cboContactPersonCity.Text = supdr["city"].ToString();
                      txtPostalCode.Text = supdr["postal_code"].ToString();
                      supConn.CloseConn();
                    }
        }

        #region click events

        private void dgvSupplierList_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dgvSupplierList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnName_Click(object sender, EventArgs e)
        {
            supConn.OpenConn();
            if (txtSupCode.Text == "" || txtSupName.Text == "" || txtSupConNo.Text == "")
            {
                MessageBox.Show("Please Fill up the form first!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                supConn.ExecuteQuery("Insert into dbo.supplier(supplier_code, name, contact_number, address, email_add, city, postal_code)" +
                    "Values('" + txtSupCode.Text + "' , '" + txtSupName.Text + "', '" + txtSupConNo.Text + "', '" + txtSupAdd.Text + "', '"+txtSupplierEmail.Text+"' , '"+cboSupplierCity.Text+"' , '"+txtSupPostalCode.Text+"')");
                supConn.CloseConn();

                supConn.OpenConn();
                supConn.ExecuteQuery("insert into dbo.contact_person(name, address, contact_number, city, postal_code, email, supplier_code)" +
                    "Values('"+cboContactName.Text+"' , '"+txtConAdd.Text+"' , '"+txtSupConNo.Text+"' , '"+cboContactPersonCity.Text+"' , '"+txtPostalCode.Text+"' , '"+txtConEmail.Text+"' , '"+txtSupCode.Text+"')");
                supConn.CloseConn();


                MessageBox.Show("Save Successfull!", "Save Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowDGV();
                Clear();
                
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }
        #endregion
        private void Supplier_List_Load(object sender, EventArgs e)
        {
            ShowDGV();
        }
        private void ShowDGV()
        {
            supConn.OpenConn();
            dgvSupplierList.DataSource = supConn.ShowDataDGV("Select * from dbo.supplier");
            supConn.CloseConn();
        }

        private void Clear()
        {
            txtSupCode.Text = "";
            txtSupName.Text = "";
            txtSupConNo.Text = "";
            txtSupAdd.Text = "";

            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
        }

        private void txtSupplierSearch_TextChanged(object sender, EventArgs e)
        {
            supConn.OpenConn();
            dgvSupplierList.DataSource = supConn.ShowDataDGV("Select * from supplier where [supplier_code] like '%"+txtSupplierSearch.Text+"%' or [Name] like '%"+txtSupplierSearch.Text+"%'");
            supConn.CloseConn();
        }

        private void dgvSupplierList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            supCode = this.dgvSupplierList.CurrentRow.Cells[1].Value.ToString();
            supName = this.dgvSupplierList.CurrentRow.Cells[2].Value.ToString();
            supAdd = this.dgvSupplierList.CurrentRow.Cells[4].Value.ToString();
            supCon = this.dgvSupplierList.CurrentRow.Cells[7].Value.ToString();

            this.Close();
        }
    }
}
