﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
namespace Kraftsmen_Mechanics
{
    public partial class Accounts_Settings : Form
    {
        public Accounts_Settings()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OpenForm(Form form)
        {
            if (pnlContainer != null)
            {
                pnlContainer.Controls.Clear();
                form.AutoScroll = true;
                form.TopLevel = false;
                form.WindowState = FormWindowState.Normal;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                /*I assume this code is in your ParentForm and so 'this' points to ParentForm that contains ContainerPanel*/
                this.pnlContainer.Controls.Add(form);
                this.pnlContainer.Dock = DockStyle.Fill;
                form.BringToFront();
                form.Show();
            }
            else
            {
                form.AutoScroll = true;
                form.TopLevel = false;
                form.WindowState = FormWindowState.Normal;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                /*I assume this code is in your ParentForm and so 'this' points to ParentForm that contains ContainerPanel*/
                this.pnlContainer.Controls.Add(form);
                this.pnlContainer.Dock = DockStyle.Fill;
                form.BringToFront();
                form.Show();
            }
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            OpenForm(new Users());
        }

        private void btnAccountInfo_Click(object sender, EventArgs e)
        {

            OpenForm(new Account_Info());
        }
        private struct RGBColor{
            public static Color color1 = Color.FromArgb(112, 119, 147);
        }
        private void Accounts_Settings_Load(object sender, EventArgs e)
        {
            OpenForm(new Account_Info());
        }
    }
}
