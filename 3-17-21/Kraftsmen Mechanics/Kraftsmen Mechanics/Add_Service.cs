﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Add_Service : Form
    {
        DBConnection dbcon = new DBConnection();
        private string docSeries;
        public Add_Service()
        {
            InitializeComponent();
        }

        private void Job_Estimate_Add_Service_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            string nDocSeries = lblDocumentSeries.Text;
            string nDOcNumber = lblDocumentNumber.Text;
            string nServiceName = txtServiceName.Text;
            string nServiceCode = txtServiceCode.Text;
            int nTax = Int32.Parse(cboServiceTax.SelectedValue.ToString());
            double nServicePrice = double.Parse(txtServicePrice.Text);
            dbcon.OpenConnection();
            SqlDataReader drTax = dbcon.DataReader("select tax_rate from dbo.tax where tax_id = '" + nTax + "'");
            drTax.Read();
            int nTaxRate = Int32.Parse(drTax["tax_rate"].ToString());
            dbcon.CloseConnection();
            int quantity = 1;
            int percent = 100;
            double nTaxPrice = nServicePrice * nTaxRate / percent;
            double total = nServicePrice + nTaxPrice;
            dbcon.CloseConnection();
            dbcon.OpenConnection();
            SqlDataReader drDT = dbcon.DataReader("select * from dbo.document_series, document_tag where document_tag.doc_tag_desc = 'Job Order' and document_series.doc_tag_id = Document_tag.doc_tag_id");
            if (drDT.Read())
            {
                docSeries = drDT["series_desc"].ToString();
            }
            dbcon.CloseConnection();
            if (docSeries == lblDocumentSeries.Text)
            {
                if(lblBomCode.Text == "")
                {
                    dbcon.OpenConnection();
                    dbcon.ExecuteQueries("insert into draft_order (job_estimate_number, price, tax_id, tax_price, total_price, Inventory_code, quantity)values" +
                        "('" + nDOcNumber + "'," +
                        "'" + nServicePrice + "'," +
                        "'" + nTax + "'," +
                        "'" + nTaxPrice + "'," +
                        "'" + total + "'," +
                        "'" + nServiceCode + "'," +
                        "'" + quantity + "')");
                    dbcon.CloseConnection();
                    MessageBox.Show("Service added", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    dbcon.OpenConnection();
                    SqlDataReader dr = dbcon.DataReader("Select * from bom where bom_code = '" + lblBomCode.Text + "'");
                    dr.Read();
                    double bomPrice = double.Parse(dr["total_price"].ToString());
                    double bomTotalTax = bomPrice * nTaxRate / percent;
                    double bomTotalPrice = bomPrice + bomTotalTax;
                    dbcon.CloseConnection();
                    dbcon.OpenConnection();
                    dbcon.ExecuteQueries("insert into draft_order (job_estimate_number, inventory_code,tax_id,bom_code, price, quantity, tax_price, total_price) select '" + nDOcNumber + "',inventory_code,'" + nTax + "','"+lblBomCode.Text+"', total_price, item_quantity,case when [P/C] = 1 then '" + bomTotalTax + "' else 0 end as 'total tax price',case when [P/C] = 1 then '" + bomTotalPrice + "' else 0 end as 'total price' from bom");
                    dbcon.CloseConnection();
                    this.Close();
                }

            }
            else
            {
                if(lblBomCode.Text == "")
                {
                    dbcon.OpenConnection();
                    dbcon.ExecuteQueries("insert into draft_estimate (job_estimate_number, price, tax_id, tax_price, total_price, Inventory_code, quantity)values" +
                        "('" + nDOcNumber + "'," +
                        "'" + nServicePrice + "'," +
                        "'" + nTax + "'," +
                        "'" + nTaxPrice + "'," +
                        "'" + total + "'," +
                        "'" + nServiceCode + "'," +
                        "'" + quantity + "')");
                    dbcon.CloseConnection();
                    MessageBox.Show("Service added", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    dbcon.OpenConnection();
                    SqlDataReader dr = dbcon.DataReader("Select * from bom where bom_code = '"+lblBomCode.Text+"'");
                    dr.Read();
                    double bomPrice = double.Parse(dr["total_price"].ToString());
                    double bomTotalTax = bomPrice * nTaxRate / percent;
                    double bomTotalPrice = bomPrice + bomTotalTax;
                    dbcon.CloseConnection();
                    dbcon.OpenConnection();
                    dbcon.ExecuteQueries("insert into draft_estimate (job_estimate_number, inventory_code,tax_id,bom_code, price, quantity, tax_price, total_price) select '" + nDOcNumber + "',inventory_code,'" + nTax + "','"+lblBomCode.Text+"', total_price, item_quantity,case when [P/C] = 1 then Try_cast('" + bomTotalTax + "' as Decimal(11,2)) else 0 end as 'total tax price',case when [P/C] = 1 then Try_cast('" + bomTotalPrice + "' as Decimal(11,2)) else 0 end as 'total price' from bom where bom_code = '"+lblBomCode.Text+"'");
                    dbcon.CloseConnection();
                    this.Close();
                }

            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Job_Estimate_Add_Service_Load_1(object sender, EventArgs e)
        {

        }

        private void cboServiceTax_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
