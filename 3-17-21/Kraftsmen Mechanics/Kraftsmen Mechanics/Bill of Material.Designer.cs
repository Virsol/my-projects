﻿
namespace Kraftsmen_Mechanics
{
    partial class Bill_of_Material
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bill_of_Material));
            this.dgvBillOfMaterialList = new System.Windows.Forms.DataGridView();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnAddBillOfMaterial = new FontAwesome.Sharp.IconButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillOfMaterialList)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBillOfMaterialList
            // 
            this.dgvBillOfMaterialList.AllowUserToAddRows = false;
            this.dgvBillOfMaterialList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBillOfMaterialList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBillOfMaterialList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvBillOfMaterialList.BackgroundColor = System.Drawing.Color.White;
            this.dgvBillOfMaterialList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBillOfMaterialList.Location = new System.Drawing.Point(16, 119);
            this.dgvBillOfMaterialList.MultiSelect = false;
            this.dgvBillOfMaterialList.Name = "dgvBillOfMaterialList";
            this.dgvBillOfMaterialList.ReadOnly = true;
            this.dgvBillOfMaterialList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBillOfMaterialList.Size = new System.Drawing.Size(1000, 526);
            this.dgvBillOfMaterialList.TabIndex = 65;
            this.dgvBillOfMaterialList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBillOfMaterialList_CellDoubleClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(701, 75);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(315, 20);
            this.txtSearch.TabIndex = 64;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(651, 78);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(44, 13);
            this.bunifuCustomLabel2.TabIndex = 63;
            this.bunifuCustomLabel2.Text = "Search:";
            // 
            // btnAddBillOfMaterial
            // 
            this.btnAddBillOfMaterial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(222)))), ((int)(((byte)(67)))));
            this.btnAddBillOfMaterial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddBillOfMaterial.FlatAppearance.BorderSize = 0;
            this.btnAddBillOfMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBillOfMaterial.ForeColor = System.Drawing.Color.White;
            this.btnAddBillOfMaterial.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.btnAddBillOfMaterial.IconColor = System.Drawing.Color.White;
            this.btnAddBillOfMaterial.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnAddBillOfMaterial.IconSize = 20;
            this.btnAddBillOfMaterial.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddBillOfMaterial.Location = new System.Drawing.Point(14, 68);
            this.btnAddBillOfMaterial.Name = "btnAddBillOfMaterial";
            this.btnAddBillOfMaterial.Padding = new System.Windows.Forms.Padding(5, 4, 5, 5);
            this.btnAddBillOfMaterial.Size = new System.Drawing.Size(180, 32);
            this.btnAddBillOfMaterial.TabIndex = 66;
            this.btnAddBillOfMaterial.Text = "Add Bill of Material";
            this.btnAddBillOfMaterial.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddBillOfMaterial.UseVisualStyleBackColor = false;
            this.btnAddBillOfMaterial.Click += new System.EventHandler(this.btnAddBillOfMaterial_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(186)))), ((int)(((byte)(156)))));
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 42);
            this.panel1.TabIndex = 67;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(11, 12);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 20);
            this.label17.TabIndex = 24;
            this.label17.Text = "Bill of Material";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(992, 9);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(25, 25);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Bill_of_Material
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1028, 657);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAddBillOfMaterial);
            this.Controls.Add(this.dgvBillOfMaterialList);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Bill_of_Material";
            this.Text = "Bill_of_Material";
            this.Load += new System.EventHandler(this.Bill_of_Material_Load_3);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillOfMaterialList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FontAwesome.Sharp.IconButton btnAddBillOfMaterial;
        public System.Windows.Forms.DataGridView dgvBillOfMaterialList;
        private System.Windows.Forms.TextBox txtSearch;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuImageButton btnExit;
    }
}