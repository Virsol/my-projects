﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace Kraftsmen_Mechanics
{
    public partial class GRPO_List : Form
    {

        private string supCode, supName, supAddress, supPerson, supContact, createdBy, dateCreated, datePosted, dateUpdated, docNum;

        public static string xstatus = "";
        
        
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string DatePosted
        {
            get { return datePosted; }
            set { datePosted = value; }
        }

        public string DateUpdated
        {
            get { return dateUpdated; }
            set { dateUpdated = value; }
        }
        public string SupPerson
        {
            get { return supPerson; }
            set { supPerson = value; }
        }
        public string SupContact
        {
            get { return supContact; }
            set { supContact = value; }
        }
        public string SupAddress
        {
            get { return supAddress; }
            set { supAddress = value; }
        }

        public string SupCode
        {
            get { return supCode; }
            set { supCode = value; }
        }
        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }
        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        public GRPO_List()
        {
            InitializeComponent();
        }

        Connection conGrpoList = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            conGrpoList.OpenConn();
            dgvGRPOList.DataSource = conGrpoList.ShowDataDGV("Select * From GRPO_List_view where ([GRPO Document]  like '%" + txtSearch.Text + "%' or [Supplier] like '%" + txtSearch.Text + "%') AND [Status] LIKE '" + cboStatus.Text + "'");
            conGrpoList.CloseConn();
        }

        

        private void cboStatus_TextChanged(object sender, EventArgs e)
        {
            if (cboStatus.Text == "OPEN")
            {
                conGrpoList.OpenConn();
                dgvGRPOList.DataSource = conGrpoList.ShowDataDGV("Select * from dbo.GRPO_List_view where Status = 'OPEN'");
                conGrpoList.CloseConn();
            }
            else if (cboStatus.Text == "CLOSED")
            {
                conGrpoList.OpenConn();
                dgvGRPOList.DataSource = conGrpoList.ShowDataDGV("Select * from dbo.GRPO_List_view where Status = 'CLOSED'");
                conGrpoList.CloseConn();
            }
        }

       
         private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GRPO_List_Load(object sender, EventArgs e)
        {
            cboStatus.Enabled = false;
            conGrpoList.OpenConn();
            dgvGRPOList.DataSource = conGrpoList.ShowDataDGV("select * from GRPO_LIST_VIEW");
            conGrpoList.CloseConn();
            //----------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdStatus = new SqlCommand("Select * from dbo.status where status_id = '5'", conn);
            SqlDataReader drStatus = cmdStatus.ExecuteReader();
            DataTable dtStatus = new DataTable();
            dtStatus.Load(drStatus);
            cboStatus.DataSource = dtStatus;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStatus.ExecuteNonQuery();


            conn.Close();
            //----------------------------------------------------------------------------//
        }

        private void dgvGRPOList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string docNum = dgvGRPOList.CurrentRow.Cells[0].Value.ToString();
            xstatus = dgvGRPOList.CurrentRow.Cells[6].Value.ToString();

            conGrpoList.OpenConn();
            SqlDataReader drGRPO = conGrpoList.DataReader("Select a.grpo_doc_num 'Document Number', a.supplier_code 'Supplier Code', b.name 'Supplier Name', b.address 'Address', b.contact_number 'Contact Number', c.name 'Contact Person', a.date_created 'Date Created', a.date_posted 'Date Posted', a.date_upload 'Date Updated', d.employee_id 'Created by', e.status_desc 'Status' from grpo a left join supplier b on a.supplier_code = b.supplier_code left join contact_person c on a.contact_person_id = c.contact_person_id left join employee d on a.employee_id = d.employee_id left join [status] e on a.status_id = e.status_id where grpo_doc_num = (Select RIGHT([GRPO Document],7) from GRPO_LIST_VIEW where [GRPO Document] = '"+docNum+"')");
            drGRPO.Read();

            DocNum = drGRPO["Document Number"].ToString();
            SupCode = drGRPO["Supplier Code"].ToString();
            SupName = drGRPO["Supplier Name"].ToString();
            SupPerson = drGRPO["Contact Person"].ToString();
            SupContact = drGRPO["Contact Number"].ToString();
            SupAddress = drGRPO["Address"].ToString();

            DateCreated = drGRPO["Date Created"].ToString();
            DatePosted = drGRPO["Date Posted"].ToString();
            DateUpdated = drGRPO["Date Updated"].ToString();

            CreatedBy = drGRPO["Created By"].ToString();


            conGrpoList.CloseConn();
            this.Close();
        }
    }
}
