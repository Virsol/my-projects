﻿
namespace Kraftsmen_Mechanics
{
    partial class Account_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Account_Info));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEditAccountInfo = new FontAwesome.Sharp.IconButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picAccountImage = new System.Windows.Forms.PictureBox();
            this.lblAccountName = new System.Windows.Forms.Label();
            this.lblAccountType = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAccountImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblAccountType);
            this.panel1.Controls.Add(this.lblAccountName);
            this.panel1.Controls.Add(this.picAccountImage);
            this.panel1.Controls.Add(this.btnEditAccountInfo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1096, 596);
            this.panel1.TabIndex = 1;
            // 
            // btnEditAccountInfo
            // 
            this.btnEditAccountInfo.BackColor = System.Drawing.Color.Transparent;
            this.btnEditAccountInfo.FlatAppearance.BorderSize = 0;
            this.btnEditAccountInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditAccountInfo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAccountInfo.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.btnEditAccountInfo.IconColor = System.Drawing.Color.Black;
            this.btnEditAccountInfo.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnEditAccountInfo.IconSize = 40;
            this.btnEditAccountInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditAccountInfo.Location = new System.Drawing.Point(50, 481);
            this.btnEditAccountInfo.Name = "btnEditAccountInfo";
            this.btnEditAccountInfo.Padding = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.btnEditAccountInfo.Size = new System.Drawing.Size(366, 61);
            this.btnEditAccountInfo.TabIndex = 2;
            this.btnEditAccountInfo.Text = "Edit Account Info";
            this.btnEditAccountInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEditAccountInfo.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(47, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Manage your account info.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account Info";
            // 
            // picAccountImage
            // 
            this.picAccountImage.Image = ((System.Drawing.Image)(resources.GetObject("picAccountImage.Image")));
            this.picAccountImage.Location = new System.Drawing.Point(50, 145);
            this.picAccountImage.Name = "picAccountImage";
            this.picAccountImage.Size = new System.Drawing.Size(270, 236);
            this.picAccountImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAccountImage.TabIndex = 3;
            this.picAccountImage.TabStop = false;
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblAccountName.Location = new System.Drawing.Point(47, 399);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(106, 18);
            this.lblAccountName.TabIndex = 4;
            this.lblAccountName.Text = "Account Name";
            // 
            // lblAccountType
            // 
            this.lblAccountType.AutoSize = true;
            this.lblAccountType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblAccountType.Location = new System.Drawing.Point(47, 437);
            this.lblAccountType.Name = "lblAccountType";
            this.lblAccountType.Size = new System.Drawing.Size(98, 18);
            this.lblAccountType.TabIndex = 5;
            this.lblAccountType.Text = "Account Type";
            // 
            // Account_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1096, 757);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Account_Info";
            this.Text = "Account_Info";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAccountImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAccountType;
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.PictureBox picAccountImage;
        private FontAwesome.Sharp.IconButton btnEditAccountInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}