﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Edit_Product : Form
    {
        public Edit_Product()
        {
            InitializeComponent();
        }

        Connection e_Prod = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region trash
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void btnSave_Click(object sender, EventArgs e)
        {

        }
        private void Edit_Product_Load_1(object sender, EventArgs e)
        {

        }
        private void Edit_Product_Load(object sender, EventArgs e)
        {
            
        }
        #endregion
        

        private void Edit_Product_Load_2(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------//
            string xUom = Inventory.co;
            string xStat = Inventory.status;


            //-------------------------------------------------------------------------//

            Inventory inven = new Inventory();
            //-------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmduom = new SqlCommand("Select * from dbo.uom", conn);
            SqlDataReader dr = cmduom.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            cboUOM.DataSource = dt;
            cboUOM.DisplayMember = "uom_desc";
            cboUOM.ValueMember = "uom_id";
            cmduom.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------//
            conn.Open();

            SqlCommand cmdStatus = new SqlCommand("Select * from status where  status_id = '1' or status_id = '2'", conn);
            SqlDataReader drStatus = cmdStatus.ExecuteReader();
            DataTable dtStatus = new DataTable();
            dtStatus.Load(drStatus);
            cboStatus.DataSource = dtStatus;
            cboStatus.DisplayMember = "status_desc";
            cboStatus.ValueMember = "status_id";
            cmdStatus.ExecuteNonQuery();

            conn.Close();
            //-------------------------------------------------------------------------//

            cboUOM.Text = xUom;
            cboStatus.Text = xStat;
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            string eCode = txtProductCode.Text;
            string eName = txtProductName.Text;
            string eDesc = txtProductDescription.Text;
            double ePrice = double.Parse(txtProductPrice.Text);
            int eUom = Int32.Parse(cboUOM.SelectedValue.ToString());
            int eStat = Int32.Parse(cboStatus.SelectedValue.ToString());

            if (txtProductCode.Text == "" || txtProductName.Text == "" || txtProductDescription.Text == "" || txtProductPrice.Text == "" || txtProductPrice.Text == "0")
            {
                MessageBox.Show("Please fill up the fields first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                e_Prod.OpenConn();
                e_Prod.ExecuteQuery("Update inventory set inventory_name = '" + eName + "', inventory_description = '" + eDesc + "', item_price = '" + ePrice + "', uom_id = '" + eUom + "', inventory_status = '" + eStat + "' where inventory_code = '" + eCode + "'");
                e_Prod.CloseConn();

                this.Close();
            }

        }
    }
}
