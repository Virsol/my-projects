﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Bill_of_Material : Form
    {
        public static string xCode = "";
        
        private string bomCode, serCode, serName, serDesc;
        private double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }
        public string BomCode
        {
            get { return bomCode; }
            set { bomCode = value; }
        }

        public string SerCode
        {
            get { return serCode; }
            set { serCode = value; }
        }

        public string SerName
        {
            get { return serName; }
            set { serName = value; }
        }

        public string SerDesc
        {
            get { return serDesc; }
            set { serDesc = value; }
        }
        public Bill_of_Material()
        {
            InitializeComponent();
        }
        Connection bom = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        #region hide
        private void Bill_of_Material_Load(object sender, EventArgs e)
        {

        }
        private void Bill_of_Material_Load_1(object sender, EventArgs e)
        {

        }
        private void Bill_of_Material_Load_2(object sender, EventArgs e)
        {

        }
        #endregion

        private void btnAddBillOfMaterial_Click(object sender, EventArgs e)
        {
            Form newFormDialog = new Form();
            try
            {
                using (Add_Bill_of_Materials add_Bill_Of_Materials = new Add_Bill_of_Materials()) 
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = this.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    add_Bill_Of_Materials.Owner = newFormDialog;

                    add_Bill_Of_Materials.lblHidden.Text = "0";

                    if (add_Bill_Of_Materials.ShowDialog() != DialogResult.OK)
                    {
                        newFormDialog.Dispose();
                        showItem();
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void showItem()
        {
            bom.OpenConn();
            dgvBillOfMaterialList.DataSource = bom.ShowDataDGV("select * from Parent_View");
            bom.CloseConn();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void Bill_of_Material_Load_3(object sender, EventArgs e)
        {
            showItem();
        }

        private void dgvBillOfMaterialList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Add_Bill_of_Materials addBOM = new Add_Bill_of_Materials();
            

                string xBomCode = this.dgvBillOfMaterialList.CurrentRow.Cells[0].Value.ToString();

                bom.OpenConn();
                SqlDataReader drServ = bom.DataReader("select total_price 'Price', bom_code 'BOM Code', a.inventory_code 'Service Code', a.inventory_name 'Service Name', a.inventory_description 'Service Desc' from bom left join inventory a on bom.inventory_code = a.inventory_code where bom_code = '" + xBomCode + "' AND [P/C] = 1");
                drServ.Read();

                xCode = drServ["BOM Code"].ToString();
                SerCode = drServ["Service Code"].ToString();
                SerName = drServ["Service Name"].ToString();
                SerDesc = drServ["Service Desc"].ToString();
                Price = double.Parse(drServ["Price"].ToString());
                

                //addBOM.txtBOMCode.Text = BomCode;
                addBOM.lblHidden.Text = "1";
                addBOM.txtServiceCode.Text = SerCode;
                addBOM.txtServiceName.Text = SerName;
                addBOM.txtServiceDesc.Text = SerDesc;
                addBOM.txtTotalPrice.Text = Price.ToString("n2");

                addBOM.btnAddItem.Enabled = false;
                addBOM.btnSave.Enabled = false;
                addBOM.btnServicesList.Enabled = false;
                addBOM.txtTotalPrice.ReadOnly = true;
                addBOM.txtTotalTax.Visible = false;
                addBOM.label5.Visible = false;

                bom.CloseConn();

                bom.OpenConn();
                addBOM.dgvBOMlist.DataSource = bom.ShowDataDGV("select * from bom_child_view where [BOM Code] = '"+xCode+"'");
            addBOM.dgvBOMlist.Columns["BOM Code"].Visible = false;
                bom.CloseConn();

            addBOM.ShowDialog();

            

        }
    }
}
