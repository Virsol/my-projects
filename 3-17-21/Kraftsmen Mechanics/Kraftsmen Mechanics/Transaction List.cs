﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Transaction_List : Form
    {

        DBConnection conn = new DBConnection();
        SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        Leobytes leobytes_Form;
        private string JEDocNum, JODocNum, sidocnum, IPDocNum;

        private int cusID;
        private int carID;
        private string nDate;
        private int estimatedBy;
        private string docNum;
        private string jedocnum;
        private int technician;
        private string JESearch,JOSearch, IPSearch, SISearch;
        public Transaction_List()
        {
            InitializeComponent();
            
        }
        public Transaction_List(Leobytes leobytes)
        {
            InitializeComponent();
            leobytes_Form = leobytes;
        }
        public int CusID
        {
            get { return cusID; }
            set { cusID = value; }
        }
        public int CarID
        {
            get { return carID; }
            set { carID = value; }
        }
        public string Date
        {
            get { return nDate; }
            set { nDate = value; }
        }
        public int empID
        {
            get { return estimatedBy; }
            set { estimatedBy = value; }
        }
        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        public int techID
        {
            get { return technician; }
            set { technician = value; }
        }
        public string SIDocNum
        {
            get { return sidocnum; }
            set { sidocnum = value; }
        }
        private void dgvRefresh()
        {
            conn.OpenConnection();
            dgvIncomingPaymentList.DataSource = conn.ShowDataInGridView("select * from incoming_payment_list where [Sales Invoce Docnum] like '%"+IPSearch+ "%' or [Customer Name] like '%" + IPSearch + "%'");
            dgvJobEstimateList.DataSource = conn.ShowDataInGridView("select * from Transact_Job_estimate  where [Job Estimate Number] like '%" + JESearch + "%' or [Customer Name] like '%" + JESearch + "%'");
            dgvJobOrderList.DataSource = conn.ShowDataInGridView("select * from Transact_job_order where [Job Number] like '%" + JOSearch + "%' or [Customer] like '%" + JOSearch + "%'");
            dgvServiceInvoiceList.DataSource = conn.ShowDataInGridView("select * from sales_invoice_view where [Docnum] like '%" + SISearch + "%' or [Customer Name] like '%" + SISearch + "%'");
            conn.CloseConnection();
        }
        private void OpenForm(Form form)
        {
            form.AutoScroll = true;
            form.TopLevel = false;
            form.WindowState = FormWindowState.Normal;
            form.Dock = DockStyle.Fill;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            leobytes_Form.PanelContainer.Controls.Add(form);
            leobytes_Form.PanelContainer.Dock = DockStyle.Fill;
            
            form.BringToFront();
            form.Show();
            
        }
        private void SI_Click(object sender, EventArgs e)
        {

        }

        private void JE_Click(object sender, EventArgs e)
        {
            dgvRefresh();
        }

        private void txtSearchJOList_TextChanged(object sender, EventArgs e)
        {
            JOSearch = txtSearchJOList.Text;
            dgvRefresh();
        }

        private void txtSearchServiceInvoiceList_TextChanged(object sender, EventArgs e)
        {
            SISearch = txtSearchServiceInvoiceList.Text;
            dgvRefresh();
        }

        private void txtSearchIncomingPayments_TextChanged(object sender, EventArgs e)
        {
            IPSearch = txtSearchIncomingPayments.Text;
            dgvRefresh();
        }

        private void dgvJobEstimateList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvJobEstimateList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Job_Estimate job_Estimate = new Job_Estimate(this);
            string sJEDocNum = this.dgvJobEstimateList.CurrentRow.Cells[0].Value.ToString();
            JEDocNum = sJEDocNum.Substring(sJEDocNum.Length - 7);
            

            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from job_estimate where job_estimate_number = (Select Right([Job Estimate Number],7) from Transact_Job_estimate where [Job Estimate Number] = '" + sJEDocNum + "');");
            if (data.Read())
            {
                docNum = data["job_estimate_number"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                carID = Int32.Parse(data["car_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["estimated_by"].ToString());
                conn.CloseConnection();
                //MessageBox.Show(CusID.ToString());

                OpenForm(job_Estimate);
                job_Estimate.lblDocumentNumber.Text = JEDocNum;
            }
        }

        private void dgvJobOrderList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Job_Order job_Order = new Job_Order(this);
            string sJODocNum = this.dgvJobOrderList.CurrentRow.Cells[0].Value.ToString();
            JODocNum = sJODocNum.Substring(sJODocNum.Length - 7);
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from job_order where job_number = (Select Right([Job Number],7) from Transact_Job_order where [job Number] = '" + JODocNum + "')");

            if (data.Read())
            {
                docNum = data["job_number"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                carID = Int32.Parse(data["car_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["approved_by"].ToString());
                technician = Int32.Parse(data["technician"].ToString());
                conn.CloseConnection();
                OpenForm(job_Order);
                job_Order.lblDocumentNumber.Text = JODocNum;
            }
        }

        private void dgvServiceInvoiceList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Service_Invoice service_Invoice = new Service_Invoice(this);
            sidocnum = this.dgvServiceInvoiceList.CurrentRow.Cells[0].Value.ToString();
            docNum = this.dgvServiceInvoiceList.CurrentRow.Cells[2].Value.ToString();
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + sidocnum + "')");
            if (data.Read())
            {
                docNum = data["job_number"].ToString();
                sidocnum = data["sales_invoice_doc_num"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["employee_id"].ToString());
                conn.CloseConnection();
                service_Invoice.lblDocumentNumber.Text = docNum;
                service_Invoice.lblInvoiceNumber.Text = sidocnum;
                OpenForm(service_Invoice);
            }
        }

        private void dgvJobOrderList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Job_Order job_Order = new Job_Order(this);
            string sJODocNum = this.dgvJobOrderList.CurrentRow.Cells[0].Value.ToString();
            JODocNum = sJODocNum.Substring(sJODocNum.Length - 7);
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from job_order where job_number = (Select Right([Job Number],7) from Transact_Job_order where [job Number] = '" + JODocNum + "')");

            if (data.Read())
            {
                docNum = data["job_number"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                carID = Int32.Parse(data["car_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["approved_by"].ToString());
                technician = Int32.Parse(data["technician"].ToString());
                conn.CloseConnection();
                OpenForm(job_Order);
                job_Order.lblDocumentNumber.Text = JODocNum;
            }
        }

        private void dgvServiceInvoiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Service_Invoice service_Invoice = new Service_Invoice(this);
            sidocnum = this.dgvServiceInvoiceList.CurrentRow.Cells[0].Value.ToString();
            docNum = this.dgvServiceInvoiceList.CurrentRow.Cells[2].Value.ToString();
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + sidocnum + "')");
            if (data.Read())
            {
                docNum = data["job_number"].ToString();
                sidocnum = data["sales_invoice_doc_num"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["employee_id"].ToString());
                conn.CloseConnection();
                service_Invoice.lblDocumentNumber.Text = docNum;
                service_Invoice.lblInvoiceNumber.Text = sidocnum;
                OpenForm(service_Invoice);
            }
        }

        private void dgvIncomingPaymentList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Incoming_Payment incoming_Payment = new Incoming_Payment(this);
            string ssidocnum = this.dgvIncomingPaymentList.CurrentRow.Cells[1].Value.ToString();
            sidocnum = ssidocnum.Substring(ssidocnum.Length - 7);
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + sidocnum + "')");
            if (data.Read())
            {
                cusID = Int32.Parse(data["customer_id"].ToString());
                incoming_Payment.lblInvoiceNumber.Text = data["sales_invoice_doc_num"].ToString();
                conn.CloseConnection();

                OpenForm(incoming_Payment);
            }
        }

        private void JO_Click(object sender, EventArgs e)
        {
            dgvRefresh();
        }

        private void Transaction_List_Load(object sender, EventArgs e)
        {
            dgvRefresh();
        }

        private void txtSearchJEList_TextChanged(object sender, EventArgs e)
        {
            JESearch = txtSearchJEList.Text;
            dgvRefresh();
        }
    }

}
