﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Job_Order_List : Form
    {
        DBConnection conn = new DBConnection();
        private int cusID;
        private int carID;
        private string nDate;
        private int estimatedBy;
        private int technician;
        private string docNum;
        private string jedocnum;
        private string jodocnum;

        public Job_Order_List()
        {
            InitializeComponent();
        }
        public int CusID
        {
            get { return cusID; }
            set { cusID = value; }
        }
        public int CarID
        {
            get { return carID; }
            set { carID = value; }
        }
        public string Date
        {
            get { return nDate; }
            set { nDate = value; }
        }
        public int empID
        {
            get { return estimatedBy; }
            set { estimatedBy = value; }
        }
        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        public int techID
        {
            get { return technician; }
            set { technician = value; }
        }
        private void Job_Order_List_Load(object sender, EventArgs e)
        {
            if (label1.Text == "Report")
            {
                conn.OpenConnection();
                dgvJobOrderList.DataSource = conn.ShowDataInGridView("Select * from Job_Order_report");
                conn.CloseConnection();

            }
            else
            {
                refreshDgvJobOrderList();
            }
        }
        private void refreshDgvJobOrderList()
        {
            conn.OpenConnection();
            dgvJobOrderList.DataSource = conn.ShowDataInGridView("select * from job_order_list_view");
            conn.CloseConnection();
        }

        private void dgvJobOrderList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (label1.Text == "Report")
            {
                PO_Report.doc_from = this.dgvJobOrderList.CurrentRow.Cells[0].Value.ToString();
            }
            else
            {
                jedocnum = dgvJobOrderList.CurrentRow.Cells[0].Value.ToString();
                conn.OpenConnection();
                SqlDataReader data = conn.DataReader("Select * from job_order where job_number = (Select Right([Job Number],7) from Job_order_list_view where [job Number] = '" + jedocnum + "')");
                data.Read();
                docNum = data["job_number"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                carID = Int32.Parse(data["car_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["approved_by"].ToString());
                technician = Int32.Parse(data["technician"].ToString());
                conn.CloseConnection();
            }
            this.Close();
        }

 
            

        private void btnCancel_Click(object sender, EventArgs e)
        {
                this.Close();
            
        }
    }
}
