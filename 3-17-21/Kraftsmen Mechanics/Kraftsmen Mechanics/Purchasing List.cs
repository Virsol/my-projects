﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Kraftsmen_Mechanics
{
    public partial class Purchasing_List : Form
    {

        private string supCode, supName, supAddress, supPerson, supContact, createdBy, dateCreated, datePosted, dateUpdated, docNum, status,employee;
        private int inst;
        private double tax, price, total_price;

        Leobytes LeoBytes;
        Connection purchList = new Connection();
        SqlConnection conn = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");

        public double Tax
        {
            get { return tax; }
            set { tax = value; }
        }

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public double TotalPrice
        {
            get { return total_price; }
            set { total_price = value; }
        }

        public int Inst
        {
            get { return inst; }
            set { inst = value; }
        }
        public string Employee
        {
            get { return employee; }
            set { employee = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string DatePosted
        {
            get { return datePosted; }
            set { datePosted = value; }
        }

        public string DateUpdated
        {
            get { return dateUpdated; }
            set { dateUpdated = value; }
        }
        public string SupPerson
        {
            get { return supPerson; }
            set { supPerson = value; }
        }

        public string SupContact
        {
            get { return supContact; }
            set { supContact = value; }
        }
        public string SupAddress
        {
            get { return supAddress; }
            set { supAddress = value; }
        }
        public string SupCode
        {
            get { return supCode; }
            set { supCode = value; }
        }
        public string SupName
        {
            get { return supName; }
            set { supName = value; }
        }

        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        public Purchasing_List(Leobytes Leo)
        {
            InitializeComponent();
            LeoBytes = Leo;
        }

        #region hide
        private void dgvOutgoingPaymentList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboSearchByOutgoingPaymentStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion
        private void dtpPOFrom_ValueChanged(object sender, EventArgs e)
        {
            string xDateFrom = dtpPOFrom.Value.ToString();
            string xDateTo = dtpPOTo.Value.ToString();
            purchList.OpenConn();
            dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * from PO_List_view where Status = '"+cboSearchByPurchasingStatus.Text+"' AND [Date Created] BETWEEN '"+ xDateFrom + "' AND '"+xDateTo+"'");
            purchList.CloseConn();
        }

        private void dtpPOTo_ValueChanged(object sender, EventArgs e)
        {
            string xDateFrom = dtpPOFrom.Value.ToString();
            string xDateTo = dtpPOTo.Value.ToString();
            purchList.OpenConn();
            dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * from PO_List_view where Status = '" + cboSearchByPurchasingStatus.Text + "' AND [Date Created] BETWEEN '" + xDateFrom + "' AND '" + xDateTo + "'");
            purchList.CloseConn();
        }

        private void dtpGRPOFrom_ValueChanged(object sender, EventArgs e)
        {
            string xDateFrom = dtpGRPOFrom.Value.ToString();
            string xDateTo = dtpGRPOTo.Value.ToString();
            purchList.OpenConn();
            dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * from GRPO_LIST_VIEW where Status = '"+cboSearchByGRPOStatus.Text+"' AND [Date Created] BETWEEN '"+xDateFrom+"' AND '"+xDateTo+"'");
            purchList.CloseConn();
        }

        private void dtpGRPOTo_ValueChanged(object sender, EventArgs e)
        {
            string xDateFrom = dtpGRPOFrom.Value.ToString();
            string xDateTo = dtpGRPOTo.Value.ToString();
            purchList.OpenConn();
            dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * from GRPO_LIST_VIEW where Status = '" + cboSearchByGRPOStatus.Text + "' AND [Date Created] BETWEEN '" + xDateFrom + "' AND '" + xDateTo + "'");
            purchList.CloseConn();
        }

        private void txtSearchOutgoingPayment_TextChanged(object sender, EventArgs e)
        {
            purchList.OpenConn();
            dgvOutgoingPaymentList.DataSource = purchList.ShowDataDGV("select ap_inv_doc_num as 'A/P Document Number', supplier.supplier_code as 'Supplier Code', supplier.[name] as 'Supplier Name', [date] as 'Date', amount as 'Amount' from outgoing_payment left join supplier on outgoing_payment.supplier_code = supplier.supplier_code where ap_inv_doc_num like '%"+txtSearchOutgoingPayment.Text+"%' or supplier.supplier_code like '%"+txtSearchOutgoingPayment.Text+"%' or supplier.[name] like '%"+txtSearchOutgoingPayment.Text+"%'");
            purchList.CloseConn();
        }
        private void cboSearchByAPInvoiceStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSearchByAPInvoiceStatus.Text == "OPEN")
            {
                purchList.OpenConn();
                dgvAPInvoiceList.DataSource = purchList.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view where Status = 'OPEN'");
                purchList.CloseConn();
            }
            else if (cboSearchByAPInvoiceStatus.Text == "CLOSED")
            {
                purchList.OpenConn();
                dgvAPInvoiceList.DataSource = purchList.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view where Status = 'CLOSED'");
                purchList.CloseConn();
            }

        }

        private void dgvAPInvoiceList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DocNum = dgvAPInvoiceList.CurrentRow.Cells[7].Value.ToString();

            AP_Invoice apInv = new AP_Invoice();
            openChildForm(apInv);

            purchList.OpenConn();
            SqlDataReader drInv = purchList.DataReader("select supplier.supplier_code 'Code' , supplier.[name] 'Name', supplier.contact_number 'Contact Number', supplier.[address] 'Address', contact_person.[name] 'Contact Person', [status].status_desc 'Status', date_posted 'Date Posted', date_created 'Date Created', date_upload 'Date Updated', employee.[name] 'Employee', ap_invoice_doc_num 'Document Number', Installment 'Installment' from ap_invoice left join supplier on ap_invoice.supplier_code = supplier.supplier_code left join contact_person on ap_invoice.contact_person_id = contact_person.contact_person_id left join [status] on ap_invoice.status_id = [status].status_id left join employee on ap_invoice.employee_id = employee.employee_id where ap_invoice_doc_num = '"+DocNum+"'");
            drInv.Read();

            DocNum = drInv["Document Number"].ToString();
            SupCode = drInv["Code"].ToString();
            SupName = drInv["Name"].ToString();
            SupContact = drInv["Contact Number"].ToString();
            SupAddress = drInv["Address"].ToString();
            SupPerson = drInv["Contact Person"].ToString();
            Status = drInv["Status"].ToString();
            DatePosted = drInv["Date Posted"].ToString();
            DateCreated = drInv["Date Created"].ToString();
            DateUpdated = drInv["Date Updated"].ToString();
            Employee = drInv["Employee"].ToString();
            Inst = Int32.Parse(drInv["Installment"].ToString());
            purchList.CloseConn();

            apInv.txtDocumentNumber.Text = DocNum;
            apInv.txtSupplierCode.Text = SupCode;
            apInv.txtSupplierName.Text = SupName;
            apInv.txtSupplierContact.Text = SupContact;
            apInv.txtSupplierAddress.Text = SupAddress;
            apInv.cboContactPerson.Text = SupPerson;

            apInv.cboStatus.Text = Status;
            apInv.dtpDateCreated.Text = DateCreated;
            apInv.dtpDatePosted.Text = DatePosted;
            apInv.dtpDateUploaded.Text = DateUpdated;

            apInv.txtCreatedBy.Text = Employee;
            apInv.lblHidden.Text = "1";

            apInv.groupBox3.Enabled = true;
            apInv.txtSearch.Enabled = false;
            apInv.dgvItemList.ReadOnly = true;
            apInv.btnSave.Enabled = false;
            apInv.dtpDateCreated.Enabled = false;
            apInv.dtpDatePosted.Enabled = false;
            apInv.dtpDateUploaded.Enabled = false;
            apInv.btnViewDocument.Enabled = false;

            if (Inst == 1)
            {
                apInv.chbInstallment.Checked = true;
            }
            else 
            {
                apInv.chbInstallment.Checked = false;
            }

            if(Status == "CLOSED")
            {
                apInv.btnPrint.Visible = true;
                apInv.btnPrint.Enabled = true;
            }
            else
            {
                apInv.btnPrint.Visible = false;
            }

            purchList.OpenConn();
            SqlDataReader drTotal = purchList.DataReader("select format(sum(tax_price), '#,#.00') as 'TAX', format(sum(total_price),'#,#.00') as 'Price',format(sum(tax_price + total_price),'#,#.00') as 'Total Price' from ap_inv_body where ap_inv_doc_num = '"+DocNum+"'");
            drTotal.Read();

            double disc = 0;

            Tax = double.Parse(drTotal["TAX"].ToString());
            Price = double.Parse(drTotal["Price"].ToString());
            TotalPrice = double.Parse(drTotal["Total Price"].ToString());

            apInv.lblTotalBeforeDiscount.Text = Price.ToString("n2");
            apInv.lblTaxValue.Text = Tax.ToString("n2");
            apInv.lblTotalPaymentDue.Text = TotalPrice.ToString("n2");
            apInv.lblDiscountValue.Text = disc.ToString("n2");

            purchList.CloseConn();

            purchList.OpenConn();
            apInv.dgvItemList.DataSource = purchList.ShowDataDGV("select ap_inv_body.inventory_code as 'Item Code', inventory.inventory_name as 'Item Name', inventory.inventory_description as 'Item Description',ap_inv_body.quantity as 'Quantity', uom.uom_desc as 'UOM', price as 'Price', tax.tax_desc as 'Tax', tax_price as 'Tax Price', discount as 'Discount', total_price as 'Total Price', ap_inv_doc_num as 'Document Number' from ap_inv_body left join inventory on ap_inv_body.inventory_code = inventory.inventory_code left join uom on ap_inv_body.uom_id = uom.uom_id left join tax on ap_inv_body.tax_id = tax.tax_id where ap_inv_doc_num = '" + DocNum+"'");
            apInv.dgvItemList.Columns["Document Number"].Visible = false;
            purchList.CloseConn();

        }

        private void dgvGRPOList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string docNum = dgvGRPOList.CurrentRow.Cells[0].Value.ToString();

            Goods_Receipt_Purchase_Order grpo = new Goods_Receipt_Purchase_Order();
            openChildForm(grpo);

            purchList.OpenConn();
            SqlDataReader drGRPO = purchList.DataReader("Select a.grpo_doc_num 'Document Number', a.supplier_code 'Supplier Code', b.name 'Supplier Name', b.address 'Address', b.contact_number 'Contact Number', c.name 'Contact Person', a.date_created 'Date Created', a.date_posted 'Date Posted', a.date_upload 'Date Updated', d.employee_id 'Created by', e.status_desc 'Status' from grpo a left join supplier b on a.supplier_code = b.supplier_code left join contact_person c on a.contact_person_id = c.contact_person_id left join employee d on a.employee_id = d.employee_id left join [status] e on a.status_id = e.status_id where grpo_doc_num = (Select RIGHT([GRPO Document],7) from GRPO_LIST_VIEW where [GRPO Document] = '" + docNum + "')");
            drGRPO.Read();

            DocNum = drGRPO["Document Number"].ToString();
            SupCode = drGRPO["Supplier Code"].ToString();
            SupName = drGRPO["Supplier Name"].ToString();
            SupPerson = drGRPO["Contact Person"].ToString();
            SupContact = drGRPO["Contact Number"].ToString();
            SupAddress = drGRPO["Address"].ToString();
            Status = drGRPO["Status"].ToString();

            DateCreated = drGRPO["Date Created"].ToString();
            DatePosted = drGRPO["Date Posted"].ToString();
            DateUpdated = drGRPO["Date Updated"].ToString();

            CreatedBy = drGRPO["Created By"].ToString();

            grpo.txtDocumentNumber.Text = DocNum;
            grpo.txtSupplierCode.Text = SupCode;
            grpo.txtSupplierName.Text = supName;
            grpo.cboContactPerson.Text = SupPerson;
            grpo.txtSupplierContact.Text = SupContact;
            grpo.txtSupplierAddress.Text = supAddress;
            grpo.cboStatus.Text = Status;

            grpo.dtpDateCreated.Text = DateCreated;
            grpo.dtpDatePosted.Text = DatePosted;
            grpo.dtpDateUploaded.Text = DateUpdated;
            grpo.lblHidden.Text = "1";

            grpo.txtCreatedBy.Text = CreatedBy;

            purchList.CloseConn();

            purchList.OpenConn();
            grpo.dgvOrderList.DataSource = purchList.ShowDataDGV("select grpo_body_id as 'ID', grpo_body.inventory_code as 'Code', inventory_name as 'Name', inventory_description as 'Description', grpo_body.quantity as 'Quantity', uom_desc as 'UOM', price as 'Price', tax_desc as 'Tax', tax_price as 'Tax Price', discount as 'Discount', total_price as 'Total', grpo_doc_num as 'Document Nummber' from grpo_body left join inventory on grpo_body.inventory_code = inventory.inventory_code left join uom on grpo_body.uom_id = uom.uom_id left join tax on grpo_body.tax_id = tax.tax_id where grpo_doc_num = '"+DocNum+"'");
            purchList.CloseConn();

            grpo.groupBox1.Enabled = false;
            grpo.groupBox2.Enabled = false;
            grpo.txtSearch.Enabled = false;
            grpo.dgvItemList.Enabled = false;
            grpo.dgvOrderList.Enabled = false;
            grpo.btnSave.Enabled = false;
            grpo.dgvOrderList.Columns["Void"].Visible = false;

            //-----------------------------------------//
            purchList.OpenConn();

            SqlDataReader drTotal = purchList.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from grpo_body where grpo_doc_num = '" + DocNum + "'");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            grpo.lblTotalBeforeDiscount.Text = total_price;
            grpo.lblTaxValue.Text = total_tax;
            grpo.lblTotalPaymentDue.Text = total_amount;
            purchList.CloseConn();
            //--------------------------------------------------------------------//
        }

        private void cboSearchByGRPOStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSearchByGRPOStatus.Text == "OPEN")
            {
                purchList.OpenConn();
                dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * from GRPO_LIST_VIEW where Status = 'OPEN'");
                purchList.CloseConn();
            }
            else if (cboSearchByGRPOStatus.Text == "CLOSED")
            {
                purchList.OpenConn();
                dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * from GRPO_LIST_VIEW where Status = 'CLOSED'");
                purchList.CloseConn();
            }
        }

       
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openChildForm(Form childForm)
        {
            childForm.AutoScroll = true;
            childForm.TopLevel = false;
            childForm.WindowState = FormWindowState.Normal;
            childForm.Dock = DockStyle.Fill;
            childForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            LeoBytes.PanelContainer.Controls.Add(childForm);
            LeoBytes.PanelContainer.Dock = DockStyle.Fill;

            childForm.BringToFront();
            childForm.Show();
        }

        private void Purchasing_List_Load(object sender, EventArgs e)
        {
            string xDateFrom = dtpPOFrom.Value.ToString();
            string xDateTo = dtpPOTo.Value.ToString();

            cboSearchByPurchasingStatus.Text = "OPEN";
            cboSearchByGRPOStatus.Text = "OPEN";
            cboSearchByAPInvoiceStatus.Text = "OPEN";


            this.dgvAPInvoiceList.Columns["Docnum"].Visible = false;

            purchList.OpenConn();
            dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * from PO_List_view where Status = 'OPEN'");
            purchList.CloseConn();

            purchList.OpenConn();
            dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * from GRPO_LIST_VIEW where Status = 'OPEN'");
            purchList.CloseConn();

            purchList.OpenConn();
            dgvAPInvoiceList.DataSource = purchList.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view where Status = 'OPEN'");
            purchList.CloseConn();

            purchList.OpenConn();
            dgvOutgoingPaymentList.DataSource = purchList.ShowDataDGV("select ap_inv_doc_num as 'A/P Document Number', supplier.supplier_code as 'Supplier Code', supplier.[name] as 'Supplier Name', [date] as 'Date', Format(amount,'#,#.00') as 'Amount', Format(remaining_balance,'#,#.00') as 'Remainning Balance' from outgoing_payment left join supplier on outgoing_payment.supplier_code = supplier.supplier_code");
            purchList.CloseConn();

        }

        private void cboSearchByPurchasingStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboSearchByPurchasingStatus.Text == "OPEN")
            {
                purchList.OpenConn();
                dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * from PO_List_view where Status = 'OPEN'");
                purchList.CloseConn();
            }
            else if (cboSearchByPurchasingStatus.Text == "CLOSED")
            {
                purchList.OpenConn();
                dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * from PO_List_view where Status = 'CLOSED'");
                purchList.CloseConn();
            }
        }
        private void txtSearchAPInvoiceList_TextChanged(object sender, EventArgs e)
        {
            purchList.OpenConn();
            dgvAPInvoiceList.DataSource = purchList.ShowDataDGV("select *,Right([Document Number],7) 'Docnum' from ap_inv_list_view where ([Document Number] like '%"+txtSearchAPInvoiceList.Text+"%' or [Supplier] like '%"+txtSearchAPInvoiceList.Text+"%') AND [Status] like '"+cboSearchByAPInvoiceStatus.Text+"'");
            purchList.CloseConn();
        }
        private void txtSearchPurchasingList_TextChanged(object sender, EventArgs e)
        {
            purchList.OpenConn();
            dgvPurchasingOrderList.DataSource = purchList.ShowDataDGV("Select * From PO_List_view where ([Document Number]  like '%" + txtSearchPurchasingList.Text + "%' or [Supplier] like '%" + txtSearchPurchasingList.Text + "%') AND [Status] LIKE '" + cboSearchByPurchasingStatus.Text + "'");
            purchList.CloseConn();
        }

        private void txtSearchGRPOList_TextChanged(object sender, EventArgs e)
        {
            purchList.OpenConn();
            dgvGRPOList.DataSource = purchList.ShowDataDGV("Select * From GRPO_List_view where ([GRPO Document]  like '%" + txtSearchGRPOList.Text + "%' or [Supplier] like '%" + txtSearchGRPOList.Text + "%') AND [Status] LIKE '" + cboSearchByGRPOStatus.Text + "'");
            purchList.CloseConn();
        }
        private void dgvPurchasingOrderList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Purchasing_Order purchOrd = new Purchasing_Order();
            openChildForm(purchOrd);

            string docNum = this.dgvPurchasingOrderList.CurrentRow.Cells[0].Value.ToString();
            /*xStatus = this.dgvPurchasingOrderList.CurrentRow.Cells[7].Value.ToString();*/

            purchList.OpenConn();
            SqlDataReader drPO = purchList.DataReader("Select a.po_document_number 'Document Number', a.supplier_code 'Supplier Code', b.name 'Supplier Name', b.address 'Address', b.contact_number 'Contact Number', c.name 'Contact Person', a.date_created 'Date Created', a.date_posted 'Date Posted', a.date_upload 'Date Updated', d.employee_id 'Created by', e.status_desc 'Status' from po a left join supplier b on a.supplier_code = b.supplier_code left join contact_person c on a.contact_person_id = c.contact_person_id left join employee d on a.employee_id = d.employee_id left join [status] e on a.status_id = e.status_id where po_document_number = (Select RIGHT([Document Number],7) from PO_List_view where [Document Number] = '" + docNum + "')");
            drPO.Read();

            DocNum = drPO["Document Number"].ToString();
            SupCode = drPO["Supplier Code"].ToString();
            SupName = drPO["Supplier Name"].ToString();
            SupPerson = drPO["Contact Person"].ToString();
            SupContact = drPO["Contact Number"].ToString();
            SupAddress = drPO["Address"].ToString();
            Status = drPO["Status"].ToString();

            DateCreated = drPO["Date Created"].ToString();
            DatePosted = drPO["Date Posted"].ToString();
            DateUpdated = drPO["Date Updated"].ToString();

            CreatedBy = drPO["Created By"].ToString();

            purchOrd.txtDocumentNumber.Text = DocNum;
            purchOrd.txtSupplierCode.Text = SupCode;
            purchOrd.txtSupplierName.Text = supName;
            purchOrd.cboContactPerson.Text = SupPerson;
            purchOrd.txtSupplierContact.Text = SupContact;
            purchOrd.txtSupplierAddress.Text = supAddress;
            purchOrd.cboStatus.Text = Status;

            purchOrd.dtpDateCreated.Text = DateCreated;
            purchOrd.dtpDatePosted.Text = DatePosted;
            purchOrd.dtpDateUploaded.Text = DateUpdated;

            purchOrd.txtCreatedBy.Text = CreatedBy;

            purchOrd.lblHidden.Text = "1";
            purchList.CloseConn();

            purchList.OpenConn();
            purchOrd.dgvOrderList.DataSource = purchList.ShowDataDGV("select po_body_id as 'ID', po_body.inventory_code as 'Code', inventory_name as 'Name', inventory_description as 'Description', po_body.quantity as 'Quantity', uom_desc as 'UOM', price as 'Price', tax_desc as 'Tax', tax_price as 'Tax Price', discount as 'Discount', total_price as 'Total', po_document_number as 'Document Nummber' from po_body left join inventory on po_body.inventory_code = inventory.inventory_code left join uom on po_body.uom_id = uom.uom_id left join tax on po_body.tax_id = tax.tax_id where po_document_number = '"+ DocNum + "'");
            purchList.CloseConn();

            purchOrd.groupBox1.Enabled = false;
            purchOrd.groupBox2.Enabled = false;
            purchOrd.txtSearch.Enabled = false;
            purchOrd.dgvItemList.Enabled = false;
            purchOrd.dgvOrderList.Enabled = false;
            purchOrd.btnSave.Enabled = false;
            purchOrd.dgvOrderList.Columns["Void"].Visible = false;

            //-----------------------------------------//
            purchList.OpenConn();

            SqlDataReader drTotal = purchList.DataReader("Select format(sum([total_price]), '#,#.00') as Total_Price, format(sum([tax_price]),'#,#.00') as Total_Tax, format(SUM(total_price + tax_price),'#,#.00') as Total_Amount from po_body where po_document_number = '" + purchOrd.txtDocumentNumber.Text + "'");
            drTotal.Read();

            string total_price = drTotal["Total_Price"].ToString();
            string total_tax = drTotal["Total_Tax"].ToString();
            string total_amount = drTotal["Total_Amount"].ToString();
            //double total_discount = double.Parse(drTotal["Total_Discount"].ToString());

            purchOrd.lblTotalBeforeDiscount.Text = total_price;
            purchOrd.lblTaxValue.Text = total_tax;
            purchOrd.lblTotalPaymentDue.Text = total_amount;
            purchList.CloseConn();

            //--------------------------------------------------------------------//
        }
    }
}
