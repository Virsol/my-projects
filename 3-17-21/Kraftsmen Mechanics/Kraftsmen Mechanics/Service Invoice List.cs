﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Kraftsmen_Mechanics
{
    public partial class Service_Invoice_List : Form
    {
        DBConnection conn = new DBConnection();
        private int cusID;
        private int carID;
        private string nDate;
        private int estimatedBy;
        private int technician;
        private string docNum;
        private string jedocnum;
        private string sidocnum;
        private string status;

        public Service_Invoice_List()
        {
            InitializeComponent();
        }
        public int CusID
        {
            get { return cusID; }
            set { cusID = value; }
        }
        public int CarID
        {
            get { return carID; }
            set { carID = value; }
        }
        public string Date
        {
            get { return nDate; }
            set { nDate = value; }
        }
        public int empID
        {
            get { return estimatedBy; }
            set { estimatedBy = value; }
        }
        public string DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }
        public int techID
        {
            get { return technician; }
            set { technician = value; }
        }
        public string SIDocNum
        {
            get { return sidocnum; }
            set { sidocnum = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        private void refreshDgvSalesInvoiceList()
        {
            conn.OpenConnection();
            dgvServiceList.DataSource = conn.ShowDataInGridView("select * from sales_invoice_view");
            conn.CloseConnection();
        }
        private void Service_Invoice_List_Load(object sender, EventArgs e)
        {
            refreshDgvSalesInvoiceList();
        }

        private void dgvServiceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
                jedocnum = dgvServiceList.CurrentRow.Cells[0].Value.ToString();
                status = dgvServiceList.CurrentRow.Cells[6].Value.ToString();
                conn.OpenConnection();
                SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + jedocnum + "')");
                data.Read();
                docNum = data["job_number"].ToString();
                sidocnum = data["sales_invoice_doc_num"].ToString();
                cusID = Int32.Parse(data["customer_id"].ToString());
                nDate = data["date"].ToString();
                estimatedBy = Int32.Parse(data["employee_id"].ToString());
                conn.CloseConnection();
                this.Close();
            
        }

        private void dgvServiceList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            jedocnum = dgvServiceList.CurrentRow.Cells[0].Value.ToString();
            status = dgvServiceList.CurrentRow.Cells[6].Value.ToString();
            conn.OpenConnection();
            SqlDataReader data = conn.DataReader("Select * from sales_invoice where sales_invoice_doc_num = (Select Right([Docnum],7) from sales_invoice_view where [Docnum] = '" + jedocnum + "')");
            data.Read();
            docNum = data["job_number"].ToString();
            sidocnum = data["sales_invoice_doc_num"].ToString();
            cusID = Int32.Parse(data["customer_id"].ToString());
            nDate = data["date"].ToString();
            estimatedBy = Int32.Parse(data["employee_id"].ToString());
            conn.CloseConnection();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Service_Invoice_List_Load_1(object sender, EventArgs e)
        {
            refreshDgvSalesInvoiceList();
        }

        private void Service_Invoice_List_Load_2(object sender, EventArgs e)
        {
            refreshDgvSalesInvoiceList();
        }
    }
}
