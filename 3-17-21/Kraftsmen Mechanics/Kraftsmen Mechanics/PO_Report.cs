﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;
using System.Drawing;
using System.Data.SqlClient;
namespace Kraftsmen_Mechanics

  
{
 
    public class PO_Report
    {
        
        public static string suphomies = "";
        public static string service = "";
       // public static string label_report = "";
        public static string Item_list = "";
        public static string custName_Report = "";
        public static string doc_from = "";
        public static string doc_to = "";
      Connection conn = new Connection();
       //SqlConnection con = new SqlConnection("Data Source=" + Properties.Settings.Default.server + ";Initial Catalog=" + Properties.Settings.Default.database + ";User ID=" + Properties.Settings.Default.username + ";Password=" + Properties.Settings.Default.password + "");
        public void show1()
        {
            Suppliers_List supply = new Suppliers_List();
            //supply.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            supply.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            supply.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            supply.Opacity = 70d;
            //supply.BackColor 
            supply.TopMost = true;
            supply.Location = supply.Location;
            supply.ShowInTaskbar = false;
            supply.ShowDialog();
        }

        public  void PO_click_1()
        {
            Purchasing_Order_Report rep = new Purchasing_Order_Report();
            Form newFormDialog = new Form();
            try
            {
                using (Suppliers_List suplist = new Suppliers_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = rep.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                       /* newFormDialog.Dispose();
                        string simps = suphomies;
                        rep.txtSupplierName.Text = simps;*/

                        // MessageBox.Show(simps);     
                       
                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        public  void generate_report()
        {
            Purchasing_Order_Report po_Rep = new Purchasing_Order_Report();
            conn.OpenConn();
                po_Rep.dgvPurchasingOrderList.DataSource = conn.ShowDataDGV("Select * from Inventory");
            conn.CloseConn();

        }
        public void Service_Report()
        {
            Sales_Report sales = new Sales_Report();
            Form newFormDialog = new Form();
            try
            {
                using (Services_List suplist = new Services_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = sales.Location;
                    newFormDialog.ShowInTaskbar = false;
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        /* newFormDialog.Dispose();
                         string simps = suphomies;
                         rep.txtSupplierName.Text = simps;*/

                        // MessageBox.Show(simps);      
                       
                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        public void Items_report()
        {
            Sales_Report sales = new Sales_Report();
            Form newFormDialog = new Form();
            try
            {
                using (Items_List suplist = new Items_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = sales.Location;
                    newFormDialog.ShowInTaskbar = false;
                    suplist.label1.Text = "Items Report";
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        /* newFormDialog.Dispose();
                         string simps = suphomies;
                         rep.txtSupplierName.Text = simps;*/

                        // MessageBox.Show(simps);      
                        // label_report = "Items Report";
                     

                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }

        }
        public void sales_transaction()
        {
            Sales_Report sales = new Sales_Report();
            Form newFormDialog = new Form();
            try
            {
                using (Customer_List suplist = new Customer_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = sales.Location;
                    newFormDialog.ShowInTaskbar = false;
                    //suplist.label1.Text = "Items Report";
                    suplist.lblreport.Text = "Sales Report";
                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        /* newFormDialog.Dispose();
                         string simps = suphomies;
                         rep.txtSupplierName.Text = simps;*/

                        // MessageBox.Show(simps);      
                        // label_report = "Items Report";


                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
        public void doc_from_list()
        {

            Sales_Report sales = new Sales_Report();
            Form newFormDialog = new Form();
            try
            {
                using (Job_Order_List suplist = new Job_Order_List())
                {
                    newFormDialog.StartPosition = FormStartPosition.Manual;
                    newFormDialog.FormBorderStyle = FormBorderStyle.None;
                    newFormDialog.WindowState = FormWindowState.Maximized;
                    newFormDialog.Opacity = .70d;
                    newFormDialog.BackColor = Color.Black;
                    newFormDialog.TopMost = true;
                    newFormDialog.Location = sales.Location;
                    newFormDialog.ShowInTaskbar = false;
                    //suplist.label1.Text = "Items Report";
                    suplist.label1.Text = "Report";

                    newFormDialog.Show();

                    suplist.Owner = newFormDialog;


                    if (suplist.ShowDialog() != DialogResult.OK)
                    {
                        /* newFormDialog.Dispose();
                         string simps = suphomies;
                         rep.txtSupplierName.Text = simps;*/

                        // MessageBox.Show(simps);      
                        // label_report = "Items Report";


                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                newFormDialog.Dispose();
            }
        }
    }
    
}
