USE [service]
GO

/****** Object:  Table [dbo].[sales_payment]    Script Date: 2/19/2021 3:05:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[sales_payment](
	[payment_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_id] [int] NULL,
	[sales_invoice_doc_num] [varchar](20) NULL,
	[date] [date] NULL,
	[amount] [decimal](11, 2) NULL,
	[amount_paid] [decimal](11, 2) NULL,
	[change] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

