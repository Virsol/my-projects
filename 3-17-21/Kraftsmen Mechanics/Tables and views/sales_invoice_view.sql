USE [service]
GO

/****** Object:  View [dbo].[Sales_invoice_view]    Script Date: 2/18/2021 4:55:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create View [dbo].[Sales_invoice_view] as
Select
sales_invoice_doc_num 'Docnum',
b.name 'Customer Name',
a.job_number 'Job Number',
a.date 'Invoice Date',
a.total_price 'Total Price',
d.name 'Created By',
c.status_desc 'Status'


from sales_invoice a 
left join customer  b on a.customer_id = b.customer_id
left join status c on a.status_id = c.status_id
left join employee d on a.employee_id = d.employee_id
GO

