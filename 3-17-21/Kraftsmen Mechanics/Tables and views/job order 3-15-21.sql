USE [service]
GO

/****** Object:  Table [dbo].[job_order]    Script Date: 3/15/2021 5:54:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[job_order](
	[job_order_id] [int] IDENTITY(1,1) NOT NULL,
	[job_number] [varchar](30) NULL,
	[job_estimate_number] [varchar](30) NULL,
	[customer_id] [int] NULL,
	[car_id] [int] NULL,
	[technician] [int] NULL,
	[checked_by] [int] NULL,
	[approved_by] [int] NULL,
	[status_id] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[date] [date] NULL,
	[with_service] [int] NULL,
 CONSTRAINT [PK_job_order] PRIMARY KEY CLUSTERED 
(
	[job_order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

