USE [service]
GO

/****** Object:  View [dbo].[Parent_View]    Script Date: 3/2/2021 3:03:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Parent_View] as

Select
a.bom_code 'ID',
a.inventory_code 'Service Code',
b.inventory_name 'Service Name',
a.item_quantity 'Quantity',
a.total_price 'Price',
a.total_tax 'Tax',
c.status_desc 'status'



From
bom a 
left join inventory b on a.inventory_code = b.inventory_code
left join status c on a.status = c.status_id
where a.[P/C] = 1
GO

