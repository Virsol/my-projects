USE [service]
GO

/****** Object:  View [dbo].[bom_child_view]    Script Date: 3/1/2021 5:58:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Create view [dbo].[bom_child_view] as
Select 
a.bom_code 'BOM Code',
a.inventory_code 'Item Code',
b.inventory_name 'Item Name',
a.item_quantity 'Quantity'



from bom a
left join inventory b on a.inventory_code = b.inventory_code
left join status c on a.status = c.status_id
where a.[P/C] = 0
GO

