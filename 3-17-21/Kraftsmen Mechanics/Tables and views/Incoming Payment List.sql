USE [service]
GO

/****** Object:  View [dbo].[Incoming_payment_list]    Script Date: 3/5/2021 4:52:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Incoming_payment_list] as

Select 
a.payment_id 'ID',
case when (Select [on/off] from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Sales Invoice') = 1
then
Concat((Select a.series_desc from document_series a left join Document_tag b on a.doc_tag_id = b.doc_tag_id where b.doc_tag_desc = 'Sales Invoice'),'-',a.sales_invoice_doc_num)
else
a.sales_invoice_doc_num
end as'Sales Invoce Docnum',
b.name 'Customer Name',
a.date 'Date',
a.amount 'Total Amount',
a.amount_paid 'Paid Amount',
a.change 'Change',
d.status_desc 'Status'


from sales_payment a left join customer b on a.customer_id = b.customer_id
left join sales_invoice c on a.sales_invoice_doc_num = c.sales_invoice_doc_num
left join status d on c.status_id = d.status_id
GO

