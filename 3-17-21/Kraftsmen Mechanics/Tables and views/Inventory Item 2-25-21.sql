USE [service]
GO

/****** Object:  View [dbo].[Inventory_Item]    Script Date: 2/25/2021 11:52:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Inventory_Item] as

SELECT a.inventory_code AS [Item Code], a.inventory_name AS [Item Name], a.inventory_description AS [Item Description], a.quantity, a.item_price AS [Item Price], c.status_desc AS Status, b.uom_desc AS UOM, 
                  CASE WHEN quantity < 50 THEN 'Critical' ELSE 'Stable' END AS [Critical Level],a.inventory_status 'Status ID'
FROM     dbo.inventory AS a LEFT OUTER JOIN
                  dbo.uom AS b ON a.uom_id = b.uom_id LEFT OUTER JOIN
                  dbo.status AS c ON a.inventory_status = c.status_id
WHERE  (a.inventory_type = 1)
GO

