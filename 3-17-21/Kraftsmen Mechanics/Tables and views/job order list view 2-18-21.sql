USE [service]
GO

/****** Object:  View [dbo].[Job_order_list_view]    Script Date: 2/18/2021 5:35:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[Job_order_list_view] as


SELECT a.job_number AS [Job Number], b.name AS Customer, a.date, c.name AS [Approved By], d.name AS Technician, a.total_price AS [Total Price], e.status_desc
FROM     dbo.job_order AS a LEFT OUTER JOIN
                  dbo.customer AS b ON a.customer_id = b.customer_id LEFT OUTER JOIN
                  dbo.employee AS c ON a.approved_by = c.employee_id LEFT OUTER JOIN
                  dbo.employee AS d ON a.technician = d.employee_id LEFT OUTER JOIN
                  dbo.status AS e ON a.status_id = e.status_id
				  left join work_progress f on a.job_number = f.job_number

WHERE  (e.status_desc = 'OPEN') and f.status = 4
--a.job_number in(Select job_number from work_progress where [status] = 4)
GO

