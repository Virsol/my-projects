USE [service]
GO

/****** Object:  Table [dbo].[outgoing_payment]    Script Date: 2/19/2021 3:05:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[outgoing_payment](
	[payment_id] [int] IDENTITY(1,1) NOT NULL,
	[supplier_code] [varchar](20) NULL,
	[ap_inv_doc_num] [varchar](20) NULL,
	[date] [date] NULL,
	[amount] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

