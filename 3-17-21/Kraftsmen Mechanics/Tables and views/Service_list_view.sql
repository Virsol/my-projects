USE [service]
GO

/****** Object:  View [dbo].[Service_List_view]    Script Date: 2/25/2021 1:03:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Service_List_view] as

Select 
a.inventory_code 'Service Code',
a.inventory_name 'Service Name',
a.inventory_description 'Service Description',
a.item_price 'Price',
b.service_desc 'Service type',
c.status_desc 'Status'



from inventory  a

left join service_type b on a.service_type_id = b.service_type_id
left join status c on a.inventory_status = c.status_id
where inventory_type = 2
GO

