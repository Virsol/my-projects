USE [service]
GO

/****** Object:  View [dbo].[Items_per_car]    Script Date: 3/17/2021 1:22:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[Items_per_car] as
Select
c.vehicle_id 'ID',
a.inventory_code 'Code',
b.inventory_name 'Item Name',
b.quantity 'Quantity',
b.item_price 'Item Price',
e.uom_desc 'UOM'
from 
item_car_application a left join inventory b on a.inventory_code = b.inventory_code
left join vehicle c on a.vehicle_id = c.vehicle_id
left join auto_make d on d.auto_make_id = c.auto_make_id
left join uom e on b.uom_id = e.uom_id
--where c.vehicle_id = '2'
--Select * from auto_make
--Select * from Inventory_Table
GO

