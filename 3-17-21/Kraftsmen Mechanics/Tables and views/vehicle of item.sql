USE [service]
GO

/****** Object:  View [dbo].[Vehicles_of_item]    Script Date: 3/17/2021 1:58:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create View [dbo].[Vehicles_of_item] as
Select 
a.inventory_Code 'Item Code',
a.Vehicle_id 'Vehicle ID',
c.auto_make_desc 'Auto Make',
b.model 'Model',
d.size_desc 'Size'



from item_car_application a
left join vehicle b on a.vehicle_id = b.vehicle_id
left join auto_make c on c.auto_make_id = b.auto_make_id
left join size d on d.size_id = b.size
GO

