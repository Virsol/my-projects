USE [service]
GO

/****** Object:  View [dbo].[Item_list_draft]    Script Date: 2/16/2021 11:12:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[Item_list_draft] as

Select 
a.job_estimate_number 'Job Number',
a.inventory_code 'Item Code',
b.inventory_name 'Item Name'

from

draft_order a left join inventory b on a.inventory_code = b.inventory_code
left join inventory_type c on b.inventory_type = c.inventory_type_id
where c.inventory_desc = 'Item'
GO

