USE [service]
GO

/****** Object:  Table [dbo].[bom]    Script Date: 2/24/2021 4:17:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom](
	[service_code] [varchar](20) NULL,
	[item_code] [varchar](20) NULL,
	[item_quantity] [int] NULL,
	[price] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

