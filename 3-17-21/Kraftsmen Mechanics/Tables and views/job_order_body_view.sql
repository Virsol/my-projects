USE [service]
GO

/****** Object:  View [dbo].[Job_order_body_view]    Script Date: 2/17/2021 6:36:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[Job_order_body_view] as


Select 
a.job_number 'Job Number',
b.inventory_name 'Item/Service Name',
a.quantity 'Quantity',
c.tax_desc 'TAX',
a.tax_price 'Tax Price',
a.total_price 'Total Price'

from job_order_body a
left join inventory b on a.inventory_code = b.inventory_code
left join tax c on c.tax_id = a.tax_id 

GO

