USE [service]
GO

/****** Object:  Table [dbo].[job_order_body]    Script Date: 3/2/2021 5:17:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[job_order_body](
	[job_order_body_id] [int] IDENTITY(1,1) NOT NULL,
	[job_number] [varchar](30) NULL,
	[inventory_code] [varchar](30) NULL,
	[price] [decimal](11, 2) NULL,
	[tax_id] [int] NULL,
	[tax_price] [decimal](11, 2) NULL,
	[quantity] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[discount_id] [int] NULL,
	[uom_id] [int] NULL,
	[bom_code] [varchar](30) NULL,
 CONSTRAINT [PK_job_order_body] PRIMARY KEY CLUSTERED 
(
	[job_order_body_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

