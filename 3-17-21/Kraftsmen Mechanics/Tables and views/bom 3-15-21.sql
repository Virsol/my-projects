USE [service]
GO

/****** Object:  Table [dbo].[bom]    Script Date: 3/15/2021 2:46:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[bom](
	[bom_code] [varchar](20) NULL,
	[inventory_code] [varchar](30) NULL,
	[item_quantity] [int] NULL,
	[total_price] [decimal](11, 2) NULL,
	[total_tax] [decimal](11, 2) NULL,
	[P/C] [int] NULL,
	[status] [int] NULL,
	[price_pl] [decimal](11, 2) NULL
) ON [PRIMARY]
GO

