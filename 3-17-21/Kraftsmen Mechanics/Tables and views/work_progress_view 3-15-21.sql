USE [service]
GO

/****** Object:  View [dbo].[Work_progress_view]    Script Date: 3/15/2021 6:00:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[Work_progress_view] as 

SELECT a.work_prog_id AS ID, b.name AS [Customer Name], c.name AS Technician, f.auto_make_desc AS [Auto Make], d.model, g.size_desc AS Size, CASE WHEN
                      (SELECT a.[on/off]
                       FROM      document_series a LEFT JOIN
                                         Document_tag b ON a.doc_tag_id = b.doc_tag_id
                       WHERE   doc_tag_desc = 'Job ORDER') = 1 THEN Concat
                      ((SELECT a.series_desc
                        FROM      document_series a LEFT JOIN
                                          Document_tag b ON a.doc_tag_id = b.doc_tag_id
                        WHERE   doc_tag_desc = 'Job ORDER'), '-', a.job_number) ELSE a.job_number END AS [Job Number], a.start_date AS [Start Date], e.status_desc AS Status
FROM     dbo.work_progress AS a LEFT OUTER JOIN
                  dbo.customer AS b ON a.customer_id = b.customer_id LEFT OUTER JOIN
                  dbo.employee AS c ON a.employee_id = c.employee_id LEFT OUTER JOIN
                  dbo.vehicle AS d ON a.car_id = d.vehicle_id LEFT OUTER JOIN
                  dbo.status AS e ON a.status = e.status_id LEFT OUTER JOIN
                  dbo.auto_make AS f ON d.auto_make_id = f.auto_make_id LEFT OUTER JOIN
                  dbo.size AS g ON d.size = g.size_id
				  left join job_order h on a.job_number = h.job_number
WHERE  (e.status_desc = 'ONGOING'and h.with_service = 1)
GO

